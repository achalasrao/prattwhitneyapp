export const enginedataShow = (item) => {
  return {
    type: "Engine_Data",
    payload: item
  };
}

export const selectedParamChart = (item) => {
  return {
    type: "selectedParamChart",
    payload: item
  }
}

export const Transdata = (item) => {
  return {
    type: "Transdata",
    payload: item
  }
}

export const RTD_DATAFETCH = (item) => {
  return {
    type: "RTD_DATA",
    payload: item
  }
}

export const IsLoaded = () => {
  return {
    type: "IsLoaded",
    payload: true
  }
}

export const IsLoadedError = () => {
  return {
    type: "IsLoaded",
    payload: true
  }
}
