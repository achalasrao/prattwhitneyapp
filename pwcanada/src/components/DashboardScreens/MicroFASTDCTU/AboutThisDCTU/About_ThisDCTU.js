import React, { Component } from 'react';
import { View, FlatList, Text, StyleSheet, ActivityIndicator, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';

let { width, height } = Dimensions.get('window');

export default class MicroFASTStatus extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSource: [],
            status: null,
            loading: false,
            refreshing: false,
            text: ''
        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Dashboard')}
                style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                </View>
            </TouchableOpacity>
        )
    });

    componentDidMount = async () => {

        this.setState({ loading: true })
        this._interval = setInterval(async () => {
            await this._onRefresh()

        }, 1000);
    }

    fetchData = async () => {

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'monitor_status'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            this.setState({ userDataSource: response.data, loading: false });

        } catch (error) {

        }
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    _onRefresh = async () => {
        await this.fetchData()
    }

    renderView = () => {
        return (
            <FlatList
                showsVerticalScrollIndicator={true}
                dataSource={this.state.userDataSource}
                renderRow={this.renderRow.bind(this)} />
        )
    }


    render() {

        const data = this.state.userDataSource.map((tabs) =>
            <View style={styles.aboutDctuRow}>
                <Text style={styles.label}>{tabs.name}</Text>
                <Text style={styles.value}>{tabs.value}</Text>
            </View>
        )

        return (

            <View style={{ flex: 1 }}>
                {
                    this.state.loading ?
                        <View style={styles.container}>
                            <ActivityIndicator size="large" color="#5B6376" />
                        </View> :
                        <View style={styles.renderContainer}>
                            <ScrollView>
                                {data}
                            </ScrollView>
                        </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    renderContainer: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        marginTop: 15,
        paddingTop: 15
    },
    label: {
        width: '40%',
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0,
        color: "#1F2A44",
        paddingLeft: 15
    },
    value: {
        width: '60%',
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#848A98"
    },
    aboutDctuRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 5
    }
});
