import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, Alert, ScrollView, Dimensions } from 'react-native';
import {
    Text,
} from 'native-base';
import { connect } from 'react-redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import { _, keys, values } from 'underscore'
import { Buffer } from 'buffer';

//Global variables
var parameterNameIdentification = [];//list of all parameter Names belonging to Identification DataSection
var newData = [];
let IdentificationDisplayData = [];
let { width, height } = Dimensions.get('window');

class AbouttheEpecs extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: [], // IdentificationDisplayData to be pushed
            loading: true,//holds the state of the spinner
            refreshing: false,// refreshing state
            selectedParam: undefined,//parameter whose value ha sto be updated
            changedValue: undefined,//the value of the parameter to be updated
            oldValue: undefined,//value before change
            position: 'top',//position of the toast
            valueChanged: false,//state that represents if user has changed the value
            fieldChanged: false,// state that represents if user confiems the change,
            isUpdateRequire: false,
            isModalVisible: false,
            itemList: [],
            channel: "",
            fetchCount: 0,
        }
        this.getParamValue = this.getParamValue.bind(this);
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('Dashboard')}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <Icon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }

    //first lifecycle method called
    async UNSAFE_componentWillMount() {
        ParameterName = [];
        newData = [];
        IdentificationDisplayData = [];
        this.setState({ loading: true });
        await this.fetchData()
        // await this.getParamValue()
    }
    componentWillUnmount() {
        ParameterName = [];
        newData = [];
    }

    //the following function fetches the xml data which contains all the parameters and data sections

    async fetchData() {
        this.setState({ fetchCount: ++this.state.fetchCount })
        var DataSectiont = [];//List ofv DataSections
        var Parametert = [];//List Of Parameters
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_trim",
                        "arg1": "--list"

                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            var copy = []
            let responseJson = await response;


            if (responseJson.data[0].value.includes("Err")) {
                this.EPECSdown();
            }
            else {

                var lines = JSON.stringify(responseJson.data[0].value);

                Parametert.push(lines[1]);

                lines = lines.replace(/,/g, "");

                var splitwise = await lines.split("\\n");


                for (var line = 1; line < splitwise.length - 1; line++) {

                    lines = splitwise[line].split("  ");

                    lines = lines.filter(Boolean)

                    DataSectiont.push(lines[0]);

                    copy.push({
                        'Data': lines[0],
                        'ParamName': lines[1],
                        'disp_name': lines[2],
                        'unit': lines[3]
                    })
                }
                this.reArrangeList(DataSectiont, copy)
                await this.getParamValue()
            }
        } catch (error) {

            if (this.state.fetchCount < 15) {
                this.fetchData()
            } else {
                this.EPECSdown();
            }
        }
    }

    EPECSdown = () => {
        Alert.alert(
            "EPECS is down",
            "Try again later.",
            [
                {
                    text: "Go Back",
                    onPress: () => {
                        this.props.navigation.navigate("Dashboard");
                    },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    //Re-arrangement of the lists
    async reArrangeList(DataSectiont, copy) {
        //extract all the DataSections
        parameterNameIdentification = [];
        if (copy.length == 0) {
            await this.fetchData()
        }
        var resultList = copy.filter(d => {
            return d.Data.match("Identification");
        });
        //filter the parameter values

        for (var j = 0; j < resultList.length; j++) {
            parameterNameIdentification.push(
                {
                    'ParamName': resultList[j].ParamName,
                    'disp_name': resultList[j].disp_name,
                    'unit': resultList[j].unit
                })
        }
        return;
    }

    //the following function, fetched the value of all the parameter belonging to data section Identification
    getParamValue = async () => {

        var data = [];//contains parameter-value response list

        var displayData = []
        var reg = "Err!"
        var reg1 = "ENGINE_SN"
        var isEngine = false;
        if (parameterNameIdentification.length == 0) {
            //if the xml was not loaded intially
            await this.fetchData()
        }
        //get the values for all the parameters
        for (var i = 0; i < parameterNameIdentification.length; i++) {

            var paramName = "--field=" + (parameterNameIdentification[i].ParamName.substr(1))
            paramName = JSON.stringify(paramName)

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?read',
                    data: [
                        {
                            "cmd": "epecs_trim",
                            "arg1": paramName
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })


                let result = response.data;

                var name = result[0].name.slice(0, -1)

                var channel = name.slice(- 1)

                if ((name.match(reg1))) {
                    isEngine = true
                    name = name.substr(0, name.lastIndexOf("_"))
                }
                else {
                    isEngine = false
                    name = name.slice(0, - 1)
                }

                if (result[0].name == "Data Error" || (reg.match(result[0].value))) {

                    i--;
                }
                else {
                    if(!result[0].name.includes("CRC")){
                    data.push({
                        'Name': name,
                        'Channel': channel,
                        'Value': result[0].value,
                        'isEngine': isEngine,
                        'disp_name': parameterNameIdentification[i].disp_name,
                        'unit': parameterNameIdentification[i].unit
                    })
                }
                }
            } catch (error) {

                i--;
            }
        }//end-for

        displayData = _.groupBy(data, data => data.Name);

        var paramNames = keys(displayData)

        var channelValues = values(displayData)

        for (var i = 1; i < paramNames.length; i++) {
        
                IdentificationDisplayData.push({
                    Name: channelValues[i][0].Name,
                    ChannelA: channelValues[i][0].Value,
                    ChannelB: channelValues[i][1].Value,
                    isEditable: channelValues[i][1].isEngine,
                    disp_name: channelValues[i][0].disp_name,
                    unit: channelValues[i][0].unit
                })
            
        }

        //update the state with the response and close the spinner
        this.setState({ data: IdentificationDisplayData, loading: false });
        return
    }

    storeChangeInfo(n) {

        var tabs = this.state.itemList
        if (n == 2) {
            newData.push({
                name: tabs.Name + "_B",
                disp_name: tabs.disp_name,
                value: this.state.changedValue,
                oldValue: (this.state.channel === "A" ? tabs.ChannelA : tabs.ChannelB),
                channel: "A",
                unit: tabs.unit

            })
            newData.push({
                name: tabs.Name + "_A",
                disp_name: tabs.disp_name,
                value: this.state.changedValue,
                oldValue: (this.state.channel === "A" ? tabs.ChannelA : tabs.ChannelB),
                channel: "B",
                unit: tabs.unit
            })
        }
        else {
            newData.push({
                name: tabs.Name + "_" + this.state.channel,
                disp_name: tabs.disp_name,
                value: this.state.changedValue,
                oldValue: (this.state.channel === "A" ? tabs.ChannelA : tabs.ChannelB),
                channel: this.state.channel,
                unit: tabs.unit
            })
        }
    }

    focus = (value) => {

        this.setState({ isInputFocussed: value })
    }

    render() {

        const data = this.state.data.map((rowData) =>

            <View style={{ flexDirection: 'row', paddingLeft: 13, paddingBottom: 5, paddingTop: 5 }} >
                <View style={{ width: '40%', paddingRight: 5 }}>
                    <Text style={styles.label}>{rowData.disp_name}</Text>
                </View>
                <View style={{ width: '30%', paddingRight: 5 }}>
                    <Text style={styles.value}>{rowData.ChannelA}</Text>
                </View>
                <View style={{ width: '30%', paddingRight: 5 }}>
                    <Text style={styles.value}>{rowData.ChannelB}</Text>
                </View>
            </View>
        )

        return (
            <View style={{ flex: 1 }}>
                {
                    this.state.loading ?
                        <View style={styles.container}>
                            <ActivityIndicator size="large" color="#5B6376" />
                        </View>
                        :

                        <View style={{ backgroundColor: '#FFFFFF', marginTop: 15, paddingBottom: 40 }}>
                            <View style={{ flexDirection: 'row', padding: 14, borderBottomColor: '#E3E5E8', borderBottomWidth: 1 }}>
                                <Text style={styles.ESN}>ESN </Text>
                                {this.props.engineSerialNumber === 'Failed to fetch' ?
                                    <Text style={[styles.ESNValue, { color: 'red' }]}> {this.props.engineSerialNumber}</Text> :
                                    <Text style={styles.ESNValue}>PCE-{this.props.engineSerialNumber}</Text>
                                }
                            </View>

                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingBottom: 14,
                                    paddingTop: 14,
                                    paddingLeft: 13
                                }}>
                                <View style={{ width: '40%' }}></View>
                                <View style={{ width: '30%', }}>
                                    <Text style={[styles.channel]}>Channel A</Text>
                                </View>
                                <View style={{ width: '30%' }}>
                                    <Text style={[styles.channel]}>Channel B</Text>
                                </View>
                            </View>
                            <ScrollView>
                            {
                                data
                            }
                            </ScrollView>
                        </View>
                }
            </View>
        )
    }
}

const mapStateToProps = ({ connection }) => {
    const { engineSerialNumber } = connection
    return { engineSerialNumber };
};

export default connect(mapStateToProps, null)(AbouttheEpecs);

const styles = StyleSheet.create({

    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    channel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376",
    },
    ESN: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    ESNValue: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0,
        color: "#5B6376"
    },
    value: {
        fontFamily: "Arial",
        fontSize: width < 400 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#848A98"
    }
});
