export const OldEngineModel = (item) => {

    return {
        type: 'OldEngineModel',
        payload: item
    }
};

export const UpdateEngineModel = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'UpdateEngineModel',
        payload: item
    }
};

// ======================

export const OldEngineSerialNumber = (item) => {
    return {
        type: 'OldEngineSerialNumber',
        payload: item
    }
};

export const UpdateEngineSerialNumber = (item) => {
    // item=item.replace(/^0+/,'')
    return {
        type: 'UpdateEngineSerialNumber',
        payload: item
    }
};

// ======================

export const OldModuleSerialNumber = (item) => {
    return {
        type: 'OldModuleSerialNumber',
        payload: item
    }
};

export const UpdateModuleSerialNumber = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'UpdateModuleSerialNumber',
        payload: item
    }
};

// ======================
export const OldTakeOffDry = (item) => {

    return {
        type: 'OldTakeOffDry',
        payload: item
    }
};
export const UpdateTakeOffDry = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateTakeOffDry',
        payload: item
    }
};

// ======================

export const OldDOTTypeCert = (item) => {

    return {
        type: 'OldDOTTypeCert',
        payload: item
    }
};
export const UpdateDOTTypeCert = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateDOTTypeCert',
        payload: item
    }
};
// ======================

export const OldFAATypeCert = (item) => {

    return {
        type: 'OldFAATypeCert',
        payload: item
    }
};
export const UpdateFAATypeCert = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateFAATypeCert',
        payload: item
    }
};

// ======================

export const UpdateGasGenerator = () => {

    return {
        type: 'UpdateGasGenerator',
    }
};

export const DontUpdateGasGenerator = () => {

    return {
        type: 'DontUpdateGasGenerator',
    }
};

export const GasGeneratorUpdateSuccess = () => {

    return {
        type: 'GasGeneratorUpdateSuccess',
    }
};

export const GasGeneratorUpdateFailed = () => {

    return {
        type: 'GasGeneratorUpdateFailed',
    }
};

export const GasGeneratorScanSuccess = () => {

    return {
        type: 'GasGeneratorScanSuccess',
    }
};

export const GasGeneratorScanFailed = () => {

    return {
        type: 'GasGeneratorScanFailed',
    }
};
