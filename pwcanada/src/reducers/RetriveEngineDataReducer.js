const INITIAL_STATE = {
  enginedata: '',
  selectedParamChart: '',
  Transdata: '',
  isload: false,
  RTD_DATA: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'Engine_Data':
      return { ...state, enginedata: action.payload };

    case 'selectedParamChart':
      return { ...state, selectedParamChart: action.payload }

    case 'Transdata':
      return { ...state, Transdata: action.payload }

    case 'RTD_DATA':
      return { ...state, RTD_DATA: action.payload }

    case 'IsLoaded':
      return { ...state, isload: action.payload }

    default:
      return state;
  }
};
