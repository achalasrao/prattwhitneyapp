const INITIAL_STATE = {
    oldEngineBuildSpec: '',
    EngineBuildSpec: '',
    EngineBuildSpecUpdate: false,
    scannedEngineBuildSpec: false,
    updatedEngineBuildSpec: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'OldengineBuildSpec':

            return { ...state, oldEngineBuildSpec: action.payload };

        case 'updateEngineBuildSpec':

            return { ...state, EngineBuildSpec: action.payload };


        case '_UpdateEngineBuildSpec':

            return { ...state, EngineBuildSpecUpdate: true };

        case 'DontUpdateEngineBuildSpec':

            return { ...state, EngineBuildSpecUpdate: false };

        case 'EngineBuildSpecUpdateSuccess':

            return { ...state, updatedEngineBuildSpec: true };

        case 'EngineBuildSpecUpdateFailed':

            return { ...state, updatedEngineBuildSpec: false };

        case 'EngineBuildSpecScanSuccess':

            return { ...state, scannedEngineBuildSpec: true };

        case 'EngineBuildSpecScanFailed':

            return { ...state, scannedEngineBuildSpec: false };

        default:
            return state;
    }
}
