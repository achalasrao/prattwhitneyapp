import React, { Component } from 'react';
import { Item, Body, ListItem, Text, Button } from 'native-base';
import { View, FlatList, StyleSheet, Alert, ActivityIndicator, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import Checkbox from 'react-native-custom-checkbox';

var selectedList = []
let { width, height } = Dimensions.get('window');

export default class LiveData extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sensors: [],//list of all teh sensors from the microFAST
            selectedCheckBox: [],//list of all user selected sensor checkboxes
            loading: true,//state that represents if spinner has to be shown or not
            selectAll: false // if selectAll checkBox is prssed or not
        }
    }

    //initial lifecycle method
    async  componentDidMount() {
        await this.stopGet()
        await this.fetchData()
    }

    //the following function gives an icon to navigate to Dashboard directly
    static navigationOptions = ({ navigation, navigationOptions }) => {
        const { params } = navigation.state;

        return {

            // title: params?params.headerTitle:'Retrieve Engine Data',
            headerLeft: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('Dashboard')}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    //the following function sends livedata_cmd stop request to microFAST
    async  stopGet() {
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'livedata_cmd',
                        'arg1': 'stop'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
        } catch (error) {

        }
    }


    //the following function sends livedata_cmd getlist request to microFAST, the response is of type
    //"sensor-ID,Sensor-Name,Sensor-Units,Sensor-Type\n"
    fetchData = async () => {

        var sensorList = []
        this.state.selectedCheckBox.push(999)

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "livedata_cmd",
                        "arg1": "getlist"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            var result = response.data;
            //if there is response data, push the data and set the loader to false
            if (response.data.length > 1) {
                sensorList.push({
                    id: "dummy",
                    name: "Select All",
                    unit: ""
                })

                //parsing xml data
                for (var i = 1; i < result.length; i++) {

                    let lines = result[i].value
                    lines = lines.replace(/,/g, " ");
                    var splitwise = await lines.split(" ");

                    //to avoid empty name being displayed
                    if (splitwise[1] == " " || splitwise[1] == "") {


                    }
                    else {
                        sensorList.push({
                            id: splitwise[0],
                            name: splitwise[1],
                            unit: splitwise[2],
                        })
                    }
                }

                selectedList = sensorList
                this.setState({ sensors: sensorList, loading: false })
            }
            //when response data is null, send the request again
            else {
                this.fetchData()
            }
        } catch (error) {
            //resend the request , if network error
            this.fetchData()
        }
    }

    //the following function checks or unchecks checkbox 
    onCheckBoxPress(item) {

        var id = item.id
        //if selectAll option is chosen
        if (item.name == "Select All") {

            if (!this.state.selectAll) {
                //if select All was not chosen before
                this.setState({ selectAll: true, selectedCheckBox: [0] })
                //this.state.selectedCheckBox.push(0)
            }
            else
                this.setState({ selectAll: false })
        }
        //if only-few fields were selected
        else {
            //un-selecting an item after selectAll
            if (this.state.selectAll) {
                let tmp = []
                const products = this.state.sensors.filter(prod => prod.name !== item.name)
                for (var i = 1; i < products.length; i++) {
                    tmp.push(products[i].id)
                }
                this.setState({ selectedCheckBox: tmp, selectAll: false })
            }
            else {
                let tmp = this.state.selectedCheckBox
                //code to check/uncheck checkbox
                if (tmp.includes(id)) {
                    tmp.splice(tmp.indexOf(id), 1);
                }
                else {
                    tmp.push(id);
                }
                this.setState({ selectedCheckBox: tmp })
            }
        }
    }

    //the following function is called when user clicks on 'View' , the selected sensors are filtered based if checked and sent to the nextpage
    onPress(event) {

        event.preventDefault()
        var tempSensor = []
        tempSensor = this.state.sensors
        //
        if (this.state.selectAll) {
            // send the entire sensor list when selectAll is checked
            this.props.navigation.navigate('LiveDataSensorList', { param: tempSensor })
        }

        //when only few fields are selected, map the selected IDs with the sensor list and navigate to next page
        else {
            tmp = this.state.selectedCheckBox

            //if no sensor is selected, prompt user to select sensors
            if (tmp.length == 1) {

                Alert.alert(
                    'Please select the sensors',
                    '',
                    [
                        {
                            text: 'Ok', onPress: () => { }
                        },
                    ],
                    { cancelable: false }
                )
            }
            //send the selected sensor data to the next page
            else {
                var tempSen = []

                for (var i = 0; i < tmp.length; i++) {

                    var resultList = tempSensor.filter(d => {
                        return d.id.match(tmp[i]);
                    });

                    tempSen.push(resultList[0])
                }
                this.props.navigation.navigate('LiveDataSensorList', { param: tempSen })
            }
        }
    }

    //------------UI------------------------------------------------------------------------------

    render() {

        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>

                    <View style={[styles.container]}>
                        <Text style={styles.selectTheValuesYo}>Select the values you want to watch.</Text>
                    </View>

                    <View style={styles.rectangle}>
                        {
                            !this.state.selectAll ?
                                <Item >

                                    <FlatList
                                        extraData={this.state}
                                        keyExtractor={(item, index) => item.id}
                                        data={this.state.sensors}
                                        renderItem={({ item }) => {
                                            return (
                                                <ListItem button onPress={() => this.onCheckBoxPress(item)} >
                                                    <Checkbox
                                                        style={{
                                                            backgroundColor: '#FFFFFF', color: '#000000', borderRadius: 5,
                                                            borderWidth: 2, borderColor: '#5B6376', margin: 10
                                                        }}

                                                        onChange={() => this.onCheckBoxPress(item)}

                                                        checked={(this.state.selectedCheckBox.includes(item.id)) ? true : false}

                                                    />
                                                    <Body>
                                                        <Text style={styles.unselected}>{item.name}</Text>
                                                    </Body>
                                                </ListItem>
                                            )
                                        }}
                                    />
                                </Item> :
                                <Item >

                                    <FlatList
                                        extraData={this.state}
                                        keyExtractor={(item, index) => item.id}
                                        data={this.state.sensors}
                                        renderItem={({ item }) => {
                                            return (
                                                <ListItem button onPress={() => this.onCheckBoxPress(item)} >
                                                    <Checkbox
                                                        style={{
                                                            backgroundColor: '#EB1616', color: '#000000', borderRadius: 5,
                                                            borderWidth: 2, borderColor: '#000000', margin: 10
                                                        }}

                                                        onChange={() => this.onCheckBoxPress(item)}

                                                        checked={true}

                                                    />
                                                    <Body>
                                                        <Text style={styles.selected}>{item.name}</Text>
                                                    </Body>
                                                </ListItem>
                                            )
                                        }}
                                    />
                                </Item>
                        }
                    </View>
                </ScrollView>

                <View style={{ marginLeft: 10, marginRight: 10 }}>
                    <View>
                        <Button id="Button" onPress={this.onPress.bind(this)} style={[styles.rectangle2]}>
                            <Text style={[styles.label, { textAlign: 'center' }]}>View</Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
};
const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    selectTheValuesYo: {
        width: 277,
        height: 41,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        marginLeft: 20,
        marginTop: 25,
        fontWeight: "bold",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: -0.34,
        color: "#1F2A44"
    },
    selectAll: {
        width: 64,
        height: 18,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#00A9E0"
    },
    rectangle: {
        marginLeft: 15,
        marginRight: 15,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8"
    },
    unselected: {
        width: 300,
        height: 24,
        fontSize: width < 450 ? 16 : 22,
        color: "#1F2A44"
    },
    selected: {
        width: 300,
        height: 24,
        fontSize: width < 450 ? 16 : 22,
        color: "#1F2A44"
    },
    rectangle2: {
        width: '100%',
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 16,
        marginTop: 13
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    }
});
