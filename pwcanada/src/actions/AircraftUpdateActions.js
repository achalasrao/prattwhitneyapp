export const oldValueAircraft_SN = (value) => {
    return {
        type: 'oldValueAircraft_SN',
        payload: value
    }
};
export const oldValueAircraft_Type = (value) => {
    return {
        type: 'oldValueAircraft_Type',
        payload: value
    }
};
export const oldValueFleet_Ident = (value) => {

    return {
        type: 'oldValueFleet_Ident',
        payload: value
    }
};
export const oldValueAircraft_Number = (value) => {
    return {
        type: 'oldValueAircraft_Number',
        payload: value
    }
};
export const oldValueTail_Number = (value) => {
    return {
        type: 'oldValueTail_Number',
        payload: value
    }
};

export const oldValueAirline_Operator = (value) => {
    return {
        type: 'oldValueAirline_Operator',
        payload: value
    }
};

export const oldValueAircraft_Owner = (value) => {
    return {
        type: 'oldValueAircraft_Owner',
        payload: value
    }
};

export const newValueAircraft_SN = (value) => {

    return {
        type: 'newValueAircraft_SN',
        payload: value
    }
};
export const newValueAircraft_Type = (value) => {
    return {
        type: 'newValueAircraft_Type',
        payload: value
    }
};
export const newValueFleet_Ident = (value) => {
    return {
        type: 'newValueFleet_Ident',
        payload: value
    }
};
export const newValueAircraft_Number = (value) => {
    return {
        type: 'newValueAircraft_Number',
        payload: value
    }
};
export const newValueTail_Number = (value) => {
    return {
        type: 'newValueTail_Number',
        payload: value
    }
};

export const newValueAirline_Operator = (value) => {
    return {
        type: 'newValueAirline_Operator',
        payload: value
    }
};

export const newValueAircraft_Owner = (value) => {
    return {
        type: 'newValueAircraft_Owner',
        payload: value
    }
};

export const AircraftUpdateRequired = () => {
    return {
        type: 'AircraftUpdateRequired',
    }
};

export const AircraftUpdateNotRequired = () => {
    return {
        type: 'AircraftUpdateNotRequired',
    }
};

export const FetchAircraftData = () => {
    return {
        type: 'FetchAircraftData',
    }
};

export const DontFetchAircraftData = () => {

    return {
        type: 'DontFetchAircraftData',
    }
};
