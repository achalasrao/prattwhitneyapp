import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Image, Dimensions } from 'react-native';
import {
    Text,
    Card,
    CardItem,
    Label
} from 'native-base';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import { _, keys, values } from 'underscore'

let ConnectedNetwork = [];
let copy = [];
let Wsignal = [];
let { width, height } = Dimensions.get('window');

export default class ConnectionDCTUGround extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEmpty: false,
            CellularData: 'empty',
            GetWifiSignal: 'empty',
            AppcrashState: false,
            writeToXmlCount: 0,
            fetchCount: 0,
        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Dashboard')}
                style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                </View>
            </TouchableOpacity>
        )
    });

    UNSAFE_componentWillMount = async () => {
        this.checkExternalSim();
    };

    checkExternalSim = async () => {
        copy = []
        this.setState({ writeToXmlCount: ++this.state.writeToXmlCount })
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "read",
                        "arg2": "Wireless"

                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response
            let responseJson = await response.data;

            if (responseJson[0].value == "Err: Too many access request (>4).  Try again later") {
                this.setState({ AppcrashState: true })
            }

            for (var i = 0; i < responseJson.length; i++) {
                var decoded = responseJson[i].value.replace(/&amp;/g, '&');
                copy.push({
                    'Param': responseJson[i].name,
                    'Value': decoded
                })

            }
        }
        catch (error) {
        }


        if (this.state.AppcrashState == true) {

        } else {

            if (copy.length > 0) {
                this.setState({ CellularData: copy })
                this.checkWifi();
            }

            if (copy.length == 0 && this.state.writeToXmlCount < 3) {

                this.checkExternalSim()
            }
        }
    }

    checkWifi = async () => {
        Wsignal = [];
        this.setState({ fetchCount: ++this.state.fetchCount })
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "wifi_get_signal"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            ConnectedNetwork = response;
            if (ConnectedNetwork.data[0].value == "") {
                this.setState({ isEmpty: true })
            }
            else if (String(ConnectedNetwork.data[0].value).includes('Failed') || String(ConnectedNetwork.data[0].value).includes('Err')) {
                for (var i = 0; i < WiFiSignal.length; i++) {
                    Wsignal.push({
                        'Value': "Failed"
                    })

                }
            }
            else {
                let WiFiSignal = ConnectedNetwork.data[0].value.split(" ")

                for (var i = 0; i < WiFiSignal.length; i++) {
                    Wsignal.push({
                        'Value': WiFiSignal[i]
                    })

                }
            }
        }
        catch (error) {

        }

        if (Wsignal.length > 0) {
            this.setState({ GetWifiSignal: Wsignal })
        }

        if (Wsignal.length == 0 && this.state.fetchCount < 3) {

            this.checkExternalSim()
        }
    }

    async incognitoMode() {


        valueList = []
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'monitor_status'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            valueList = values(response.data)
            valueList = valueList.filter(d => d.name.includes("Incognito Mode"))
            
            if (valueList[0].value.match("disabled")) {
                this.props.navigation.navigate('EnableIncognitoMode')
            }
            else {
                this.props.navigation.navigate('DisableIncognitoMode')
            }
        } catch (error) {

        }
    }

    render() {
        if (this.state.AppcrashState == true) {
            return (
                <View>
                    <Card>
                        <CardItem>
                            <Text>Err: Too many access request (>4).  Try again later</Text>
                        </CardItem>
                    </Card>

                </View>
            )
        } else {

            if (this.state.CellularData[0] === undefined || this.state.GetWifiSignal[0] === undefined) {
                return (
                    <View>Loading...</View>
                )
            } else {

                return (
                    <View style={{ flex: 1 }}>

                        <ScrollView>
                            <Card style={styles.rectangle}>
                                
                                <View>
                                    {this.state.isEmpty ? <Image
                                        style={styles.Image}
                                        source={require('../../../../assets/IconPack/TestTransmission/wifiOff.png')}
                                        resizeMode='contain'
                                    /> : <Image
                                        style={styles.Image}
                                            source={require('../../../../assets/IconPack/TestTransmission/wifi.png')}
                                            resizeMode='contain'
                                        />}
                                </View>
                                <View style={{ paddingLeft: 8 }}>

                                    <View>
                                        <View>
                                            <Text style={styles.head}>WiFi</Text>
                                        </View>
                                    </View>

                                    <View style={width = '80%'}>
                                        <Text style={styles.connectedTo}>
                                            Connected to
                                            {(this.state.GetWifiSignal[0].Value === "" || this.state.GetWifiSignal[0].Value == undefined) ? " --" :
                                                <Text style={{
                                                        fontFamily: 'Arial',
                                                        fontSize: width < 450 ? 16 : 22,
                                                        fontWeight: "normal",
                                                        fontStyle: "normal",
                                                        lineHeight: width < 450 ? 18 : 24,
                                                        letterSpacing: 0.5,
                                                        color: "#1F2A44"
                                                }}>{this.state.GetWifiSignal[2].Value}
                                                </Text>
                                            }
                                        </Text>
                                    </View>

                                    {/* {
                                        this.state.GetWifiSignal[2].Value === undefined ? "--" : <View>
                                               <Text style={styles.connectedTo}>{this.state.GetWifiSignal[2].Value}</Text>
                                        </View>
                                    } */}

                                    {/* <View>
                                        <Text style={styles.connectedTo}>{this.state.GetWifiSignal[2].Value}</Text>
                                    </View> */}

                                    <View>
                                        <View>
                                            <Text style={styles.condescription}>Used to send data when available</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <View>
                                            <TouchableOpacity style={styles.testButton} onPress={() => this.props.navigation.navigate('TestTransmissionWifi')}>
                                                <Label style={styles.testButtonText}>Test</Label>
                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                    <View>
                                        <View>
                                            <TouchableOpacity style={styles.configureButton} onPress={() => this.props.navigation.navigate('WifiModeScreen')}>
                                                <Label style={styles.configureButtonText}>Configure</Label>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </Card>

                            <Card style={styles.rectangle} >
                                <View style={{ justifyContent: "flex-start" }}>
                                    {this.state.CellularData[0].Value == "INTERNAL" || "EXTERNAL" ? <Image
                                        style={styles.Image}
                                        source={require('../../../../assets/IconPack/TestTransmission/cellular.png')}
                                        resizeMode='contain'
                                    /> : <Image
                                            style={styles.Image}    
                                            source={require('../../../../assets/IconPack/TestTransmission/cellularOff.png')}
                                            resizeMode='contain'
                                        />}
                                </View>

                                <View style={{ paddingLeft: 8 }}>

                                    <View>
                                        <View>
                                            <Text style={styles.head}>Cellular</Text>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        {this.state.CellularData[0].Value == "INTERNAL" ?
                                            <View style={styles.connectDescription}>
                                                <Text>Connected to
                                                    {(this.state.CellularData[0].Value === "" || this.state.CellularData[0].Value == undefined) ? " --" : <Text style= {{ fontWeight: 'bold' }}>{this.state.CellularData[5].Value} </Text>} on <Text style={{ fontWeight: 'bold' }}>INTERNAL SIM Card</Text> </Text>
                                            </View>
                                            :
                                            <View style={styles.connectDescription}>
                                                <Text>Connected to
                                                {(this.state.CellularData[0].Value === "" || this.state.CellularData[0].Value == undefined) ? " --" : <Text style={{ fontWeight: 'bold' }}>{this.state.CellularData[1].Value}</Text>} on <Text style={{ fontWeight: 'bold' }}>External SIM Card</Text> </Text>
                                            </View>
                                        }
                                    </View>

                                    <View>
                                        <View>
                                            <Text style={styles.condescription}>Used when Wifi is unavailable</Text>
                                        </View>
                                    </View>
                                    <View>
                                        <View>
                                            <TouchableOpacity style={styles.testButton} onPress={() => this.props.navigation.navigate('TestTransmissionCellular')}>
                                                <Label style={styles.testButtonText}>Test</Label>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View>
                                        <View>
                                            <TouchableOpacity style={styles.configureButton} onPress={() => this.props.navigation.navigate('ConfigureCellular')}>
                                                <Label style={styles.configureButtonText}>Configure</Label>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </Card>

                            <Card style={styles.rectangle} >
                                <View style={{ justifyContent: "flex-start" }}>
                                    <Image
                                        style={styles.Image}
                                        source={require('../../../../assets/IconPack/TestTransmission/incognito.png')}
                                        resizeMode='contain'
                                    />
                                </View>

                                <View style={{ paddingLeft: 8 }}>

                                    <View>
                                        <View>
                                            <TouchableOpacity style={styles.IncognitoButton} onPress={() => this.incognitoMode()}>
                                                <Label style={styles.IncognitoButtonText}>Incognito Mode</Label>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </View>
                            </Card>

                        </ScrollView>
                    </View>
                )
            }
        }
    }
}

const styles = StyleSheet.create({

    rectangle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 10,
        flexDirection: 'row',
        padding: 10
    },
    head: {
        alignItems: "center",
        fontFamily: "Arial",
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#1F2A44",
        fontSize: width < 450 ? 18 : 24,
        paddingLeft: width < 450 ? 0 : 22
    },
    testButton: {
        width: width < 450 ? 210 : 400,
        marginLeft: width < 450 ? 0 : 22,
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        marginBottom: 15
    },
    testButtonText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#FFFFFF"
    },
    configureButton: {
        width: width < 450 ? 210 : 400,
        marginLeft: width < 450 ? 0 : 22,
        height: 40,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#00A9E0",
        marginBottom: 15
    },
    configureButtonText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 1,
        textAlign: "center",
        color: "#00A9E0",
        paddingTop: 10
    },
    connectedTo: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        height: 28,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.5,
        color: "#1F2A44",
        textAlign: 'left',
        paddingLeft: width < 450 ? 0 : 22
    },
    connectDescription: {
        width: '80%', 
        paddingLeft: width < 450 ? 0 : 22
    },
    condescription: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 16 : 22,
        letterSpacing: 0.44,
        color: "#666666",
        textAlign: 'justify', 
        paddingLeft: width < 450 ? 0 : 22
    },
    IncognitoButton: {
        width: width < 450 ? 210 : 400,
        marginLeft: width < 450 ? 0 : 22,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        marginBottom: 15
    },
    IncognitoButtonText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#FFFFFF"
    },
    Image: {
        width: width < 450 ? 50 : 70,
        height: width < 450 ? 50 : 70
    }
});
