import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, TouchableOpacity, TextInput, Alert, Keyboard, ScrollView } from 'react-native';
import {
    Label
} from 'native-base';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';



const totalTime = (14 * 24 * 60)


export default class EnableIncognitoMode extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSource: [],
            status: null,
            loading: false,
            refreshing: false,
            text: '',
            days: 0,
            hours: 0,
            minutes: 0,
            comments: "",
            height : 0, 
            disableStatus: false,// to diable or enable buttons
            waitStatus: true,//represents upload wait status 
            confirmationPercentage: 0//represents percentage of confirmation report from ground station

        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Dashboard')}
                style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                </View>
            </TouchableOpacity>
        )
    });

    componentDidMount = async () => {

        console.log("called didmount")
    }



    componentWillUnmount() {
     
        clearInterval(this._interval);
   
        
    }

    
    // the following function enable incognito mode by passing start(requestNumber =1) or force_start(requestNumber =2) based on user input
    async onSubmit(event) {
        console.log("called onSubmit")
        Keyboard.dismiss()
        this.setState({ loading: true })
    
        let timeEntered = 0
     
        let days = this.state.days
        let minutes = this.state.minutes
        let hours = this.state.hours
        let comments = this.state.comments

        timeEntered = parseInt((days * 1440)) + parseInt((hours * 60)) + parseInt(minutes)
    

    
        //check is maximum allowed days is less than 14
        if (timeEntered <= totalTime && days !== undefined) {

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "incognito",
                            "arg1": "start",
                            "arg2": "-d " + days,
                            "arg3": "-h " + hours,
                            "arg4": "-m " + minutes,
                            "arg5": "-c" + comments
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })


                let result = response.data;
                if (result.length > 0) {
                    this.setState({ loading: false })
                }

       
                //start logic, 
                
                    if (result[0].value.includes("Success")) {

                        this.setState({ disableStatus: true })
                        this.checkConfirmationStatus()
                    }
                    else {
                        Alert.alert(
                            'Error',
                            result[0].value,
                            [
                                {
                                    text: 'OK', onPress: () => { this.setState({ loading: false }) },
                                }
                            ],
                            { cancelable: false }
                        )
                    }
                

            }
            catch (error) { }

        }//end-if
        //if number of days> 14
        else {
            Alert.alert(
                'Input cannot be processed ',
                'Maximum allowed is 14 days',

                [
                    {
                        text: 'OK', onPress: () => { this.setState({ loading: false }) },
                    }
                ],
                { cancelable: false }
            )

        }

    }
    async onStopPress()
    {
        console.log("onstoppress")
        this.setState({disableStatus:false})
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "incognito",
                        "arg1": "stop"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            

        } catch (error) {

        }

    }

    //following function will check the status of ground confirmation after start incognito mode is executed successfully.
    async checkConfirmationStatus() {
        if(this.state.disableStatus){
    
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "incognito",
                        "arg1": "info"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })


            let result = response.data;

            let uploadstatus = result.filter(d => d.name.includes("state"))
            let uploadWait = result.filter(d => d.name.includes("upload_wait"))

            if (uploadstatus[0].value.includes("in_progress")) {


                this.setState({ waitStatus: true, confirmationPercentage: uploadWait[0].value })
                this.checkConfirmationStatus()
            }


            else if (uploadstatus[0].value.includes("disabled")) {

                let toSend =[]
                toSend.push({
                    type:"enable",
                    days:this.state.days,
                    hours: this.state.hours,
                    minutes: this.state.minutes,
                    comments: this.state.comments
                })
                console.log("status", toSend, uploadWait[0].value)
                uploadWait[0].value.includes("pass")?
                //
                this.setState({ waitStatus: false, confirmationPercentage: uploadWait[0].value }):
               this.props.navigation.navigate('ForceStartIncognito', { param: toSend})
       
                
                
            }



        }
        catch (error) { }
    }
    }



    render() {
  
        const data = this.state.userDataSource.map((tabs) =>
            <View style={styles.aboutDctuRow}>
                <Text style={styles.label}>{tabs.name}</Text>
                <Text style={styles.value}>{tabs.value}</Text>
            </View>
        )

        return (

            <View style={{ flex: 1 }}>
                {

                    <View >
                        <ScrollView>
                        <View style={{ flexDirection: 'row', paddingLeft: 30, borderBottomColor: '#e5e5e5', borderBottomWidth: 1 }}>
                            <Text style={styles.ESN}>Disable Cellular/Wifi </Text>
                        </View>
                        <View >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: 'white',
                                    alignItems: 'center',
                                    width: '100%',
                                    paddingLeft: 25,
                                    paddingBottom: 5,
                                    paddingTop: 20
                                }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Days</Text>
                                </View>
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={(text) => this.setState({ days: text })}
                                        style={[styles.textInput, { borderColor: "#333333" }]}

                                    ></TextInput>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                width: '100%',
                                paddingLeft: 25,
                                paddingBottom: 5,
                                paddingTop: 5
                            }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Hours</Text>
                                </View>
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={(text) => this.setState({ hours: text })}
                                        style={[styles.textInput, { borderColor: "#333333" }]}

                                    ></TextInput>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                width: '100%',
                                paddingLeft: 25,
                                paddingBottom: 5,
                                paddingTop: 5
                            }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Minutes</Text>
                                </View >
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={(text) => this.setState({ minutes: text })}
                                        style={[styles.textInput, { borderColor: "#333333" }]}

                                    ></TextInput>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                width: '100%',
                                paddingLeft: 25,
                                paddingBottom: 5,
                                paddingTop: 5
                            }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Comments</Text>
                                </View >
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        multiline={true}
                                        placeholder={"Upto 128 chars"}
                                        blurOnSubmit={true}
                                        maxLength ={128}
                                        onChangeText={(text) => this.setState({ comments: text })}
                                        onContentSizeChange={(event) => {
                                            this.setState({ height: event.nativeEvent.contentSize.height })
                                        }}
                                        style={[styles.textInput, { borderColor: "#333333" ,height: Math.max(35, this.state.height)}]}
                                        

                                    ></TextInput>
                                </View>
                            </View>
                        </View>
                        <View style={{ paddingTop: 20, paddingBottom: 20, width: '80%' }}>
                            <Text style={styles.thisScreenDisplays}>Disable Wifi and Cellular Network now until the time period specified above.</Text>
                            <Text style={styles.thisScreenDisplays}>(Maximum allowed is 14 days)</Text>
                        </View>
                        {/* disableStatus disables the disable button based on start command response(Screen 1A to !B transformation) */}
                        {!this.state.disableStatus ?
                            <View style={styles.buttonAlign}>
                                <TouchableOpacity style={styles.testButton} onPress={this.onSubmit.bind(this, 1)} >
                                    <Label style={styles.testButtonText}><Text style={{ color: '#ffffff' }} >Disable</Text></Label>
                                </TouchableOpacity>
                            </View> :
                       
                                    <View>
                                        {/* if waitSTatus(in_progress ground confirmation) is true then Screen IB is displayed else screen 1C is displayed */}
                                        {
                                            this.state.waitStatus ?
                                                <View>
                                                    <View style={{ paddingTop: 20 }}>
                                                        <Text style={[styles.thisScreenDisplays, { color: "blue" }]}>Waiting for ground confirmation: {this.state.confirmationPercentage}</Text>
                                                    </View> 
                                                    <View style={styles.buttonAlign}>
                                                        <TouchableOpacity style={styles.testButton} onPress={this.onStopPress.bind(this, 1)} >
                                                            <Label style={styles.testButtonText}><Text style={{ color: '#ffffff' }} >Stop</Text></Label>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View> :

                                                <View>
                                                    {/* if incognito mode is disabled(this.state.confirmationPercentage==pass), screen 1C is displayed else screen 1D will be displayed */}
                                                    { this.state.confirmationPercentage.includes("pass") ?
                                                  
                                                        <View style={{ paddingTop: 0 }}>
                                                                <Text style={[styles.thisScreenDisplays, { color: "blue" }]}>Waiting for ground confirmation: {this.state.confirmationPercentage}</Text>
                                                                <Text style={[styles.thisScreenDisplays, { color: "blue" }]}>Celluar and Wifi are disabled</Text>
                                                        </View> 
                                                    
                                                   
                                                        :
                                                        <View></View>
                                                    }
                                                </View>

                                        }
                                    </View>
                                
                           

                        }
                        {
                            this.state.loading ?

                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20 }}>
                                    <View></View>
                                    <ActivityIndicator size="large" color="#5B6376" />
                                </View> :
                                <View></View>
                        }
                        </ScrollView>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    renderContainer: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginTop: 15,
        paddingTop: 15
    },


    aboutDctuRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 5
    },
    heading: {
        width: 156,
        height: 22,
        fontFamily: "Arial",
        fontSize: 18,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#000000",
        paddingLeft: 45
    },
    label: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        paddingBottom: 5
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    thisScreenDisplays: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0.5,
        color: "#333333",
        paddingLeft: 25
    },
    buttonAlign: {
        marginLeft: 25,
        marginRight: 25,
        flexDirection: 'row'
    },
    testButton: {
        width: '100%',
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        marginBottom: 15,
        paddingRight: 5
    },
    testButtonText: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#ffffff"
    },
    ESN: {
        fontFamily: "Arial",
        fontSize: 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "rgb(102,102,102)",
        paddingBottom: 20,
        paddingTop: 20
    },

});
