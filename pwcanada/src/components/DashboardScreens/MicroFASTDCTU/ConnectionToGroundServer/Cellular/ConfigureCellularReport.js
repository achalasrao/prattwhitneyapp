import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, Dimensions } from 'react-native';
import {
    Text,
    Content,
} from 'native-base';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import { connect } from "react-redux";
import { UpdateSuccessNotifier } from '../../../../common';
import { FetchCellularData } from '../../../../../actions';

let { width, height } = Dimensions.get('window');

class ConfigureCellularReport extends Component {

    constructor(props) {
        super();

        this.customBackHandler = this.customBackHandler.bind(this);
        this.state = {

            isVisible: false,
            loading: false,
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };


    customBackHandler = () => {
        this.props.navigation.navigate('ConfigureCellular')
        this.props.FetchCellularData()
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    render() {

        let {
            Carrier, APN, User, Password, OldCarrier, OldAPN, OldUser, OldPassword, OldActive_SIM, Active_SIM
        } = this.props

        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }

        return (
            <View style={{ flex: 1 }}>
                <Content>
                {/* <View>
                        {
                            isUpdateFailed ?
                                <View>
                                    <UpdateFailedNotifier />
                                    <View style={{ paddingLeft: 18 }}>
                                        <Text style={styles.TextStyle}>Configure Cellular Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View> :
                                <View> */}

                                    <UpdateSuccessNotifier />
                                    <View style={{ paddingLeft: 18 }}>
                                        <Text style={styles.TextStyle}>Configure Cellular Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                {/* </View>
                        }
                    </View> */}

                    <View>
                        <View style={styles.rectangleConatinerStyle}>
                            {
                                (OldActive_SIM !== Active_SIM) ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Active_SIM</Text>
                                        <Text style={styles.rectangleContentTextStyle}>{Active_SIM}</Text>
                                    </View> :
                                    <View />
                            }
                            {
                                OldCarrier !== Carrier ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Carrier</Text>
                                        <Text style={styles.rectangleContentTextStyle}>{Carrier}</Text>
                                    </View> :
                                    <View />
                            }
                            {
                                OldAPN !== APN ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>APN</Text>
                                        <Text style={styles.rectangleContentTextStyle}>{APN}</Text>
                                    </View> : <View />
                            }
                            {
                                OldUser !== User ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>User</Text>
                                        <Text style={styles.rectangleContentTextStyle}>{User}</Text>
                                    </View> : <View />
                            }
                            {
                                OldPassword !== Password ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Password</Text>
                                        <Text style={styles.rectangleContentTextStyle}>{Password}</Text>
                                    </View> : <View />
                            }
                        </View>
                    </View>

                    <TouchableOpacity
                        style={styles.backToDashboardButton}
                        onPress={() => {
                            this.props.navigation.navigate('Dashboard')

                        }}
                    >
                        <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        )
    }
}

const mapStateToProps = ({ CellularConfig }) => {
    const { Active_SIM, APN, Carrier, Password, User, OldActive_SIM, OldCarrier, OldAPN, OldUser, OldPassword } = CellularConfig


    return {
        Active_SIM, APN, Carrier, Password, User, OldActive_SIM, OldCarrier, OldAPN, OldUser, OldPassword
    };
};

export default connect(mapStateToProps, { FetchCellularData })(ConfigureCellularReport)

const styles = StyleSheet.create({
    TextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 24 : 30,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 12
    },
    lastUpdateTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#848A98"
    },
    rectangleConatinerStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 20,
        paddingBottom: 20,
        paddingTop: 20
    },
    childRectangleStyle: {
        paddingTop: 10,
        paddingLeft: 16
    },
    rectangleTitleTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44"
    },
    rectangleContentTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#848A98",
        paddingTop: 16
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    }
})
