const INITIAL_STATE = {
  connectionState: '',
  connectionType: '',
  connectionStatus: '',
  dashBoardLoaded: false,
  aircrafttail: '',
  engineSerialNumber: '',
  isTNEmpty: false,
  isTNError: false,
  isNotMF: false,
  tailNumber: '',
  fetchEOFData: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'connectionStateChanged':
      return { ...state, connectionState: action.payload };

    case 'connectionTypeChanged':
      return { ...state, connectionType: action.payload };

    case 'connectionStatus':

      return { ...state, connectionStatus: action.payload };

    case 'TNError':

      return { ...state, isTNEmpty: false, isTNError: true, isNotMF: false };

    case 'TNEmpty':

      return { ...state, isTNEmpty: true, isTNError: false, isNotMF: false };

    case 'TNFetchSuccess':

      return { ...state, isTNEmpty: false, isTNError: false, isNotMF: false, tailNumber: action.payload };

    case 'EngineSerialNumber':
      return { ...state, engineSerialNumber: action.payload };

    case 'FetchEOFData':
      return { ...state, fetchEOFData: true };

    case 'DontFetchEOFData':
      return { ...state, fetchEOFData: false };

    default:
      return state;
  }
};
