import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    TextInput,
    Alert,
    ActivityIndicator
} from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import axios from 'axios';
import Modal from 'react-native-modal';
import {
    OldEngineModel,
    OldEngineSerialNumber,
    OldModuleSerialNumber,
    OldTakeOffDry,
    OldDOTTypeCert,
    OldFAATypeCert,
    UpdateEngineModel,
    UpdateEngineSerialNumber,
    UpdateModuleSerialNumber,
    UpdateTakeOffDry,
    UpdateDOTTypeCert,
    UpdateFAATypeCert,
    UpdateGasGenerator,
    DontUpdateGasGenerator,
    GasGeneratorScanFailed,
    FetchEngineData,
} from '../../../../../actions'


var result = []

class updateGasGenerator extends Component {

    state = {
        field1Focused: false,
        field2Focused: false,
        field3Focused: false,
        field4Focused: false,
        field5Focused: false,
        field6Focused: false,
        updateFailed: false,
        updateSuccess: false,
        isModalVisible: false,
        isLoading: false,
        xmlUpdateCount: 0,
        fieldUpdateCount: 0,
        epecsUpdateCount: 0
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('headerTilte'),
        };
    };

    componentDidMount() {
        result = []
        if (!this.props.sameESN) {
            Alert.alert(
                'Channel A and Channel B Engine Serial Number values are different',
                'Channel A value is being displayed.',
                [
                    {
                        text: 'OK', onPress: () => { },
                    }
                ],
                { cancelable: false }
            )
        }
    }

    reverseValue = () => {
        this.setState({ isModalVisible: false })
        this.props.DontUpdateGasGenerator()
        this.props.GasGeneratorScanFailed()
        let {
            oldEngineModel,
            oldEngineSerialNumber,
            oldModuleSerialNumber,
            oldTakeOffDry,
            oldDOTTypeCert,
            oldFAATypeCert
        } = this.props
        this.props.UpdateEngineModel(oldEngineModel)
        this.props.UpdateEngineSerialNumber(oldEngineSerialNumber)
        this.props.UpdateModuleSerialNumber(oldModuleSerialNumber)
        this.props.UpdateTakeOffDry(oldTakeOffDry)
        this.props.UpdateDOTTypeCert(oldDOTTypeCert)
        this.props.UpdateFAATypeCert(oldFAATypeCert)
    }

    componentDidUpdate() {

        let {
            oldEngineModel,
            oldEngineSerialNumber,
            oldModuleSerialNumber,
            oldTakeOffDry,
            oldDOTTypeCert,
            oldFAATypeCert,
            engineModel,
            engineSerialNumber,
            moduleSerialNumber,
            takeOffDry,
            DOTTypeCert,
            FAATypeCert,
        } = this.props

        if ((oldEngineModel !== engineModel) ||
            (oldEngineSerialNumber !== engineSerialNumber) ||
            (oldModuleSerialNumber !== moduleSerialNumber) ||
            (oldTakeOffDry !== takeOffDry) ||
            (oldDOTTypeCert !== DOTTypeCert) ||
            (oldFAATypeCert !== FAATypeCert)) {
            this.props.UpdateGasGenerator()
        } else {
            this.props.DontUpdateGasGenerator()
        }
    }

    updateEPECS = async () => {
        this.setState({ epecsUpdateCount: ++this.state.epecsUpdateCount })
        let { engineSerialNumber } = this.props
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "epecs_trim",
                        "arg1": "--field=ENGINE_SN_A",
                        "arg2": `--value=${engineSerialNumber}`

                    },
                    {
                        "cmd": "epecs_trim",
                        "arg1": "--field=ENGINE_SN_B",
                        "arg2": `--value=${engineSerialNumber}`

                    },
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            let responseJson = await response;
            result.push()
            if (String(responseJson.data).includes('Err')) {
                result.push("Failed")
                this.updateFields()
            } else {
                result.push("success")
                this.updateFields()
            }
        }
        catch (err) {
            if (this.state.epecsUpdateCount < 2) {
                return this.updateEPECS()
            }
            this.updateWasFailure()
        }
    }

    updateFields = async () => {
        this.setState({ fieldUpdateCount: ++this.state.fieldUpdateCount })
        let { EngineID, ModuleID } = this.props.selectedEngineModule
        let {
            engineModel,
            engineSerialNumber,
            moduleSerialNumber,
            takeOffDry,
            DOTTypeCert,
            FAATypeCert,
        } = this.props

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=1]/value`,
                        "arg3": `${engineModel}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=3]/value`,
                        "arg3": `${moduleSerialNumber}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=4]/value`,
                        "arg3": `${takeOffDry}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=5]/value`,
                        "arg3": `${DOTTypeCert}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=6]/value`,
                        "arg3": `${FAATypeCert}`
                    },
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response
            let responseJson = await response;
            var lines = JSON.stringify(responseJson.data);
            let splitlines = lines.split(":")
            var k = 0;

            for (var i = 0; i < splitlines.length; i++) {
                if (splitlines[i].includes("Success")) {

                    result.push("success")

                }
                else if (splitlines[i].includes("Err")) {

                    result.push("Failed")
                }
            }
            if (result.length < 9 && responseJson.data[0].name.includes("Data Error")) {
                for (var k = 0; k < 5; k++)
                    result.push("Failed")
            }
        }
        catch (err) {
            if (this.state.fieldUpdateCount < 2) {
                return this.updateFields()
            }

        }
        this.writeToXml()
    }

    writeToXml = async () => {
        this.setState({ xmlUpdateCount: ++this.state.xmlUpdateCount })
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            let responseJson = await response;
            if (responseJson.data[0].value === "Success") {
                this.updateWasSuccess()
            } else {
                this.updateWasFailure()
            }
        }
        catch (error) {
            if (this.state.xmlUpdateCount < 2) {
                return this.writeToXml()
            }
            this.updateWasFailure()
        }
    }

    updateWasFailure = () => {
        this.setState({ updateFailed: true, updateSuccess: false, isLoading: false })
    }

    updateWasSuccess = () => {
        this.setState({ updateFailed: false, updateSuccess: true, isLoading: false })
        this.props.navigation.navigate('updateReportGasGenerator', { param: result, headerTilte: this.props.selectedEngineModule.ModuleName })
        // this.props.FetchEngineData()
    }

    confirmedUpdate = () => {
        this.setState({ isModalVisible: false, isLoading: true })
        this.updateEPECS()
    }

    renderModal = () => {
        let {
            oldEngineModel,
            oldEngineSerialNumber,
            oldModuleSerialNumber,
            oldTakeOffDry,
            oldDOTTypeCert,
            oldFAATypeCert,
            engineModel,
            engineSerialNumber,
            moduleSerialNumber,
            takeOffDry,
            DOTTypeCert,
            FAATypeCert,
        } = this.props

        return (

            <View style={styles.modalStyle}>

                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={styles.modalHeader}>You are about to load this data to the DCTU</Text>
                </View>

                <View style={{ width: '100%', padding: 10, paddingTop: 10 }}>
                    <ScrollView
                        keyboardShouldPersistTaps={true}
                        style={{ height: 300, paddingRight: 10 }}
                    >

                        {
                            (oldEngineModel === engineModel) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Engine Model is being changed from {oldEngineModel} to {engineModel}</Text>
                                </View>
                        }
                        {
                            (oldEngineSerialNumber === engineSerialNumber) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Engine Serial Number is being changed from PCE-{oldEngineSerialNumber} to PCE-{engineSerialNumber}</Text>
                                </View>
                        }
                        {
                            (oldModuleSerialNumber === moduleSerialNumber) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Gas Generator Serial Number is being changed from {oldModuleSerialNumber} to {moduleSerialNumber}</Text>
                                </View>
                        }
                        {
                            (oldTakeOffDry === takeOffDry) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Take-off Dry is being changed from {oldTakeOffDry} SHP to {takeOffDry} SHP</Text>
                                </View>
                        }
                        {
                            (oldDOTTypeCert === DOTTypeCert) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>TCCA Certificate is being changed from {oldDOTTypeCert} to {DOTTypeCert}</Text>
                                </View>
                        }
                        {
                            (oldFAATypeCert === FAATypeCert) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>FAA Certificate is being changed from {oldFAATypeCert} to {FAATypeCert}</Text>
                                </View>
                        }

                    </ScrollView>
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>This will erase previous data</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 13 }}>

                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this.reverseValue()}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.confirmedUpdate()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>

                </View>
            </View>

        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#0033ab" />
                    </View> :
                    <ScrollView 
                        keyboardDismissMode={true}
                        keyboardShouldPersistTaps="always">
                        <View style={styles.tileStyle}>

                            {
                                this.props.selectedEngineModule.scannable === 'true' &&
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ updateSuccess: false })
                                        this.props.navigation.navigate('scannerGasGenerator', { headerTilte: this.props.selectedEngineModule.ModuleName })
                                    }}
                                >
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image
                                            style={styles.qrCodeScan}
                                            source={require('../../../../../assets/IconPack/ViewEditEngineData/LRUs/qrCodeScan.png')}
                                            resizeMode='contain'
                                        />
                                        <Text style={styles.scanQrCode}>Scan 2D barcode</Text>
                                    </View>
                                </TouchableOpacity>
                            }

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Engine Model</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field1Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedGasGenerator ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateEngineModel(text)
                                        this.props.GasGeneratorScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field1Focused: true, })}
                                    onBlur={() => this.setState({ field1Focused: false })}
                                    value={this.props.engineModel}
                                    UITextContentType="numeric">
                                </TextInput>

                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Engine Serial Number</Text>

                                <View style={[styles.textInput, {
                                    borderColor: this.state.field2Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    backgroundColor: (this.props.engineSerialNumber).includes("Err") ? '#D3D3D3' : this.state.updateSuccess ? '#e6f2d3' : this.props.scannedGasGenerator ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                }]}>
                                    <Text style={[styles.customTextESN_PCE, { width: 40 }]}>PCE-</Text>
                                    <TextInput
                                        style={[styles.CustomtextInput, { width: '85%' }]}
                                        onChangeText={(text) => {
                                            this.props.UpdateEngineSerialNumber(text)
                                            this.props.GasGeneratorScanFailed()
                                        }}
                                        maxLength={8}
                                        onFocus={() => this.setState({ field2Focused: true, })}
                                        onBlur={() => this.setState({ field2Focused: false })}
                                        editable={((this.props.engineSerialNumber).includes("Err")) ? false : true}
                                        value={this.props.engineSerialNumber}
                                        UITextContentType="numeric">
                                    </TextInput>
                                </View>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Gas Generator Serial Number</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field3Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedGasGenerator ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateModuleSerialNumber(text)
                                        this.props.GasGeneratorScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field3Focused: true, })}
                                    onBlur={() => this.setState({ field3Focused: false })}
                                    value={this.props.moduleSerialNumber}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Take-Off Dry</Text>
                                <View style={[styles.textInput, {
                                    borderColor: this.state.field4Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedGasGenerator ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                }]}>
                                    <TextInput
                                        style={[styles.CustomtextInput, { width: '85%' }]}
                                        onChangeText={(text) => {
                                            this.props.UpdateTakeOffDry(text)
                                            this.props.GasGeneratorScanFailed()
                                        }}
                                        onFocus={() => this.setState({ field4Focused: true, })}
                                        onBlur={() => this.setState({ field4Focused: false })}
                                        value={this.props.takeOffDry}
                                        UITextContentType="numeric">
                                    </TextInput>
                                    <Text style={{
                                        textAlign: 'center',
                                        paddingRight: 10,
                                        fontFamily: "HelveticaNeue",
                                        fontSize: 12,
                                        fontWeight: "300",
                                        fontStyle: "normal",
                                        letterSpacing: 0,
                                        color: "#666666"
                                    }}>SHP</Text>
                                </View>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>TCCA Certificate</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field5Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedGasGenerator ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateDOTTypeCert(text)
                                        this.props.GasGeneratorScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field5Focused: true, })}
                                    onBlur={() => this.setState({ field5Focused: false })}
                                    value={this.props.DOTTypeCert}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>FAA Certificate</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field6Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedGasGenerator ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateFAATypeCert(text)
                                        this.props.GasGeneratorScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field6Focused: true })}
                                    onBlur={() => this.setState({ field6Focused: false })}
                                    value={this.props.FAATypeCert}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                        </View>

                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-between',
                            marginTop: 15,
                            marginLeft: 15,
                            marginRight: 15,
                            marginBottom: 15,
                        }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ updateSuccess: false })
                                    this.reverseValue()
                                }}
                                style={styles.cancelButton}
                            >
                                <Text style={styles.cancelLabel}>Cancel</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                disabled={!this.props.gasGeneratorUpdate}
                                onPress={() => this.setState({ isModalVisible: true })}
                                style={[styles.updateButton, {
                                    backgroundColor: this.props.gasGeneratorUpdate ? '#0033ab' : '#e5e5e5'
                                }]}>
                                <Text style={[styles.updateLabel, {
                                    color: this.props.gasGeneratorUpdate ? '#fff' : '#666'
                                }]}>Update</Text>
                            </TouchableOpacity>
                        </View>
                        <Modal
                            style={styles.modalContainer}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={() => this.setState({ isModalVisible: true })}
                        >{this.renderModal()}</Modal>
                    </ScrollView>}
            </View>
        )
    }
}

const mapStateToProps = ({ EngineDataReducer, GasGeneratorReducer }) => {
    const { selectedEngineModule, sameESN } = EngineDataReducer
    const {
        oldEngineModel,
        oldEngineSerialNumber,
        oldModuleSerialNumber,
        oldTakeOffDry,
        oldDOTTypeCert,
        oldFAATypeCert,
        engineModel,
        engineSerialNumber,
        moduleSerialNumber,
        takeOffDry,
        DOTTypeCert,
        FAATypeCert,
        gasGeneratorUpdate,
        scannedGasGenerator
    } = GasGeneratorReducer
    return {
        selectedEngineModule,
        sameESN,
        oldEngineModel,
        oldEngineSerialNumber,
        oldModuleSerialNumber,
        oldTakeOffDry,
        oldDOTTypeCert,
        oldFAATypeCert,
        engineModel,
        engineSerialNumber,
        moduleSerialNumber,
        takeOffDry,
        DOTTypeCert,
        FAATypeCert,
        gasGeneratorUpdate,
        scannedGasGenerator
    };
};

export default connect(mapStateToProps, {
    OldEngineModel,
    OldEngineSerialNumber,
    OldModuleSerialNumber,
    OldTakeOffDry,
    OldDOTTypeCert,
    OldFAATypeCert,
    UpdateEngineModel,
    UpdateEngineSerialNumber,
    UpdateModuleSerialNumber,
    UpdateTakeOffDry,
    UpdateDOTTypeCert,
    UpdateFAATypeCert,
    UpdateGasGenerator,
    DontUpdateGasGenerator,
    GasGeneratorScanFailed,
    FetchEngineData
})(withNavigationFocus(updateGasGenerator));

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e5e5e5",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        padding: 18
    },
    qrCodeScan: {
        width: 24,
        height: 24
    },
    scanQrCode: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#0033ab",
        textAlignVertical: 'center',
        paddingLeft: 8
    },
    label: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        paddingBottom: 5
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    customTextESN_PCE: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    CustomtextInput: {
        height: 40,
        borderRadius: 6,
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#0033ab",
        justifyContent: 'center'
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: 'center'
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        width: '100%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#d6d6d6",
        padding: 23
    },
    modalHeader: {
        fontFamily: "HelveticaNeue",
        fontSize: 17,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.25,
        color: "#000000"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#fff",
        justifyContent: 'center'
    },
    previousLabelStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#0033ab"
    },
    confirmButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: 'center'
    },
    confirmLabelStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    }
})
