export const selectedEngineModule = (item) => {

    return {
        type: 'selectedEngineModule',
        payload: item
    }
};

export const FetchEngineData = () => {

    return {
        type: 'FetchEngineData',
    }
};

export const DontFetchEngineData = () => {

    return {
        type: 'DontFetchEngineData',
    }
};

export const StartLoader = () => {

    return {
        type: 'StartLoader',
    }
};

export const StopLoader = () => {

    return {
        type: 'StopLoader',
    }
};

export const DifferentESN = () => {

    return {
        type: 'DifferentESN',
    }
};

export const SameESN = () => {

    return {
        type: 'SameESN',
    }
};
