import React, { Component } from 'react';
import {
    Text,
    View,
    ActivityIndicator,
    StyleSheet,
    Dimensions
} from 'react-native';
import {
    Content
} from 'native-base';
import axios from 'axios';
import { connect } from 'react-redux';

let { width, height } = Dimensions.get('window');

class SnapChannelA extends Component {
    constructor(props) {
        super(props)

        this.state = {
            loading: true,
            userDataSource: [],
            writeToXmlCount: 0,
            writetoXMLSnappoints: 0,
            isEmpty: false
        }
    }

    UNSAFE_componentWillMount() {
        this.setState({ loading: true })

        this.snappoints();
    }

    async snappoints() {
        let snapdata = [];
        datapoints = []

        snapdata = "--code=" + this.props.enginedata.Code + " --type=" + this.props.enginedata.Type + " --ch=" + this.props.enginedata.Channel + " --date_s=\"" + this.props.enginedata.RunTime + "\" [--snap=ONLY]";
        this.setState({ writetoXMLSnappoints: ++this.state.writetoXMLSnappoints })
        // let param = "--code=0119 --type=EVENT --ch=A --date_s=\"2018-06-05 04:00:00\" [--snap=ONLY]"

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": snapdata
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            let responseJson = await response.data[0].value;

            var splitwise = await responseJson.split(",");

            if (splitwise[0] != "") {
                datapoints.push(splitwise);
                this.parameters();
            } else {
                alert('Not able to fetch the Snap Parameters.Please try again!')
            }
        }
        catch (error) {
            if (datapoints.length == 0 && this.state.writetoXMLSnappoints < 3) {

                this.snappoints()
            } else if (this.state.writetoXMLSnappoints >= 3 && datapoints.length == 0) {
                alert("Cannot Communicate with the MicroFAST")
                this.setState({ loading: false })
            }
        }
    }

    async parameters() {

        param = [];
        result = []
        parsedData = []
        let snapdata = datapoints[0].slice(2)
        var typecheck = "EXCEED"

        if (this.props.enginedata.Type === typecheck) {
            param = "--rtd=RTD_EXCEED --ch=" + this.props.enginedata.Channel
        } else {
            param = "--rtd=RTD_COMMON --ch=" + this.props.enginedata.Channel
        }

        var snapparameter = JSON.stringify(param);

        this.setState({ writeToXmlCount: ++this.state.writeToXmlCount })
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": snapparameter
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            let responseJson = response.data;

            responseJson.map((item) => {
                var splitwise = item.value.split(",");
                parsedData.push({
                    'ParaName': splitwise[1],
                    'Unit': splitwise[2],

                })
            })

            for (var i = 0; i < snapdata.length; i++) {

                result.push({
                    'Parameter': parsedData[i].ParaName,
                    'Unit': parsedData[i].Unit,
                    'Value': snapdata[i]
                })
            }

            if (result.length == 0 && this.state.writeToXmlCount >= 3) {
                this.setState({ isEmpty: true })
            }

            this.setState({ userDataSource: result, loading: false });
        } catch (error) {

            if (result.length == 0 && this.state.writeToXmlCount < 3) {

                return this.parameters();
            } else if (this.state.writeToXmlCount > 3 && result.length == 0) {
                alert("Cannot Communicate with the MicroFAST")
            }
        }

        if (result.length == 0 && this.state.writeToXmlCount < 3) {
            this.parameters();
        } else if (this.state.writeToXmlCount > 3 && result.length == 0) {
            alert("Cannot Communicate with the MicroFAST")
        }
    }

    renderView = () => {
        if (this.state.isEmpty == true) {
            return (
                <View>
                    <Text>Unable to load data !</Text>
                </View>
            )
        } else {
            return (            
                <View>
                    {
                        this.state.userDataSource.map((item) => {
                            return(
                                <View style={{ backgroundColor: '#FFFFFF', paddingLeft: 5 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ width: '40%' }}>
                                        <Text style={styles.ParamName}>{item.Parameter}</Text>
                                    </View>
                                    <View style={{ width: '60%', flexDirection: 'row' }}>
                                        <Text style={styles.ParamValue}>{item.Value}</Text>
                                        <Text style={styles.ParamUnit}>{item.Unit}</Text>
                                    </View>
                                </View>
                            </View>
                            )
                        })
                    }
                </View>
            )
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.rectangle1}>
                    <View style={{ borderBottomColor: '#E3E5E8', borderBottomWidth: 0.8, paddingLeft: 10 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.labelText}>Description</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.valueText}>{this.props.enginedata.Description}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.labelText}>Code</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.valueText}>{this.props.enginedata.Code}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.labelText}>Logged</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.valueText}>{this.props.enginedata.RunTime}</Text>
                            </View>
                        </View>

                    </View>

                    {(this.props.enginedata.Type === "EXCEED") ?
                        <View style={{ paddingLeft: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Peak Value</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Peak}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Duration</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Duration}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Area</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Zone}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Channel</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Channel}</Text>
                                </View>
                            </View>
                        </View>

                        :

                        <View style={{ paddingLeft: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Channel</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Channel}</Text>
                                </View>
                            </View>
                        </View>
                    }
                </View>

                {/* call this inside a render row */}
                <Content style={{ paddingTop: 10 }}>
                    <View style={{ backgroundColor: '#FFFFFF', paddingLeft: 10, margin: 10, paddingTop: 10, }}>
                        {this.renderView()}
                    </View>
                </Content>
            </View>
        )
    }
}

const mapStateToProps = ({ RetriveEngineData }) => {

    const { enginedata } = RetriveEngineData
    return { enginedata };
};

export default connect(mapStateToProps, null)(SnapChannelA);


const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center'
    },
    rectangle1: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8"
    },
    labelText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    valueText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: -0.34,
        color: "#1F2A44"
    },
    ParamName: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 16 : 22,
        letterSpacing: 1,
        color: "#5B6376"
    },
    ParamValue: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 16 : 22,
        letterSpacing: 1,
        color: "#1F2A44"
    },
    ParamUnit: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 10 : 12,
        letterSpacing: 1,
        color: "#5B6376"
    }
})
