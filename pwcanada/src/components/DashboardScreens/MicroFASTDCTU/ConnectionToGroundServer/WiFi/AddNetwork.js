import React, { Component } from 'react';
import { View, StyleSheet, Alert, TouchableOpacity, Keyboard, TextInput } from 'react-native';
import axios from 'axios';
import {
    Content,
    Text,
    Icon,
    Picker,
    Label
} from 'native-base';
import { confNameChanged, confSsidChanged, confMacChanged, confKeyChanged, updateConfigured, configurationFoccussed, InitialStatecalling } from '../../../../../actions';
import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import Toast, { DURATION } from 'react-native-easy-toast';
import IonicIcon from 'react-native-vector-icons/Ionicons';

class AddNetwork extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hidden: true,
            position: 'top',
            isHidden: false,
            selectedCat: '',
            fieldChanged: false,
            nameFocused: false,
            ssidFocused: false,
            macFocused: false,
            securityFocused: false,
            keyFocused: false
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <View style={{ marginLeft: 10 }}>
                    <TouchableOpacity
                        onPress={() => { params.backHandler() }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <Icon name='ios-arrow-back' style={{ fontSize: 30, color: '#fff' }} />
                            <Text style={{ color: '#fff', fontSize: 20 }}> Back</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    onValueChange = (value) => {

        this.setState({ loading: true });
        this.setState({ selectedCat: value })
        if (value === 'none') {
            this.setState({ isHidden: true })
        } else {
            this.setState({ isHidden: false })
        }
    }

    customBackHandler = () => {
        if (this.state.fieldChanged) {
            Alert.alert(
                'There are unsaved changes',
                'Do you want to continue?',
                [
                    {
                        text: 'Yes', onPress: () => {
                            this.props.navigation.goBack()
                        }
                    },
                    {
                        text: 'No', onPress: () => {

                        }
                    }
                ],
                { cancelable: false }
            )
        } else {
            this.props.navigation.goBack()
        }
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    toggleVisible() {
        this.setState({ hidden: !this.state.hidden })
        Keyboard.dismiss()
    }

    async componentDidUpdate() {
        if (this.props.configurationFocus == true) {

            this.props.confNameChanged(this.props.AddToConf.ssid);
            this.props.confSsidChanged(this.props.AddToConf.ssid);
            this.props.confMacChanged(this.props.AddToConf.mac);
            if (this.props.AddToConf.security == '[ESS]') {

                var value = 'none';

                this.setState({
                    selectedCat: value,
                    isHidden: true
                });
            }
            else if (this.props.AddToConf.security.includes('WPA2')) {

                var value = 'WPA2';

                this.setState({
                    selectedCat: value,
                    isHidden: false
                });
            }
            this.props.configurationFoccussed(false);
            this.render();
        } else {

        }
    }

    oncallingapi = async () => {
        const { name, ssid, mac, keys } = this.props;
        var sec = this.state.selectedCat;

        if (ssid != "") {

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "wifi_add_ap",
                            "arg1": JSON.stringify(name),
                            "arg2": JSON.stringify(ssid),
                            "arg3": mac == undefined ? "\"\"" : JSON.stringify(mac),
                            "arg4": JSON.stringify(sec),
                            "arg5": keys == "" ? "\"\"" : JSON.stringify(keys)
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })
                let responseJSON = await response;

                if (responseJSON.data[0].value === "Success") {

                    this.refs.toastWithSuccessStyle.show("Values Updated Successfully", DURATION.LENGTH_LONG);
                    this.setState({ loading: false });
                    this.props.updateConfigured(true);
                    this.props.InitialStatecalling();
                    this.props.navigation.navigate('ConfiguredAp');
                } else {
                    this.props.InitialStatecalling();
                    this.refs.toastWithStyle.show(responseJSON.data[0].value, 3000);
                }

                if (responseJSON.length == 0) {

                }
            } catch (error) {

            }
        }

    }
    onNameChange(text) {
        this.setState({ loading: true });
        this.props.confNameChanged(text);
    }
    onSsidChange(text) {
        this.setState({ loading: true });
        this.props.confSsidChanged(text);
    }
    onMacChange(text) {
        this.setState({ loading: true });
        this.props.confMacChanged(text);
    }

    onKeyChange(text) {
        this.setState({ loading: true });
        this.props.confKeyChanged(text);
    }

    async onSubmit(event) {
        event.preventDefault();
        Alert.alert(
            'Do you want to add the network?',
            '',
            [
                { text: 'Yes', onPress: () => this.updateValue() },

                { text: 'No', onPress: () => { } },
            ],
            { cancelable: true }
        )
    }

    async updateValue() {
        const { name, ssid, mac, keys } = this.props;

        var sec = this.state.selectedCat;



        if (sec === "WPA2" || sec === "WEP") {
            if (name != "" && ssid != "" && keys != "" && sec != "") {
                this.oncallingapi();
            } else {
                // alert("Name, SSID, Security and Key are mandatory.");
                Alert.alert(
                    'Name, SSID, Security and Key cannot be empty',
                    '',
                    [
                        { text: 'OK', onPress: () => { } },
                    ],
                    { cancelable: true }
                )
            }

        } else {

            if (name != "" && ssid != "" && sec != "") {
                this.oncallingapi();
            } else {
                // alert("Name, SSID, Security and Key are mandatory.");
                Alert.alert(
                    'Name, SSID, Security and Key cannot be empty',
                    '',
                    [
                        { text: 'OK', onPress: () => { } },
                    ],
                    { cancelable: true }
                )
            }
        }
    }

    render() {
        let src = this.state.hidden ? require('../../../../../assets/icons/visible.png') : require('../../../../../assets/icons/hidden.png')

        return (
            <View style={{ flex: 1 }}>
                <Toast ref="toastWithStyle" style={{ backgroundColor: '#8B0000' }} position={this.state.position} />
                <Toast ref="toastWithSuccessStyle" style={{ backgroundColor: 'green' }} position={this.state.position} />
                <Content>
                    <View style={{ backgroundColor: '#ffffff', margin: 15, padding: 15 }} >

                        <View>
                            <View style={{ padding: 10 }}>
                                <Text style={styles.label}>Name</Text>
                                <TextInput
                                    autoCorrect={false}
                                    autoCapitalize={'none'}
                                    onChangeText={this.onNameChange.bind(this)}
                                    value={this.props.AddToConf.ssid == undefined ? this.props.name : this.props.AddToConf.ssid}
                                    style={[styles.textInput, { borderColor: this.state.nameFocused ? '#00A9E0' : '#bdbdbd' }]}
                                    onFocus={() => this.setState({ nameFocused: true })}
                                    onBlur={() => this.setState({ nameFocused: false })}
                                ></TextInput>
                            </View>

                            <View style={{ padding: 10 }}>
                                <Text style={styles.label}>SSID</Text>
                                <TextInput
                                    autoCorrect={false}
                                    autoCapitalize={'none'}
                                    onChangeText={this.onSsidChange.bind(this)}
                                    value={this.props.AddToConf.ssid == undefined ? this.props.ssid : this.props.AddToConf.ssid}
                                    style={[styles.textInput, { borderColor: this.state.ssidFocused ? '#00A9E0' : '#bdbdbd' }]}
                                    onFocus={() => this.setState({ ssidFocused: true })}
                                    onBlur={() => this.setState({ ssidFocused: false })}
                                ></TextInput>

                                <View style={styles.visibleContainer}></View>
                            </View>

                            <View style={{ padding: 10 }}>

                                <Text style={styles.label}>Mac(Optional)</Text>
                                <TextInput
                                    style={[styles.textInput, { borderColor: this.state.macFocused ? '#00A9E0' : '#bdbdbd' }]}
                                    onFocus={() => this.setState({ macFocused: true })}
                                    onBlur={() => this.setState({ macFocused: false })}
                                    autoCorrect={false}
                                    autoCapitalize={'none'}
                                    onChangeText={this.onMacChange.bind(this)}
                                    value={this.props.mac}
                                ></TextInput>

                                <View style={styles.visibleContainer}></View>
                            </View>

                            <View style={{ padding: 10 }}>
                                <Text style={styles.label}>Security</Text>
                                <View
                                    onFocus={() => this.setState({ securityFocused: true })}
                                    onBlur={() => this.setState({ securityFocused: false })}
                                >
                                    <Picker
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        mode="dialog"
                                        placeholder="Security Protocol"
                                        selectedValue={this.state.selectedCat}
                                        onValueChange={this.onValueChange.bind(this)}
                                        style={[styles.textInput, { borderColor: this.state.securityFocused ? '#00A9E0' : '#bdbdbd' }]}
                                    >

                                        <Picker.Item label='WPA2' value='WPA2' key='1' />
                                        <Picker.Item label='WEP' value='WEP' key='2' />
                                        <Picker.Item label='None' value='none' key='3' />
                                    </Picker>

                                </View>
                            </View>

                            <View style={{ padding: 10 }}>

                                {!this.state.isHidden &&

                                    <View>
                                        <Text style={styles.label}>Key</Text>

                                        <View style={[styles.textInput, {
                                            width: '100%',
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            borderWidth: 1,
                                            borderColor: this.state.keyFocused ? '#00A9E0' : '#bdbdbd'
                                        }]}>

                                            <TextInput
                                                style={[styles.CustomtextInput, { width: '90%' }]}
                                                onFocus={() => this.setState({ keyFocused: true })}
                                                onBlur={() => this.setState({ keyFocused: false })}
                                                autoCorrect={false}
                                                secureTextEntry={this.state.hidden}
                                                autoCapitalize={'none'}
                                                onChangeText={this.onKeyChange.bind(this)}
                                                value={this.props.keys}
                                            />
                                            <View style={{ width: '10%', paddingLeft: 30 }}>
                                                <TouchableOpacity onPress={() => this.setState({ hidden: !this.state.hidden })}>
                                                    <View>
                                                        <IonicIcon
                                                            name={this.state.hidden ? 'ios-eye-off' : 'ios-eye'}
                                                            style={{ fontSize: 35, color: '#00A9E0' }} 
                                                        />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>

                                        </View>
                                    </View>
                                }
                            </View>
                        </View>
                    </View>
                    <View>
                        <View style={styles.buttonAlign}>
                            <TouchableOpacity style={styles.testButton} onPress={this.onSubmit.bind(this)} >
                                <Label style={styles.testButtonText}><Text style={{ color: '#ffffff' }} >Add Network</Text></Label>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>
            </View>
        )
    }
}


const mapStateToProp = ({ footer }) => {
    const { name, ssid, mac, keys, AddToConf, configurationFocus } = footer;
    return { name, ssid, mac, keys, AddToConf, configurationFocus };
};

export default connect(mapStateToProp, { confNameChanged, confSsidChanged, confMacChanged, confKeyChanged, updateConfigured, configurationFoccussed, InitialStatecalling })(withNavigationFocus(AddNetwork));


const styles = StyleSheet.create({
    label: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        paddingBottom: 5
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    buttonstyle: {
        backgroundColor: '#ffcc33',
        alignSelf: 'stretch'
    },
    buttonTextStyle: {
        color: '#333333',
        fontSize: 16
    },
    visibleContainer: {
        justifyContent: 'center',
        alignContent: 'center',
        paddingLeft: 5,
        paddingRight: 15
    },
    buttonAlign: {
        marginLeft: 15,
        marginRight: 15,
        marginEnd: 15,
        flexDirection: 'row'
    },
    testButton: {
        width: '100%',
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        marginBottom: 15,
        paddingRight: 5
    },
    testButtonText: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#ffffff"
    },
    CustomtextInput: {
        height: 40,
        borderRadius: 6,
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
})
