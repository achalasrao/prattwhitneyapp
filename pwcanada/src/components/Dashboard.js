import React, { Component } from "react";
import {
    Linking,
    ScrollView,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Alert,
    Text, 
    Dimensions
} from "react-native";
import IonicIcon from "react-native-vector-icons/Ionicons";
import OctIcon from "react-native-vector-icons/Octicons";
import { connect } from "react-redux";
import axios from "axios";
import {
    EngineSerialNumber,
    TNEmpty,
    TNError,
    TNFetchSuccess
} from "../actions";

let { width, height } = Dimensions.get('window');

const tabs = [
    { id: "1", name: "Live Data", src: require("../assets/icons/play.png") },
    {
        id: "2",
        name: "MicroFAST Status",
        src: require("../assets/icons/monitorStatus.png")
    },
    {
        id: "3",
        name: "Test Transmission",
        src: require("../assets/icons/testTransmission.png")
    },
    {
        id: "4",
        name: "Customer Activation",
        src: require("../assets/icons/customer.png")
    },
    {
        id: "5",
        name: "MicroFAST Configuration",
        src: require("../assets/icons/microFAST.png")
    },
    {
        id: "6",
        name: "MicroFAST Wireless Connections",
        src: require("../assets/icons/connection.png")
    },
    {
        id: "7",
        name: "Retrieve Engine Data",
        src: require("../assets/icons/engine.png")
    },
    {
        id: "8",
        name: "Engine Information",
        src: require("../assets/icons/Epecs.png")
    }
];

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fetchResponse: "",
            failed: "",
            fetchCount: 0,
            epecsFetchCount: 0
        };


    }

    customExitHandler = () => {
        Alert.alert(
            "Please disconnect from DCTU before exiting",
            "",
            [
                {
                    text: "Exit",
                    onPress: () => {
                        Linking.canOpenURL("app-settings:")
                            .then(supported => {
                                Linking.openURL("App-Prefs:root=WIFI");
                            })
                            .catch(error => { });
                    },
                    style: "destructive"
                },
                {
                    text: "Cancel",
                    onPress: () => { },
                    style: "cancel"
                }
            ],
            { cancelable: false }
        );
    };

    UNSAFE_componentWillMount = async () => {
        if (this.props.connectionStatus === "MicroFAST") {
            this.fetchdata();
        }
        this.props.navigation.setParams({
            exitHandler: this.customExitHandler
        });
    };

    componentDidMount = () => {

    };

    dashboardIdChanged(Id) {
        switch (Id) {
            //navigation logic
            // =====View/Edit Engine Data====
            case "Engine Data":
                return this.props.navigation.navigate("EngineData");
            case "EPECS Change":
                return this.props.navigation.navigate("EOF");
            // return this.props.navigation.navigate("EECReplacement");

            case "Engine Trims":
                return this.props.navigation.navigate("Trim");
            case "LRUs":
                return this.props.navigation.navigate("LRUList");

            // =====Engine Diagnostic======
            case "Events & Exceedances":
                return this.props.navigation.navigate("ChannelA");
            case "Faults":
                return this.props.navigation.navigate("TabularFault");
            case "Live Data":
                return this.props.navigation.navigate("LiveData");
            case "About the EPECS":
                return this.props.navigation.navigate("AbouttheEpecs");

            // =====Usage Data====
            case "Engine Usage":
                return this.props.navigation.navigate("EngineUsage");
            case "LCF":
                return this.props.navigation.navigate("LCF");
            case "CREEP":
                return this.props.navigation.navigate("Creep");

            // ====MicroFAST DCTU=====
            case "Connection to ground server":
                return this.props.navigation.navigate("ConnectionDCTUGround");
            case "About this DCTU":
                return this.props.navigation.navigate("AboutThisDCTU");

            // ====Aircraft====
            case "View / edit aircraft data":
                return this.props.navigation.navigate("AircraftUpdate");

            case "7":
                return (
                    this.props.navigation.navigate("RetrieveEngineData"),
                    this.props.navigation.setParams({
                        headerTitle: "Retrieve Engine Data"
                    })
                );
            case "8":
                return this.props.navigation.navigate("EngineInformation");

            default:

        }
    }

    connectionProcessor = item => {
        if (this.props.connectionStatus === "Not MicroFAST") {
            this.microFASTIsDown(); 
            // this.dashboardIdChanged(item);  // remove later
        } else if (this.props.connectionStatus === "MicroFAST") {
            this.dashboardIdChanged(item);
        } else {
            this.connectToMicroFASTPrompt();
            // this.dashboardIdChanged(item); // remove later
        }
    };

    async fetchdata() {
        this.setState({ fetchCount: ++this.state.fetchCount });
        try {
            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "xml_access",
                        arg1: "read",
                        arg2: "Aircraft/Tail_Number"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });

            let responseJson = await response.data[0].value;
            if (String(responseJson).length === 0 && this.state.fetchCount < 2) {
                this.fetchdata();
            } else {
                let parsedTailNumber = responseJson;

                if (parsedTailNumber.includes("Err")) {
                    this.props.TNError();
                } else if (parsedTailNumber === "") {
                    this.props.TNEmpty();
                } else {
                    parsedTailNumber = responseJson.replace(/&amp;/g, "&");
                    this.props.TNFetchSuccess(parsedTailNumber);
                    this.fetchESN();
                }
            }
        } catch (error) {
            if (this.state.fetchCount < 2) {
                return this.fetchdata();
            }
            this.setState({ failed: true });
        }
    }

    fetchESN = async () => {
        this.setState({ epecsFetchCount: ++this.state.epecsFetchCount });
        try {
            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "epecs_trim",
                        arg1: "--field=ENGINE_SN_A"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });
            engineSN_ChA = await response.data[0].value;
            if (!String(engineSN_ChA).includes("Err")) {
                this.props.EngineSerialNumber(engineSN_ChA);
            } else {
                this.props.EngineSerialNumber("Failed to fetch");
            }
        } catch (error) {
            if (this.state.epecsFetchCount < 2) {
                return this.fetchESN();
            }
        }
    };

    enableWifiPrompt = () => {
        return Alert.alert(
            "Please enable Wi-Fi",
            "Redirect to settings.",
            [
                {
                    text: "Redirect",
                    onPress: () => {
                        this._reDirect();
                    }
                },
                {
                    text: "Dismiss",
                    onPress: () => { },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    connectToMicroFASTPrompt = () => {
        return Alert.alert(
            "Please connect to DCTU",
            "Redirect to settings.",
            [
                {
                    text: "Redirect",
                    onPress: () => {
                        this._reDirect();
                    }
                },
                {
                    text: "Dismiss",
                    onPress: () => { },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    microFASTIsDown = () => {
        return Alert.alert(
            "DCTU is down",
            "Try again later",
            [
                {
                    text: "Dismiss",
                    onPress: () => { },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    _reDirect() {
        Linking.canOpenURL("app-settings:")
            .then(supported => {
                Linking.openURL("App-Prefs:root=WIFI");
            })
            .catch(error => { });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    {
                        this.props.connectionStatus !== "MicroFAST" ? (
                        <View
                            style={[
                                styles.tileStyle,
                                {
                                    justifyContent: "flex-start",
                                    paddingTop: 15,
                                    backgroundColor: "#FF5C42",
                                    paddingLeft: 20
                                }
                            ]}
                        >
                            <View>
                                <OctIcon name="alert" style={styles.alertIcon} />
                            </View>
                            <View
                                style={{
                                    width: "80%",
                                    paddingLeft: 10,
                                    justifyContent: "center"
                                }}
                            >
                                <Text style={styles.connectionErrorText}>Connection error</Text>
                            </View>
                        </View>
                    ) : this.props.isTNEmpty ? (
                        <View
                            style={[
                                styles.tileStyle,
                                {
                                    backgroundColor: "#FF5C42",
                                    justifyContent: "flex-start",
                                    paddingTop: 15,
                                    paddingLeft: 10
                                }
                            ]}
                        >
                            <View>
                                <Image
                                    style={{
                                        width: 26,
                                        height: 18,
                                    }}
                                    source={require("../assets/IconPack/EOF/error.png")}
                                    resizeMode="contain"
                                />
                            </View>
                            <View style={{ width: "80%", paddingLeft: 5 }}>
                                <Text style={styles.updatedate}>
                                    Aircraft Tail Number not configured
                                </Text>
                            </View>
                        </View>
                    ) : this.props.isError ? (
                        <View
                            style={[
                                styles.tileStyle,
                                {
                                    backgroundColor: "#FF5C42",
                                    justifyContent: "flex-start",
                                    paddingTop: 15
                                }
                            ]}
                        >
                            <View>
                                <Image
                                    style={{
                                        width: 26,
                                        height: 18,
                                        paddingLeft: 50,
                                        alignContent: "center"
                                    }}
                                    source={require("../assets/IconPack/EOF/error.png")}
                                    resizeMode="contain"
                                />
                            </View>
                            <View style={{ width: "80%", paddingLeft: 5 }}>
                                <Text style={styles.updatedate}>
                                    Failed to fetch Aircraft Tail Number
                                </Text>
                            </View>
                        </View>
                    ) : (
                                    <View
                                        style={[
                                            styles.tileStyle,
                                            {
                                                backgroundColor: "#71FFB1",
                                                justifyContent: "flex-start",
                                                paddingTop: 15
                                            }
                                        ]}
                                    >
                                        <View>
                                            <Image
                                                style={{
                                                    width: 26,
                                                    height: 18,
                                                    paddingLeft: 50,
                                                    alignContent: "center"
                                                }}
                                                source={require("../assets/IconPack/Dashboard/check.png")}
                                                resizeMode="contain"
                                            />
                                        </View>
                                        <View style={{ width: "80%", paddingLeft: 5 }}>
                                            <Text style={styles.updatedate}>App connected to {this.props.tailNumber}</Text>
                                            {/* <Text style={styles.updatedate}></Text> */}
                                        </View>
                                    </View>
                                )}

                    <View style={styles.tileStyle}>
                        <View>
                            <Image
                                style= {styles.Image}
                                source={require("../assets/IconPack/Dashboard/engine04.png")}
                                resizeMode="contain"
                            />
                        </View>
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={styles.editEngineD}>View / Edit Engine Data</Text>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("Engine Data")}
                            >
                                <Text style={styles.label}>
                                    Engine Data{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("Engine Trims")}
                            >
                                <Text style={styles.label}>
                                    Engine Trims{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("LRUs")}
                            >
                                <Text style={styles.label}>
                                    LRUs{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("About the EPECS")}
                            >
                                <Text style={styles.label}>
                                    About the EEC{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("EPECS Change")}
                            >
                                <Text style={styles.label}>
                                    EEC Replacement{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.tileStyle}>
                        <View>
                            <Image
                                style={styles.Image}
                                source={require("../assets/IconPack/Dashboard/engineDiagnostic.png")}
                                resizeMode="contain"
                            />
                        </View>
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={styles.editEngineD}>Engine Diagnostic</Text>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("Events & Exceedances")}
                            >
                                <Text style={styles.label}>
                                    Events & Exceedances{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("Faults")}
                            >
                                <Text style={styles.label}>
                                    Faults{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("Live Data")}
                            >
                                <Text style={styles.label}>
                                    Live Data{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.tileStyle}>
                        <View>
                            <Image
                                style={styles.Image}
                                source={require("../assets/IconPack/Dashboard/logbook.png")}
                                resizeMode="contain"
                            />
                        </View>
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={styles.editEngineD}>Usage Data</Text>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("Engine Usage")}
                            >
                                <Text style={styles.label}>
                                    Engine Usage{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.connectionProcessor("LCF")}>
                                <Text style={styles.label}>
                                    LCFs{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("CREEP")}
                            >
                                <Text style={styles.label}>
                                    CREEP{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.tileStyle}>
                        <View>
                            <Image
                                style={styles.Image}
                                source={require("../assets/IconPack/Dashboard/microFast.png")}
                                resizeMode="contain"
                            />
                        </View>
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={styles.editEngineD}>DCTU</Text>

                            <TouchableOpacity
                                onPress={() =>
                                    this.connectionProcessor("Connection to ground server")
                                }
                            >
                                <Text style={styles.label}>
                                    Connection to ground server{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.connectionProcessor("About this DCTU")}
                            >
                                <Text style={styles.label}>
                                    About this DCTU{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={[styles.tileStyle, { marginBottom: 15 }]}>
                        <View>
                            <Image
                                style={styles.Image}
                                source={require("../assets/IconPack/Dashboard/plane.png")}
                                resizeMode="contain"
                            />
                        </View>
                        <View style={{ paddingLeft: 8 }}>
                            <Text style={styles.editEngineD}>Aircraft</Text>

                            <TouchableOpacity
                                onPress={() =>
                                    this.connectionProcessor("View / edit aircraft data")
                                }
                            >
                                <Text style={styles.label}>
                                    View / Edit Aircraft Data{" "}
                                    <IonicIcon
                                        name="ios-arrow-forward"
                                        style={styles.labelIcon}
                                    />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ marginLeft: 20, marginBottom: 22 }}>
                        <Text style={styles.appVersion145}>App Version: 8.0.16</Text>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("UserManual")}
                        >
                            <Text style={styles.label}>
                                App User Guide{" "}
                                <IonicIcon name="ios-arrow-forward" style={styles.labelIcon} />
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = ({ connection }) => {
    const {
        connectionStatus,
        isTNEmpty,
        isTNError,
        isNotMF,
        tailNumber
    } = connection;
    return {
        connectionStatus,
        isTNEmpty,
        isTNError,
        isNotMF,
        tailNumber
    };
};

export default connect(
    mapStateToProps,
    { EngineSerialNumber, TNEmpty, TNError, TNFetchSuccess }
)(Dashboard);

//connecting action and state to props

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        flexDirection: "row",
        padding: 10
    },
    updatedate: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0.5,
        color: "#1F2A44"
    },
    connectionErrorText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0.5,
        color: "#FFFFFF"
    },
    editEngineD: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#1F2A44",
        paddingTop: 13
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#00A9E0",
        paddingTop: 13
    },
    labelIcon: {
        fontSize: width < 450 ? 16 : 22,
        opacity: 0.23,
        color: "#00A9E0"
    },
    alertIcon: {
        fontSize: width < 450 ? 30 : 40,
        color: "#F7E164"
    },
    appVersion145: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.44,
        color: "#848A98"
    },
    Image: {
        width: width < 450 ? 50 : 80,
        height: width < 450 ? 50 : 80
    }
});
