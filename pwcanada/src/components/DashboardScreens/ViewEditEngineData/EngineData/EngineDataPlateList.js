import React, { Component } from "react";
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    ActivityIndicator,
    Alert,
    Dimensions
} from "react-native";
import IonicIcon from "react-native-vector-icons/Ionicons";
import { withNavigationFocus } from "react-navigation";
import { connect } from "react-redux";
import {
    selectedEngineModule,
    DontFetchEngineData,
    GasGeneratorScanFailed,
    PerformanceDataScanFailed,
    PowerSectionScanFailed,
    EngineBuildSpecScanFailed,
    StartLoader,
    StopLoader,
    DifferentESN,
    SameESN,
    OldEngineModel,
    OldEngineSerialNumber,
    OldModuleSerialNumber,
    OldTakeOffDry,
    OldDOTTypeCert,
    OldFAATypeCert,
    UpdateEngineModel,
    UpdateEngineSerialNumber,
    UpdateModuleSerialNumber,
    UpdateTakeOffDry,
    UpdateDOTTypeCert,
    UpdateFAATypeCert,
    OldPowerSectionSerialNumber,
    updatePowerSectionSerialNumber,
    OldengineBuildSpec,
    updateEngineBuildSpec,
    OldNG,
    OldAtSHP,
    OldITTTrim,
    OldOHMS,
    OldCompTrimWeight1,
    OldCompTrimWeight2,
    OldCompTrimWeight3,
    OldCompTrimWeight4,
    OldEngineBuildSpec,
    UpdateNG,
    UpdateAtSHP,
    UpdateITTTrim,
    UpdateOHMS,
    UpdateCompTrimWeight1,
    UpdateCompTrimWeight2,
    UpdateCompTrimWeight3,
    UpdateCompTrimWeight4,
    UpdateEngineBuildSpec,
    EngineSerialNumber
} from "../../../../actions";
import axios from "axios";

let { width, height } = Dimensions.get('window');


var parseString = require("react-native-xml2js").parseString;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// fetch the Mdoule array from the MicroFAST
// Parse and separate the details
// Feed the array to view
// OnClick Capture the selected Module item details and navigate to updateLRU.js
//
// Coded on: Oct 12 2018
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

let engineSN_ChA = "",
    ITT_Trim = "",
    engineSN_ChB = "";
var parameterList = []

class EngineDataPlate extends Component {
    state = {
        // to store module data
        GasGenerator: "",
        PowerSection: "",
        PerformanceData: "",
        BuildSpec: "",
        epecsfetchChACount: 0,
        epecsfetchChBCount: 0,
        epecsTrimfetchCount: 0,
        xmlFetchCount: 0
    };

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate("Dashboard")}
                style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                }}
            >
                <IonicIcon
                    name="ios-arrow-back"
                    style={{ fontSize: 35, color: "#000000", paddingLeft: 8 }}
                />
                <View style={{ justifyContent: "center" }}>
                    <Text style={{ textAlign: "center", fontSize: 18, paddingBottom: 4 }}>
                        {" "}
                        Dashboard
          </Text>
                </View>
            </TouchableOpacity>
        )
    });

    componentDidMount() {

        this.epecsESNchannelAfetch();
        this.props.StartLoader();
    }

    componentDidUpdate = () => {
        if (this.props.fetchEngineData) {
            this.epecsESNchannelAfetch();
        }
    };

    epecsESNchannelAfetch = async () => {
        this.setState({ epecsfetchChACount: ++this.state.epecsfetchChACount });
        try {
            this.props.DontFetchEngineData();
            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "epecs_trim",
                        arg1: "--field=ENGINE_SN_A"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });
            engineSN_ChA = await response.data[0].value;
            if (engineSN_ChA.includes(`<MicroFAST_Box>`)) {
                return this.XMLError();
            }
            else {

                this.props.EngineSerialNumber(engineSN_ChA);
                this.epecsESNchannelBfetch();
            }

        } catch (error) {
            if (this.state.epecsfetchChACount <= 2) {
                this.epecsESNchannelAfetch();
            } else {
                this.FailedtoLoadData();
            }
        }
    };

    EPECSdown = () => {
        Alert.alert(
            "EEC is down",
            "Try again later.",
            [
                {
                    text: "Go Back",
                    onPress: () => {
                        this.props.navigation.navigate("Dashboard");
                    },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    FailedtoLoadData = () => {
        Alert.alert(
            "Failed to load data",
            "Try again later.",
            [
                {
                    text: "Go Back",
                    onPress: () => {
                        this.props.navigation.navigate("Dashboard");
                    },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    XMLError = () => {
        Alert.alert(
            "Failed to fetch Engine Serial Number",
            "Try again later.",
            [
                {
                    text: "Go Back",
                    onPress: () => {
                        this.props.navigation.navigate("Dashboard");
                    },
                    style: "destructive"
                }
            ],
            { cancelable: false }
        );
    };

    epecsESNchannelBfetch = async () => {
        this.setState({ epecsfetchChBCount: ++this.state.epecsfetchChBCount });
        try {

            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "epecs_trim",
                        arg1: "--field=ENGINE_SN_B"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });
            engineSN_ChB = await response.data[0].value;

            if (engineSN_ChB.includes(`<MicroFAST_Box>`)) {
                return this.XMLError();
            }
            else {

                this.epecsTrimchannelAfetch();
            }

        } catch (error) {
            if (this.state.epecsfetchChBCount <= 2) {
                this.epecsESNchannelBfetch();
            } else {
                this.FailedtoLoadData();
            }
        }
    };

    epecsTrimchannelAfetch = async () => {
        this.setState({ epecsTrimfetchCount: ++this.state.epecsTrimfetchCount });
        try {
            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "epecs_trim",
                        arg1: "--field=ITT_TRIM_A"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });
            ITT_Trim = await response.data[0].value;

            if (ITT_Trim.includes(`<MicroFAST_Box>`)) {
                return this.XMLError();
            } else if (
                ITT_Trim.includes("Err")
            ) {
                this.fetch()
                // this.EPECSdown();
            } else if (!ITT_Trim.includes("Err")) {
                this.fetch();
            }
        } catch (error) {
            if (this.state.epecsTrimfetchCount <= 2) {
                this.epecsTrimchannelAfetch();
            } else {
                this.FailedtoLoadData();
            }
        }
    };

    fetch = async () => {
        try {
            this.setState({ xmlFetchCount: ++this.state.xmlFetchCount });
            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "xml_access",
                        arg1: "read"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });

            //Ftech the response and pass it to parser
            this.props.DontFetchEngineData();
            this.props.GasGeneratorScanFailed();
            this.props.PerformanceDataScanFailed();
            this.props.PowerSectionScanFailed();
            this.props.EngineBuildSpecScanFailed();
            let fetchResponse = await response.data[0].value;
            if (this.state.xmlFetchCount <= 2) {
                return this.parser(fetchResponse);
            } else {
                return this.FailedtoLoadData();
            }
        }
        catch (error) {
            if (this.state.xmlFetchCount <= 2) {
                this.FailedtoLoadData();
            } else {
                this.fetch();
            }
        }
    };

    // Parser and extractor for Engine Data
    parser = response => {
        let moduleArray,
            EngineID,
            GasGenerator = [],
            PerformanceData = [],
            BuildSpec = [],
            PowerSection = [];

        parseString(response, function (err, result) {
            moduleArray = result.MicroFAST_Box.Engine[0].Module;
            EngineID = result.MicroFAST_Box.Engine[0].$.ID;
            let GasGeneratorfield = [],
                PerformanceDatafield = [],
                BuildSpecfield = [],
                PowerSectionfield = [];

            moduleArray.map(x => {
                parameterList.push(x.name[0])
                // Gas Generator
                if (x.name[0] === "Gas Generator") {
                    let temp = [];

                    x.data_field.map(y => {
                        if (y.name[0] === "Engine Serial Number") {
                            temp.push({
                                FieldName: y.name[0],
                                FieldID: y.$.ID,
                                FieldValue: engineSN_ChA,
                                FieldStoreXML: y.store_xml[0],
                                isPresent: false
                            });
                        } else {
                            temp.push({
                                FieldName: y.name[0],
                                FieldID: y.$.ID,
                                FieldValue: y.value[0],
                                FieldStoreXML: y.store_xml[0]
                            });
                        }
                    });
                    GasGeneratorfield.push(temp);
                    GasGenerator.push({
                        ModuleName: x.name[0],
                        scannable: x.scannable[0],
                        Fields: GasGeneratorfield,
                        EngineID: EngineID,
                        ModuleID: x.$.ID
                    });
                }

                // Performance Data
                if (x.name[0] === "Performance Data") {
                    let temp = [];

                    x.data_field.map(y => {
                        if (y.name[0] === "ITT Trim") {
                            temp.push({
                                FieldName: y.name[0],
                                FieldID: y.$.ID,
                                FieldValue: ITT_Trim,
                                FieldStoreXML: y.store_xml[0]
                            });
                        } else {
                            temp.push({
                                FieldName: y.name[0],
                                FieldID: y.$.ID,
                                FieldValue: y.value[0],
                                FieldStoreXML: y.store_xml[0]
                            });
                        }
                    });
                    PerformanceDatafield.push(temp);
                    PerformanceData.push({
                        ModuleName: x.name[0],
                        scannable: x.scannable[0],
                        Fields: PerformanceDatafield,
                        EngineID: EngineID,
                        ModuleID: x.$.ID
                    });
                }

                // Build Spec
                if (x.name[0] === "Build Spec") {
                    let temp = [];

                    x.data_field.map(y => {
                        temp.push({
                            FieldName: y.name[0],
                            FieldID: y.$.ID,
                            FieldValue: y.value[0],
                            FieldStoreXML: y.store_xml[0]
                        });
                    });
                    BuildSpecfield.push(temp);
                    BuildSpec.push({
                        ModuleName: x.name[0],
                        scannable: x.scannable[0],
                        Fields: BuildSpecfield,
                        EngineID: EngineID,
                        ModuleID: x.$.ID
                    });
                }

                // Power Section
                if (x.name[0] === "Power Section") {
                    let temp = [];

                    x.data_field.map(y => {
                        temp.push({
                            FieldName: y.name[0],
                            FieldID: y.$.ID,
                            FieldValue: y.value[0],
                            FieldStoreXML: y.store_xml[0]
                        });
                    });
                    PowerSectionfield.push(temp);
                    PowerSection.push({
                        ModuleName: x.name[0],
                        scannable: x.scannable[0],
                        Fields: PowerSectionfield,
                        EngineID: EngineID,
                        ModuleID: x.$.ID
                    });
                }
            });
        });

        this.props.OldEngineModel(GasGenerator[0].Fields[0][0].FieldValue);
        this.props.OldEngineSerialNumber(GasGenerator[0].Fields[0][1].FieldValue);
        this.props.OldModuleSerialNumber(GasGenerator[0].Fields[0][2].FieldValue);
        this.props.OldTakeOffDry(GasGenerator[0].Fields[0][3].FieldValue);
        this.props.OldDOTTypeCert(GasGenerator[0].Fields[0][4].FieldValue);
        this.props.OldFAATypeCert(GasGenerator[0].Fields[0][5].FieldValue);
        this.props.UpdateEngineModel(GasGenerator[0].Fields[0][0].FieldValue);
        this.props.UpdateEngineSerialNumber(GasGenerator[0].Fields[0][1].FieldValue);
        this.props.UpdateModuleSerialNumber(GasGenerator[0].Fields[0][2].FieldValue);
        this.props.UpdateTakeOffDry(GasGenerator[0].Fields[0][3].FieldValue);
        this.props.UpdateDOTTypeCert(GasGenerator[0].Fields[0][4].FieldValue);
        this.props.UpdateFAATypeCert(GasGenerator[0].Fields[0][5].FieldValue);
        this.props.OldengineBuildSpec(BuildSpec[0].Fields[0][0].FieldValue);
        this.props.updateEngineBuildSpec(BuildSpec[0].Fields[0][0].FieldValue);
        this.props.OldPowerSectionSerialNumber(PowerSection[0].Fields[0][0].FieldValue);
        this.props.updatePowerSectionSerialNumber(PowerSection[0].Fields[0][0].FieldValue);
        this.props.OldNG(PerformanceData[0].Fields[0][0].FieldValue);
        this.props.OldAtSHP(PerformanceData[0].Fields[0][1].FieldValue);
        this.props.OldITTTrim(PerformanceData[0].Fields[0][2].FieldValue);
        this.props.OldOHMS(PerformanceData[0].Fields[0][3].FieldValue);
        this.props.OldCompTrimWeight1(PerformanceData[0].Fields[0][4].FieldValue);
        this.props.OldCompTrimWeight2(PerformanceData[0].Fields[0][5].FieldValue);
        this.props.OldCompTrimWeight3(PerformanceData[0].Fields[0][6].FieldValue);
        this.props.OldCompTrimWeight4(PerformanceData[0].Fields[0][7].FieldValue);
        this.props.OldEngineBuildSpec(PerformanceData[0].Fields[0][8].FieldValue);
        this.props.UpdateNG(PerformanceData[0].Fields[0][0].FieldValue);
        this.props.UpdateAtSHP(PerformanceData[0].Fields[0][1].FieldValue);
        this.props.UpdateITTTrim(PerformanceData[0].Fields[0][2].FieldValue);
        this.props.UpdateOHMS(PerformanceData[0].Fields[0][3].FieldValue);
        this.props.UpdateCompTrimWeight1(PerformanceData[0].Fields[0][4].FieldValue);
        this.props.UpdateCompTrimWeight2(PerformanceData[0].Fields[0][5].FieldValue);
        this.props.UpdateCompTrimWeight3(PerformanceData[0].Fields[0][6].FieldValue);
        this.props.UpdateCompTrimWeight4(PerformanceData[0].Fields[0][7].FieldValue);
        this.props.UpdateEngineBuildSpec(PerformanceData[0].Fields[0][8].FieldValue);

        this.setState({
            GasGenerator: GasGenerator[0],
            PerformanceData: PerformanceData[0],
            BuildSpec: BuildSpec[0],
            PowerSection: PowerSection[0]
        });
        this.ESNresolver();
        this.props.StopLoader();
    };

    ESNresolver = () => {
        if (engineSN_ChA === engineSN_ChB) {
            return this.props.SameESN();
        }
        this.props.DifferentESN();
    };

    render() {

        return (
            <View style={{ flex: 1 }}>
                {this.props.isLoading ? (
                    <View
                        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
                    >
                        <ActivityIndicator size="large" color="#5B6376" />
                    </View>
                ) : (
                        <View style={{ flex: 1 }}>
                            <ScrollView>
                                <View style={styles.tileStyle}>
                                    <View>
                                        <Image
                                            style= {styles.Image}
                                            source={require("../../../../assets/IconPack/ViewEditEngineData/EngineData/serial.png")}
                                            resizeMode="contain"
                                        />
                                    </View>
                                    <View style={{ paddingLeft: 8 }}>
                                        <Text style={styles.engineDataPlates}>Engine Data Plates</Text>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.props.selectedEngineModule(this.state.GasGenerator);
                                                this.props.navigation.navigate("updateGasGenerator", {
                                                    headerTilte: this.state.GasGenerator.ModuleName
                                                });
                                            }}
                                        >
                                            <Text style={styles.label}>
                                                {parameterList[0]}{" "}
                                                <IonicIcon
                                                    name="ios-arrow-forward"
                                                    style={styles.labelIcon}
                                                />
                                            </Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => {
                                                this.props.selectedEngineModule(
                                                    this.state.PerformanceData
                                                );
                                                this.props.navigation.navigate("updatePerformanceData", {
                                                    headerTilte: this.state.PerformanceData.ModuleName
                                                });
                                            }}
                                        >
                                            <Text style={styles.label}>
                                                {parameterList[1]}{" "}
                                                <IonicIcon
                                                    name="ios-arrow-forward"
                                                    style={styles.labelIcon}
                                                />
                                            </Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => {
                                                this.props.selectedEngineModule(this.state.BuildSpec);
                                                this.props.navigation.navigate("updateEngineBuildSpec_", {
                                                    headerTilte: this.state.BuildSpec.ModuleName
                                                });
                                            }}
                                        >
                                            <Text style={styles.label}>
                                                {parameterList[2]}{" "}
                                                <IonicIcon
                                                    name="ios-arrow-forward"
                                                    style={styles.labelIcon}
                                                />
                                            </Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                             onPress={() => {
                                                this.props.selectedEngineModule(this.state.PowerSection);
                                                this.props.navigation.navigate("updatePowerSection", {
                                                    headerTilte: this.state.PowerSection.ModuleName
                                                });
                                            }}
                                        >
                                            <Text style={styles.label}>
                                                {parameterList[3]}{" "}
                                                <IonicIcon
                                                    name="ios-arrow-forward"
                                                    style={styles.labelIcon}
                                                />
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    )}
            </View>
        );
    }
}

const mapStateToProps = ({ EngineDataReducer }) => {
    const {
        fetchEngineData,
        selectedEngineModule,
        isLoading
    } = EngineDataReducer;
    return { fetchEngineData, selectedEngineModule, isLoading };
};

export default connect(
    mapStateToProps,
    {
        selectedEngineModule,
        DontFetchEngineData,
        GasGeneratorScanFailed,
        PerformanceDataScanFailed,
        PowerSectionScanFailed,
        EngineBuildSpecScanFailed,
        StartLoader,
        StopLoader,
        DifferentESN,
        SameESN,
        OldEngineModel,
        OldEngineSerialNumber,
        OldModuleSerialNumber,
        OldTakeOffDry,
        OldDOTTypeCert,
        OldFAATypeCert,
        UpdateEngineModel,
        UpdateEngineSerialNumber,
        UpdateModuleSerialNumber,
        UpdateTakeOffDry,
        UpdateDOTTypeCert,
        UpdateFAATypeCert,
        OldPowerSectionSerialNumber,
        updatePowerSectionSerialNumber,
        OldengineBuildSpec,
        updateEngineBuildSpec,
        OldNG,
        OldAtSHP,
        OldITTTrim,
        OldOHMS,
        OldCompTrimWeight1,
        OldCompTrimWeight2,
        OldCompTrimWeight3,
        OldCompTrimWeight4,
        OldEngineBuildSpec,
        UpdateNG,
        UpdateAtSHP,
        UpdateITTTrim,
        UpdateOHMS,
        UpdateCompTrimWeight1,
        UpdateCompTrimWeight2,
        UpdateCompTrimWeight3,
        UpdateCompTrimWeight4,
        UpdateEngineBuildSpec,
        EngineSerialNumber
    }
)(withNavigationFocus(EngineDataPlate));

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        flexDirection: "row",
        padding: 10
    },
    engineDataPlates: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#000000",
        paddingTop: 13
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#00A9E0",
        paddingTop: 13
    },
    labelIcon: {
        fontSize: width < 450 ? 16 : 22,
        opacity: 0.23,
        color: "#00A9E0"
    },
    Image: {
        width: width < 450 ? 50 : 80,
        height: width < 450 ? 50 : 80
    }
});
