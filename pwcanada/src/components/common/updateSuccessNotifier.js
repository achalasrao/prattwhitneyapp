import React from 'react';
import { Text, View, StyleSheet, Image, Dimensions } from 'react-native';

let { width, height } = Dimensions.get('window');

const UpdateSuccessNotifier = () => (
    <View style={styles.container}>
        <Image
            style={styles.checkIcon}
            source={require('../../assets/IconPack/Dashboard/check.png')}
            resizeMode='contain'
        />
        <View style={{ paddingLeft: 5 }}>
            <Text style={styles.updatedate}>Values updated</Text>
            <Text style={styles.updatedate}>Successfully</Text>
            <Text style={styles.updatedate}>{new Date().toUTCString()}</Text>
        </View>
    </View>
)

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: '#71FFB1',
        paddingTop: 15,
        justifyContent: 'flex-start',
        paddingBottom: 15,
        paddingLeft: 15
    },
    checkIcon: {
        width: width < 450 ? 30 : 40,
        height: width < 450 ? 30 : 40,
        paddingLeft: 30,
        alignContent: "center"
    },
    updatedate: {
        fontFamily: "Arial",
        fontSize:  width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#1F2A44",
        paddingLeft: 10
    }
})

export { UpdateSuccessNotifier }
