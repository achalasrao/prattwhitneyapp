const INITIAL_STATE = {
    fetchLCFData: false,
    fetchCreepData: false,
    fetchEngineUsageData: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'FetchLCFData':
            return { ...state, fetchLCFData: true };

        case 'DontFetchLCFData':

            return { ...state, fetchLCFData: false };

        case 'FetchCreepData':
            return { ...state, fetchCreepData: true };

        case 'DontFetchCreepData':

            return { ...state, fetchCreepData: false };

        case 'FetchEngineUsageData':
            return { ...state, fetchEngineUsageData: true };

        case 'DontFetchEngineUsageData':

            return { ...state, fetchEngineUsageData: false };
        default:
            return state;
    }
};
