import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { FetchEngineData } from '../../../../../actions';
import { UpdateSuccessNotifier } from '../../../../common';
import { UpdateFailedNotifier } from '../../../../common';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';

class PerformanceDataUpdateReport extends Component {

    state = {
        isModalVisible: false
    }
    constructor(props) {
        super(props)
        this.customBackHandler = this.customBackHandler.bind(this);
        report = this.props.navigation.state.params.param;

    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            title: navigation.getParam('headerTilte'),
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    customBackHandler = () => {
        this.props.navigation.navigate('EngineData')
        this.props.FetchEngineData()
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    renderViewTaskCard = () => (
        <View style={{ height: '70%', backgroundColor: 'white', padding: 10, borderRadius: 5, justifyContent: 'center' }}>

            <View style={{ paddingLeft: 5, paddingTop: 10, paddingRight: 5 }}>
                <Text style={[styles.viewTaskCardText, { textAlign: 'justify', paddingBottom: 10, fontSize: 15 }]}>In order
                to successfully update trims, please complete the following steps in the cockpit:</Text>

                <View style={{ paddingLeft: 10, paddingRight: 10 }}>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>1.</Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#1E2787' }}> PLA </Text>
                            to
                                <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#59585E' }}> IDLE</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>2.</Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#1E2787' }}> RUN/OFF </Text>
                            switch to
                                <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#29AF19' }}> RUN</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>3.</Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#1E2787' }}> RUN/OFF </Text>
                            switch to
                                <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#E82B1D' }}> OFF</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>4.</Text>
                        <Text style={[styles.viewTaskCardText]}>Move the
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#1E2787' }}> PLA </Text>
                            to Max Take-Off within 5 seconds
                            </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>5.</Text>
                        <Text style={[styles.viewTaskCardText]}>Move the
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: '#1E2787' }}> PLA </Text>
                            to Max reverse within 5 seconds
                            </Text>
                    </View>
                </View>

                <Text style={[styles.viewTaskCardText, { textAlign: 'justify', paddingBottom: 10, fontSize: 15, paddingTop: 10 }]}>
                    Please also confirm that the trim on the maintenance page is the same as the one entered from the MicroFAST.
                </Text>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 15 }}>
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({ isModalVisible: false })}
                        style={styles.updateButton}>
                        <Text style={styles.updateLabel}>OK</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    )

    render() {
        let {
            oldNG,
            oldAtSHP,
            oldITTTrim,
            oldOHMS,
            oldCompTrimWeight1,
            oldCompTrimWeight2,
            oldCompTrimWeight3,
            oldCompTrimWeight4,
            oldEngineBuildSpec,
            NG,
            AtSHP,
            ITTTrim,
            OHMS,
            CompTrimWeight1,
            CompTrimWeight2,
            CompTrimWeight3,
            CompTrimWeight4,
            EngineBuildSpec,
        } = this.props

        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View>
                        {report.includes("Failed") ?
                            <View>
                                <UpdateFailedNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text style={styles.LRUnameTextStyle}>{this.props.selectedEngineModule.ModuleName} Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View> :
                            <View>
                                <UpdateSuccessNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text style={styles.LRUnameTextStyle}>{this.props.selectedEngineModule.ModuleName} Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>

                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>


                        <View style={styles.rectangleConatinerStyle}>
                            {
                                oldNG !== NG ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>NG</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[0].includes("success") ? "green" : "red" }]}>  {NG}</Text>
                                    </View> :
                                    <View />
                            }
                            {
                                oldAtSHP !== AtSHP ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>At SHP</Text>
                                        < Text style={[styles.rectangleContentTextStyle, { color: report[1].includes("success") ? "green" : "red" }]}>  {AtSHP}</Text>
                                    </View> : <View />
                            }
                            {
                                oldITTTrim !== ITTTrim ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>ITT Trim</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[2].includes("success") ? "green" : "red" }]}>  {ITTTrim}</Text>
                                    </View> : <View />
                            }
                            {
                                oldOHMS !== OHMS ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>OHText style</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[0].includes("success") ? "green" : "red" }]}>  {OHMS}</Text>
                                    </View> : <View />
                            }
                            {
                                oldCompTrimWeight1 !== CompTrimWeight1 ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Comp Trim Weight 1</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[4].includes("success") ? "green" : "red" }]}> {CompTrimWeight1}</Text>
                                    </View> : <View />
                            }
                            {
                                oldCompTrimWeight2 !== CompTrimWeight2 ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Comp Trim Weight 2</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[5].includes("success") ? "green" : "red" }]}> {CompTrimWeight2}</Text>
                                    </View> : <View />
                            }
                            {
                                oldCompTrimWeight3 !== CompTrimWeight3 ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Comp Trim Weight 3</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[6].includes("success") ? "green" : "red" }]}>  {CompTrimWeight3}</Text>
                                    </View> : <View />
                            }
                            {
                                oldCompTrimWeight4 !== CompTrimWeight4 ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Comp Trim Weight 4</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[7].includes("success") ? "green" : "red" }]}> {CompTrimWeight4}</Text>
                                    </View> : <View />
                            }
                            {
                                oldEngineBuildSpec !== EngineBuildSpec ?
                                    <View style={styles.childRectangleStyle}>
                                        <Text style={styles.rectangleTitleTextStyle}>Engine Build Spec</Text>
                                        <Text style={[styles.rectangleContentTextStyle, { color: report[8].includes("success") ? "green" : "red" }]}> {EngineBuildSpec}</Text>
                                    </View> : <View />
                            }
                        </View>

                        <TouchableOpacity
                            style={styles.backToDashboardButton}
                            onPress={() => {
                                this.props.navigation.navigate('Dashboard')
                                this.props.FetchEngineData()
                            }}
                        >
                            <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
                <Modal
                    style={styles.modalContainer}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={() => this.setState({ isModalVisible: false })}
                >{this.renderViewTaskCard()}</Modal>
            </View>
        )
    }
}

const mapStateToProps = ({ PerformanceDataReducer, EngineDataReducer }) => {
    const {
        oldNG,
        oldAtSHP,
        oldITTTrim,
        oldOHMS,
        oldCompTrimWeight1,
        oldCompTrimWeight2,
        oldCompTrimWeight3,
        oldCompTrimWeight4,
        oldEngineBuildSpec,
        NG,
        AtSHP,
        ITTTrim,
        OHMS,
        CompTrimWeight1,
        CompTrimWeight2,
        CompTrimWeight3,
        CompTrimWeight4,
        EngineBuildSpec,
    } = PerformanceDataReducer
    const { selectedEngineModule } = EngineDataReducer

    return {
        selectedEngineModule,
        oldNG,
        oldAtSHP,
        oldITTTrim,
        oldOHMS,
        oldCompTrimWeight1,
        oldCompTrimWeight2,
        oldCompTrimWeight3,
        oldCompTrimWeight4,
        oldEngineBuildSpec,
        NG,
        AtSHP,
        ITTTrim,
        OHMS,
        CompTrimWeight1,
        CompTrimWeight2,
        CompTrimWeight3,
        CompTrimWeight4,
        EngineBuildSpec,
    };
};

export default connect(mapStateToProps, { FetchEngineData })(PerformanceDataUpdateReport);

const styles = StyleSheet.create({

    LRUnameTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#000000",
        paddingTop: 12
    },
    lastUpdateTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#666666"
    },
    rectangleConatinerStyle: {
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e5e5e5",
        marginTop: 20,
        paddingBottom: 20,
        paddingTop: 20
    },
    childRectangleStyle: {
        paddingTop: 10,
        paddingLeft: 16
    },
    rectangleTitleTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333"
    },
    rectangleContentTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#666666",
        paddingTop: 16
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: 16,
        color: "#0033ab"
    },
    backToDashboard: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#0033ab"
    },
    approveInStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0.5,
        color: "#333333"
    },
    viewTaskCard: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0.5,
        color: "rgb(48,89,187)"
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: 'center',
    },
    viewTaskCardText: {
        fontFamily: "HelveticaNeue",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0,
        color: "#333333"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff",
    },
})
