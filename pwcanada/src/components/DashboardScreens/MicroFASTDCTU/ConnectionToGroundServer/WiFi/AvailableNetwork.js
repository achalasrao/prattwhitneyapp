import React, { Component } from 'react';
import { View, Image, StyleSheet, ActivityIndicator, TouchableOpacity, Dimensions } from 'react-native';
import { Footer, FooterTab, Text, Button, Content, } from 'native-base';
import { connect } from 'react-redux'
import { availableWifiList, onAddtoConfiguration, configurationFoccussed } from '../../../../../actions';
import axios from 'axios';
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/dist/FontAwesome';

var index = 0
let { width, height } = Dimensions.get('window');

class AvailableNetwork extends Component {
    constructor(props) {
        super(props)

        this.state = {
            AvailableDataSource: [],
            loading: false,
            modalVisible: false,
            loaderVisible: false,
            refreshing: false,
            isEmpty: false,
            fetchToXmlCount: 0,
        }
    }

    UNSAFE_componentWillMount = async () => {
        this.setState({ loading: true })
    }

    _onRefresh = async () => {
        this.setState({ loading: true, fetchToXmlCount: 0 })
        this.fetchingData()
    }

    componentDidMount = async () => {
        await this.fetchingData()
    }

    addtoConfiguration = async (item) => {

        this.props.onAddtoConfiguration(item);
        this.props.configurationFoccussed(true);
        this.props.navigation.navigate('AddNetwork');
    }

    fetchingData = async () => {
        let data = [];
        this.setState({ fetchToXmlCount: ++this.state.fetchToXmlCount })

        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "wifi_scan",
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            let responseJson = await response;
            var lines = JSON.stringify(responseJson.data[0].value);
            var notFound = true;
            var splitwise = await lines.split("\\n");
            for (var line = 0; line < splitwise.length; line++) {

                lines = splitwise[line].split(" ");
                if (lines[0] != "bssid" && notFound) {
                    continue;
                }

                notFound = false;
                if (lines[0] == "bssid" && !notFound) continue;

                if (splitwise[line] == '"') continue;

                var tabs = await splitwise[line].split('\\t');
                var jsondata = [];

                jsondata['bssid'] = tabs[0];
                jsondata['frequency'] = tabs[1];
                jsondata['signal_strength'] = tabs[2];
                jsondata['security'] = tabs[3];
                jsondata['ssid'] = tabs[4];

                var filtering = jsondata['ssid'].replace(/[  \\x00 ]/g, "");
                jsondata['ssid'] = filtering;

                if (jsondata['ssid'] != "") {

                    data.push(jsondata);

                }
            }

            if (data.length == 0 && this.state.fetchToXmlCount < 3) {
                this.fetchingData()
            } else if (this.state.fetchToXmlCount >= 3 && data.length == 0) {

                this.setState({ loading: false, isEmpty: true })
            } else if (data.length > 0) {
                this.setState({ AvailableDataSource: data, loading: false, isEmpty: false });
            }

        } catch (error) {

            if (data.length == 0 && this.state.fetchToXmlCount < 3) {
                this.fetchingData()
            } else if (this.state.fetchToXmlCount >= 3 && data.length == 0) {

                this.setState({ loading: false, isEmpty: true })
            }
        }
    }

    renderRow(item) {

        let range = item.signal_strength;
        let src = '';

        if (range < -1 && range > -59) {
            src = require('../../../../../assets/icons/goodlevel.png');

        } else if (range < -60 && range > -89) {
            src = require('../../../../../assets/icons/mediumlevel.png');

        } else if (range < -90) {
            src = require('../../../../../assets/icons/lowlevel.png');

        }

        return (
            <View style={{ width: '100%', height: 60, flexDirection: 'row', backgroundColor: '#fff', marginBottom: 1.5 }}>
                <View style={{ width: '55%', flexDirection: 'column', paddingLeft: 10, justifyContent: 'center', }}>
                    <View style={{
                        height: '50%',
                        justifyContent: 'center',
                        paddingTop: 4
                    }}>
                        <Text style={{
                            fontSize: 16,
                            color: 'rgb(51,51,51)',
                            fontFamily: 'Arial',
                            textAlignVertical: 'bottom'
                        }}>{item.ssid}</Text>
                    </View>
                    <View style={{ height: '50%', justifyContent: 'center', paddingBottom: 4 }}>
                        <Text style={{ fontSize: 14, color: 'rgb(102,102,102)', fontFamily: 'Arial', textAlignVertical: 'top' }}>{item.security}</Text>
                    </View>
                </View>

                <View style={{ width: '15%', justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{
                        flex: 1,
                        alignSelf: 'stretch',
                        width: 50,
                        height: 50
                    }}
                        source={src}
                        resizeMode='contain' />
                </View>

                <View style={{ width: '15%', justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity style={styles.circularbtn} onPress={() => this.addtoConfiguration(item)}>
                        <Icon name="plus" size={25} style={styles.checkIcon} />
                    </TouchableOpacity>
                </View>

                <View style={{ width: '15%', justifyContent: 'center', alignItems: 'center', paddingRight: 1 }}>
                    <TouchableOpacity onPress={() => this.infoAvailableList(true, item)}>
                        <Image style={{
                            flex: 1,
                            alignSelf: 'stretch',
                            width: 34,
                            height: 34
                        }}
                            source={require('../../../../../assets/IconPack/ConfiguredNetworks/btnInfo.png')}
                            resizeMode='contain' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    async infoAvailableList(visible, item) {

        this.setState({ modalVisible: visible, selectedItem: item });
        this.props.availableWifiList(item);
    }

    renderView() {
        if (this.state.isEmpty == true) {
            return (
                <View style={{ backgroundColor: 'rgba(0, 0, 0, 0.1)' }}>
                    <Text style={{ textAlign: "center", backgroundColor: 'rgba(0, 0, 0, 0.1)' }}>No Networks Available !</Text>
                </View>
            )
        } else {
            return (
                this.state.AvailableDataSource.map((tabs) =>
                    this.renderRow(tabs)
                )
            )
        }
    }

    render() {
        return (
            <View style={{ flex: 1, marginTop: 10 }}>
                {
                    this.state.loading ?
                        <View style={[styles.container]}>
                            <ActivityIndicator size="large" color="#5B6376" />
                        </View> :
                        <Content>
                            <View style={{ backgroundColor: '#edeff0', }} >
                                {this.renderView()}
                            </View>
                        </Content>
                }

                <Modal
                    isVisible={this.state.modalVisible}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}
                >
                    <View style={styles.modalContent}>
                        <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', paddingBottom: 10, width: '100%' }}>
                            <Text style={styles.infoListSSID}>{this.props.avawifilist.ssid}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>SSID :</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.avawifilist.ssid}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Frequency :</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.avawifilist.frequency}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Security :</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.avawifilist.security}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>BSSID :</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.avawifilist.bssid}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Signal Strength :</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.avawifilist.signal_strength}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Flags :</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.avawifilist.flags}</Text>
                            </View>
                        </View>

                        <View>

                            <Button style={styles.closebtn} onPress={() => this.infoAvailableList(!this.state.modalVisible, this.state.selectedItem)}><Text style={styles.txtbtnclose}>Close</Text></Button>
                        </View>
                    </View>
                </Modal>
                <Footer>
                    <FooterTab>
                        <View style={styles.checkbox}>
                            <View style={[styles.checkbox, { width: '100%' }]}>
                                <Text style={styles.refresh} onPress={() => this._onRefresh()} >Refresh</Text>
                            </View>
                        </View>
                    </FooterTab>
                </Footer>
            </View>
        )
    }
}

const mapStateToProp = ({ footer }) => {
    const { ConfigureApFocussed, AddConfigurationFocussed, AvailableApFocussed, avawifilist, AddToConf } = footer;
    return { ConfigureApFocussed, AddConfigurationFocussed, AvailableApFocussed, avawifilist, AddToConf };
};

export default connect(mapStateToProp, { availableWifiList, onAddtoConfiguration, configurationFoccussed })(AvailableNetwork);

const styles = StyleSheet.create({
    txtbtnclose: {
        color: 'white'
    },
    closebtn: {
        backgroundColor: '#00A9E0',
        marginTop: 20
    },
    modalContent: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    circularbtn: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 32,
        height: 32,
        backgroundColor: '#00A9E0',
        borderRadius: 100
    },
    listConatiner: {
        backgroundColor: '#000',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginRight: 2,
        marginLeft: 2
    },
    headerContainer: {
        flexDirection: 'row',
        flex: 1,
        borderBottomColor: '#000',
        backgroundColor: '#F8F8F8',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2
    },
    headerTitleStyle: {
        color: '#ffcc33',
        fontFamily: 'Arial'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#edeff0'
    },
    dialogContentView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    navigationBar: {
        borderBottomColor: '#b5b5b5',
        borderBottomWidth: 0.5,
        backgroundColor: '#ffffff'
    },
    navigationTitle: {
        padding: 10
    },
    navigationButton: {
        padding: 10
    },
    navigationLeftButton: {
        paddingLeft: 20,
        paddingRight: 40
    },
    navigator: {
        flex: 1
    },
    header: {
        backgroundColor: '#333333'
    },
    footer: {
        backgroundColor: '#333333'
    },
    title: {
        color: '#696969',
        fontWeight: 'bold',
        fontSize: 18,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        paddingTop: 20
    },
    headingConatiner: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    headerTxt: {
        fontSize: 18,
        fontFamily: 'Arial',
        color: '#0081c6'
    },
    txtContainer: {
        flexDirection: 'row',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 15,
        justifyContent: 'space-around'
    },
    labelstyle: {
        fontSize: 18,
        paddingLeft: 20,
        textAlign: 'left',
        justifyContent: 'center',
        alignSelf: 'center',
        fontFamily: 'Arial'
    },
    textArea: {
        color: '#000',
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: 18,
        lineHeight: 23,
        flex: 1
    },
    inputTextArea: {
        color: '#000',
        paddingRight: 10,
        paddingLeft: 40,
        fontSize: 18,
        lineHeight: 23,
        flex: 1,
        alignContent: 'flex-start'
    },
    inputdropdown: {
        color: '#000',
        paddingRight: 10,
        paddingLeft: 40,
        fontSize: 18,
        lineHeight: 23,
        flex: 1,
        alignContent: 'flex-start',
        width: undefined
    },
    iconStyle: {
        color: '#ADADAD'
    },
    focusediconStyle: {
        color: '#ffcc33'
    },
    focusedicontextStyle: {
        color: '#ffcc33'
    },
    buttonstyle: {
        backgroundColor: '#ffcc33',
        alignSelf: 'stretch'
    },
    buttonContainer: {
        alignSelf: 'stretch',
        justifyContent: 'center',
        flexDirection: 'row',
        elevation: 1,
        paddingLeft: 80,
        paddingRight: 80,
        marginTop: 25,
        alignSelf: 'stretch'
    },
    buttonTextStyle: {
        color: '#333333',
        fontSize: 20,
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white'
    },
    checkbox: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 10,
        paddingTop: 10,
    },
    refresh: {
        height: 22,
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        color: "#00A9E0",
        justifyContent: 'center',
        alignContent: 'center'
    },
    checkIcon: {
        height: 22,
        textAlign: 'right',
        color: "#ffffff"
    },
    infoListSSID: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.25,
        color: "rgb(0,0,0)",
        textAlign: 'left'
    },
    inofListLabel: {
        fontFamily: "Arial",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.25,
        color: "rgb(102,102,102)",
        textAlign: 'left'
    },
    infoListValue: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.25,
        color: "rgb(51,51,51)",
        textAlign: 'left'
    }
});
