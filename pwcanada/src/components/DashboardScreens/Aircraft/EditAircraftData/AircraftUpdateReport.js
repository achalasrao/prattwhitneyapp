import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import {
    Text,
    Content,
    Card,
} from 'native-base';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import { _, keys, values } from 'underscore';
import { connect } from 'react-redux';
import { UpdateSuccessNotifier } from '../../../common';
import { FetchAircraftData } from '../../../../actions';

let { width, height } = Dimensions.get('window');

class AircraftUpdateReport extends Component {
    constructor(props) {
        super(props)

        this.customBackHandler = this.customBackHandler.bind(this);
        report = this.props.navigation.state.params.param;

        this.state = {
            isVisible: false,
            loading: false,
            writeToXmlCount: 0,
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    customBackHandler = () => {
        this.props.navigation.navigate('AircraftUpdate')
        this.props.FetchAircraftData()
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    render() {
        let {
            OldAircraft_SN,
            OldAircraft_Type,
            OldFleet_Ident,
            OldAircraft_Number,
            OldTail_Number,
            OldAirline_Operator,
            OldAircraft_Owner,
            NewAircraft_SN,
            NewAircraft_Type,
            NewFleet_Ident,
            NewAircraft_Number,
            NewTail_Number,
            NewAirline_Operator,
            NewAircraft_Owner,
        } = this.props

        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <Content>
                        <View>
                            {
                                report.includes("Failed") ?
                                    <View>
                                        <UpdateFailedNotifier />

                                        <View style={{ paddingLeft: 18 }}>
                                            <Text style={styles.TextStyle}>Aircraft Data Values</Text>
                                            <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                        </View>
                                    </View> :
                                    <View>
                                        <UpdateSuccessNotifier />

                                        <View style={{ paddingLeft: 18 }}>
                                            <Text style={styles.TextStyle}>Aircraft Data Values</Text>
                                            <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                        </View>
                                    </View>
                            }
                        </View>

                        <Card style={styles.card} >
                            <View
                                style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingTop: 10,
                                    backgroundColor: '#FFFFFF'
                                }}>
                            </View>
                            <View >
                                <View style={{ paddingLeft: 8 }}>
                                    {
                                        (OldAircraft_SN !== NewAircraft_SN) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Aircraft Serial Number</Text>
                                                </View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewAircraft_SN}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                    {
                                        (OldAircraft_Number !== NewAircraft_Number) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Aircraft Number</Text>
                                                </View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewAircraft_Number}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                    {
                                        (OldFleet_Ident !== NewFleet_Ident) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Fleet Ident</Text></View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewFleet_Ident}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                    {
                                        (OldAircraft_Owner !== NewAircraft_Owner) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Aircraft Owner</Text>
                                                </View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewAircraft_Owner}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                    {
                                        (OldAirline_Operator !== NewAirline_Operator) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Airline Operator</Text>
                                                </View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewAirline_Operator}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                    {
                                        (OldTail_Number !== NewTail_Number) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Tail Number</Text>
                                                </View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewTail_Number}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                    {
                                        (OldAircraft_Type !== NewAircraft_Type) ?
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ width: '60%', paddingLeft: 10, paddingBottom: 10 }}>
                                                    <Text style={[styles.label1]}>Aircraft Type</Text>
                                                </View>
                                                <View style={{ width: '40%', paddingBottom: 10 }}>
                                                    <Text style={[styles.label2, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}>{NewAircraft_Type}</Text>
                                                </View>
                                            </View> :
                                            <View />
                                    }
                                </View>
                            </View>
                        </Card>
                        <TouchableOpacity
                            style={styles.backToDashboardButton}
                            onPress={() => {
                                this.props.navigation.navigate('Dashboard')
                            }}>
                            <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                        </TouchableOpacity>
                    </Content>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = ({ AircraftUpdateReducer }) => {
    const { OldAircraft_SN,
        OldAircraft_Type,
        OldFleet_Ident,
        OldAircraft_Number,
        OldTail_Number,
        OldAirline_Operator,
        OldAircraft_Owner,
        NewAircraft_SN,
        NewAircraft_Type,
        NewFleet_Ident,
        NewAircraft_Number,
        NewTail_Number,
        NewAirline_Operator,
        NewAircraft_Owner
    } = AircraftUpdateReducer

    return {
        OldAircraft_SN,
        OldAircraft_Type,
        OldFleet_Ident,
        OldAircraft_Number,
        OldTail_Number,
        OldAirline_Operator,
        OldAircraft_Owner,
        NewAircraft_SN,
        NewAircraft_Type,
        NewFleet_Ident,
        NewAircraft_Number,
        NewTail_Number,
        NewAirline_Operator,
        NewAircraft_Owner
    };
};

export default connect(mapStateToProps, { FetchAircraftData })(AircraftUpdateReport)

const styles = StyleSheet.create({

    TextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 12
    },
    lastUpdateTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#848A98",
    },
    card: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8"
    },
    label1: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 5,
        textAlign: 'left'
    },
    label2: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 5,
        textAlign: 'left'
    },
    backToDashboardButton: {
        marginTop: 10,
        marginLeft: 15
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    }
})
