import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, Text, ScrollView, Dimensions } from "react-native";
import { connect } from "react-redux";
import { UpdateSuccessNotifier } from "../../../common";
import IonicIcon from "react-native-vector-icons/Ionicons";

let { width, height } = Dimensions.get('window');

class LRUupdateReport extends Component {

    constructor(props) {
        super(props)
        this.customBackHandler = this.customBackHandler.bind(this);
        report = this.props.navigation.state.params.param;
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            title: navigation.getParam("headerTilte"),
            headerLeft: (
                <TouchableOpacity
                    onPress={() => params.backHandler()}
                    style={{
                        width: "100%",
                        flexDirection: "row",
                        justifyContent: "center"
                    }}
                >
                    <IonicIcon
                        name="ios-arrow-back"
                        style={{ fontSize: 35, color: "#000000", paddingLeft: 8 }}
                    />
                    <View style={{ justifyContent: "center" }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    customBackHandler = () => {
        this.props.navigation.navigate('LRUList')
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    render() {
        let previousSN = this.props.selectedLRU.SN,
            previousPN = this.props.selectedLRU.PN,
            newSN = this.props.SNValue,
            newPN = this.props.PNValue;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View>
                        {
                            report.includes("Failed") ?
                                <View>
                                    <UpdateFailedNotifier />
                                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                        <Text style={styles.LRUnameTextStyle}>
                                            {this.props.selectedLRU.LRUName} Values
                                    </Text>
                                        <Text style={styles.lastUpdateTextStyle}>
                                            Last updated {new Date().toUTCString()}
                                        </Text>
                                    </View>
                                </View> :
                                <View>
                                    <UpdateSuccessNotifier />
                                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                        <Text style={styles.LRUnameTextStyle}>
                                            {this.props.selectedLRU.LRUName} Values
                                </Text>
                                        <Text style={styles.lastUpdateTextStyle}>
                                            Last updated {new Date().toUTCString()}
                                        </Text>
                                    </View>
                                </View>
                        }
                    </View>

                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                        <View style={styles.rectangleConatinerStyle}>
                            {previousPN !== newPN ? (
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>Part Number</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}> {newPN}</Text>
                                </View>
                            ) : (
                                    <View />
                                )}
                            {previousSN !== newSN ? (
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>Serial Number</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report.includes("success") ? "#34CB81" : "#EB1616" }]}> {newSN}</Text>
                                </View>
                            ) : (
                                    <View />
                                )}
                        </View>

                        <TouchableOpacity
                            style={styles.backToDashboardButton}
                            onPress={() => this.props.navigation.navigate("Dashboard")}
                        >
                            <Text style={styles.backToDashboard}>
                                <IonicIcon name="ios-arrow-back" style={styles.leftIcon} /> Back
                                to Dashboard
                        </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = ({ LRUreducer }) => {
    const { selectedLRU, SNValue, PNValue } = LRUreducer;
    return { selectedLRU, SNValue, PNValue };
};

export default connect(
    mapStateToProps,
    null
)(LRUupdateReport);

const styles = StyleSheet.create({

    LRUnameTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 24 : 30,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 12
    },
    lastUpdateTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#1F2A44"
    },
    rectangleConatinerStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 20,
        paddingBottom: 20,
        paddingTop: 20
    },
    childRectangleStyle: {
        paddingTop: 10,
        paddingLeft: 16
    },
    rectangleTitleTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44"
    },
    rectangleContentTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44",
        paddingTop: 16
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    }
});
