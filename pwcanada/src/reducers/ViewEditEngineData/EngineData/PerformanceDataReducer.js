const INITIAL_STATE = {
    oldNG: '',
    oldAtSHP: '',
    oldITTTrim: '',
    oldOHMS: '',
    oldCompTrimWeight1: '',
    oldCompTrimWeight2: '',
    oldCompTrimWeight3: '',
    oldCompTrimWeight4: '',
    oldEngineBuildSpec: '',
    NG: '',
    AtSHP: '',
    ITTTrim: '',
    OHMS: '',
    CompTrimWeight1: '',
    CompTrimWeight2: '',
    CompTrimWeight3: '',
    CompTrimWeight4: '',
    EngineBuildSpec: '',
    performanceDataUpdate: false,
    scannedPerformanceData: false,
    updatedPerformanceData: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'OldNG':

            return { ...state, oldNG: action.payload };

        case 'OldAtSHP':

            return { ...state, oldAtSHP: action.payload };

        case 'OldITTTrim':

            return { ...state, oldITTTrim: action.payload };

        case 'OldOHMS':

            return { ...state, oldOHMS: action.payload };

        case 'OldCompTrimWeight1':

            return { ...state, oldCompTrimWeight1: action.payload };

        case 'OldCompTrimWeight2':

            return { ...state, oldCompTrimWeight2: action.payload };

        case 'OldCompTrimWeight3':

            return { ...state, oldCompTrimWeight3: action.payload };

        case 'OldCompTrimWeight4':

            return { ...state, oldCompTrimWeight4: action.payload };

        case 'OldEngineBuildSpec':

            return { ...state, oldEngineBuildSpec: action.payload };

        case 'UpdateNG':

            return { ...state, NG: action.payload };

        case 'UpdateAtSHP':

            return { ...state, AtSHP: action.payload };

        case 'UpdateITTTrim':

            return { ...state, ITTTrim: action.payload };

        case 'UpdateOHMS':

            return { ...state, OHMS: action.payload };

        case 'UpdateCompTrimWeight1':

            return { ...state, CompTrimWeight1: action.payload };

        case 'UpdateCompTrimWeight2':

            return { ...state, CompTrimWeight2: action.payload };

        case 'UpdateCompTrimWeight3':

            return { ...state, CompTrimWeight3: action.payload };

        case 'UpdateCompTrimWeight4':

            return { ...state, CompTrimWeight4: action.payload };

        case 'UpdateEngineBuildSpec':

            return { ...state, EngineBuildSpec: action.payload };

        case 'UpdatePerformanceData':

            return { ...state, performanceDataUpdate: true };

        case 'DontUpdatePerformanceData':

            return { ...state, performanceDataUpdate: false };

        case 'PerformanceDataUpdateSuccess':

            return { ...state, updatedPerformanceData: true };

        case 'PerformanceDataUpdateFailed':

            return { ...state, updatedPerformanceData: false };

        case 'PerformanceDataScanSuccess':

            return { ...state, scannedPerformanceData: true };

        case 'PerformanceDataScanFailed':

            return { ...state, scannedPerformanceData: false };

        default:
            return state;
    }
}
