import React, { Component } from 'react';
import {
    Text,
    ScrollView,
    View,
    ActivityIndicator,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import {
    VictoryChart,
    VictoryLine,
    VictoryAxis,
    VictoryTheme,
} from "victory-native";
import axios from 'axios';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import SelectMultiple from 'react-native-select-multiple';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import OctIcon from 'react-native-vector-icons/Octicons';

let TransData = [], ParamData = [], ChartData = [], myTimeOut
let { width, height } = Dimensions.get('window');

class TraceChannelA extends Component {

    state = {
        selectedParam: [],
        dataSource: [{ label: 'Loading...', value: 'Loading...' }],
        isModalVisible: false,
        selectExceeded: false,
        isLoading: false,
        timeOut: false,
        initiateTraceDataFetchCount: 0,
        traceDataFetchcount: 0,
        fetchRTDCOMMON_RTD_EXCEEDCount: 0,
        loadingButton: false,
    }

    UNSAFE_componentWillMount = () => {

        // fetch the Trace values for the Exceedance/Event item
        ChartData = []
        this.initiateTraceDataFetch()
        myTimeOut = setTimeout(() => { this.setState({ timeOut: true }) }, 30000)
    }

    initiateTraceDataFetch = async () => {
        this.setState({ initiateTraceDataFetchCount: ++this.state.initiateTraceDataFetchCount })
        this.setState({ isLoading: true })

        let { Channel } = this.props.enginedata
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": `--get=TRANS --ch=${Channel}`
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            let responseJson = await response.data;
            if (responseJson.length == 0) {
                this.traceDataFetch()
            } else {
                this.initiateTraceDataFetch()
            }
        }
        catch (error) {
            if (this.state.initiateTraceDataFetchCount < 2) {
                this.initiateTraceDataFetch()
            }
        }
    }

    traceDataFetch = async () => {
        this.setState({ traceDataFetchcount: ++this.state.traceDataFetch })

        let { Channel, Code, RunTime, Type } = this.props.enginedata

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": `--code=${Code} --type=${Type} --ch=${Channel} --date_s=\"${RunTime}\" `
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            response = await response.data;

            TransData = response.slice(1, - 1)

            if (TransData.length > 0) {
                ParamData = []
                this.fetchRTDCOMMON_RTD_EXCEED()
            } else {
                this.traceDataFetch()
            }
        }
        catch (error) {
            if (this.state.traceDataFetchcount < 2) {
                this.traceDataFetch()
            }
        }
    }

    fetchRTDCOMMON_RTD_EXCEED = async () => {
        ParamData = []

        let count = 0
        let { Type, Channel } = this.props.enginedata
        let arg1 = Type === 'EXCEED' ? "--rtd=RTD_EXCEED" : "--rtd=RTD_COMMON"
        try {
            this.setState({ fetchRTDCOMMON_RTD_EXCEEDCount: ++this.state.fetchRTDCOMMON_RTD_EXCEEDCount })

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": `--rtd=RTD_COMMON --ch=${Channel}`
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            responseJson = await response.data;

            if (responseJson.length == 0) {
                this.fetchRTDCOMMON_RTD_EXCEED()
            } else {
                responseJson.map(item => {
                    ParamData.push({
                        'id': count,
                        'Parameter': item.value.trim().split(',')[1],
                        'Unit': item.value.trim().split(',')[2],
                        'label': item.value.trim().split(',')[1],
                        'value': count
                    })
                    count++
                })

                const SortedData = ParamData.sort((a, b) => a.Parameter.localeCompare(b.Parameter))

                this.setState({ dataSource: SortedData, isLoading: false })

                clearTimeout(myTimeOut)
            }
        }
        catch (error) {

            if (this.state.fetchRTDCOMMON_RTD_EXCEEDCount < 6) {
                this.fetchRTDCOMMON_RTD_EXCEED()
            }
        }
    }

    parseTransdata = () => {
        ChartData = []
        let temp = [], ParamDetails = []
        let selected = this.state.selectedParam
        let dataSet = this.state.dataSource
        selected.map(x => {
            ID = x.value
            dataSet.forEach((y) => {
                if (y.id == ID) {
                    ParaName = y.Parameter
                    ParaUnit = y.Unit
                }
            })
            TransData.forEach((item) => {
                var splitwise = item.value.split(",");
                let stepTime = splitwise[1]
                let sliced = splitwise.slice(3)
                let paramValue = sliced[ID]
                if (String(paramValue).includes('.')) {
                    paramValue = String(paramValue).split('.')[0]
                }
                temp.push({
                    'x': stepTime,
                    'y': paramValue,
                    'id': ID,
                    'ParamName': ParaName,
                    'Unit': ParaUnit
                })
            })
            ParamDetails = temp.filter((x) => {
                if (x.id == ID) {
                    return (x)
                }
            })
            this.setState({ isModalVisible: false, loadingButton: false })
            ChartData.push(ParamDetails)
        })

    }

    onSelectionsChange = (selectedParam) => {
        this.setState({ selectedParam })
        if (selectedParam.length > 6) {
            this.setState({ selectExceeded: true })
        } else {
            this.setState({ selectExceeded: false })
            // this.parseTransdata()
        }
    }

    renderLabel = (label, style) => {

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ marginLeft: 10 }}>
                    <Text style={style}>{label}</Text>
                </View>
            </View>
        )
    }

    addTrace = () => {
        this.setState({ isModalVisible: true, loadingButton: false })
        this.fetchRTDCOMMON_RTD_EXCEED()
    }

    modalClose = () => {
        this.setState({ isModalVisible: false })
        this.parseTransdata()
    }

    render() {

        return (
            <View style={{ flex: 1 }}>

                <View style={styles.rectangle1}>
                    <View style={{ borderBottomColor: '#E3E5E8', borderBottomWidth: 0.8, paddingLeft: 10 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.labelText}>Description</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.valueText}>{this.props.enginedata.Description}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.labelText}>Code</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.valueText}>{this.props.enginedata.Code}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                            <View style={{ width: '40%' }}>
                                <Text style={styles.labelText}>Logged</Text>
                            </View>
                            <View style={{ width: '60%' }}>
                                <Text style={styles.valueText}>{this.props.enginedata.RunTime}</Text>
                            </View>
                        </View>

                    </View>
                    {(this.props.enginedata.Type === "EXCEED") ?
                        <View style={{ paddingLeft: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Peak Value</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Peak}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Duration</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Duration}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Area</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Zone}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Channel</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Channel}</Text>
                                </View>
                            </View>
                        </View>
                        :
                        <View style={{ paddingLeft: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 4 }}>
                                <View style={{ width: '40%' }}>
                                    <Text style={styles.labelText}>Channel</Text>
                                </View>
                                <View style={{ width: '60%' }}>
                                    <Text style={styles.valueText}>{this.props.enginedata.Channel}</Text>
                                </View>
                            </View>
                        </View>
                    }
                </View>
                <ScrollView>
                    {ChartData.map(item => {
                        if (item[0].ParamName === 'DATE_TIME') {
                            return (
                                <View style={{ marginTop: 2, backgroundColor: '#FFFFFF', paddingLeft: 10 }}>
                                    <Text style={styles.chartTile}>{item[0].ParamName}</Text>
                                    <VictoryChart
                                        theme={VictoryTheme.material}
                                        // width={375}
                                        height={175}
                                        scale={{ x: "time" }}
                                        responsive={true}
                                        gutter={10}
                                    >
                                        <VictoryAxis
                                            fixLabelOverlap={true}
                                            theme={VictoryTheme.material}
                                            tickCount={5}
                                            tickFormat={(t) => `${t}s`}
                                            fixLabelOverlap={true}
                                            style={{
                                                axis: { stroke: "#E3E5E8" },
                                                axisLabel: { fontSize: 13, padding: 25 },
                                                tickLabels: { fontSize: 12, padding: 5 },
                                                grid: { stroke: "#E3E5E8" }
                                            }}
                                        />
                                        <VictoryAxis dependentAxis
                                            tickCount={5}
                                            label={item[0].Unit}
                                            fixLabelOverlap={true}
                                            theme={VictoryTheme.material}
                                            style={{
                                                axis: { stroke: "#E3E5E8" },
                                                axisLabel: { fontSize: 13, padding: 38 },
                                                tickLabels: { fontSize: 5, padding: 3 },
                                            }}
                                        />
                                        <VictoryLine
                                            style={{
                                                data: { stroke: "#00A9E0" }
                                            }}
                                            data={item}
                                            x="x"
                                            y="y"
                                        />
                                    </VictoryChart>
                                </View>
                            )
                        } else if (item[0].ParamName === 'WFSEL') {
                            return (
                                <View style={{ marginTop: 2, backgroundColor: '#FFFFFF', paddingLeft: 10 }}>
                                    <Text style={styles.chartTile}>{item[0].ParamName}</Text>
                                    <VictoryChart
                                        theme={VictoryTheme.material}
                                        // width={375}
                                        height={175}
                                        scale={{ x: "time" }}
                                        responsive={true}
                                        gutter={10}
                                    >
                                        <VictoryAxis
                                            fixLabelOverlap={true}
                                            theme={VictoryTheme.material}
                                            tickCount={5}
                                            tickFormat={(t) => `${t}s`}
                                            fixLabelOverlap={true}
                                            style={{
                                                axis: { stroke: "#E3E5E8" },
                                                tickLabels: { fontSize: 12, padding: 5 },
                                                grid: { stroke: "#E3E5E8" }
                                            }}
                                        />
                                        <VictoryAxis dependentAxis
                                            tickCount={5}
                                            label={item[0].Unit}
                                            fixLabelOverlap={true}
                                            theme={VictoryTheme.material}
                                            style={{
                                                axis: { stroke: "#E3E5E8" },
                                                axisLabel: { fontSize: 13, padding: 38 },
                                                tickLabels: { fontSize: 5, padding: 3 },
                                            }}
                                        />
                                        <VictoryLine
                                            style={{
                                                data: { stroke: "#00A9E0" }
                                            }}
                                            data={item}
                                            x="x"
                                            y="y"
                                        />
                                    </VictoryChart>
                                </View>
                            )
                        } else {
                            return (
                                <View style={{ marginTop: 2, backgroundColor: '#FFFFFF', paddingLeft: 10 }}>
                                    <Text style={styles.chartTile}>{item[0].ParamName}</Text>
                                    <VictoryChart
                                        theme={VictoryTheme.material}
                                        // width={375}
                                        height={175}
                                        scale={{ x: "time" }}
                                        responsive={true}
                                        gutter={10}
                                    >
                                        <VictoryAxis
                                            fixLabelOverlap={true}
                                            theme={VictoryTheme.material}
                                            tickCount={5}
                                            tickFormat={(t) => `${t}s`}
                                            fixLabelOverlap={true}
                                            style={{
                                                axis: { stroke: "#E3E5E8" },
                                                axisLabel: { fontSize: 13, padding: 25 },
                                                tickLabels: { fontSize: 12, padding: 5 },
                                                grid: { stroke: "#E3E5E8" }
                                            }}
                                        />
                                        <VictoryAxis dependentAxis
                                            tickCount={5}
                                            label={item[0].Unit}
                                            fixLabelOverlap={true}
                                            theme={VictoryTheme.material}
                                            style={{
                                                axis: { stroke: "#E3E5E8" },
                                                axisLabel: { fontSize: 13, padding: 38 },
                                                tickLabels: { fontSize: 12, padding: 3 },
                                            }}
                                        />
                                        <VictoryLine
                                            style={{
                                                data: { stroke: "#00A9E0" }
                                            }}
                                            data={item}
                                            x="x"
                                            y="y"
                                        />
                                    </VictoryChart>
                                </View>
                            )
                        }
                    })}

                    {!this.state.isLoading ?
                        <TouchableOpacity
                            onPress={() => this.addTrace()}
                            style={{
                                backgroundColor: '#FFFFFF',
                                // borderWidth:1,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                flexDirection: 'row',
                                width: '100%',
                                height: 45,
                                paddingLeft: 10,
                                paddingBottom: 10
                            }}
                        >
                            <IonicIcon name='ios-add-circle' style={{ fontSize: 35, color: '#00A9E0' }} /><Text> Add Trace</Text>
                        </TouchableOpacity> :
                        !this.state.timeOut ?
                            <View style={{
                                backgroundColor: '#FFFFFF',
                                // borderWidth:1,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                flexDirection: 'row',
                                width: '100%',
                                height: 45,
                                paddingTop: 10,
                                paddingLeft: 10,
                                paddingBottom: 10
                            }}>
                                <Text style={styles.labelText}>Loading data.....  </Text>
                                <ActivityIndicator size='small' color="#00A9E0" />
                            </View> :
                            <View style={{
                                backgroundColor: '#FFFFFF',
                                // borderWidth:1,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                flexDirection: 'row',
                                width: '100%',
                                height: 45,
                                paddingTop: 10,
                                paddingLeft: 10,
                                paddingBottom: 10
                            }}>
                                <OctIcon name='alert' style={styles.alertIcon} />
                                <Text style={styles.labelText}> Failed to load data </Text>
                            </View>
                    }
                </ScrollView>

                <Modal
                    style={styles.modalContainer}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={() => { }}>
                    <View style={styles.modalStyle}>
                        <SelectMultiple
                            renderLabel={this.renderLabel}
                            items={this.state.dataSource}
                            selectedItems={this.state.selectedParam}
                            onSelectionsChange={this.onSelectionsChange}
                            checkboxStyle={{ width: 20, height: 20 }}
                            selectedCheckboxStyle={{ width: 20, height: 20 }}
                        />
                        {
                            this.state.selectExceeded ?

                                <TouchableOpacity
                                    onPress={() => { }}
                                    style={styles.updateExceedButton}
                                >
                                    <Text style={styles.exceedText}>Maximum selection exceeded</Text>
                                </TouchableOpacity>
                                :
                                !this.state.loadingButton ?

                                    <TouchableOpacity
                                        onPress={() => {
                                            if (this.state.selectedParam.length > 0) {
                                                ChartData = []
                                                this.parseTransdata()
                                                this.setState({ loadingButton: true })
                                            } else if (this.state.selectedParam.length == 0) {

                                                ChartData = []
                                                this.setState({ loadingButton: true, isModalVisible: false })
                                            } else {
                                                this.setState({ isModalVisible: false })
                                            }
                                        }}
                                        style={styles.updateButton}
                                    >
                                        <Text style={styles.updateLabel}>{this.state.selectedParam.length <= 0 ? 'Close' : 'Show Trace'}</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                        style={styles.updateButton}
                                        onPress={() => this.setState({ loadingButton: true })}
                                    >
                                        <ActivityIndicator color='#5B6376' size='small' />
                                    </TouchableOpacity>
                        }
                    </View>
                </Modal>
            </View>
        )
    }
}

const mapStateToProps = ({ RetriveEngineData }) => {

    const { enginedata } = RetriveEngineData
    return { enginedata };
};

export default connect(mapStateToProps, null)(TraceChannelA)

const styles = StyleSheet.create({

    rectangle1: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8"
    },
    labelText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    valueText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: -0.34,
        color: "#1F2A44"
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        width: '80%',
        height: '90%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5
    },
    updateButton: {
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center',
        padding: 10,
        marginTop: 5
    },
    updateExceedButton: {
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center',
        padding: 10,
        marginTop: 5
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    chartTile: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#E3E5E8",
        paddingTop: 10
    },
    exceedText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: -0.34,
        color: "#EB1616",
        textAlign: 'center'
    },
    alertIcon: {
        fontSize: width < 450 ? 30 : 40,
        color: "#F7E164"
    },
})
