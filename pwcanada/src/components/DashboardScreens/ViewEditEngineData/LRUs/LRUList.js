import React, { Component } from "react";
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Text,
    Dimensions
} from "react-native";
import IonicIcon from "react-native-vector-icons/Ionicons";
import { withNavigationFocus } from "react-navigation";
import { connect } from "react-redux";
import axios from "axios";
import {
    selectedLRU,
    SNtoUpdate,
    PNtoUpdate,
    LRUfetch,
    DontReFetch,
    ReFetch,
    disableUpdate
} from "../../../../actions";

let { width, height } = Dimensions.get('window');
var parseString = require("react-native-xml2js").parseString;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// fetch the LRU array from the MicroFAST
// Parse and separate the details depending on the item Category
// Feed the array to view
// OnClick Capture the selected LRU item details and navigate to updateLRU.js
//
// Coded on: Oct 08 2018
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class LRUlist extends Component {
    constructor(props) {
        super(props);

        this.state = {
            LRU: [],
            ENGINE: [],
            userDataSourceValves: [],
            userDataSourceSensors: [],
            userDataSourceControlUnits: [],
            userDataSourceWireHarness: [],
            isLoading: true
        };
    }

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate("Dashboard")}
                style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                }}
            >
                <IonicIcon
                    name="ios-arrow-back"
                    style={{ fontSize: 35, color: "#000000", paddingLeft: 8 }}
                />
                <View style={{ justifyContent: "center" }}>
                    <Text style={{ textAlign: "center", fontSize: 18, paddingBottom: 4 }}>
                        {" "}
                        Dashboard
          </Text>
                </View>
            </TouchableOpacity>
        )
    });

    componentDidMount = () => {
        this.fetch();
    };

    UNSAFE_componentWillUpdate = () => {
        if (this.props.FetchLRU) {
            this.setState({ isLoading: true });
            this.fetch();
        }
    };

    fetch = async () => {
        this.props.DontReFetch();
        try {
            let response = await axios({
                method: "post",
                url: "http://microfast/cgi-bin/data_access.cgi?read",
                data: [
                    {
                        cmd: "xml_access",
                        arg1: "read"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });

            ///Parser for the response
            let responseJson = await response.data[0].value;
            parseString(responseJson, function (err, result) {
                _engine = result.MicroFAST_Box.Engine;
            });
            this.setState({ ENGINE: _engine });
            this.parser();
        } catch (error) {
            this.fetch();
        }
    };

    parser = () => {
        let ValvesDetectors = [],
            SensorsTransducers = [],
            ControlUnits = [],
            WireHarness = [];
        let engine = this.state.ENGINE;
        engine.map(item => {
            item.LRU.map(mod => {
                if (mod.CAT == "Valves and Detectors") {
                    ValvesDetectors.push({
                        LRUID: mod.$.ID,
                        LRUName: mod.name[0],
                        PN: mod.PN[0],
                        SN: mod.SN[0],
                        EngineID: item.$.ID,
                        scannable: mod.scannable[0]
                    });
                } else if (mod.CAT == "Wire Harness") {
                    WireHarness.push({
                        LRUID: mod.$.ID,
                        LRUName: mod.name[0],
                        PN: mod.PN[0],
                        SN: mod.SN[0],
                        EngineID: item.$.ID,
                        scannable: mod.scannable[0]
                    });
                } else if (mod.CAT == "Sensors and Transducers") {
                    SensorsTransducers.push({
                        LRUID: mod.$.ID,
                        LRUName: mod.name[0],
                        PN: mod.PN[0],
                        SN: mod.SN[0],
                        EngineID: item.$.ID,
                        scannable: mod.scannable[0]
                    });
                } else if (mod.CAT == "Control Units") {
                    ControlUnits.push({
                        LRUID: mod.$.ID,
                        LRUName: mod.name[0],
                        PN: mod.PN[0],
                        SN: mod.SN[0],
                        EngineID: item.$.ID,
                        scannable: mod.scannable[0]
                    });
                }
            });
        });
        if (ValvesDetectors.length > 0) {
            this.setState({ userDataSourceValves: ValvesDetectors });
        }

        if (WireHarness.length > 0) {
            this.setState({ userDataSourceWireHarness: WireHarness });
        }

        if (SensorsTransducers.length > 0) {
            this.setState({ userDataSourceSensors: SensorsTransducers });
        }

        if (ControlUnits.length > 0) {
            this.setState({ userDataSourceControlUnits: ControlUnits });
        }
        this.setState({ isLoading: false });
    };

    clickHandler = selectedLRU => {
        this.props.selectedLRU(selectedLRU);
        let { SN, PN } = selectedLRU;
        this.props.PNtoUpdate(PN);
        this.props.SNtoUpdate(SN);
        this.props.navigation.navigate("updateLRU", {
            headerTilte: selectedLRU.LRUName
        });
        this.props.disableUpdate();
    };

    renderView = () => {
        if (this.state.isLoading) {
            return (
                <View
                    style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
                >
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            );
        }
        return (
            <ScrollView>
                <View style={styles.tileStyle}>
                    <View>
                        <Image
                            style={styles.Image}
                            source={require("../../../../assets/IconPack/ViewEditEngineData/LRUs/detectors.png")}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ paddingLeft: 8 }}>
                        <Text style={styles.editEngineD}>Valves & Detectors</Text>
                        {
                            this.state.userDataSourceValves.map(item => {
                                return (
                                    <TouchableOpacity onPress={() => this.clickHandler(item)}>
                                        <View style={{ width: width < 450 ? "90%" : "100%" }}>
                                            <Text style={styles.label}>
                                                {item.LRUName}{" "}
                                                <IonicIcon
                                                    name="ios-arrow-forward"
                                                    style={styles.labelIcon}
                                                />
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            })
                        }
                    </View>
                </View>

                <View style={styles.tileStyle}>
                    <View>
                        <Image
                           style={styles.Image}
                            source={require("../../../../assets/IconPack/ViewEditEngineData/LRUs/sensors.png")}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ paddingLeft: 8 }}>
                        <Text style={styles.editEngineD}>Sensors & Transducers</Text>
                        {
                            this.state.userDataSourceSensors.map(item => {
                                return (
                                    <TouchableOpacity onPress={() => this.clickHandler(item)}>
                                        <View style={{ width: width < 450 ? "90%" : "100%" }}>
                                            <Text style={styles.label}>
                                                {item.LRUName}{" "}
                                                <IonicIcon
                                                    name="ios-arrow-forward"
                                                    style={styles.labelIcon}
                                                />
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            })
                        }
                    </View>
                </View>

                <View style={styles.tileStyle}>
                    <View>
                        <Image
                            style={styles.Image}
                            source={require("../../../../assets/IconPack/ViewEditEngineData/LRUs/controlUnit.png")}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ paddingLeft: 8 }}>
                        <Text style={styles.editEngineD}>Control Units</Text>
                        {this.state.userDataSourceControlUnits.map(item => {
                            return (
                                <TouchableOpacity onPress={() => this.clickHandler(item)}>
                                    <View style={{ width: width < 450 ? "90%" : "100%" }}>
                                        <Text style={styles.label}>
                                            {item.LRUName}{" "}
                                            <IonicIcon
                                                name="ios-arrow-forward"
                                                style={styles.labelIcon}
                                            />
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </View>
                <View style={[styles.tileStyle, { marginBottom: 13 }]}>
                    <View>
                        <Image
                            style={styles.Image}
                            source={require("../../../../assets/IconPack/ViewEditEngineData/LRUs/wire.png")}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ paddingLeft: 8 }}>
                        <Text style={styles.editEngineD}>Wire Harness</Text>
                        {this.state.userDataSourceWireHarness.map(item => {
                            return (
                                <TouchableOpacity onPress={() => this.clickHandler(item)}>
                                    <View style={{ width: width < 450 ? "90%" : "100%" }}>
                                        <Text style={styles.label}>
                                            {item.LRUName}{" "}
                                            <IonicIcon
                                                name="ios-arrow-forward"
                                                style={styles.labelIcon}
                                            />
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </View>
            </ScrollView>
        );
    };

    render() {
        return <View style={{ flex: 1 }}>{this.renderView()}</View>;
    }
}

const mapStateToProps = ({ LRUreducer }) => {
    const { FetchLRU } = LRUreducer;
    return { FetchLRU };
};

export default connect(
    mapStateToProps,
    {
        selectedLRU,
        SNtoUpdate,
        PNtoUpdate,
        LRUfetch,
        DontReFetch,
        ReFetch,
        disableUpdate
    }
)(withNavigationFocus(LRUlist));

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        flexDirection: "row",
        padding: 10
    },
    editEngineD: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#000000",
        paddingTop: 13
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#00A9E0",
        paddingTop: 13
    },
    labelIcon: {
        fontSize: width < 450 ? 16 : 22,
        opacity: 0.23
    },
    Image: {
        width: width < 450 ? 50 : 80,
        height: width < 450 ? 50 : 80
    }
});
