import React, { Component } from 'react';
import { View, StyleSheet, Vibration, Alert, Dimensions, Animated, Easing, Slider } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Toast, { DURATION } from 'react-native-easy-toast';
import { connect } from 'react-redux';
import {
    UpdateEngineModel,
    UpdateEngineSerialNumber,
    UpdateModuleSerialNumber,
    UpdateTakeOffDry,
    UpdateDOTTypeCert,
    UpdateFAATypeCert,
    GasGeneratorScanSuccess,
    GasGeneratorScanFailed,
} from '../../../../../actions'
import FeatherIcon from 'react-native-vector-icons/FontAwesome'

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Scan the fields once the Scan button is pressed
// Upon successful scan vibrate
// Redirect to updateLRU.js and auto populate the fields
// 
// Coded on: Oct 08 2018
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class BarcodeReader extends Component {
    constructor(props) {
        super(props)
        this.animatedValue = new Animated.Value(0)
        this.state = {
            moduleSN: '',
            pauseQR: false,
            zoom: 0,
            navBackRoute: ''
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('headerTilte'),
        };
    };

    componentDidMount = () => {
        this.animate()
        this.props.GasGeneratorScanFailed()
    }

    componentWillUnmount = () => {
        clearTimeout(this._interval)
    }

    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    change = (value) => {
        this.setState(() => {
            return {
                zoom: parseFloat(value),
            };
        });
    }

    render() {
        const { height, width } = Dimensions.get('window');
        const maskRowHeight = Math.round((height - 300) / 20);
        const maskColWidth = (width - 300) / 2;
        const movingMargin = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 300, 0]
        })

        return (
            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={styles.preview}
                        zoom={this.state.zoom}
                        type={RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.off}
                        onBarCodeRead={this.onBarCodeRead.bind(this)}
                        permissionDialogTitle={'Permission to use camera'}
                        permissionDialogMessage={'We need your permission to use your phone\'s camera'}
                    />
                    <View style={styles.maskOutter}>
                        <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
                        <View style={[{ flex: 30 }, styles.maskCenter]}>
                            <View style={[{ width: maskColWidth }, styles.maskFrame]} />
                            <View style={styles.maskInner}>
                                <Animated.View
                                    style={{
                                        marginLeft: movingMargin,
                                        height: '100%',
                                        width: '1%',
                                        backgroundColor: '#0033ab'
                                    }} />
                            </View>
                            <View style={[{ width: maskColWidth }, styles.maskFrame]} />
                            </View>
                        <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
                    </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                        {/* <Text style={{ color: '#fff', textAlign: 'center' }}>Zoom</Text> */}
                        <FeatherIcon name='search-minus' style={{ fontSize: 25, color: '#fff' }} />
                    </View>
                    <View style={{ width: '80%' }}>
                        <Slider
                            style={styles.sliderStyle}
                            maximumValue={0.1}
                            minimumValue={0}
                            onValueChange={(val) => this.change(val)}
                            value={this.state.zoom}
                        />
                    </View>
                    <View style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                        {/* <Text style={{ color: '#fff', textAlign: 'center' }}>Zoom</Text> */}
                        <FeatherIcon name='search-plus' style={{ fontSize: 25, color: '#fff' }} />
                    </View>
                </View>
                <Toast
                    ref="toast"
                    style={{ backgroundColor: '#fff' }}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: '#000' }}
                />
            </View>
        );
    }

    // the function invoked when the QR Code is detected
    onBarCodeRead(e) {
        if (!this.state.pauseQR) {
            if (e.data.trim().includes('/')) {
                this.QRCodeScanner(e.data)
            } else {
                this.InvalidFormatAlert()
            }
        }
    }

    // Alert prompt about invalid QR Codes
    InvalidFormatAlert = () => {
        Vibration.vibrate()
        this.setState({ pauseQR: true })
        Alert.alert(
            'Invalid 2D barcode',
            'Please use a valid 2D barcode for scanning.',
            [
                {
                    text: 'Retry', onPress: () => {
                        this.setState({ pauseQR: false })
                    }
                },
                {
                    text: 'Go Back', onPress: () => {
                        this.props.navigation.navigate('updateGasGenerator')
                        this.props.GasGeneratorScanFailed()
                    }, style: 'cancel'
                }
            ],
            { cancelable: false }
        )
    }

    // field extractor
    QRCodeScanner = (e) => {
        let split = e.split('/')
        if (split.length === 6) {
            if (this.state.moduleSN !== split[2]) {
                this.props.UpdateEngineModel(split[0])
                this.props.UpdateEngineSerialNumber(split[1].split('-')[1])
                this.props.UpdateModuleSerialNumber(split[2])
                this.props.UpdateTakeOffDry(split[3].split(' ')[0])
                this.props.UpdateDOTTypeCert(split[4])
                this.props.UpdateFAATypeCert(String(split[5].split('\\n')).slice(0, -2))
                this.setState({ pauseQR: true, moduleSN: split[2] })
                Vibration.vibrate()
                this.props.GasGeneratorScanSuccess()
                this.props.navigation.navigate('updateGasGenerator', { headerTilte: this.props.selectedEngineModule.ModuleName })
            } else {
                this.setState({ pauseQR: false })
            }
        } else {
            this.InvalidFormatAlert()
        }
    }
}

const mapStateToProps = ({ EngineDataReducer }) => {
    const { selectedEngineModule } = EngineDataReducer
    return { selectedEngineModule };
};

export default connect(mapStateToProps, {
    UpdateEngineModel,
    UpdateEngineSerialNumber,
    UpdateModuleSerialNumber,
    UpdateTakeOffDry,
    UpdateDOTTypeCert,
    UpdateFAATypeCert,
    GasGeneratorScanSuccess,
    GasGeneratorScanFailed,
})(BarcodeReader);

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 22,
        marginLeft: 30,
        marginRight: 30
    },
    maskOutter: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    maskInner: {
        width: 300,
        backgroundColor: 'transparent',
        borderColor: 'white',
        borderWidth: 1
    },
    maskFrame: {
        backgroundColor: 'rgba(1,1,1,0.6)'
    },
    maskRow: {
        width: '100%',
    },
    maskCenter: { flexDirection: 'row' },
    scanButton: {
        width: '100%',
        height: 68,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#0033ab",
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    sliderStyle: {
        marginLeft: 10,
        marginRight: 10,
    }
});
