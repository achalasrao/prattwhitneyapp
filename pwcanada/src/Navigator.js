import Ionic from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity, View, Text, Image, Dimensions } from 'react-native';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import React from 'react';
import { createStackNavigator, createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import SplashScreen from './loader';
import Dashboard from './components/Dashboard';
import AboutThisDCTU from './components/DashboardScreens/MicroFASTDCTU/AboutThisDCTU/About_ThisDCTU';
import LiveData from './components/DashboardScreens/LiveData/LiveData';
import LiveDataSensorList from './components/DashboardScreens/LiveData/LiveDataSensorList';

//=========MicroFAST Wireless connection-------


//======= WiFiMode Screens ========
import ConfiguredAp from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/WiFi/ConfiguredAp';
import AvailableNewtwork from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/WiFi/AvailableNetwork';
import AddNetwork from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/WiFi/AddNetwork';

//------Test Transmission ------
import TestTransmissionCellular from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/Cellular/TestTransmissionCellular';
import TestTransmissionWifi from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/WiFi/TestTransmissionWifi';
import ConnectionDCTUGround from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/ConnectionDCTUGround';
import ConfigureCellular from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/Cellular/ConfigureCellular';
import ConfigureCellularReport from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/Cellular/ConfigureCellularReport';
import EnableIncognitoMode from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/EnableIncognitoMode'
import DisableIncognitoMode from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/DisableIncognitoMode'
import ForceStartIncognito from './components/DashboardScreens/MicroFASTDCTU/ConnectionToGroundServer/ForceStartIncognito'

//=====Reterive Engine Data =====
import Snapshot from './components/DashboardScreens/RetrieveEngineData/Snapshot';
import Trace from './components/DashboardScreens/RetrieveEngineData/TracePlots';
import FaultA from './components/DashboardScreens/RetrieveEngineData/FaultA';
import ChannelA from './components/DashboardScreens/RetrieveEngineData/ChannelA';

// ====Engine Information===


// ===========EPECS==============
import Trim from './components/DashboardScreens/EngineInformation/epecs/Trim';
import TrimReport from './components/DashboardScreens/EngineInformation/epecs/TrimReport';
import EOF from './components/DashboardScreens/EngineInformation/epecs/EOF';
import Report from './components/DashboardScreens/EngineInformation/epecs/Report';
import EOFReport from './components/DashboardScreens/EngineInformation/epecs/EOFReport';
import Epecs_LCF from './components/DashboardScreens/EngineInformation/epecs/Epecs_LCF';
import Epecs_EngineUsage from './components/DashboardScreens/EngineInformation/epecs/Epecs_EngineUsgae';
import Epecs_Creep from './components/DashboardScreens/EngineInformation/epecs/Epecs_Creep';

//-----MicroFAST LRU -------------


//------------AirCraft--------------
import AircraftUpdate from '../src/components/DashboardScreens/Aircraft/EditAircraftData/AircraftUpdate';
import AircraftUpdateReport from '../src/components/DashboardScreens/Aircraft/EditAircraftData/AircraftUpdateReport';

// ---------------------------------
import UserManual from './components/UserManual';

// =======View/Edit Engine Data======
import LRUList from '../src/components/DashboardScreens/ViewEditEngineData/LRUs/LRUList';
import updateLRU from '../src/components/DashboardScreens/ViewEditEngineData/LRUs/updateLRU';
import scanLRU from '../src/components/DashboardScreens/ViewEditEngineData/LRUs/scanLRU';
import updateLRUReport from '../src/components/DashboardScreens/ViewEditEngineData/LRUs/updateReport';
import EngineDataPlateList from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/EngineDataPlateList';
import updateGasGenerator from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/GasGenerator/updateGasGenerator';
import scannerGasGenerator from './components/DashboardScreens/ViewEditEngineData/EngineData/GasGenerator/scannerGasGenerator';
import updateReportGasGenerator from './components/DashboardScreens/ViewEditEngineData/EngineData/GasGenerator/updateReport';
import updatePerformanceData from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/PerformanceData/updatePerfromaceData';
import updateReportPerformanceData from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/PerformanceData/updateReport';
import scannerPerformanceData from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/PerformanceData/scanner';
import updatePowerSection from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/PowerSection/updatePowerSection';
import updateReportPowerSection from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/PowerSection/updateReport';
import scannerPowerSection from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/PowerSection/scanner';
import updateEngineBuildSpec_ from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/BuildSpec/updateBuildSpec';
import updateReportEngineBuildSpec_ from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/BuildSpec/updateReport';
import scannerBuildSpec from '../src/components/DashboardScreens/ViewEditEngineData/EngineData/BuildSpec/scanner';
import AbouttheEpecs from './components/DashboardScreens/ViewEditEngineData/AboutThisEPECS/AbouttheEpecs';

let { width, height } = Dimensions.get('window');

const AboutThisDCTUStack = createStackNavigator(
  {
    AboutThisDCTU: {
      screen: AboutThisDCTU,
      navigationOptions: {
        title: 'About this DCTU',
      }
    }
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <Text style={{ textAlign: 'center' }}><Ionic name='ios-arrow-back' style={{ fontSize: 24, color: '#000000', }} /> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }
    },
  }
)

const SplashScreenStack = createStackNavigator(
  {
    SplashScreen: {
      screen: SplashScreen,
      navigationOptions: {
        header: null
      }
    }
  },

)

const DashboardStack = createStackNavigator(
  {
    Dashboard: {
      screen: Dashboard,
      navigationOptions: {
        title: 'Dashboard'
      }
    }
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }
)

const TestTransmissionStack = createStackNavigator(
  {

    TestTransmissionCellular: {
      screen: TestTransmissionCellular,
      navigationOptions: {
        title: 'Test Cellular Connection',
      }
    },

    TestTransmissionWifi: {
      screen: TestTransmissionWifi,
      navigationOptions: {
        title: 'Test WiFi Connection',
      }
    }
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }
    },
  }
)

const LiveDataStack = createStackNavigator(
  {
    LiveData: {
      screen: LiveData,
      navigationOptions: {
        title: 'Live Data',
      }
    },
    LiveDataSensorList:
    {
      screen: LiveDataSensorList,
      navigationOptions: {
        title: 'Watch Live Data ',
      }
    },
    DrawerLiveNavigation: {
      screen: LiveData
    }
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }
)


const TrimStack = createStackNavigator(
  {
    Trim: {
      screen: Trim,
      navigationOptions: {
        title: 'Trims',
      }
    },
    TrimReport: {
      screen: TrimReport,
      navigationOptions: {
        title: 'Trims',
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }

)
const AbouttheEpecsStack = createStackNavigator(
  {
    AbouttheEpecs: {
      screen: AbouttheEpecs,
      navigationOptions: {
        title: 'About the EEC',
      }
    }
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }
)

const CreepStack = createStackNavigator(
  {
    Creep: {
      screen: Epecs_Creep,
      navigationOptions: {
        title: 'CREEP',
      }
    },
    Report: {
      screen: Report,
      navigationOptions: {
        title: 'Report',
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }

)
const LCFStack = createStackNavigator(
  {
    LCF: {
      screen: Epecs_LCF,
      navigationOptions: {
        title: 'LCFs',
      }
    },
    Report: {
      screen: Report,
      navigationOptions: {
        title: 'Report',
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }

)
const EngineUsageStack = createStackNavigator(
  {
    EngineUsage: {
      screen: Epecs_EngineUsage,
      navigationOptions: {
        title: 'Engine Usage',
      }
    },
    Report: {
      screen: Report,
      navigationOptions: {
        title: 'Report',
      }
    },

  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }

)

const RetrieveEngineDataStack = createStackNavigator(
  {
    ChannelA: {
      screen: ChannelA,
      navigationOptions: {
        title: 'Events & Exceedances',
      }
    },

    SnapTrace: {
      screen: createMaterialTopTabNavigator({
        Snap: {
          screen: Snapshot,
          navigationOptions: ({ navigation }) => ({
            title: 'Snapshot',
          }),
        },
        Trace: {
          screen: Trace,
          navigationOptions: ({ navigation }) => ({
            title: 'Trace',
          }),
        },
      },
        {
          navigationOptions: ({ navigation }) => ({
            tabBarOptions: {
              style: {
                backgroundColor: '#00A9E0',
                fontFamily: "Arial",
                fontSize: 16,
                fontWeight: "500",
                fontStyle: "normal",
                letterSpacing: -0.1,
              },
              activeTintColor: '#62DBFF',
              inactiveTintColor: 'rgb(102,102,102)',
              indicatorStyle: {
                backgroundColor: 'rgb(0,51,171)',
                height: 5,
              }
            }
          })
        }
      ),
      navigationOptions: ({ navigation }) => ({
        title: navigation.getParam('headerTitle'),
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('ChannelA')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }),
    },

    snapOnly: {
      screen: createMaterialTopTabNavigator({
        Snap: {
          screen: Snapshot,
          navigationOptions: ({ navigation }) => ({
            title: 'Snapshot',
          }),
        },
      },
        {
          navigationOptions: ({ navigation }) => ({
            tabBarOptions: {
              style: {
                backgroundColor: '#00A9E0',
                fontFamily: "Arial",
                fontSize: 16,
                fontWeight: "500",
                fontStyle: "normal",
                letterSpacing: -0.1,
              },
              activeTintColor: '#62DBFF',
              inactiveTintColor: 'rgb(102,102,102)',
              indicatorStyle: {
                backgroundColor: 'rgb(0,51,171)',
                height: 5,
              }
            }
          })
        }
      ),
      navigationOptions: ({ navigation }) => ({
        title: navigation.getParam('headerTitle'),
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('ChannelA')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16,
              marginBottom: 10
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }),
    },
  },


  {
    navigationOptions: ({ navigation }) => {
      const { params } = navigation.state

      return {
        gesturesEnabled: false,
        title: 'Events & Exceedance',
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('Dashboard')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }

)


const TabularFaultDataStack = createStackNavigator(
  {

    FaultA: {
      screen: FaultA,
      navigationOptions: {
        title: 'Faults',
      }
    },

    faultSnapTrace: {
      screen: createMaterialTopTabNavigator({
        Snap: {
          screen: Snapshot,
          navigationOptions: ({ navigation }) => ({
            title: 'Snapshot',
          }),
        },
        Trace: {
          screen: Trace,
          navigationOptions: ({ navigation }) => ({
            title: 'Trace',
          }),
        },
      },
        {
          navigationOptions: ({ navigation }) => ({
            tabBarOptions: {
              style: {
                backgroundColor: '#00A9E0',
                fontFamily: "Arial",
                fontSize: 16,
                fontWeight: "500",
                fontStyle: "normal",
                letterSpacing: -0.1,
              },
              activeTintColor: '#62DBFF',
              inactiveTintColor: 'rgb(102,102,102)',
              indicatorStyle: {
                backgroundColor: 'rgb(0,51,171)',
                height: 5,
              }
            }
          })
        }
      ),
      navigationOptions: ({ navigation }) => ({
        title: navigation.getParam('headerTitle'),
        swipeEnabled: false,
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('FaultA')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }),
    },

    faultSnapOnly: {
      screen: createMaterialTopTabNavigator({
        Snap: {
          screen: Snapshot,
          navigationOptions: ({ navigation }) => ({
            title: 'Snapshot',
          }),
        },
      },
        {
          navigationOptions: ({ navigation }) => ({
            tabBarOptions: {
              style: {
                backgroundColor: '#00A9E0',
                fontFamily: "Arial",
                fontSize: 16,
                fontWeight: "500",
                fontStyle: "normal",
                letterSpacing: -0.1,
              },
              activeTintColor: '#62DBFF',
              inactiveTintColor: 'rgb(102,102,102)',
              indicatorStyle: {
                backgroundColor: 'rgb(0,51,171)',
                height: 5,
              }
            }
          })
        }
      ),
      navigationOptions: ({ navigation }) => ({
        title: navigation.getParam('headerTitle'),
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('FaultA')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }),
    },
  },

  {
    navigationOptions: ({ navigation }) => {
      const { params } = navigation.state

      return {
        gesturesEnabled: false,
        title: 'Faults',
        lazy: true,
        removeClippedSubviews: true,
        headerStyle: {
          backgroundColor: '#e5e5e5',
          fontSize: 16
        },
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('Dashboard')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },


      };
    },
  }
)
const ViewEditEngineData = createStackNavigator(
  {
    LRUList: {
      screen: LRUList,
      navigationOptions: {
        title: 'LRUs',
      }
    },
    updateLRU: {
      screen: updateLRU,
    },
    scanLRU: {
      screen: scanLRU,
    },
    updateLRUReport: {
      screen: updateLRUReport,
    },
    EngineData: {
      screen: EngineDataPlateList,
      navigationOptions: {
        title: 'Engine Data',
      },
    },
    updateGasGenerator: {
      screen: updateGasGenerator
    },
    updatePerformanceData: {
      screen: updatePerformanceData
    },
    updateReportPerformanceData: {
      screen: updateReportPerformanceData
    },
    scannerPerformanceData: {
      screen: scannerPerformanceData
    },
    updatePowerSection: {
      screen: updatePowerSection
    },
    updateReportPowerSection: {
      screen: updateReportPowerSection
    },
    scannerPowerSection: {
      screen: scannerPowerSection
    },
    updateEngineBuildSpec_: {
      screen: updateEngineBuildSpec_
    },
    updateReportEngineBuildSpec_: {
      screen: updateReportEngineBuildSpec_
    },
    scannerBuildSpec: {
      screen: scannerBuildSpec
    },
    scannerGasGenerator: {
      screen: scannerGasGenerator
    },
    updateReportGasGenerator: {
      screen: updateReportGasGenerator
    }
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        // headerLeft: (
        //   <TouchableOpacity
        //     onPress={() => navigation.navigate('Dashboard')}
        //     style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
        //     <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
        //     <View style={{ justifyContent: 'center' }}>
        //       <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
        //     </View>
        //   </TouchableOpacity>
        // ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        )
      }
    },
  }
)

const ConnectionToGS = createStackNavigator(
  {
    ConnectionDCTUGround: {
      screen: ConnectionDCTUGround,
      navigationOptions: {
        title: 'Connection  between DCTU and ground server',
      }
    },
    EnableIncognitoMode: {
      screen: EnableIncognitoMode,
      navigationOptions: {
        title: 'Disable Cellular/Wifi',
      }
    },
    DisableIncognitoMode: {
      screen: DisableIncognitoMode,
      navigationOptions: {
        title: 'Disable Cellular/Wifi',
      }
    },
    ForceStartIncognito: {
      screen: ForceStartIncognito,
      navigationOptions: {
        title: 'Disable Cellular/Wifi',
      }
    },
    ConfigureCellular: {
      screen: ConfigureCellular,
      navigationOptions: {
        title: 'Configure Cellular',
      }
    },
    ConfigureCellularReport: {
      screen: ConfigureCellularReport,
      navigationOptions: {
        title: 'Report',
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <Text style={{ textAlign: 'center' }}><Ionic name='ios-arrow-back' style={{ fontSize: 24, color: '#000000', }} /> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }
    },
  }
)

const EngineInformationStack = createStackNavigator(
  {
    EOF: {
      screen: EOF,
      navigationOptions: {
        title: 'EEC Replacement',
      }
    },

    // EECReplacement: {
    //   screen: EECReplacement,
    //   navigationOptions: {
    //     title: 'EEC Replacement',
    //   }
    // },

    EOFReport: {
      screen: EOFReport,
      navigationOptions: {
        title: 'EEC Change',
      }
    },

    // ==========
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        // headerRight: (
        //   <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
        //     <Image style={{
        //       flex: 1,
        //       resizeMode: 'contain',
        //       width: 50,
        //       height: 50,
        //       marginRight: 16
        //     }}
        //       source={require('./assets/Logos/pratt-logo.png')}
        //       resizeMode='contain' />
        //   </TouchableOpacity>
        // ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }
)

const UserManualStack = createStackNavigator(
  {
    UserManual: {
      screen: UserManual,
      navigationOptions: {
        title: 'App User Guide',
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 22,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('Dashboard')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      };
    },
  }
)

const AircraftStack = createStackNavigator(
  {
    AircraftUpdate: {
      screen: AircraftUpdate,
      navigationOptions: {
        title: 'View / Edit Aircraft',
      }
    },
    AircraftUpdateReport: {
      screen: AircraftUpdateReport,
      navigationOptions: {
        title: 'View / Edit Aircraft',
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerLeft: (
          <TouchableOpacity
            onPress={() => navigation.navigate('Dashboard')}
            style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
            <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }
    },
  }
)

const MicroFASTWirelessConnectionsStack = createStackNavigator(
  {

    WifiModeScreen: {
      screen: createMaterialTopTabNavigator({
        ConfiguredAp: {
          screen: ConfiguredAp,
          navigationOptions: {
            title: 'Configured Networks',
          }
        },
        AvailableNewtwork: {
          screen: AvailableNewtwork,
          navigationOptions: {
            title: 'Available Networks',
          }
        },
        AddNetwork: {
          screen: AddNetwork,
          navigationOptions: {
            title: 'Add Network',
          }
        }
      },
        {
          navigationOptions: ({ navigation }) => {
            return {
              swipeEnabled: false,
              tabBarOptions: {
                style: {
                  backgroundColor: '#00A9E0',
                  fontFamily: "Arial",
                  fontSize: width < 450 ? 16 : 22,
                  fontWeight: "500",
                  fontStyle: "normal",
                  letterSpacing: -0.1,
                },
                activeTintColor: '#FFFFFF',
                inactiveTintColor: '#5B6376',
                indicatorStyle: {
                  backgroundColor: '#62DBFF',
                  height: 5,
                }
              },

              headerLeft: (
                <TouchableOpacity
                  onPress={() => navigation.navigate('Dashboard')}
                  style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                  <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                  <View style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Dashboard</Text>
                  </View>
                </TouchableOpacity>
              ),
              headerRight: (
                <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                  <Image style={{
                    flex: 1,
                    resizeMode: 'contain',
                    width: 50,
                    height: 50,
                    marginRight: 16
                  }}
                    source={require('./assets/Logos/pratt-logo.png')}
                    resizeMode='contain' />
                </TouchableOpacity>
              ),
            }
          },
          swipeEnabled: false,
          tabBarOptions: {
            style: {
              backgroundColor: '#00A9E0',
              fontFamily: "Arial",
              fontSize: width < 450 ? 16 : 22,
              fontWeight: "500",
              fontStyle: "normal",
              letterSpacing: -0.1,
            },
            activeTintColor: '#FFFFFF',
            inactiveTintColor: '#5B6376',
            indicatorStyle: {
              backgroundColor: '#62DBFF',
              height: 5,
            }
          }
        }
      ),
      navigationOptions: ({ navigation }) => {
        return {
          title: 'Configure Wifi Networks',
          headerLeft: (
            <TouchableOpacity
              onPress={() => navigation.navigate('ConnectionDCTUGround')}
              style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
              <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
              <View style={{ justifyContent: 'center' }}>
                <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
              </View>
            </TouchableOpacity>
          ),
          headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
              <Image style={{
                flex: 1,
                resizeMode: 'contain',
                width: 50,
                height: 50,
                marginRight: 16
              }}
                source={require('./assets/Logos/pratt-logo.png')}
                resizeMode='contain' />
            </TouchableOpacity>
          ),
        }
      },
    },
  },
  {
    navigationOptions: ({ navigation }) => {
      return {
        gesturesEnabled: false,
        // swipeEnabled: false,
        headerStyle: {
          backgroundColor: '#e5e5e5',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
          fontFamily: "Arial",
          fontSize: 24,
          fontWeight: "bold",
          fontStyle: "normal",
          letterSpacing: -0.5,
          alignSelf: 'flex-start'
        },
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <Text style={{ textAlign: 'center' }}><Ionic name='ios-arrow-back' style={{ fontSize: 24, color: '#000000', }} /> Back</Text>
            </View>
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{
              flex: 1,
              resizeMode: 'contain',
              width: 50,
              height: 50,
              marginRight: 16
            }}
              source={require('./assets/Logos/pratt-logo.png')}
              resizeMode='contain' />
          </TouchableOpacity>
        ),
      }
    },
  }
)

const Navigator = createStackNavigator(
  {
    SplashScreen: SplashScreenStack,
    Dashboard: DashboardStack,
    ViewEditEngineDataLRUlist: ViewEditEngineData,
    MicroFASTDCTU: ConnectionToGS,
    AboutThisDCTU: AboutThisDCTUStack,
    TestTransmission: TestTransmissionStack,
    Trim: TrimStack,
    AbouttheEpecs: AbouttheEpecsStack,
    Creep: CreepStack,
    LCF: LCFStack,
    TabularFault: TabularFaultDataStack,
    // Report:ReporterStack, 
    EngineUsage: EngineUsageStack,
    LiveData: LiveDataStack,
    RetrieveEngineData: RetrieveEngineDataStack,
    EngineInformation: EngineInformationStack,
    Aircraft: AircraftStack,
    MicroFASTWirelessConnections: MicroFASTWirelessConnectionsStack,
    UserManual: UserManualStack
  },
  {
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false,
    }
  }
)

const App = createAppContainer(Navigator);

export default App;
