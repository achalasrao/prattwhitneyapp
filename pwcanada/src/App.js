import React, { Component } from 'react';
import { PermissionsAndroid, AppState } from 'react-native';
import { Provider } from 'react-redux';
import { Root } from "native-base";
import { createStore, applyMiddleware } from 'redux';
import Navigator from './Navigator';
import ReduxThunk from 'redux-thunk';
import RNExitApp from 'react-native-exit-app'
import reducers from './reducers';

export default class App extends Component {

  UNSAFE_componentWillMount = async () => {
    AppState.addEventListener('change', this._handleAppStateChange);

  }

  componentDidMount = async () => {
    await this.request()
  }

  componentWillUnmount = async () => {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'background') {
      RNExitApp.exitApp();
    }
  }

  request = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Wifi networks',
          'message': 'We need your permission in order to find wifi networks'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {

      } else {

      }
    } catch (err) {

    }
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Root>
        <Provider store={store}>

          <Navigator />

        </Provider>
      </Root>
    );
  }
}
