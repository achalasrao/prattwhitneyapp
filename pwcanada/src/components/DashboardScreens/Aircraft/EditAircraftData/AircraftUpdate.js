import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    Dimensions,
    ScrollView
} from 'react-native';
import {
    Text,
    Content,
    Label,
} from 'native-base';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import { connect } from "react-redux";
import {
    oldValueAircraft_SN,
    oldValueAircraft_Type,
    oldValueFleet_Ident,
    oldValueAircraft_Number,
    oldValueTail_Number,
    oldValueAirline_Operator,
    oldValueAircraft_Owner,
    newValueAircraft_SN,
    newValueAircraft_Type,
    newValueFleet_Ident,
    newValueAircraft_Number,
    newValueTail_Number,
    newValueAirline_Operator,
    newValueAircraft_Owner,
    AircraftUpdateRequired,
    AircraftUpdateNotRequired,
    TNEmpty,
    TNError,
    TNFetchSuccess,
    FetchAircraftData,
    DontFetchAircraftData
} from '../../../../actions';

let { width, height } = Dimensions.get('window');

class AircraftUpdate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            isInputFocussed: false,
            resultList: [],
            Parameter: [],
            DataSection: [],
            check: true,
            loading: false,
            refershing: false,
            selectedParam: undefined,
            changedValue: undefined,
            oldValue: undefined,
            position: 'top',
            fieldChanged: false,
            isUpdateRequire: false,
            isModalVisible: false,
            writeToXmlCount: 0,
            AppcrashState: false,
            xmlUpdateCount: 0,
            updateFailed: false,
            updateSuccess: false,
            isLoading: false,
            fieldUpdateCount: 0,
            fetchCount: 0,
            field1Focused: false,
            field2Focused: false,
            field3Focused: false,
            field4Focused: false,
            field5Focused: false,
            field6Focused: false,
            field7Focused: false,
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('Dashboard')}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    async UNSAFE_componentWillMount() {
        await this.fetchdata()
    }

    async fetchdata() {
        this.setState({ loading: true });
        let responseJson = [];
        this.setState({ fetchCount: ++this.state.fetchCount })
        this.props.DontFetchAircraftData();
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "read",
                        "arg2": "Aircraft"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            responseJson = await response.data;

            if (responseJson[0].value == "Err: Too many access request (>4).  Try again later") {
                this.setState({ AppcrashState: true })
            }

            if (responseJson.length > 0) {
                var decoded;
                if (responseJson[0].name == "Aircraft_SN") {
                    decoded = responseJson[0].value.replace(/&amp;/g, '&');
                    this.props.oldValueAircraft_SN(decoded)
                    this.props.newValueAircraft_SN(decoded)
                }
                if (responseJson[1].name == "Aircraft_Type") {
                    decoded = responseJson[1].value.replace(/&amp;/g, '&');
                    this.props.oldValueAircraft_Type(decoded)
                    this.props.newValueAircraft_Type(decoded)
                }
                if (responseJson[2].name == "Fleet_Ident") {
                    decoded = responseJson[2].value.replace(/&amp;/g, '&');
                    this.props.oldValueFleet_Ident(decoded)
                    this.props.newValueFleet_Ident(decoded)
                }
                if (responseJson[3].name == "Aircraft_Number") {
                    decoded = responseJson[3].value.replace(/&amp;/g, '&');
                    this.props.oldValueAircraft_Number(decoded)
                    this.props.newValueAircraft_Number(decoded)
                }
                if (responseJson[4].name == "Tail_Number") {
                    decoded = responseJson[4].value.replace(/&amp;/g, '&');
                    this.props.oldValueTail_Number(decoded)
                    this.props.newValueTail_Number(decoded)
                    if (decoded === '') {
                        this.props.TNEmpty()
                    } else {
                        this.props.TNFetchSuccess(decoded)
                    }
                }
                if (responseJson[6].name == "Airline_Operator") {
                    decoded = responseJson[6].value.replace(/&amp;/g, '&');
                    this.props.oldValueAirline_Operator(decoded)
                    this.props.newValueAirline_Operator(decoded)
                }
                if (responseJson[7].name == "Aircraft_Owner") {
                    decoded = responseJson[7].value.replace(/&amp;/g, '&');
                    this.props.oldValueAircraft_Owner(decoded)
                    this.props.newValueAircraft_Owner(decoded)
                }
                this.setState({ loading: false });
            } else if (responseJson.length == 0 && this.state.fetchCount < 3) {
                this.fetchdata();
            }

        } catch (error) {

            this.props.TNError();
        }

        if (responseJson.length == 0 && this.state.fetchCount < 3) {

            this.fetchdata()
        }
    }

    _onRefresh = async () => {
        await this.fetchdata()
    }

    componentDidUpdate = () => {

        const { OldAircraft_SN,
            OldAircraft_Type,
            OldFleet_Ident,
            OldAircraft_Number,
            OldTail_Number,
            OldAirline_Operator,
            OldAircraft_Owner,
            NewAircraft_SN,
            NewAircraft_Type,
            NewFleet_Ident,
            NewAircraft_Number,
            NewTail_Number,
            NewAirline_Operator,
            NewAircraft_Owner,
        } = this.props

        if ((OldAircraft_SN !== NewAircraft_SN) ||
            (OldAircraft_Type !== NewAircraft_Type) ||
            (OldFleet_Ident !== NewFleet_Ident) ||
            (OldAircraft_Number !== NewAircraft_Number) ||
            (OldTail_Number !== NewTail_Number) ||
            (OldAirline_Operator !== NewAirline_Operator) ||
            (OldAircraft_Owner !== NewAircraft_Owner)) {
            this.props.AircraftUpdateRequired()
        } else {
            this.props.AircraftUpdateNotRequired()
        }

        if (this.props.fetchAircraftData) {
            this.fetchdata();
        }
    }

    newValueAircraft_SN(text) {
        this.props.newValueAircraft_SN(text)
    }

    newValueAircraft_Type(text) {
        this.props.newValueAircraft_Type(text)
    }

    newValueAircraft_Number(text) {
        this.props.newValueAircraft_Number(text)
    }

    newValueFleet_Ident(text) {
        this.props.newValueFleet_Ident(text)
    }

    newValueAircraft_Owner(text) {
        this.props.newValueAircraft_Owner(text)
    }

    newValueAirline_Operator(text) {
        this.props.newValueAirline_Operator(text)
    }

    newValueTail_Number(text) {
        this.props.newValueTail_Number(text)
    }

    onFocusChanged() {
        this.setState({ fieldFocussed: true })
    }

    async onSubmit(event) {
        event.preventDefault();

        this.setState({ isModalVisible: true })
    }

    writeToXml = async () => {
        this.setState({ xmlUpdateCount: ++this.state.xmlUpdateCount })

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            let responseJson = await response;
            if (responseJson.data[0].value === "Success") {
                this.updateWasSuccess()
            } else {
                this.updateWasFailure()
            }
        }
        catch (error) {
            if (this.state.xmlUpdateCount < 2) {
                return this.writeToXml()
            }
            this.updateWasFailure()
        }
    }

    renderModal = () => {
        const { OldAircraft_SN,
            OldAircraft_Type,
            OldFleet_Ident,
            OldAircraft_Number,
            OldTail_Number,
            OldAirline_Operator,
            OldAircraft_Owner,
            NewAircraft_SN,
            NewAircraft_Type,
            NewFleet_Ident,
            NewAircraft_Number,
            NewTail_Number,
            NewAirline_Operator,
            NewAircraft_Owner,
        } = this.props

        return (

            <View style={styles.modalStyle}>
                <ScrollView>
                    <View style={{ width: '100%', padding: 10 }}>
                        <Text style={styles.modalHeader}>You are about to load this data to the DCTU</Text>
                    </View>

                    <View style={{ width: '100%', padding: 10, paddingTop: 10, paddingRight: 10 }}>
                        {
                            (OldAircraft_SN === NewAircraft_SN) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Aircraft Serial Number is being changed from {OldAircraft_SN} to {NewAircraft_SN}</Text>
                                </View>
                        }
                        {
                            (OldAircraft_Type === NewAircraft_Type) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Aircraft Type is being changed from {OldAircraft_Type} to {NewAircraft_Type}</Text>
                                </View>
                        }
                        {
                            (OldFleet_Ident === NewFleet_Ident) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Fleet Ident is being changed from {OldFleet_Ident} to {NewFleet_Ident}</Text>
                                </View>
                        }
                        {
                            (OldAircraft_Number === NewAircraft_Number) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Aircraft Number is being changed from {OldAircraft_Number} to {NewAircraft_Number}</Text>
                                </View>
                        }
                        {
                            (OldTail_Number === NewTail_Number) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Tail Number is being changed from {OldTail_Number} to {NewTail_Number}</Text>
                                </View>
                        }
                        {
                            (OldAirline_Operator === NewAirline_Operator) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Airline Operator is being changed from {OldAirline_Operator} to {NewAirline_Operator}</Text>
                                </View>
                        }
                        {
                            (OldAircraft_Owner === NewAircraft_Owner) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Aircraft Owner is being changed from {OldAircraft_Owner} to {NewAircraft_Owner}</Text>
                                </View>
                        }
                    </View>

                    <View style={{ paddingTop: 15 }}>
                        <Text style={styles.eraseDataTextStyle}>This will erase previous data</Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 13 }}>

                        <TouchableOpacity
                            style={styles.previousButtonStyle}
                            onPress={() => this.previous()}
                        >
                            <Text style={styles.previousLabelStyle}>Previous</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.confirmButtonStyle}
                            onPress={() => this.confirmedUpdate()}
                        >
                            <Text style={styles.confirmLabelStyle}>Confirm</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </View>
        )
    }

    async confirmedUpdate() {

        let {
            NewAircraft_SN,
            NewAircraft_Type,
            NewFleet_Ident,
            NewAircraft_Number,
            NewTail_Number,
            NewAirline_Operator,
            NewAircraft_Owner
        } = this.props

        this.setState({ loading: true, fieldUpdateCount: ++this.state.fieldUpdateCount })

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Aircraft/Aircraft_SN`,
                        "arg3": JSON.stringify(NewAircraft_SN)
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Aircraft/Aircraft_Type`,
                        "arg3": JSON.stringify(NewAircraft_Type)
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Aircraft/Fleet_Ident',
                        "arg3": JSON.stringify(NewFleet_Ident)
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Aircraft/Aircraft_Number',
                        "arg3": JSON.stringify(NewAircraft_Number)
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Aircraft/Tail_Number',
                        "arg3": JSON.stringify(NewTail_Number)
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Aircraft/Airline_Operator',
                        "arg3": JSON.stringify(NewAirline_Operator)
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Aircraft/Aircraft_Owner',
                        "arg3": JSON.stringify(NewAircraft_Owner)
                    },
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response
            let responseJson = await response;

            var lines = JSON.stringify(responseJson.data);
            let splitlines = lines.split(":")

            for (var i = 0; i < splitlines.length; i++) {
                if (splitlines[i].includes("Success")) {
                    result = "success"
                }
                else if (splitlines[i].includes("Err")) {
                    result = "Failed"
                }
            }

            if (String(responseJson.data).includes('Data Error')) {
                this.updateWasFailure()
            } else {
                var decoded = NewTail_Number.replace(/&amp;/g, '&');
                if (decoded === '') {
                    this.props.TNEmpty();
                } else {
                    this.props.TNFetchSuccess(decoded);
                }
                this.writeToXml();
            }
        }
        catch (err) {
            if (this.state.fieldUpdateCount < 2) {
                return this.updateFields()
            }
        }
    }

    previous = async () => {
        const { OldAircraft_SN,
            OldAircraft_Type,
            OldFleet_Ident,
            OldAircraft_Number,
            OldTail_Number,
            OldAirline_Operator,
            OldAircraft_Owner,
        } = this.props

        this.props.newValueAircraft_SN(OldAircraft_SN)
        this.props.newValueAircraft_Type(OldAircraft_Type)
        this.props.newValueAircraft_Number(OldAircraft_Number)
        this.props.newValueFleet_Ident(OldFleet_Ident)
        this.props.newValueAircraft_Owner(OldAircraft_Owner)
        this.props.newValueAirline_Operator(OldAirline_Operator)
        this.props.newValueTail_Number(OldTail_Number)
        this.setState({ isModalVisible: false, hidden: true, loading: true, changedValue: undefined, selectedParam: undefined, fieldChanged: false })

        await this.fetchdata()
    }

    updateWasFailure = () => {
        this.setState({ updateFailed: true, updateSuccess: false, isLoading: false })
    }
    updateWasSuccess = () => {
        this.setState({ updateFailed: false, updateSuccess: true, isModalVisible: false })
        this.props.navigation.navigate("AircraftUpdateReport", { param: result })
    }

    render() {
        const {
            OldAircraft_SN,
            OldAircraft_Type,
            OldFleet_Ident,
            OldAircraft_Number,
            OldTail_Number,
            OldAirline_Operator,
            OldAircraft_Owner,
        } = this.props

        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (

            <Content>
                <View style={{ padding: 15, backgroundColor: '#FFFFFF', marginTop: 10 }}>
                    <View style={styles.itemContainer}>

                        <View style={styles.labelContainer}>
                            <Text style={[styles.label]}>Aircraft Serial Number</Text>
                        </View>

                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={[styles.textInput, {
                                    borderColor: this.state.field1Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field1Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field1Focused: true })}
                                onBlur={() => this.setState({ field1Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueAircraft_SN(text)}
                                value={ this.props.NewAircraft_SN }
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.itemContainer}>

                        <View style={styles.textInputContainer}>
                            <Text style={[styles.label]}>Aircraft Type</Text>
                        </View>

                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={[styles.textInput, {
                                    borderColor: this.state.field2Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field2Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field2Focused: true })}
                                onBlur={() => this.setState({ field2Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueAircraft_Type(text)}
                                value={this.props.NewAircraft_Type}
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>

                    </View>

                    <View style={styles.itemContainer}>
                        <View style={styles.textInputContainer}>
                            <Text style={[styles.label]}>Fleet Ident</Text>
                        </View>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={[styles.textInput, {
                                    borderColor: this.state.field3Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field3Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field3Focused: true })}
                                onBlur={() => this.setState({ field3Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueFleet_Ident(text)}
                                value={this.props.NewFleet_Ident}
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.itemContainer}>
                        <View style={styles.textInputContainer}>
                            <Text style={[styles.label]}>Aircraft Number</Text>
                        </View>
                        <View style={styles.textInputContainer}>
                            <TextInput

                                style={[styles.textInput, {
                                    borderColor: this.state.field4Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field4Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field4Focused: true })}
                                onBlur={() => this.setState({ field4Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueAircraft_Number(text)}
                                value={this.props.NewAircraft_Number}
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.itemContainer}>
                        <View style={styles.textInputContainer}>
                            <Text style={[styles.label]}>Tail Number</Text>
                        </View>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={[styles.textInput, {
                                    borderColor: this.state.field5Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field5Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field5Focused: true })}
                                onBlur={() => this.setState({ field5Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueTail_Number(text)}
                                value={this.props.NewTail_Number}
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.itemContainer}>
                        <View style={styles.textInputContainer}>
                            <Text style={[styles.label]}>Airline Operator</Text>
                        </View>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={[styles.textInput, {
                                    borderColor: this.state.field6Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field6Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field6Focused: true })}
                                onBlur={() => this.setState({ field6Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueAirline_Operator(text)}
                                value={this.props.NewAirline_Operator}
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.itemContainer}>
                        <View style={styles.textInputContainer}>
                            <Text style={[styles.label]}>Aircraft Owner</Text>
                        </View>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                style={[styles.textInput, {
                                    borderColor: this.state.field7Focused ? "#000000" : "#000000",
                                    backgroundColor: this.state.field7Focused ? "#FFFFFF" : "#F7F7F7",
                                }]}
                                onFocus={() => this.setState({ field7Focused: true })}
                                onBlur={() => this.setState({ field7Focused: false })}
                                autoCorrect={false}
                                autoCapitalize={'none'}
                                placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#848A98' } : { fontStyle: 'italic', color: '#848A98' }}
                                onChangeText={(text) => this.newValueAircraft_Owner(text)}
                                value={this.props.NewAircraft_Owner}
                                selectionColor={"#00A9E0"} //change the cursor color
                            ></TextInput>
                        </View>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30, marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                    <TouchableOpacity
                        onPress={() => { this._onRefresh() }}
                        style={styles.cancelButton}
                    >
                        <Text style={styles.cancelLabel}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        disabled={!this.props.isUpdateRequired}
                        style={[styles.updateButton, {
                            backgroundColor: this.props.isUpdateRequired ? "#00A9E0" : "#62DBFF"
                        }]}

                        onPress={this.onSubmit.bind(this)} >
                        <Label style={styles.testButtonText}> <Text style={[styles.updateLabel, {
                            color: this.props.isUpdateRequired ? '#FFFFFF' : '#5B6376'
                        }]}>Update</Text></Label>
                    </TouchableOpacity>
                </View>
                <View>
                    <Modal
                        style={styles.modalContainer}
                        isVisible={this.state.isModalVisible}
                        onBackdropPress={() => this.setState({ isModalVisible: false })}
                    >{this.renderModal()}</Modal>
                </View>
            </Content>
        )
    }
}

const mapStateToProps = ({ AircraftUpdateReducer }) => {
    const { OldAircraft_SN,
        OldAircraft_Type,
        OldFleet_Ident,
        OldAircraft_Number,
        OldTail_Number,
        OldAirline_Operator,
        OldAircraft_Owner,
        NewAircraft_SN,
        NewAircraft_Type,
        NewFleet_Ident,
        NewAircraft_Number,
        NewTail_Number,
        NewAirline_Operator,
        NewAircraft_Owner,
        isUpdateRequired,
        fetchAircraftData
    } = AircraftUpdateReducer

    return {
        OldAircraft_SN,
        OldAircraft_Type,
        OldFleet_Ident,
        OldAircraft_Number,
        OldTail_Number,
        OldAirline_Operator,
        OldAircraft_Owner,
        NewAircraft_SN,
        NewAircraft_Type,
        NewFleet_Ident,
        NewAircraft_Number,
        NewTail_Number,
        NewAirline_Operator,
        NewAircraft_Owner,
        isUpdateRequired,
        fetchAircraftData
    };
};

export default connect(mapStateToProps, {
    oldValueAircraft_SN,
    oldValueAircraft_Type,
    oldValueFleet_Ident,
    oldValueAircraft_Number,
    oldValueTail_Number,
    oldValueAirline_Operator,
    oldValueAircraft_Owner,
    newValueAircraft_SN,
    newValueAircraft_Type,
    newValueFleet_Ident,
    newValueAircraft_Number,
    newValueTail_Number,
    newValueAirline_Operator,
    newValueAircraft_Owner,
    AircraftUpdateRequired,
    AircraftUpdateNotRequired,
    TNEmpty,
    TNError,
    TNFetchSuccess,
    FetchAircraftData,
    DontFetchAircraftData
})(AircraftUpdate)


const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#5B6376",
        textAlignVertical: 'center'
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#00A9E0",
        justifyContent: 'center'
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    cancelLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#00A9E0"
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        width: '100%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        padding: 23
    },
    modalHeader: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.25,
        color: "#000000"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        justifyContent: 'center'
    },
    previousLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    confirmButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    confirmLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    textInput: {
        height: 40,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#BABBB1",
        paddingLeft: 10,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#A5A9B4",
        textAlign: 'left'
    },
    testButtonText: {
        fontFamily: "Arial",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        color: "#FFFFFF"
    },
    itemContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        paddingBottom: 5
    },
    labelContainer: {
        justifyContent: 'center',
        width: '50%'
    },
    textInputContainer: {
        justifyContent: 'center',
        width: '50%'
    }
});
