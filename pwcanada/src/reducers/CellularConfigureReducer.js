const INITIAL_STATE = {
    OldActive_SIM: '',
    Active_SIM: '',
    OldCarrier: '',
    OldAPN: '',
    OldUser: '',
    OldPassword: '',
    OldInternalCarrier: '',
    Carrier: '',
    APN: '',
    User: '',
    Password: '',
    InternalCarrier: '',
    isUpdateRequired: false,
    fetchCellularData: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'oldValueCellularActiveSIM':
            return { ...state, OldActive_SIM: action.payload }

        case 'UpdateRequired':

            return { ...state, isUpdateRequired: true }

        case 'UpdateNotRequired':

            return { ...state, isUpdateRequired: false }

        case 'oldValueCellularCarrier':

            return { ...state, OldCarrier: action.payload }

        case 'oldValueCellularAPN':

            return { ...state, OldAPN: action.payload }

        case 'oldValueCellularUser':
            return { ...state, OldUser: action.payload }

        case 'oldValueCellularPassword':

            return { ...state, OldPassword: action.payload }

        case 'oldValueCellularInternalCarrier':

            return { ...state, OldInternalCarrier: action.payload }

        case 'newValueCellularActiveSIM':
            return { ...state, Active_SIM: action.payload }

        case 'newValueCellularCarrier':

            return { ...state, Carrier: action.payload }

        case 'newValueCellularAPN':

            return { ...state, APN: action.payload }

        case 'newValueCellularUser':

            return { ...state, User: action.payload }

        case 'newValueCellularPassword':

            return { ...state, Password: action.payload }

        case 'newValueCellularInternalCarrier':

            return { ...state, InternalCarrier: action.payload }

        case 'FetchCellularData':
            return { ...state, fetchCellularData: true };

        case 'DontFetchCellularData':

            return { ...state, fetchCellularData: false };
        default:
            return state;
    }
};
