const INITIAL_STATE = {
    selectedEngineModule: '',
    fetchEngineData:false,
    gasGenerator:'',
    powerSection:'',
    performanceData:'',
    buildSpec:'',
    isLoading:false,
    sameESN:false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'selectedEngineModule':
       
            return { ...state, selectedEngineModule: action.payload };

        case 'FetchEngineData':
          
            return { ...state, fetchEngineData: true };

        case 'DontFetchEngineData':
           
            return { ...state, fetchEngineData: false };

        case 'StartLoader':
     
            return { ...state, isLoading: true };

        case 'StopLoader':
 
            return { ...state, isLoading: false };

        case 'DifferentESN':
          
            return { ...state, sameESN: false };

        case 'SameESN':
         
            return { ...state, sameESN: true };


        default:
            return state;

    }
}
