import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, Dimensions } from 'react-native';
import {
    Text,
    Content,
    Card,
} from 'native-base';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import { _, keys, values } from 'underscore';
import { UpdateSuccessNotifier } from '../../../common';
import { connect } from 'react-redux';
import { FetchTrim } from '../../../../actions';

let { width, height } = Dimensions.get('window');
var reportList = []
var displayDataList = []
var isUpdateFailed = false

export class TrimReport extends Component {
    constructor(props) {
        super(props)

        this.state = {

            isVisible: false,
            loading: false,
            showTaskCard: false
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    }

    customBackHandler = () => {
        this.props.navigation.navigate('Trim')
        this.props.FetchTrim()
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    async UNSAFE_componentWillMount() {
        var displayData = []
        reportList = this.props.navigation.state.params.param;
        if (reportList.some(e => e.disp_name === " Temporary Trim Item")) {
            this.setState({ showTaskCard: true })

        }
        displayData = _.groupBy(reportList, reportList => reportList.disp_name);
        var paramNames = keys(displayData)
        var channelValues = values(displayData)

        for (var i = 0; i < paramNames.length; i++) {
            if (!channelValues[i][0].status.includes("Success"))
                isUpdateFailed = true
            if (channelValues[i].length > 1) {
                let channelA = channelValues[i][0].channel === "A" ? channelValues[i][0].value : channelValues[i][1].value
                let colorA = channelValues[i][0].channel === "A" ? channelValues[i][0].color : channelValues[i][1].color

                let channelB = channelValues[i][0].channel === "B" ? channelValues[i][0].value : channelValues[i][1].value
                let colorB = channelValues[i][0].channel === "A" ? channelValues[i][0].color : channelValues[i][1].color
                displayDataList.push({
                    // Name: channelValues[i][0].Name,
                    ChannelA: channelA,
                    ChannelB: channelB,
                    disp_name: channelValues[i][0].disp_name,
                    unit: channelValues[i][0].unit,
                    colorA: colorA,
                    colorB: colorB
                })
            }
            else {
                if (!channelValues[i][0].status.includes("Success"))
                    isUpdateFailed = true
                let channelA = channelValues[i][0].channel === "A" ? channelValues[i][0].value : "-"
                let colorA = channelValues[i][0].channel === "A" ? channelValues[i][0].color : "#000000"

                let channelB = channelValues[i][0].channel === "B" ? channelValues[i][0].value : "-"
                let colorB = channelValues[i][0].channel === "B" ? channelValues[i][0].color : "#000000"
                displayDataList.push({
                    // Name: channelValues[i][0].Name,
                    ChannelA: channelA,
                    ChannelB: channelB,
                    disp_name: channelValues[i][0].disp_name,
                    unit: channelValues[i][0].unit,
                    colorA: colorA,
                    colorB: colorB
                })
            }
        }
    }

    componentWillUnmount() {
        displayDataList = [];
        isUpdateFailed = false;
    }

    renderContent = () => (

        <View style={{ height: '70%', backgroundColor: '#FFFFFF', padding: 10, borderRadius: 5, justifyContent: 'center' }}>

            <View style={{ paddingLeft: 5, paddingTop: 10, paddingRight: 5 }}>
                <Text style={[styles.viewTaskCardText, { textAlign: 'justify', paddingBottom: 10, fontSize: 15 }]}>In order to successfully update trims, please complete the following steps in the cockpit:</Text>

                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>1. </Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}>PLA</Text>to
                            <Text style={{ fontFamily: 'Arial', color: '#5B6376' }}>IDLE</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>2. </Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}>RUN/OFF</Text>switch to
                            <Text style={{ fontFamily: 'Arial', color: '#34CB81' }}>RUN</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>3. </Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}>RUN/OFF</Text>switch to
                            <Text style={{ fontFamily: 'Arial', color: '#EB1616' }}>OFF</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>4. </Text>
                        <Text style={[styles.viewTaskCardText]}>Move the
                        <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}>PLA</Text>to Max Take-Off within 5 seconds
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>5. </Text>
                        <Text style={[styles.viewTaskCardText]}>Move the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}>PLA</Text>to Max reverse within 5 seconds
                            </Text>
                    </View>
                </View>

                <Text style={[styles.viewTaskCardText, { textAlign: 'justify', paddingBottom: 10, fontSize: 15, paddingTop: 10 }]}>Please also confirm that the trim on the maintenance page is the same as the one entered from the DCTU.</Text>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 15 }}>
                    <TouchableOpacity
                        id="Button" Title='Submit' onPress={() => this.setState({ isVisible: false })}
                        style={styles.updateButton}>
                        <Text style={styles.updateLabel}>OK</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )

    render() {

        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <Content>
                    <View>
                        {isUpdateFailed ?
                            <View>
                                <UpdateFailedNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text></Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View> :
                            <View>
                                <UpdateSuccessNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text></Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>
                    <View>
                        {this.state.showTaskCard ?
                            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                                <Text></Text>
                                <Text style={styles.updatedate}>Approve in the cockpit to complete the ITT Trim Update</Text>
                                <Text></Text>
                                <TouchableOpacity onPress={() => { this.setState({ isVisible: true }) }}>
                                    <Text style={styles.viewTaskCard}>View Task Card</Text>
                                </TouchableOpacity>
                                <Modal isVisible={this.state.isVisible}>
                                    {this.renderContent()}
                                </Modal>
                            </View>
                            : <View></View>
                        }
                    </View>
                    <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                        <Text></Text>
                        <Text></Text>
                        <Text style={styles.trimValuesLastUpd}>Trim Values</Text>
                        <Text style={styles.time}>Last updated on {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(Date.now())}EST</Text>
                    </View>
                    <Card>
                        <View
                            style={{
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingBottom: 10,
                                paddingTop: 10,
                                backgroundColor: '#FFFFFF',
                            }}>
                            <View style={{ width: '40%' }}>
                                <Text style={{ paddingLeft: 18, fontSize: 16, color: '#3E485E', fontFamily: 'Arial', flex: 1 }}></Text>
                            </View>
                            <View style={{ width: '20%' }}>
                                <Text style={[styles.channel]}>ChannelA</Text>
                            </View>
                            <View style={{ width: '20%' }}>
                                <Text style={[styles.channel]}>ChannelB</Text>
                            </View>
                        </View>
                        <View >
                            <View style={{ paddingLeft: 8 }}>
                                {displayDataList.map((item) => {
                                    let color = item.color
                                    let unit = item.unit
                                    let disp_name = item.disp_name
                                    if (disp_name === " Temporary Trim Item") {
                                        disp_name = "ITT TRIM"
                                    }
                                    if (item.unit.match("NA")) {

                                        unit = ""
                                    }
                                    return (

                                        <View style={{
                                            flexDirection: 'row',
                                            backgroundColor: '#FFFFFF',
                                            alignItems: 'center',
                                            width: '100%',
                                            paddingLeft: 5,
                                            paddingBottom: 5,
                                            paddingTop: 5
                                        }}>

                                            <View style={{ width: '50%' }}>
                                                <Text style={[styles.label1, { textAlign: 'left' }]}>{disp_name}</Text>
                                            </View>

                                            <View style={{ width: '15%', marginLeft: 5, marginRight: 10 }}>
                                                <Text style={[styles.label2, { textAlign: 'left', color: item.colorA }]}>{item.ChannelA}</Text>
                                            </View>

                                            <View style={{ width: '15%' }}>
                                                <Text style={[styles.label2, { textAlign: 'left', color: item.colorB }]}>{item.ChannelB}</Text>
                                            </View>

                                            <View style={{ width: '20%', paddingLeft: 5 }}>
                                                <Text style={[styles.unit, { textAlign: 'left' }]}>{unit}</Text>
                                            </View>
                                        </View>
                                    )
                                })}
                            </View>
                        </View>
                    </Card>
                    <TouchableOpacity
                        style={styles.backToDashboardButton}
                        onPress={() => {
                            this.props.navigation.navigate('Dashboard')
                        }}
                    >
                        <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        )
    }
}

export default connect(null, { FetchTrim })(TrimReport)

const styles = StyleSheet.create({
    
    textStyle: {
        height: 40,
        textAlign: 'left'
    },
    updatebox: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 10,
        paddingTop: 10,
        backgroundColor: '#FFFFFF'
    },
    updatedate: {
        height: 63,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#5B6376",
        paddingTop: 15
    },
    channel: {
        width: 54,
        height: 14,
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376"
    },
    label: {
        width: 120,
        height: 40,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0,
        color: "#5B6376",
        flex: 3
    },
    unit: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376"
    },
    time: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376"
    },
    trimValuesLastUpd: {
        fontFamily: "Arial",
        fontSize: 20,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: '#1F2A44'
    },
    viewTaskCardText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 16 : 20,
        letterSpacing: 0,
        color: "#1F2A44"
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    backToDashboardButton: {
        fontSize: width < 450 ? 16 : 22,
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    viewTaskCard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    lastUpdateTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#1F2A44"
    },
})
