const INITIAL_STATE = {
    OldAircraft_SN: '',
    OldAircraft_Type: '',
    OldFleet_Ident: '',
    OldAircraft_Number: '',
    OldTail_Number: '',
    OldAirline_Operator: '',
    OldAircraft_Owner: '',
    NewAircraft_SN: '',
    NewAircraft_Type: '',
    NewFleet_Ident: '',
    NewAircraft_Number: '',
    NewTail_Number: '',
    NewAirline_Operator: '',
    NewAircraft_Owner: '',
    fetchAircraftData: false,
    isUpdateRequired: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'oldValueAircraft_SN':
            return { ...state, OldAircraft_SN: action.payload }

        case 'oldValueAircraft_Type':

            return { ...state, OldAircraft_Type: action.payload }

        case 'oldValueFleet_Ident':

            return { ...state, OldFleet_Ident: action.payload }

        case 'oldValueAircraft_Number':
            return { ...state, OldAircraft_Number: action.payload }

        case 'oldValueTail_Number':

            return { ...state, OldTail_Number: action.payload }


        case 'oldValueAirline_Operator':
            return { ...state, OldAirline_Operator: action.payload }


        case 'oldValueAircraft_Owner':

            return { ...state, OldAircraft_Owner: action.payload }

        //New
        case 'newValueAircraft_SN':
            return { ...state, NewAircraft_SN: action.payload }

        case 'newValueAircraft_Type':

            return { ...state, NewAircraft_Type: action.payload }

        case 'newValueFleet_Ident':

            return { ...state, NewFleet_Ident: action.payload }

        case 'newValueAircraft_Number':
            return { ...state, NewAircraft_Number: action.payload }

        case 'newValueTail_Number':

            return { ...state, NewTail_Number: action.payload }

        case 'newValueAirline_Operator':
            return { ...state, NewAirline_Operator: action.payload }

        case 'newValueAircraft_Owner':

            return { ...state, NewAircraft_Owner: action.payload }

        case 'AircraftUpdateRequired':

            return { ...state, isUpdateRequired: true }

        case 'AircraftUpdateNotRequired':

            return { ...state, isUpdateRequired: false }

        case 'FetchAircraftData':

            return { ...state, fetchAircraftData: true };

        case 'DontFetchAircraftData':

            return { ...state, fetchAircraftData: false };

        default:

            return state;
    }
};
