const INITIAL_STATE = {
    selectedLRU: '',
    PNValue: '',
    SNValue: '',
    LRUFetch: false,
    isScanSuccess: false,
    isUpdateRequired: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'selectedLRU':

            return { ...state, selectedLRU: action.payload };

        case 'PNtoUpdate':

            return { ...state, PNValue: action.payload };

        case 'SNtoUpdate':

            return { ...state, SNValue: action.payload };

        case 'ValvesDetectors':

            return { ...state, SNValue: action.payload };

        case 'ReFetch':

            return { ...state, FetchLRU: action.payload };

        case 'DontReFetch':

            return { ...state, FetchLRU: action.payload };

        case 'scanSuccess':

            return { ...state, isScanSuccess: action.payload };

        case 'scanFailed':

            return { ...state, isScanSuccess: action.payload };

        case 'enableUpdate':

            return { ...state, isUpdateRequired: action.payload };

        case 'disableUpdate':

            return { ...state, isUpdateRequired: action.payload };

        default:
            return state;
    }
};
