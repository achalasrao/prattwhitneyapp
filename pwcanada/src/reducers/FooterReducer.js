import { FOOTER_TAB_FOCUSSED, CONF_NAME_CHANGED, CONF_SSID_CHANGED, CONF_MAC_CHANGED, CONF_SECURITY_CHANGED, CONF_KEY_CHANGED, CONF_WIFI_LIST, AVA_WIFI_LIST, ADD_CONFIGURATION, ADD_CONFIGURATION_SUCCESS } from '../actions/types';

const INITIAL_STATE = {
  name: '',
  ssid: '',
  mac: '',
  security: '',
  keys: '',
  confwifilist: '',
  avawifilist: '',
  ConfigureApFocussed: true,
  AddConfigurationFocussed: false,
  AvailableApFocussed: false,
  text: null,
  error: '',
  loading: false,
  AddToConf: '',
  updateConfiguration: '',
  configurationFocus: '',
  allconfigured: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case CONF_WIFI_LIST:
      return { ...state, confwifilist: action.payload };

    case AVA_WIFI_LIST:
      return { ...state, avawifilist: action.payload };

    case CONF_NAME_CHANGED:
      return { ...state, name: action.payload };

    case CONF_SSID_CHANGED:
      return { ...state, ssid: action.payload };

    case CONF_MAC_CHANGED:
      return { ...state, mac: action.payload };

    case CONF_SECURITY_CHANGED:
      return { ...state, security: action.payload };

    case CONF_KEY_CHANGED:
      return { ...state, keys: action.payload };

    case ADD_CONFIGURATION:
      return { ...state, loading: true, error: '' };

    case ADD_CONFIGURATION_SUCCESS:
      return { ...state, ...INITIAL_STATE, text: action.payload };

    case 'InitialStatecalling':
      return { ...INITIAL_STATE }

    case 'UpdateConfigured':
      return { ...state, updateConfiguration: action.payload }

    case "AllconfiguredNetworks":
      return { ...state, allconfigured: action.payload }

    case 'configurationFoccussed':
      return { ...state, configurationFocus: action.payload }

    case 'AddToConf':
      return { ...state, AddToConf: action.payload };

    default:
      return state;
  }
};
