import React from "react";
import { StyleSheet, Dimensions, View, Text, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import Pdf from "react-native-pdf";

export default class UserManual extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state

    return {
      headerLeft: (
        <View style={{ marginLeft: 10 }}>
          <TouchableOpacity
            onPress={() => { params.backHandler() }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
              <Icon name='ios-arrow-back' style={{ fontSize: 30, color: '#000' }} />
              <Text style={{ color: '#000', fontSize: 20 }}> Back</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
  }

  customBackHandler = () => {

    this.props.navigation.navigate('Dashboard')

  }

  async  componentDidMount() {

    this.props.navigation.setParams({
      backHandler: this.customBackHandler
    });

  }

  render() {
    const source = require("../assets/UserManual.pdf");

    return (
      <View style={styles.container}>
        <Pdf
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {

          }}
          onPageChanged={(page, numberOfPages) => {

          }}
          onError={error => {

          }}
          style={styles.pdf}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width,
    padding: 5
  }
});
