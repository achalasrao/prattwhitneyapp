//LCF 
export const FetchLCFData = () => {
    return {
        type: 'FetchLCFData',
    }
};

export const DontFetchLCFData = () => {

    return {
        type: 'DontFetchLCFData',
    }
};

//Creep 
export const FetchCreepData = () => {
    return {
        type: 'FetchCreepData',
    }
};

export const DontFetchCreepData = () => {

    return {
        type: 'DontFetchCreepData',
    }
};

//EngineUsage 
export const FetchEngineUsageData = () => {
    return {
        type: 'FetchEngineUsageData',
    }
};

export const DontFetchEngineUsageData = () => {

    return {
        type: 'DontFetchEngineUsageData',
    }
};
