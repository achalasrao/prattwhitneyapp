export const OldengineBuildSpec = (item) => {

    return {
        type: 'OldengineBuildSpec',
        payload: item
    }
};
export const updateEngineBuildSpec = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'updateEngineBuildSpec',
        payload: item
    }
};

// // ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// // Update Actions

export const _UpdateEngineBuildSpec = () => {

    return {
        type: '_UpdateEngineBuildSpec',
    }
};

export const DontUpdateEngineBuildSpec = () => {

    return {
        type: 'DontUpdateEngineBuildSpec',
    }
};

export const EngineBuildSpecUpdateSuccess = () => {

    return {
        type: 'EngineBuildSpecUpdateSuccess',
    }
};

export const EngineBuildSpecUpdateFailed = () => {

    return {
        type: 'EngineBuildSpecUpdateFailed',
    }
};
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Scan Actions

export const EngineBuildSpecScanSuccess = () => {

    return {
        type: 'EngineBuildSpecScanSuccess',
    }
};

export const EngineBuildSpecScanFailed = () => {

    return {
        type: 'EngineBuildSpecScanFailed',
    }
};
