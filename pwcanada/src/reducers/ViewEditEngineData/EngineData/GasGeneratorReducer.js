const INITIAL_STATE = {
    oldEngineModel: '',
    oldEngineSerialNumber: '',
    oldModuleSerialNumber: '',
    oldTakeOffDry: '',
    oldDOTTypeCert: '',
    oldFAATypeCert: '',
    engineModel: '',
    engineSerialNumber: '',
    moduleSerialNumber: '',
    takeOffDry: '',
    DOTTypeCert: '',
    FAATypeCert: '',
    gasGeneratorUpdate: false,
    scannedGasGenerator: false,
    updatedGasGenerator: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'OldEngineModel':

            return { ...state, oldEngineModel: action.payload };

        case 'OldEngineSerialNumber':

            return { ...state, oldEngineSerialNumber: action.payload };

        case 'OldModuleSerialNumber':

            return { ...state, oldModuleSerialNumber: action.payload };

        case 'OldTakeOffDry':

            return { ...state, oldTakeOffDry: action.payload };

        case 'OldDOTTypeCert':

            return { ...state, oldDOTTypeCert: action.payload };

        case 'OldFAATypeCert':

            return { ...state, oldFAATypeCert: action.payload };

        case 'UpdateEngineModel':

            return { ...state, engineModel: action.payload };

        case 'UpdateEngineSerialNumber':

            return { ...state, engineSerialNumber: action.payload };

        case 'UpdateModuleSerialNumber':

            return { ...state, moduleSerialNumber: action.payload };

        case 'UpdateTakeOffDry':

            return { ...state, takeOffDry: action.payload };

        case 'UpdateDOTTypeCert':

            return { ...state, DOTTypeCert: action.payload };

        case 'UpdateFAATypeCert':

            return { ...state, FAATypeCert: action.payload };

        case 'UpdateGasGenerator':

            return { ...state, gasGeneratorUpdate: true };

        case 'DontUpdateGasGenerator':

            return { ...state, gasGeneratorUpdate: false };

        case 'GasGeneratorUpdateSuccess':

            return { ...state, updatedGasGenerator: true };

        case 'GasGeneratorUpdateFailed':

            return { ...state, updatedGasGenerator: false };

        case 'GasGeneratorScanSuccess':

            return { ...state, scannedGasGenerator: true };

        case 'GasGeneratorScanFailed':

            return { ...state, scannedGasGenerator: false };

        default:
            return state;
    }
}
