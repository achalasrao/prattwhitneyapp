import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    Dimensions
} from 'react-native';
import {
    Content,
    Card,
    Input,
    Text
} from 'native-base';
import { connect } from 'react-redux';
import { _, keys, values } from 'underscore';
import Modal from 'react-native-modal';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import { FetchEOFData, DontFetchEOFData } from '../../../../actions';
import OctIcon from "react-native-vector-icons/Octicons";

let { width, height } = Dimensions.get('window');
let parseString = require('react-native-xml2js').parseString;
var borderList = Array();
var dataList = [];


export class EOF extends Component {
    constructor(props) {
        super(props)

        this.state = {
            items: [],
            userDataSourceTrim: [],
            userDataSourceEUHD: [],
            userDataSourceCreep: [],
            userDataSourceLCF: [],
            resultList: [],
            Parameter: [],
            DataSection: [],
            check: true,
            loading: false,
            refreshing: false,
            toggleDropdown: true,
            selectedParam: undefined,
            changedValue: undefined,
            oldValue: undefined,
            position: 'top',
            valueChanged: false,
            fieldChanged: false,
            isVisible: false,
            isModalVisible: false
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}>Back</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }

    customBackHandler = () => {
        if (this.state.fieldChanged) {
            Alert.alert(
                'There are unsaved changes',
                'Do you want to continue?',
                [
                    {
                        text: 'Yes', onPress: () => {
                            this.props.navigation.navigate('Dashboard')
                        }
                    },
                    {
                        text: 'No', onPress: () => {

                        }
                    }
                ],
                { cancelable: false }
            )
        } else {
            this.props.navigation.navigate('Dashboard')
        }
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    async UNSAFE_componentWillMount() {
        dataList = [];

        await this.fetchData()
    }

    async componentDidUpdate() {
        if (this.props.fetchEOFData) {
            await this.fetchData();
        }
    }

    async fetchData() {
        this.props.DontFetchEOFData();
        this.setState({ loading: true });
        var EngineDataPush = [];
        var EngineData;

        var paramNAme = ["Creep", "EUHD", "Trim", "LCF"]
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "xml_eof_access",
                        "arg1": "read"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response
            let responseJson = await response.data[0].value;

            parseString(responseJson, function (err, result) {

                EngineData = result.MicroFAST_EndOfFlightData.Engine;
                EngineData.map((item) => {
                    EngineDataPush.push(item);
                })
            });

            var param = EngineData[0];

            let dataSectionList = keys(param)

            let parameterList = values(param)

            for (var i = 0; i < dataSectionList.length; i++) {
                let flightDAta = []
                let flightDataNew = []
                var displayData = []

                for (var j = 0; j < parameterList[i].length; j++) {
                    let param = values(parameterList[i][j])

                    Object.keys(param).map(function (key, index) {

                        let a = param[key]
                        let b = values(a[0])
                        let parameter = a[0].parameter[0]

                        dataList.push({
                            DataSection: dataSectionList[i],
                            Parameter: parameter,
                            Value: a[0].value[0],
                            Channel: parameter.slice(- 1)
                        })
                        flightDAta.push({
                            DataSection: dataSectionList[i],
                            Parameter: parameter.substr(0, parameter.lastIndexOf("_")),
                            Value: a[0].value,
                            color: "#000000",
                            channel: parameter.slice(- 1)
                        })
                    });
                }

                displayData = _.groupBy(flightDAta, flightDAta => flightDAta.Parameter);
                var paramNames = keys(displayData)
                var channelValues = values(displayData)

                var rowNum = 0
                for (var k = 0; k < paramNames.length; k++) {
                    //  console.log("Values",channelValues[k][0].Value[0], parseInt(channelValues[k][1].Value),channelValues[k][0].Value[0]==channelValues[k][1].Value[0]? "ssame" : "different"  )

                    if (!(channelValues[k][0].Parameter.includes("STD_TRIM")) && !(channelValues[k][0].Parameter.includes("Q_GAIN_TRM")) && !(channelValues[k][0].Parameter.includes("Q_BIAS_TRM"))) {


                        flightDataNew.push({
                            Name: channelValues[k][0].Parameter,
                            oldchannelA: channelValues[k][0].Value[0],
                            oldchannelB: channelValues[k][1].Value[0],
                            state: channelValues[k][0].Value[0] == channelValues[k][1].Value[0] ? "same" : "different",
                            newChannelA: "",
                            newChannelB: "",
                            isRowAChanged: false,
                            isRowBChanged: false,
                            isRowAFocussed: false,
                            isRowBFocussed: false,
                            rowNum: rowNum,
                            isRowAupdated: "",
                            isRowBupdated: "",
                        })
                        rowNum++
                    }


                }
                // console.log("before", dataList)
                // dataList = dataList.filter(d=>!(d.Parameter.includes("STD_TRIM"))&&!(d.Parameter.includes("Q_GAIN_TRM"))&&!(d.Parameter.includes("Q_BIAS_TRM")))
                // flightDataNew = flightDataNew.filter(d=>!(d.Name.includes("STD_TRIM"))&&!(d.Name.includes("Q_GAIN_TRM"))&&!(d.Name.includes("Q_BIAS_TRM")))
                // console.log("after",dataList )
                this.storeValues(dataSectionList[i], flightDataNew)
            }
        } catch (error) {

        }

        if (this.state.userDataSourceCreep.length > 0 || this.state.userDataSourceEUHD.length > 0 || this.state.userDataSourceLCF.length > 0 || this.state.userDataSourceTrim) {
            this.setState({ loading: false })
        }
        return;
    }
    storeValues(data, flightDAta) {
        //  console.log("flightdata", flightDAta, flightDAta.filter(d=>d.Name.includes("STD_TRIM")||d.Name.includes("Q_GAIN_TRM")||d.Name.includes("Q_BIAS_TRM")))
        switch (data) {
            case "EUHD":
                this.setState({ userDataSourceEUHD: flightDAta });

            case "Trim":
                // flightDAta = flightDAta.filter(d=>!(d.Name.includes("STD_TRIM"))&&!(d.Name.includes("Q_GAIN_TRM"))&&!(d.Name.includes("Q_BIAS_TRM")))
                this.setState({ userDataSourceTrim: flightDAta });

                return;
            case "Creep":
                this.setState({ userDataSourceCreep: flightDAta });
                return;
            case "LCF":
                this.setState({ userDataSourceLCF: flightDAta });
                return;
            default:
                return;
        }
    }


    async isFocussed(channelSelected, tabs, data) {
        borderList = data
        if (channelSelected != null && tabs.rowNum != null) {
            var rowNumSelected = tabs.rowNum
            if (tabs.state === "same") {
                borderList[rowNumSelected].isRowAFocussed = true
                borderList[rowNumSelected].isRowBFocussed = true
                this.setState({ rowInfo: rowNumSelected })
            } else {
                if (channelSelected === "A") {

                    borderList[rowNumSelected].isRowAFocussed = true

                    this.setState({ rowInfo: rowNumSelected })
                } else {

                    borderList[rowNumSelected].isRowBFocussed = true

                    this.setState({ rowInfo: rowNumSelected })
                }
            }
        }
    }

    async  unFocussed(channelSelected, tabs) {

        if (channelSelected != null && tabs.rowNum != null) {
            var rowNumSelected = tabs.rowNum

            if (tabs.state === "same") {
                borderList[rowNumSelected].isRowAFocussed = false
                borderList[rowNumSelected].isRowBFocussed = false
                if (borderList[rowNumSelected].isRowAChanged === true && borderList[rowNumSelected].isRowBChanged === true) {
                    this.storeChangeInfo(2, rowNumSelected, channelSelected)
                }

            } else {
                if (channelSelected === "A") {
                    borderList[rowNumSelected].isRowAFocussed = false
                    if (borderList[rowNumSelected].isRowAChanged === true) {
                        this.storeChangeInfo(1, rowNumSelected, channelSelected)
                    }

                } else if (channelSelected === "B") {

                    borderList[rowNumSelected].isRowBFocussed = false
                    if (borderList[rowNumSelected].isRowBChanged === true) {
                        this.storeChangeInfo(1, rowNumSelected, channelSelected)
                    }
                }
            }
        }
    }

    storeChangeInfo(n, rowNumSelected, channelSelected) {
        console.log("selected", n, channelSelected, rowNumSelected)
        let res = []
        res = dataList.filter(d => (d.Parameter === borderList[rowNumSelected].Name + "_A") || (d.Parameter === borderList[rowNumSelected].Name + "_B"));
        let updateList = []
        console.log("res", res, updateList, dataList.length)
        if (n == 2) {

            updateList = dataList.filter(d => (d.Parameter !== borderList[rowNumSelected].Name + "_A") && (d.Parameter !== borderList[rowNumSelected].Name + "_B"));
            updateList.push({
                DataSection: res[0].DataSection,
                Parameter: res[0].Parameter,
                Value: borderList[rowNumSelected].newChannelA,
                Channel: "A",
            })
            updateList.push({
                DataSection: res[1].DataSection,
                Parameter: res[1].Parameter,
                Value: borderList[rowNumSelected].newChannelB,
                Channel: "B",
            })
        }
        else {

            if (borderList[rowNumSelected].isRowAChanged === true && channelSelected === "A") {
                res = res.filter(d => d.Channel.match("A"));
                updateList = dataList.filter(d => (d.Parameter !== borderList[rowNumSelected].Name + "_A"));
                console.log("updateList if", updateList)
                updateList.push({
                    DataSection: res[0].DataSection,
                    Parameter: res[0].Parameter,
                    Value: borderList[rowNumSelected].newChannelA,
                    Channel: "A",

                })
            } else if (borderList[rowNumSelected].isRowBChanged === true && channelSelected === "B") {
                res = res.filter(d => d.Channel.match("B"));
                updateList = dataList.filter(d => (d.Parameter !== borderList[rowNumSelected].Name + "_B"));
                console.log("updateList elss", updateList)
                updateList.push({
                    DataSection: res[0].DataSection,
                    Parameter: res[0].Parameter,
                    Value: borderList[rowNumSelected].newChannelB,
                    Channel: "B",
                })
            }
        }
        console.log("before", updateList.length, dataList.length)
        dataList = updateList
        console.log("after", updateList.length, dataList.length)
    }

    async  onValueChange(tabs, channel, i, value) {

        if (tabs.state === "different") {

            if (channel === "A") {
                borderList[i].isRowAupdated = value
                borderList[i].isRowBupdated = ""
                borderList[i].isRowAChanged = true
                borderList[i].newChannelA = value
            } else {
                borderList[i].isRowAupdated = ""
                borderList[i].isRowBupdated = value
                borderList[i].isRowBChanged = true
                borderList[i].newChannelB = value
            }
        } else {
            borderList[i].isRowAupdated = value
            borderList[i].isRowBupdated = value
            borderList[i].isRowAChanged = true
            borderList[i].isRowBChanged = true
            borderList[i].newChannelA = value
            borderList[i].newChannelB = value
        }

        if (value != undefined) {
            this.setState({ isUpdateRequire: true })
        }
    }

    async onLockPress(tabs) {

        var num = tabs.rowNum
        if (borderList[num].isRowAChanged === false && borderList[num].isRowBChanged === false) {

        } else {

            if (borderList[num].state === "different") {

                if (tabs.isRowAupdated === "") {
                    borderList[num].newChannelA = tabs.isRowBupdated
                    borderList[num].newChannelB = tabs.isRowBupdated
                    borderList[num].isRowAFocussed = true
                    borderList[num].isRowAChanged = true
                    borderList[num].state = "same"
                    this.setState({ isUnlock: true })
                } else {
                    borderList[num].newChannelA = tabs.isRowAupdated
                    borderList[num].newChannelB = tabs.isRowAupdated
                    borderList[num].isRowBFocussed = true
                    borderList[num].isRowBChanged = true
                    borderList[num].state = "same"
                    this.setState({ isUnlock: true })
                }
                this.storeChangeInfo(2, num, "AB")

            } else {
                let res = dataList.filter(d => d.Parameter.match(borderList[num].Name));

                if (borderList[num].state === "same") {
                    dataList = dataList.filter(d => !d.Parameter.match(borderList[num].Name));
                    dataList.push({
                        DataSection: res[0].DataSection,
                        Parameter: res[0].Parameter,
                        Value: borderList[num].newChannelA,
                        Channel: "A",
                    })
                    dataList.push({
                        DataSection: res[1].DataSection,
                        Parameter: res[1].Parameter,
                        Value: borderList[num].newChannelB,
                        Channel: "B",
                    })
                }

                borderList[num].state = "different"
                this.setState({ isUnlock: true })
            }
        }
    }


    async onSubmit(event) {

        event.preventDefault();

        Alert.alert(
            'You are about to load this data to the EEC',
            'This will permanently erase all previous data',

            [
                // { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                { text: 'Previous', onPress: () => { } },
                { text: 'Confirm', onPress: () => this.updateValue() },
            ],
            { cancelable: true }
        )
    }

    //the following function updates all the values enetered by the user after confirmation.
    async  updateValue() {
        console.log("datalist ", dataList)
        this.setState({ loading: true, isModalVisible: false })


        for (var i = 0; i < dataList.length; i++) {

            var paramName = JSON.stringify("--field=" + dataList[i].Parameter)
            var changedValue = JSON.stringify("--value=" + dataList[i].Value)
            let arg2 = JSON.stringify("Engine[@ID=1]/" + dataList[i].DataSection + "/" + dataList[i].Parameter + "/value")
            let arg3 = JSON.stringify(dataList[i].Value)
            console.log("paramName", arg2)
            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "epecs_trim",
                            "arg1": paramName,
                            "arg2": changedValue
                        },
                        {
                            "cmd": "xml_eof_access",
                            "arg1": "write",
                            "arg2": arg2,
                            "arg3": dataList[i].Value
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })

                let result = response.data;

                console.log("result******", result)


            } catch (error) {

                //retry the update command  in case of network error
                j--;
            }


        }//end for -i

        //Display the update status report

        //clear the list 
        UpdatedData = []

        this.writeToXml()

        return
    }

    writeToXml = async () => {
        let paramName
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_eof_access",
                        "arg1": "write"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            let responseJson = await response;

        }
        catch (error) {

        }
        this.props.navigation.navigate('EOFReport')
    }

    renderModal = () => {

        return (
            <View style={styles.modalStyle}>
                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={styles.modalHeader}>You are about to load this data to the EEC</Text>
                </View>
                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>This will permanently erase all previous data</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 13 }}>
                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this.setState({ isModalVisible: false })}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.updateValue()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderList(dataSection, data) {

        return (
            <View style={styles.tileStyle}>

                <View style={{ paddingLeft: 8 }}>
                    <Text style={styles.editEngineD}>{dataSection}</Text>

                    <Content contentContainerStyle={{ marginTop: 15 }}>
                        <Card style={{ backgroundColor: '#edeff0', }}>

                            <View
                                style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end',
                                    alignItems: 'flex-end',
                                    paddingBottom: 10,
                                    paddingTop: 10,
                                    backgroundColor: '#FFFFFF',
                                }}>
                                <View style={{ width: '25%' }}>
                                    <Text style={[styles.channel]}>Channel A</Text>
                                </View>
                                <View style={{ width: '28%' }}>
                                    <Text style={[styles.channelB]}>Channel B</Text>
                                </View>
                            </View>

                            <View>
                                {data.map((tabs) => {

                                    return (
                                        <View key={tabs.Name} style={{ flexDirection: 'row', backgroundColor: 'white', alignItems: 'center', width: '100%', paddingLeft: 5, paddingBottom: 5, paddingTop: 5 }}>

                                            <View style={{ width: '45%' }}>
                                                <Text style={styles.label}>{tabs.Name}</Text>
                                            </View>

                                            <View style={{ width: width < 450 ? "18%" : "15%"  }}>
                                                <Input
                                                    keyboardType="numeric"
                                                    returnKeyType='done'
                                                    editable={tabs.isEditable}

                                                    value={tabs.isRowAFocussed === true ? tabs.newChannelA : tabs.isRowAChanged === true ? tabs.newChannelA : tabs.isRowAChanged == true ? tabs.newChannelA : tabs.oldchannelA}
                                                    onFocus={() => this.isFocussed("A", tabs, data)}
                                                    onBlur={() => this.unFocussed("A", tabs)}
                                                    style={{
                                                        fontSize: 18,
                                                        color: "#848A98",
                                                        fontFamily: 'Arial', borderRadius: 10, paddingLeft: 5,
                                                        borderColor: tabs.state === "same" ? '#000000' : '#EB1616',
                                                        borderWidth: 1,
                                                        shadowColor: this.state.fieldFocused ? '#00A9E0' : '#FFFFFF',
                                                        shadowOffset: { width: 0, height: 2 },
                                                        shadowOpacity: 0.8,
                                                        shadowRadius: 2,
                                                        textAlign: 'left',
                                                        backgroundColor: 'white'
                                                    }}
                                                    onChangeText={this.onValueChange.bind(this, tabs, "A", tabs.rowNum)}
                                                    autoCorrect={false}
                                                    autoCapitalize={'none'}
                                                />
                                            </View>

                                            <View style={{ width: width < 450 ? '15%' : '12.5%' }}>
                                                <View style={{
                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                }}>
                                                    {
                                                        tabs.state === "same" ?

                                                            <TouchableOpacity onPress={() => this.onLockPress(tabs)}>
                                                                <Image
                                                                    style={{
                                                                        width: 30,
                                                                        height: 30
                                                                    }}
                                                                    source={require('../../../../assets/IconPack/EngineUsage/off.png')}
                                                                    resizeMode='contain'
                                                                /></TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => this.onLockPress(tabs)}>

                                                                <Image
                                                                    style={{
                                                                        width: 30,
                                                                        height: 30
                                                                    }}
                                                                    source={require('../../../../assets/IconPack/EngineUsage/on.png')}
                                                                    resizeMode='contain'
                                                                />

                                                            </TouchableOpacity>
                                                    }
                                                </View>

                                            </View>

                                            <View style={{ width:  width < 450 ? "18%" : "15%" }}>

                                                <Input
                                                    keyboardType="numeric"
                                                    returnKeyType='done'
                                                    value={tabs.isRowBFocussed === true ? tabs.newChannelB : tabs.isRowBChanged === true ? tabs.newChannelB : tabs.isRowBChanged == true ? tabs.newChannelB : tabs.oldchannelB}
                                                    onFocus={() => this.isFocussed("B", tabs, data)}
                                                    onBlur={() => this.unFocussed("B", tabs)}
                                                    style={{
                                                        fontSize: 18, color: "#848A98", fontFamily: 'Arial', borderRadius: 10, paddingLeft: 5,
                                                        borderColor: tabs.state === "same" ? '#000000' : '#EB1616',
                                                        borderWidth: 1,
                                                        shadowColor: this.state.fieldFocused ? '#00A9E0' : '#FFFFFF',
                                                        shadowOffset: { width: 0, height: 2 },
                                                        shadowOpacity: 0.8,
                                                        shadowRadius: 2,
                                                        textAlign: 'left',
                                                        backgroundColor: '#FFFFFF'
                                                    }}
                                                    autoCorrect={false}
                                                    autoCapitalize={'none'}
                                                    onChangeText={this.onValueChange.bind(this, tabs, "B", tabs.rowNum)}
                                                />
                                            </View>

                                            <View style={{ width: '15%', paddingLeft: 5 }}>
                                                <Text style={styles.unit}>{tabs.unit}</Text>
                                            </View>
                                            
                                            <View></View>
                                        </View>
                                    )
                                })}
                            </View>

                        </Card>
                    </Content>
                </View>
            </View>

        )
    }
    renderView = () => {

        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <ScrollView>
                <View style={styles.container2}>
                    <View>
                        <OctIcon name="alert" style={styles.alertIcon} />
                    </View>

                    <View style={{ width: '80%', marginLeft: 10, marginRight: 15, }}>
                        <Text style={styles.thisScreenDisplays}>This screen displays EEC data captured at the end of last flight.</Text>
                        <Text></Text>
                        <Text style={styles.thisScreenDisplays}>In case of an EEC replacement,</Text>
                        <Text style={styles.thisScreenDisplays}>use this data to populate the new</Text>
                        <Text style={styles.thisScreenDisplays}>EEC.</Text>
                        <Text></Text>
                        <Text style={styles.thisScreenDisplays}>CAUTION!</Text>
                        <Text style={styles.updatedate}>In case of difference between data on this screen and data in the engine logbook, then<Text style={[styles.updatedate, fontWeight = 'bold']}>data in the engine logbook must be used</Text>to populate the new EEC.</Text>
                    </View>
                </View>
                <View style={styles.tileStyle}>
                    <View style={{ flexDirection: 'row', padding: 14 }}>
                        <Text style={styles.ESN}>ESN </Text>
                        {

                            this.props.engineSerialNumber === 'Failed to fetch' ?
                                <Text style={[styles.ESNValue, { color: '#EB1616' }]}> {this.props.engineSerialNumber}</Text> :
                                <Text style={styles.ESNValue}>PCE-{this.props.engineSerialNumber}</Text>
                        }
                    </View>
                </View>
                <View>
                    {this.renderList("Creep", this.state.userDataSourceCreep)}
                    {this.renderList("Engine Usage", this.state.userDataSourceEUHD)}
                    {this.renderList("LCF", this.state.userDataSourceLCF)}
                    {this.renderList("Trims", this.state.userDataSourceTrim)}
                </View>



                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30, marginLeft: 15, marginRight: 15, marginBottom: 15 }}>

                    <TouchableOpacity
                        onPress={() => this.setState({ isModalVisible: true })}
                        style={[styles.updateButton, {
                            backgroundColor: '#62DBFF'
                        }]}>
                        <Text style={[styles.updateLabel, { color: '#5B6376' }]}>Send to EEC</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Modal
                        style={styles.modalContainer}
                        isVisible={this.state.isModalVisible}
                        onBackdropPress={() => this.setState({ isModalVisible: false })}
                    >{this.renderModal()}</Modal>
                </View>

            </ScrollView>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderView()}
            </View>
        )
    }
}


const mapStateToProps = ({ connection }) => {
    const { engineSerialNumber, fetchEOFData } = connection
    return { engineSerialNumber, fetchEOFData };
};

export default connect(mapStateToProps, { FetchEOFData, DontFetchEOFData })(EOF);

const styles = StyleSheet.create({
    tileStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        flexDirection: 'row',
        padding: 10
    },
    editEngineD: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#000000",
        paddingTop: 13
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0,
        color: "#5B6376",
        textAlign: "left",
        paddingRight: 10 
    },
    labelIcon: {
        fontSize: width < 450 ? 16 : 22,
        opacity: 0.23
    },
    updateButton: {
        width: '100%',
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    channel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376",
        textAlign: "left"
    },
    unit: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376",
    },
    channelB: {
        width: 74,
        height: 14,
        fontFamily: "Arial",
        fontSize: 10,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376",
        textAlign: "left",
        paddingLeft: 18
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        justifyContent: 'center'
    },
    previousLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    confirmButtonStyle: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    confirmLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        width: '100%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        padding: 23
    },
    modalHeader: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 20 : 22,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 20 : 22,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    container2: {
        flexDirection: 'row',
        backgroundColor: '#FFEE17',
        paddingTop: 15,
        justifyContent: 'center',
        paddingBottom: 15,
        paddingLeft: 20
    },
    updatedate: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#1F2A44"
    },
    ESN: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    ESNValue: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    thisScreenDisplays: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0.5,
        color: "#1F2A44"
    },
    alertIcon: {
        fontSize: width < 450 ? 30 : 40,
        color: "#F2CD00"
    },
})
