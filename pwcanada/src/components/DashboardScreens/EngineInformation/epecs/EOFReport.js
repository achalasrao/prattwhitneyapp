import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native'
import { Text } from 'native-base';
import Modal from 'react-native-modal';
import { _, keys, values } from 'underscore'
import IonicIcon from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux';
import { FetchEOFData } from '../../../../actions'
import OctIcon from "react-native-vector-icons/Octicons";

let { width, height } = Dimensions.get('window');

class EOFReport extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isVisible: false,
            loading: false,
            showTaskCard: true
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    customBackHandler = () => {

        this.props.navigation.navigate('EOF')
        this.props.FetchEOFData()
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    renderContent = () => (

        <View style={{ height: "auto", backgroundColor: '#FFFFFF', padding: 10, borderRadius: 5, justifyContent: 'center' }}>
            <View style={{ paddingLeft: 5, paddingTop: 10, paddingRight: 5 }}>
                <Text style={[styles.viewTaskCardText, { textAlign: 'justify', paddingBottom: 10, fontSize: 15 }]}>In order
                to successfully update trims, please complete the following steps in the cockpit:</Text>

                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>1. </Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}> PLA </Text>
                            to
                                <Text style={{ fontFamily: 'Arial', color: '#5B6376' }}> IDLE</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>2. </Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}> RUN/OFF </Text>
                            switch to
                                <Text style={{ fontFamily: 'Arial', color: '#34CB81' }}> RUN</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>3. </Text>
                        <Text style={[styles.viewTaskCardText]}>Set the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}> RUN/OFF </Text>
                            switch to
                                <Text style={{ fontFamily: 'Arial', color: '#EB1616' }}> OFF</Text>
                        </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>4. </Text>
                        <Text style={[styles.viewTaskCardText]}>Move the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}> PLA </Text>
                            to Max Take-Off within 5 seconds
                            </Text>
                    </View>

                    <View style={{ paddingBottom: 5, flexDirection: 'row' }}>
                        <Text style={[styles.viewTaskCardText]}>5. </Text>
                        <Text style={[styles.viewTaskCardText]}>Move the
                            <Text style={{ fontFamily: 'Arial', color: '#00A9E0' }}> PLA </Text>
                            to Max reverse within 5 seconds
                            </Text>
                    </View>
                </View>

                <Text style={[styles.viewTaskCardText, { textAlign: 'justify', paddingBottom: 10, fontSize: 15, paddingTop: 10 }]}>
                    Please also confirm that the trim on the maintenance page is the same as the one entered from the MicroFAST.
                </Text>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 15 }}>
                    <TouchableOpacity
                        id="Button" Title='Submit' onPress={() => this.setState({ isVisible: false })}
                        style={styles.updateButton}>
                        <Text style={styles.updateLabel}>OK</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    )


    render() {

        return (
            <View>
                <View style={[ styles.titleStyle, { backgroundColor: "#71FFB1", justifyContent: "flex-start", marginTop: 10 } ]} >
                        <Image
                            style={styles.checkImage}
                            source={require('../../../../assets/IconPack/Dashboard/check.png')}
                            resizeMode="contain"
                        />
                        <View style={{ paddingLeft: 10, width: width < 450 ? "90%" : "100%" }}>
                            <Text style={styles.updatedate}>Data successfully loaded from MicroFAST DCTU to EEC</Text>
                        </View>
                </View>
        
                <View style={[ styles.titleStyle, { backgroundColor: "#FFFF5D", justifyContent: "flex-start", marginTop: 5 } ]}>
                    <View>
                        <OctIcon name="alert" style={styles.alertIcon} />
                    </View>
                    <View style={{ paddingLeft: 10, width: width < 450 ? "90%" : "100%" }}>
                        <Text style={styles.updatedate}>Approve in the cockpit to complete the Engine Change procedure</Text>
                    </View>
                </View>
                <View>
                    {this.state.showTaskCard ?

                        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                            <Text></Text>
                            <Text style={styles.updatedate}>Approve in the cockpit to complete the ITT Trim Update</Text>
                            <Text></Text>
                            <TouchableOpacity onPress={() => { this.setState({ isVisible: true }) }}>
                                <Text style={styles.viewTaskCard}>View Task Card</Text>
                            </TouchableOpacity>
                            <Modal isVisible={this.state.isVisible}>
                                {this.renderContent()}
                            </Modal>
                        </View>
                        : <View>View A</View>
                    }
                </View>

                <TouchableOpacity
                    style={styles.backToDashboardButton}
                    onPress={() => {
                        this.props.navigation.navigate('Dashboard')
                    }}
                >
                    <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default connect(null, { FetchEOFData })(EOFReport)

const styles = StyleSheet.create({

    titleStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginLeft: 10,
        marginRight: 10,
        flexDirection: "row",
        padding: 10
    },
    updatedate: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0.5,
        color: "#1F2A44",
        paddingLeft: 5,
        paddingRight: 10
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10,
        paddingLeft: 5
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    viewTaskCard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0.5,
        color: "#00A9E0",
        paddingLeft: 5
    },
    viewTaskCardText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0,
        color: "#1F2A44"
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    backToDashboardButton: {
        marginTop: 16,
        marginLeft: 10
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    alertIcon: {
        fontSize: width < 450 ? 30 : 40,
        color: "#FFEE17",
        alignContent: "center"
    },
    checkImage: {
        width: width < 450 ? 30 : 40,
        height: width < 450 ? 30 : 40,
        alignContent: "center"
    }
})
