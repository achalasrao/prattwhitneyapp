import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator,
} from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import axios from 'axios';
import Modal from 'react-native-modal';
import {
    OldNG,
    OldAtSHP,
    OldITTTrim,
    OldOHMS,
    OldCompTrimWeight1,
    OldCompTrimWeight2,
    OldCompTrimWeight3,
    OldCompTrimWeight4,
    OldEngineBuildSpec,
    UpdateNG,
    UpdateAtSHP,
    UpdateITTTrim,
    UpdateOHMS,
    UpdateCompTrimWeight1,
    UpdateCompTrimWeight2,
    UpdateCompTrimWeight3,
    UpdateCompTrimWeight4,
    UpdateEngineBuildSpec,
    UpdatePerformanceData,
    DontUpdatePerformanceData,
    PerformanceDataScanFailed,
    FetchEngineData,
} from '../../../../../actions'

var result = []

class updatePerformanceData extends Component {

    state = {
        field1Focused: false,
        field2Focused: false,
        field3Focused: false,
        field4Focused: false,
        field5Focused: false,
        field6Focused: false,
        field7Focused: false,
        field8Focused: false,
        field9Focused: false,
        updateFailed: false,
        updateSuccess: false,
        isModalVisible: false,
        isLoading: false,
        updateFieldCount: 0,
        updateEPECCount: 0,
        writeXMLCount: 0
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('headerTilte'),
        };
    };

    componentDidMount() {
        result = []
    }

    reverseValue = () => {
        this.setState({ isModalVisible: false })
        this.props.DontUpdatePerformanceData()
        this.props.PerformanceDataScanFailed()
        let {
            oldNG,
            oldAtSHP,
            oldITTTrim,
            oldOHMS,
            oldCompTrimWeight1,
            oldCompTrimWeight2,
            oldCompTrimWeight3,
            oldCompTrimWeight4,
            oldEngineBuildSpec
        } = this.props
        this.props.UpdateNG(oldNG)
        this.props.UpdateAtSHP(oldAtSHP)
        this.props.UpdateITTTrim(oldITTTrim)
        this.props.UpdateOHMS(oldOHMS)
        this.props.UpdateCompTrimWeight1(oldCompTrimWeight1)
        this.props.UpdateCompTrimWeight2(oldCompTrimWeight2)
        this.props.UpdateCompTrimWeight3(oldCompTrimWeight3)
        this.props.UpdateCompTrimWeight4(oldCompTrimWeight4)
        this.props.UpdateEngineBuildSpec(oldEngineBuildSpec)
    }

    componentDidUpdate() {
        let {
            oldNG,
            oldAtSHP,
            oldITTTrim,
            oldOHMS,
            oldCompTrimWeight1,
            oldCompTrimWeight2,
            oldCompTrimWeight3,
            oldCompTrimWeight4,
            oldEngineBuildSpec,
            NG,
            AtSHP,
            ITTTrim,
            OHMS,
            CompTrimWeight1,
            CompTrimWeight2,
            CompTrimWeight3,
            CompTrimWeight4,
            EngineBuildSpec,
        } = this.props

        if ((oldNG !== NG) ||
            (oldAtSHP !== AtSHP) ||
            (oldITTTrim !== ITTTrim) ||
            (oldOHMS !== OHMS) ||
            (oldCompTrimWeight1 !== CompTrimWeight1) ||
            (oldCompTrimWeight2 !== CompTrimWeight2) ||
            (oldCompTrimWeight3 !== CompTrimWeight3) ||
            (oldCompTrimWeight4 !== CompTrimWeight4) ||
            (oldEngineBuildSpec !== EngineBuildSpec)) {
            this.props.UpdatePerformanceData()
        } else {
            this.props.DontUpdatePerformanceData()
        }
    }

    updateFields = async () => {
        this.setState({ updateFieldCount: ++this.state.updateFieldCount })
        let { EngineID, ModuleID } = this.props.selectedEngineModule
        let {
            NG,
            AtSHP,
            OHMS,
            CompTrimWeight1,
            CompTrimWeight2,
            CompTrimWeight3,
            CompTrimWeight4,
            EngineBuildSpec,
            ITTTrim
        } = this.props
        result = []
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=1]/value`,
                        "arg3": `${NG}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=2]/value`,
                        "arg3": `${AtSHP}`
                    },
                    {
                        "cmd": "epecs_trim",
                        "arg1": "--field=TEMP_TRIM_A",
                        "arg2": `--value=${ITTTrim}`
                    },

                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=4]/value`,
                        "arg3": `${OHMS}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=5]/value`,
                        "arg3": `${CompTrimWeight1}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=6]/value`,
                        "arg3": `${CompTrimWeight2}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=7]/value`,
                        "arg3": `${CompTrimWeight3}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=8]/value`,
                        "arg3": `${CompTrimWeight4}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=9]/value`,
                        "arg3": `${EngineBuildSpec}`
                    },
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response
            let responseJson = await response;
            var lines = JSON.stringify(responseJson.data);

            let splitlines = lines.split(":")
            var k = 0;


            for (var i = 0; i < splitlines.length; i++) {
                if (splitlines[i].includes("Success")) {
                    result.push("success")
                }
                else if (splitlines[i].includes("Err")) {
                    result.push("Failed")
                }
            }
            if (result.length < 9 && responseJson.data[0].name.includes("Data Error")) {
                for (var k = 0; k < 7; k++)
                    result.push("Failed")
            }
        }

        catch (err) {
            if (this.state.fieldUpdateCount < 2) {
                return this.updateFields()
            }
            this.updateWasFailure()
        }
        this.writeToXml()
    }


    writeToXml = async () => {
        this.setState({ writeXMLCount: ++this.state.writeXMLCount })
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            let responseJson = await response;
            this.setState({ isLoading: false })
            if (responseJson.data[0].value === "Success") {
                this.updateWasSuccess()
            } else {
                this.updateWasFailure()
            }
        }
        catch (error) {
            if (this.state.writeXMLCount < 2) {
                return this.writeToXml()
            }
            this.updateWasFailure()
        }
    }

    updateWasFailure = () => {
        this.setState({ updateFailed: true, updateSuccess: false })
    }

    updateWasSuccess = () => {
        this.setState({ updateFailed: false, updateSuccess: true })
        this.props.navigation.navigate('updateReportPerformanceData', { param: result, headerTilte: this.props.selectedEngineModule.ModuleName })
        // this.props.FetchEngineData()
    }

    confirmedUpdate = () => {
        this.setState({ isModalVisible: false, isLoading: true })
        this.updateFields()
    }

    renderModal = () => {
        let {
            oldNG,
            oldAtSHP,
            oldITTTrim,
            oldOHMS,
            oldCompTrimWeight1,
            oldCompTrimWeight2,
            oldCompTrimWeight3,
            oldCompTrimWeight4,
            oldEngineBuildSpec,
            NG,
            AtSHP,
            ITTTrim,
            OHMS,
            CompTrimWeight1,
            CompTrimWeight2,
            CompTrimWeight3,
            CompTrimWeight4,
            EngineBuildSpec,
        } = this.props

        return (

            <View style={styles.modalStyle}>

                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={styles.modalHeader}>You are about to load this data to the DCTU</Text>
                </View>

                <View style={{ width: '100%', padding: 10, paddingTop: 10 }}>
                    <ScrollView style={{ height: 300, paddingRight: 10 }}>


                        {
                            (oldNG === NG) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>NG is being changed from {oldNG} to {NG}</Text>
                                </View>
                        }
                        {
                            (oldAtSHP === AtSHP) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>AT SHP is being changed from {oldAtSHP} to {AtSHP}</Text>
                                </View>
                        }
                        {
                            (oldITTTrim === ITTTrim) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>ITT TRIM is being changed from {oldITTTrim} to {ITTTrim}</Text>
                                </View>
                        }
                        {
                            (oldOHMS === OHMS) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>OHMS is being changed from {oldOHMS} to {OHMS}</Text>
                                </View>
                        }
                        {
                            (oldCompTrimWeight1 === CompTrimWeight1) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Comp Trim Weight 1 is being changed from {oldCompTrimWeight1} to {CompTrimWeight1}</Text>
                                </View>
                        }
                        {
                            (oldCompTrimWeight2 === CompTrimWeight2) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Comp Trim Weight 2 is being changed from {oldCompTrimWeight2} to {CompTrimWeight2}</Text>
                                </View>
                        }
                        {
                            (oldCompTrimWeight3 === CompTrimWeight3) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Comp Trim Weight 3 is being changed from {oldCompTrimWeight3} to {CompTrimWeight3}</Text>
                                </View>
                        }
                        {
                            (oldCompTrimWeight4 === CompTrimWeight4) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Comp Trim Weight 4 is being changed from {oldCompTrimWeight4} to {CompTrimWeight4}</Text>
                                </View>
                        }
                        {
                            (oldEngineBuildSpec === EngineBuildSpec) ? <View /> :
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>Engine Build Spec is being changed from {oldEngineBuildSpec} to {EngineBuildSpec}</Text>
                                </View>
                        }

                    </ScrollView>
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>This will erase previous data</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 13 }}>

                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this.reverseValue()}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.confirmedUpdate()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#0033ab" />
                    </View> :
                    <ScrollView
                        keyboardDismissMode={true}
                        keyboardShouldPersistTaps="always"
                    >
                        <View style={styles.tileStyle}>

                            {this.props.selectedEngineModule.scannable === 'true' && <TouchableOpacity
                                onPress={() => {
                                    this.setState({ updateSuccess: false })
                                    this.props.navigation.navigate('scannerPerformanceData', { headerTilte: this.props.selectedEngineModule.ModuleName })
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    <Image
                                        style={styles.qrCodeScan}
                                        source={require('../../../../../assets/IconPack/ViewEditEngineData/LRUs/qrCodeScan.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.scanQrCode}>Scan 2D barcode</Text>
                                </View>
                            </TouchableOpacity>}

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>NG</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field1Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateNG(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field1Focused: true, })}
                                    onBlur={() => this.setState({ field1Focused: false })}
                                    value={this.props.NG}
                                    UITextContentType="numeric"
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>At SHP</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field2Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateAtSHP(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field2Focused: true, })}
                                    onBlur={() => this.setState({ field2Focused: false })}
                                    value={this.props.AtSHP}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>ITT Trim</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field3Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: (this.props.ITTTrim).includes("Err") ? '#D3D3D3' : this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateITTTrim(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field3Focused: true, })}
                                    onBlur={() => this.setState({ field3Focused: false })}
                                    value={((this.props.ITTTrim).includes("Err")) ? " " : this.props.ITTTrim}
                                    editable={((this.props.ITTTrim).includes("Err")) ? false : true}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>OHMS</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field4Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateOHMS(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field4Focused: true, })}
                                    onBlur={() => this.setState({ field4Focused: false })}
                                    value={this.props.OHMS}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Comp Trim Weight 1</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field5Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateCompTrimWeight1(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field5Focused: true, })}
                                    onBlur={() => this.setState({ field5Focused: false })}
                                    value={this.props.CompTrimWeight1}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Comp Trim Weight 2</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field6Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateCompTrimWeight2(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field6Focused: true, })}
                                    onBlur={() => this.setState({ field6Focused: false })}
                                    value={this.props.CompTrimWeight2}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Comp Trim Weight 3</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field7Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateCompTrimWeight3(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field7Focused: true, })}
                                    onBlur={() => this.setState({ field7Focused: false })}
                                    value={this.props.CompTrimWeight3}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Comp Trim Weight 4</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field8Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateCompTrimWeight4(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field8Focused: true, })}
                                    onBlur={() => this.setState({ field8Focused: false })}
                                    value={this.props.CompTrimWeight4}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>

                            <View style={{ paddingTop: 10 }}>
                                <Text style={styles.label}>Engine Build Spec</Text>
                                <TextInput
                                    style={[styles.textInput, {
                                        borderColor: this.state.field9Focused ? '#0033ab' : this.state.updateFailed ? '#e12c4e' : '#bdbdbd',
                                        backgroundColor: this.state.updateSuccess ? '#e6f2d3' : this.props.scannedPerformanceData ? '#e6f2d3' : this.state.updateFailed ? 'red' : '#fff'
                                    }]}
                                    onChangeText={(text) => {
                                        this.props.UpdateEngineBuildSpec(text)
                                        this.props.PerformanceDataScanFailed()
                                    }}
                                    onFocus={() => this.setState({ field9Focused: true })}
                                    onBlur={() => this.setState({ field9Focused: false })}
                                    value={this.props.EngineBuildSpec}
                                    textContentType={"telephoneNumber"}
                                ></TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-between',
                            marginTop: 15,
                            marginLeft: 15,
                            marginRight: 15,
                            marginBottom: 15,
                        }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ updateSuccess: false })
                                    this.reverseValue()
                                }}
                                style={styles.cancelButton}
                            >
                                <Text style={styles.cancelLabel}>Cancel</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                disabled={!this.props.performanceDataUpdate}
                                onPress={() => this.setState({ isModalVisible: true })}
                                style={[styles.updateButton, {
                                    backgroundColor: this.props.performanceDataUpdate ? '#0033ab' : '#e5e5e5'
                                }]}>
                                <Text style={[styles.updateLabel, {
                                    color: this.props.performanceDataUpdate ? '#fff' : '#666'
                                }]}>Update</Text>
                            </TouchableOpacity>
                        </View>
                        <Modal
                            style={styles.modalContainer}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={() => this.setState({ isModalVisible: true })}
                        >{this.renderModal()}</Modal>
                    </ScrollView>
                }
            </View>
        )
    }
}

const mapStateToProps = ({ EngineDataReducer, PerformanceDataReducer }) => {
    const { selectedEngineModule } = EngineDataReducer
    const {
        oldNG,
        oldAtSHP,
        oldITTTrim,
        oldOHMS,
        oldCompTrimWeight1,
        oldCompTrimWeight2,
        oldCompTrimWeight3,
        oldCompTrimWeight4,
        oldEngineBuildSpec,
        NG,
        AtSHP,
        ITTTrim,
        OHMS,
        CompTrimWeight1,
        CompTrimWeight2,
        CompTrimWeight3,
        CompTrimWeight4,
        EngineBuildSpec,
        performanceDataUpdate,
        scannedPerformanceData,
    } = PerformanceDataReducer

    return {
        selectedEngineModule,
        oldNG,
        oldAtSHP,
        oldITTTrim,
        oldOHMS,
        oldCompTrimWeight1,
        oldCompTrimWeight2,
        oldCompTrimWeight3,
        oldCompTrimWeight4,
        oldEngineBuildSpec,
        NG,
        AtSHP,
        ITTTrim,
        OHMS,
        CompTrimWeight1,
        CompTrimWeight2,
        CompTrimWeight3,
        CompTrimWeight4,
        EngineBuildSpec,
        performanceDataUpdate,
        scannedPerformanceData,
    };
};

export default connect(mapStateToProps, {
    OldNG,
    OldAtSHP,
    OldITTTrim,
    OldOHMS,
    OldCompTrimWeight1,
    OldCompTrimWeight2,
    OldCompTrimWeight3,
    OldCompTrimWeight4,
    OldEngineBuildSpec,
    UpdateNG,
    UpdateAtSHP,
    UpdateITTTrim,
    UpdateOHMS,
    UpdateCompTrimWeight1,
    UpdateCompTrimWeight2,
    UpdateCompTrimWeight3,
    UpdateCompTrimWeight4,
    UpdateEngineBuildSpec,
    UpdatePerformanceData,
    DontUpdatePerformanceData,
    PerformanceDataScanFailed,
    FetchEngineData
})(withNavigationFocus(updatePerformanceData));

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e5e5e5",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        padding: 18
    },
    qrCodeScan: {
        width: 24,
        height: 24
    },
    scanQrCode: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#0033ab",
        textAlignVertical: 'center',
        paddingLeft: 8
    },
    label: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        paddingBottom: 5
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#0033ab",
        justifyContent: 'center'
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: 'center'
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        width: '100%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#d6d6d6",
        padding: 23
    },
    modalHeader: {
        fontFamily: "HelveticaNeue",
        fontSize: 17,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.25,
        color: "#000000"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#fff",
        justifyContent: 'center'
    },
    previousLabelStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#0033ab"
    },
    confirmButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: 'center'
    },
    confirmLabelStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    }
})
