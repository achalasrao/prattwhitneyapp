export const connectionStateChanged = (text) => {

  return {
    type: 'connectionStateChanged',
    payload: text
  };
};

export const connectionTypeChanged = (text) => {

  return {
    type: 'connectionTypeChanged',
    payload: text
  };
};

export const connectionStatus = (text) => {

  return {
    type: 'connectionStatus',
    payload: text
  };
};

export const TNError = () => {

  return {
    type: 'TNError',
  };
};

export const TNEmpty = () => {

  return {
    type: 'TNEmpty',
  };
};
export const TNFetchSuccess = (value) => {

  return {
    type: 'TNFetchSuccess',
    payload: value
  };
};

export const EngineSerialNumber = (text) => {

  return {
    type: 'EngineSerialNumber',
    payload: text
  };
};


//EOF 
export const FetchEOFData = () => {
  return {
    type: 'FetchEOFData',
  }
};

export const DontFetchEOFData = () => {

  return {
    type: 'DontFetchEOFData',
  }
};
