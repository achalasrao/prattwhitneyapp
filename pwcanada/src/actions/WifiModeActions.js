import { FOOTER_TAB_FOCUSSED, CONF_NAME_CHANGED, CONF_SSID_CHANGED, CONF_MAC_CHANGED, CONF_SECURITY_CHANGED, CONF_KEY_CHANGED, CONF_WIFI_LIST, AVA_WIFI_LIST, ADD_CONFIGURATION, ADD_CONFIGURATION_SUCCESS } from './types';

export const confNameChanged = (text) => {

  return {
    type: CONF_NAME_CHANGED,
    payload: text
  };
};

export const confSsidChanged = (text) => {
  return {
    type: CONF_SSID_CHANGED,
    payload: text
  };
};

export const confMacChanged = (text) => {
  return {
    type: CONF_MAC_CHANGED,
    payload: text
  };
};

export const confSecurityChanged = (text) => {
  return {
    type: CONF_SECURITY_CHANGED,
    payload: text
  };
};

export const confKeyChanged = (text) => {
  return {
    type: CONF_KEY_CHANGED,
    payload: text
  };
};

export const confWifiShowList = (text) => {
  return {
    type: CONF_WIFI_LIST,
    payload: text
  };
};

export const availableWifiList = (text) => {
  return {
    type: AVA_WIFI_LIST,
    payload: text
  };
};

export const AllConfiguredNetworks = (text) => {
  return {
    type: "AllconfiguredNetworks",
    payload: text
  }
}

export const updateConfigured = (text) => {
  return {
    type: "UpdateConfigured",
    payload: text
  }
}

export const InitialStatecalling = () => {
  return {
    type: "InitialStatecalling",
    payload: ""
  }
}

export const configurationFoccussed = (text) => {
  return {
    type: "configurationFoccussed",
    payload: text
  }
}

export const onAddtoConfiguration = (text) => {
  return {
    type: "AddToConf",
    payload: text
  };
}
