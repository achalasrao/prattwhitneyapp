export const OldNG = (item) => {

    return {
        type: 'OldNG',
        payload: item
    }
};
export const UpdateNG = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'UpdateNG',
        payload: item
    }
};

// ======================

export const OldAtSHP = (item) => {

    return {
        type: 'OldAtSHP',
        payload: item
    }
};
export const UpdateAtSHP = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'UpdateAtSHP',
        payload: item
    }
};

// ======================

export const OldITTTrim = (item) => {

    return {
        type: 'OldITTTrim',
        payload: item
    }
};
export const UpdateITTTrim = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'UpdateITTTrim',
        payload: item
    }
};

// ======================
export const OldOHMS = (item) => {

    return {
        type: 'OldOHMS',
        payload: item
    }
};
export const UpdateOHMS = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'UpdateOHMS',
        payload: item
    }
};

// ======================

export const OldCompTrimWeight1 = (item) => {

    return {
        type: 'OldCompTrimWeight1',
        payload: item
    }
};
export const UpdateCompTrimWeight1 = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateCompTrimWeight1',
        payload: item
    }
};
// ======================
export const OldCompTrimWeight2 = (item) => {

    return {
        type: 'OldCompTrimWeight2',
        payload: item
    }
};
export const UpdateCompTrimWeight2 = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateCompTrimWeight2',
        payload: item
    }
};
// ======================
export const OldCompTrimWeight3 = (item) => {

    return {
        type: 'OldCompTrimWeight3',
        payload: item
    }
};
export const UpdateCompTrimWeight3 = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateCompTrimWeight3',
        payload: item
    }
};
// ======================
export const OldCompTrimWeight4 = (item) => {

    return {
        type: 'OldCompTrimWeight4',
        payload: item
    }
};
export const UpdateCompTrimWeight4 = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateCompTrimWeight4',
        payload: item
    }
};
// ======================
export const OldEngineBuildSpec = (item) => {

    return {
        type: 'OldEngineBuildSpec',
        payload: item
    }
};
export const UpdateEngineBuildSpec = (item) => {
    item = item.replace(/^0+/, '')

    return {
        type: 'UpdateEngineBuildSpec',
        payload: item
    }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Update Actions

export const UpdatePerformanceData = () => {

    return {
        type: 'UpdatePerformanceData',
    }
};

export const DontUpdatePerformanceData = () => {

    return {
        type: 'DontUpdatePerformanceData',
    }
};

export const PerformanceDataUpdateSuccess = () => {

    return {
        type: 'PerformanceDataUpdateSuccess',
    }
};

export const PerformanceDataUpdateFailed = () => {

    return {
        type: 'PerformanceDataUpdateFailed',
    }
};
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Scan Actions

export const PerformanceDataScanSuccess = () => {

    return {
        type: 'PerformanceDataScanSuccess',
    }
};

export const PerformanceDataScanFailed = () => {

    return {
        type: 'PerformanceDataScanFailed',
    }
};
