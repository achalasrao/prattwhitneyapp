import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, ScrollView, TouchableOpacity, Image, Dimensions } from 'react-native'
import {
    Text
} from 'native-base';
import { enginedataShow } from '../../../actions'
import axios from 'axios';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons'

var tableFault = []
var count = 1;
let { width, height } = Dimensions.get('window');

class FaultA extends Component {
    constructor(props) {
        super(props)

        this.state = {
            visibleModal: null,
            visibleModal: null,
            loading: true,
            tableHeadExceed: ['Description', 'Code', 'Run Time', 'Serial Number', 'Snap', 'Trace', 'Zone', 'Duration'],
            tableHeadEVENTFAult: ['Description', 'Code', 'Run Time', 'Serial Number', 'Snap', 'Trace'],
            widthArrExceed: [250, 80, 200, 100, 80, 80, 150, 100],
            widthArrEvent: [300, 150, 250, 100, 150, 150],
        }
        var type = "";
    }
    async reset() {
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": " --reset=[ALL] --ch=[A/B]"
                    },
                ],
                headers: { 'Content-Type': 'application/json' },
            })
        }
        catch (error) {

        }
    }

    static navigationOptions = ({ navigation }) => {

        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <View style={{ marginLeft: 10 }}>
                    <TouchableOpacity
                        onPress={() => { params.backHandler() }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <Icon name='ios-arrow-back' style={{ fontSize: 30, color: '#000000' }} />
                            <Text style={{ color: '#000000', fontSize: 20 }}> Back</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    customBackHandler = () => {

        this.props.navigation.navigate('Dashboard')

    }

    async UNSAFE_componentWillMount() {
        table = []
        await this.startEpecsRecord("A")
        await this.startEpecsRecord("B")

        this.setState({ loading: false });
    }

    async  componentDidMount() {

        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
        tableFault = []
    }

    async   componentWillUnmount() {
        tableFault = []
    }


    async startEpecsRecord(channel) {

        var result = ""
        tab = []
        this.reset()
        // let param = "--get=[TRANS, " + this.type +"]"+ " --ch=A";

        let param = "--get=[FAULT] --ch=" + channel;

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": param
                    },

                ],
                headers: { 'Content-Type': 'application/json' },
            })

            result = response.data[0].value


        } catch (error) {

        }

        this.fetchData(channel)
    }
    async fetchData(channel) {

        count++;

        var result = "";
        var reg = "End of Buffer"
        var errorReg = "Err:"
        let param = "--type=[FAULT] --ch=" + channel + " --num=10";

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_records",
                        "arg1": param
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            result = JSON.stringify(response.data[0].value);
            let responseList = response.data

            if (result.match(reg) || result === "" || (result.match(errorReg))) {

                await this.reset()
                //await  this.fetchData()
                await this.startEpecsRecord(channel);

            }
            else {

                for (var i = 0; i < responseList.length; i++) {

                    var splitwise = await responseList[i].value.split(",");

                    var snap = splitwise[5].substring(splitwise[5].indexOf("=") + 1, splitwise[5].length);
                    var trans = splitwise[6].substring(splitwise[6].indexOf("=") + 1, splitwise[6].length);

                    tableFault.push({
                        Description: splitwise[4],
                        Code: splitwise[2],
                        RunTime: splitwise[1],
                        Snap: snap,
                        Trans: trans,
                        Peak: "",
                        Zone: "",
                        Duration: "",
                        Type: "FAULT",
                        Channel: channel
                    })

                }//end-for

                // table = table.sort((a, b) => moment(b[2]).valueOf() - moment(a[2]).valueOf());
                this.reset()
                this.setState({ loading: false });

            }//end-else
        }
        catch (error) {

        }
        if (result === "") {
            this.fetchData()
        }
    }

    async showrowdata(rowData) {
        this.setState({ visibleModal: 6 })
        this.props.enginedataShow(rowData);
    }


    snaptable = (item) => {

        // this.setState({ visibleModal: null })
        // this.props.navigation.navigate('SnapTable', { param: 'Snap Parameters' })

        this.props.enginedataShow(item);
        if (String(item.Trans).includes('YES')) {
            return this.props.navigation.navigate('faultSnapTrace', { headerTitle: item.Description })
        }
        this.props.navigation.navigate('faultSnapOnly', { headerTitle: item.Description })
    }

    transtable() {

        this.setState({ visibleModal: null })

        this.props.navigation.navigate('TransParam', { param: 'Select Parameter' })
    }

    snapchartcheck() {
        if (this.props.enginedata[4] == 'YES') {
            return (
                <TouchableOpacity style={styles.circularbtn} onPress={() => this.snaptable()}>
                    <Image style={{
                        flex: 1,
                        alignSelf: 'stretch',
                        width: undefined,
                        height: undefined,

                    }}
                        source={require('../../../assets/icons/table.png')}
                        resizeMode='contain' />

                </TouchableOpacity>
            )
        } else {

        }
    }

    transchartcheck() {

        if (this.props.enginedata[5] == undefined) {

        } else {

            var check = JSON.stringify(this.props.enginedata[5]).includes('YES');

            if (check == true) {
                return (
                    <TouchableOpacity style={styles.circularbtn} onPress={() => this.transtable()}>
                        <Image style={{
                            flex: 1,
                            alignSelf: 'stretch',
                            width: 30,
                            height: 30,
                            marginTop: 5

                        }}
                            source={require('../../../assets/icons/chart.png')}
                            resizeMode='contain' />

                    </TouchableOpacity>
                )

            } else {


            }
        }
    }

    renderModalContent = () => {

        return (
            <View style={{ backgroundColor: '#FFFFFF', paddingTop: 3, paddingBottom: 5, paddingRight: 5, borderRadius: 5 }}>
                <TouchableOpacity style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                }} onPress={() => this.setState({ visibleModal: null, fieldChanged: false })}>
                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-end', width: '100%' }}>
                        <Icon name='ios-close-circle-outline' style={{ fontSize: 25, color: '#EB1616', }} />
                    </View>
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 10 }}>
                    <View style={{ width: '100%', paddingBottom: 10 }} >
                        <Text style={{ fontSize: 14, color: '#1F2A44', fontFamily: 'Arial', textAlign: 'center' }}>{this.props.enginedata[0]}</Text>
                    </View>
                    <View style={{ width: '90%', paddingBottom: 10 }} >
                        <View style={{ width: '100%' }}>
                            <View style={{ flexDirection: 'row' }} >
                                <Text style={{
                                    fontSize: 18, color: '#1F2A44', fontFamily: 'Arial', borderWidth: 1,
                                    borderColor: '#000000',
                                    opacity: 0.2,
                                    textAlign: 'center',
                                    width: '20%', paddingTop: 5, paddingBottom: 5
                                }}>SNAP </Text>
                                <Text style={{
                                    fontSize: 18, color: '#1F2A44', fontFamily: 'Arial', borderWidth: 1,
                                    borderColor: '#000000',
                                    opacity: 0.2,
                                    textAlign: 'center',
                                    width: '60%', paddingTop: 5, paddingBottom: 5
                                }}>{this.props.enginedata[4]}
                                </Text>
                                <View style={{ marginLeft: 5 }}>
                                    {this.snapchartcheck()}
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }} >
                                <Text style={{
                                    fontSize: 18, color: '#1F2A44', fontFamily: 'Arial', borderWidth: 1,
                                    borderColor: '#000000',
                                    opacity: 0.2,
                                    textAlign: 'center',
                                    width: '20%', paddingTop: 5, paddingBottom: 5
                                }}>TRANS </Text>

                                <Text style={{
                                    fontSize: 18, color: '#1F2A44', fontFamily: 'Arial', borderWidth: 1,
                                    borderColor: '#000000',
                                    opacity: 0.2,
                                    textAlign: 'center',
                                    width: '60%', paddingTop: 5, paddingBottom: 5
                                }}>{this.props.enginedata[5]}

                                </Text>
                                <View style={{ marginLeft: 5 }}>
                                    {this.transchartcheck()}
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    render() {

        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }

        else {
            return (
                <ScrollView>
                    <View style={styles.tileStyle}>
                        <View style={{ flexDirection: 'row', padding: 14 }}>
                            <Text style={styles.ESN}>ESN </Text>
                            {this.props.engineSerialNumber === 'Failed to fetch' ?
                                <Text style={[styles.ESNValue, { color: '#EB1616' }]}> {this.props.engineSerialNumber}</Text> :
                                <Text style={styles.ESNValue}>PCE- {this.props.engineSerialNumber}</Text>
                            }
                        </View>
                    </View>
                    <View style={[styles.tileStyle, {marginBottom: 15}]}>
                        <View>
                            <Text style={styles.heading}>Last 20 Faults</Text>
                            <Text></Text>
                            <Text></Text>
                            {tableFault.map((item) => {
                                return (
                                    <View>
                                        <View style={{ flexDirection: "row", width: width < 450 ? "70%" : "100%" }}>
                                            <TouchableOpacity onPress={() => this.snaptable(item)}>
                                                <Text style={styles.label}>{item.Description}</Text>
                                            </TouchableOpacity>
                                            <Text style={styles.parentheses}> ({item.Channel})</Text>
                                            <Text style={styles.parentheses}> ({item.Code}) </Text>
                                            <Icon name='ios-arrow-forward' style={{ fontSize: width < 450 ? 16 : 22, opacity: 0.5, textAlignVertical: 'center' }} />
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <View style={{ width: '25%' }}>
                                                <Text style={[styles.label1, { textAlign: 'left' }]}>{item.RunTime}</Text>
                                            </View>
                                            <View style={{ width: '25%' }}>
                                                <Text style={[styles.label1, { textAlign: 'left' }]}>{item.Peak}</Text>
                                            </View>
                                            <View style={{ width: '25%' }}>
                                                <Text style={[styles.label1, { textAlign: 'center' }]}>{item.Duration}</Text>
                                            </View>
                                            <View style={{ width: '25%' }}>
                                                <Text style={[styles.label1, { textAlign: 'center' }]}>{item.Zone}</Text>
                                            </View>
                                        </View>
                                        <Text></Text>
                                    </View>
                                )
                            })}
                        </View>
                    </View>
                </ScrollView>
            )
        }
    }
}

const mapStateToProps = ({ RetriveEngineData, connection }) => {

    const { enginedata } = RetriveEngineData
    const { engineSerialNumber } = connection
    return { enginedata, engineSerialNumber };
};


export default connect(mapStateToProps, { enginedataShow })(FaultA);


const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center'
    },
    circularbtn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
        height: 35,
        marginLeft: 10
    },
    modalbox: {
        backgroundColor: "#FFFFFF",
        height: 200,
        borderRadius: 4,
        borderColor: "#000000",
        opacity: 0.1
    },
    modalContent: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: "center",
        alignItems: "center"
    },
    channel: {
        lineHeight: width < 450 ? 16 : 22,
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#1F2A44",
        textAlign: "left"
    },
    label1: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 13,
        justifyContent: 'flex-start'
    },
    tileStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        padding: 5
    },
    heading: {
        height: 22,
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#1F2A44"
    },
    label: {
        fontFamily: "Arial",
        fontSize:  width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#00A9E0"
    },
    parentheses: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.44,
        lineHeight: width < 450 ? 18 : 24,
        color: "#1F2A44"
    },
    date: {
        height: 16,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44"
    },
    time: {
        height: 14,
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44"
    },
    ESN: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#5B6376"
    },
    ESNValue: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#5B6376"
    },
});
