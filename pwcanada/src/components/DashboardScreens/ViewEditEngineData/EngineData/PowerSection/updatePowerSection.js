import React, { Component } from "react";
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    TextInput
} from "react-native";
import { withNavigationFocus } from "react-navigation";
import { connect } from "react-redux";
import axios from "axios";
import Modal from "react-native-modal";
import {
    OldPowerSectionSerialNumber,
    updatePowerSectionSerialNumber,
    UpdatePowerSection,
    DontUpdatePowerSection,
    PowerSectionScanFailed,
    FetchEngineData
} from "../../../../../actions";

var result = ""

class updatePowerSection extends Component {
    state = {
        field1Focused: false,

        updateFailed: false,
        updateSuccess: false,
        isModalVisible: false,
        updateFieldCount: 0,
        writeToXmlCount: 0
    };

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam("headerTilte")
        };
    };

    reverseValue = () => {
        this.setState({ isModalVisible: false });
        this.props.DontUpdatePowerSection();
        this.props.PowerSectionScanFailed();
        let { oldModuleSerialNumber } = this.props;
        this.props.updatePowerSectionSerialNumber(oldModuleSerialNumber);
    };

    componentDidUpdate() {
        let { oldModuleSerialNumber, moduleSerialNumber } = this.props;

        if (oldModuleSerialNumber !== moduleSerialNumber) {
            this.props.UpdatePowerSection();
        } else {
            this.props.DontUpdatePowerSection();
        }
    }

    updateFields = async () => {
        this.setState({ updateFieldCount: ++this.state.updateFieldCount });
        let { EngineID, ModuleID } = this.props.selectedEngineModule;
        let { moduleSerialNumber } = this.props;

        try {
            let response = await axios({
                method: "post",
                url: "http://microFAST/cgi-bin/data_access.cgi?update",
                data: [
                    {
                        cmd: "xml_access",
                        arg1: "write",
                        arg2: `Engine[@ID=${EngineID}]/Module[@ID=${ModuleID}]/data_field[@ID=1]/value`,
                        arg3: `${moduleSerialNumber}`
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });

            ///Parser for the response
            let responseJson = await response;

            var lines = JSON.stringify(responseJson.data);
            let splitlines = lines.split(":")

            for (var i = 0; i < splitlines.length; i++) {
                if (splitlines[i].includes("Success")) {
                    result = "success"

                }
                else if (splitlines[i].includes("Err")) {

                    result = "Failed"
                }
            }
            if (String(responseJson.data).includes("Data Error")) {
                this.updateWasFailure();
            } else {
                this.writeToXml();
            }
        } catch (err) {
            if (this.state.updateFieldCount < 2) {
                return this.updateFields();
            }
            this.updateWasFailure();
        }
    };

    writeToXml = async () => {
        this.setState({ writeToXmlCount: ++this.state.writeToXmlCount });
        try {
            let response = await axios({
                method: "post",
                url: "http://microFAST/cgi-bin/data_access.cgi?update",
                data: [
                    {
                        cmd: "xml_access",
                        arg1: "write"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });
            let responseJson = await response;
            if (responseJson.data[0].value === "Success") {
                this.updateWasSuccess();
            } else {
                this.updateWasFailure();
            }
        } catch (error) {
            if (this.state.writeToXmlCount < 2) {
                return this.writeToXml();
            }
            this.updateWasFailure();
        }
    };

    updateWasFailure = () => {
        this.setState({ updateFailed: true, updateSuccess: false });
    };

    updateWasSuccess = () => {
        this.setState({ updateFailed: false, updateSuccess: true });
        this.props.navigation.navigate("updateReportPowerSection", {
            param: result,
            headerTilte: this.props.selectedEngineModule.ModuleName
        });
        // this.props.FetchEngineData()
    };

    confirmedUpdate = () => {
        this.setState({ isModalVisible: false });
        this.updateFields();
    };

    renderModal = () => {
        let {
            oldModuleSerialNumber,

            moduleSerialNumber
        } = this.props;

        return (
            <View style={styles.modalStyle}>
                <View style={{ width: "100%", padding: 10 }}>
                    <Text style={styles.modalHeader}>
                        You are about to load this data to the DCTU
          </Text>
                </View>

                <View
                    style={{
                        width: "100%",
                        padding: 10,
                        paddingTop: 10,
                        paddingRight: 10
                    }}
                >
                    {oldModuleSerialNumber === moduleSerialNumber ? (
                        <View />
                    ) : (
                            <View style={{ flexDirection: "row" }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>
                                    Power Section Serial Number is being changed from{" "}
                                    {oldModuleSerialNumber} to {moduleSerialNumber}
                                </Text>
                            </View>
                        )}
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>
                        This will erase previous data
          </Text>
                </View>

                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        paddingTop: 13
                    }}
                >
                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this.reverseValue()}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.confirmedUpdate()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.tileStyle}>
                    {this.props.selectedEngineModule.scannable === "true" && (
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ updateSuccess: false });
                                this.props.navigation.navigate("scannerPowerSection", {
                                    headerTilte: this.props.selectedEngineModule.ModuleName
                                });
                            }}
                        >
                            <View style={{ flexDirection: "row" }}>
                                <Image
                                    style={styles.qrCodeScan}
                                    source={require("../../../../../assets/IconPack/ViewEditEngineData/LRUs/qrCodeScan.png")}
                                    resizeMode="contain"
                                />
                                <Text style={styles.scanQrCode}>Scan 2D barcode</Text>
                            </View>
                        </TouchableOpacity>
                    )}

                    <View style={{ paddingTop: 10 }}>
                        <Text style={styles.label}>Power Section Serial Number</Text>
                        <TextInput
                            style={[
                                styles.textInput,
                                {
                                    borderColor: this.state.field1Focused
                                        ? "#0033ab"
                                        : this.state.updateFailed
                                            ? "#e12c4e"
                                            : "#bdbdbd",
                                    backgroundColor: this.state.updateSuccess
                                        ? "#e6f2d3"
                                        : this.props.scannedPowerSection
                                            ? "#e6f2d3"
                                            : this.state.updateFailed
                                                ? "red"
                                                : "#fff"
                                }
                            ]}
                            onChangeText={text => {
                                this.props.updatePowerSectionSerialNumber(text);
                                this.props.PowerSectionScanFailed();
                            }}
                            onFocus={() => this.setState({ field1Focused: true })}
                            onBlur={() => this.setState({ field1Focused: false })}
                            value={this.props.moduleSerialNumber}
                            UITextContentType="numeric"
                        />
                    </View>
                </View>

                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: 15,
                        marginLeft: 15,
                        marginRight: 15,
                        marginBottom: 15
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ updateSuccess: false });
                            this.reverseValue();
                        }}
                        style={styles.cancelButton}
                    >
                        <Text style={styles.cancelLabel}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        disabled={!this.props.powerSectionUpdate}
                        onPress={() => this.setState({ isModalVisible: true })}
                        style={[
                            styles.updateButton,
                            {
                                backgroundColor: this.props.powerSectionUpdate
                                    ? "#0033ab"
                                    : "#e5e5e5"
                            }
                        ]}
                    >
                        <Text
                            style={[
                                styles.updateLabel,
                                {
                                    color: this.props.powerSectionUpdate ? "#fff" : "#666"
                                }
                            ]}
                        >
                            Update
            </Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    style={styles.modalContainer}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={() => this.setState({ isModalVisible: true })}
                >
                    {this.renderModal()}
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = ({ EngineDataReducer, PowerSectionReducer }) => {
    const { selectedEngineModule } = EngineDataReducer;
    const {
        oldModuleSerialNumber,
        moduleSerialNumber,
        powerSectionUpdate,
        scannedPowerSection
    } = PowerSectionReducer;
    return {
        selectedEngineModule,
        oldModuleSerialNumber,
        moduleSerialNumber,
        powerSectionUpdate,
        scannedPowerSection
    };
};

export default connect(
    mapStateToProps,
    {
        OldPowerSectionSerialNumber,
        updatePowerSectionSerialNumber,
        UpdatePowerSection,
        DontUpdatePowerSection,
        PowerSectionScanFailed,
        FetchEngineData
    }
)(withNavigationFocus(updatePowerSection));

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e5e5e5",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        padding: 18
    },
    qrCodeScan: {
        width: 24,
        height: 24
    },
    scanQrCode: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#0033ab",
        textAlignVertical: "center",
        paddingLeft: 8
    },
    label: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        paddingBottom: 5
    },
    textInput: {
        width: "100%",
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: "left"
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#0033ab",
        justifyContent: "center"
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: "center"
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab"
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    },
    modalContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    modalStyle: {
        width: "100%",
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#d6d6d6",
        padding: 23
    },
    modalHeader: {
        fontFamily: "HelveticaNeue",
        fontSize: 17,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0.25,
        color: "#000000"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#fff",
        justifyContent: "center"
    },
    previousLabelStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#0033ab"
    },
    confirmButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: "center"
    },
    confirmLabelStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff"
    }
});
