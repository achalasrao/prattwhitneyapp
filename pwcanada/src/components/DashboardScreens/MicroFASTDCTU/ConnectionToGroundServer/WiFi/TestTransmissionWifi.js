import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, Image, TouchableOpacity, Dimensions } from 'react-native';
import {
    Text,
    Card,
    ListItem,
    Content,
    Label
} from 'native-base';
import axios from 'axios';
import XMLParser from 'react-xml-parser';
import ProgressCircle from 'react-native-progress-circle';
import IonicIcon from 'react-native-vector-icons/Ionicons';

let { width, height } = Dimensions.get('window');
var index = 1

export class TestTransmissionWifi extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSource: [],
            loading: true,
            testStarted: false,
            refreshing: false,
            testCompleted: false
        }
        this.reRunTest = this.reRunTest.bind(this);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('ConnectionDCTUGround')}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    UNSAFE_componentWillMount = async () => {

        await this._onRefresh();

    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    async checkSIM() {
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': "xml_access",
                        'arg1': "read"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            var xmlText = response.data[0].value
            var xml = new XMLParser().parseFromString(xmlText);    // Assume xmlText contains the example XML


            var activeSIM = xml.getElementsByTagName('Active_SIM')
            const simType = activeSIM[0].value;

        } catch (error) {
            await this.checkSIM()
        }
    }


    startTest = async () => {

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'txtest_wlan',
                        'arg1': 'start'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

        } catch (error) {

        }
    }

    fetchData = async () => {
        index = 1

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'txtest_wlan',
                        'arg1': 'status'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            var result = response.data[1]

            if (result.name == "Data Error") {
                await this.fetchData()
            }
            else if (result.value == "InProgress") {
                this.setState({ userDataSource: response.data, loading: false });

            }
            else {
                this.setState({ userDataSource: response.data, loading: false });
                clearInterval(this._interval);
            }
            if (response.status === 200) {
            }
        } catch (error) {
            this.fetchData()
        }
    }

    reRunTest = async () => {

        await this.startTest()
        this._interval = setInterval(async () => {

            await this.fetchData();
        }, 2000);
    }

    _onRefresh = async () => {
        await this.startTest()
        this.setState({ testStarted: true })
        this._interval = setInterval(async () => {
            this.fetchData()

        }, 2000);
    }

    renderRow(tabs) {

        let bgcolor;
        var thenum = tabs.value.replace(/^\D+/g, '')
        let num = parseInt(thenum)
        let inProgressCheck = String(tabs.value)
        let color = ''
        let name = tabs.name.toUpperCase()

        if (name === "STATE") {
            name = "TEST_STATUS"

        }
        if (tabs.value === 'Pass') {
            color = '#34CB81',
                bgcolor = '#FFFFFF',
                src = require('../../../../../assets/icons/passed.png')

        } else if (tabs.value === 'Fail') {
            color = '#EB1616',
                bgcolor = '#FFFFFF',
                src = require('../../../../../assets/icons/failed.png')

        } else if (tabs.value === '') {
            tabs.value = '--',
                color = '#BABBB1',
                bgcolor = '#E3E5E8',
                src = ''

        } else if (tabs.value == 'Fail: SIM card no detected') {
            color = '#BABBB1',
            bgcolor = '#E3E5E8'
            src = require('../../../../../assets/icons/failed.png')

        }
        else {
            color = '#BABBB1',
            bgcolor = '#E3E5E8',
            src = ''
        }

        index++;

        return (
            <ListItem style={{ flex: 1, flexDirection: 'row', borderBottomWidth: 0.4, borderBottomColor: '#E3E5E8', backgroundColor: bgcolor == "#FFFFFF" ? "#FFFFFF" : "#F7F7F7" }} itemDivider>
                <Text style={{ fontSize: width < 450 ? 16 : 22, color: '#1F2A44', fontFamily: 'Arial', flex: 1.5 }}>{name}</Text>
                {
                    inProgressCheck.includes('InProgress_') ?
                        <View style={{ flexDirection: 'row' }} >
                            <ProgressCircle
                                percent={num}
                                radius={20}
                                borderWidth={5}
                                color="#00A9E0"
                                shadowColor="#A5A9B4"
                                bgColor="#E3E5E8"
                            >
                                <Text style={{ fontSize: width < 450 ? 16 : 22, color: "#1F2A44" }}>{num}</Text>
                            </ProgressCircle>
                            <Text style ={{fontSize: width < 450 ? 16 : 22, color: "#1F2A44"}}> In Progress</Text>
                        </View>
                        : <Text
                            style={{
                                fontSize: width < 450 ? 16 : 22,
                                color: color,
                                fontFamily: 'Arial',
                                flex: 1.5, textAlign: 'left',
                                paddingLeft: 20
                            }}>{tabs.value}</Text>}

                <Image style={{
                    flex: .5,
                    alignSelf: 'stretch',
                    width: 35,
                    height: 35
                }}
                    source={src}
                    resizeMode='contain' />
            </ListItem>
        )
    }


    renderView() {
        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>

                {
                    this.state.userDataSource.map((tabs) =>
                        this.renderRow(tabs)
                    )
                }
            </View>

        )
    }

    renderButton() {
        if (this.state.loading || !this.state.testStarted) {
            return (
                <View />
            )
        }
        return (
            <View>

                <View style={styles.buttonAlign}>
                    <TouchableOpacity style={styles.testButton} onPress={this.reRunTest.bind(this)} >
                        <Label style={styles.testButtonText}><Text style={{ color: '#FFFFFF' }} >Run Test</Text></Label>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {
                    this.state.loading || !this.state.testStarted ?
                        <View style={[styles.container]}>
                            <ActivityIndicator size="large" color="#5B6376" />
                        </View> :
                        <Content>
                            <Card style={{ backgroundColor: '#E3E5E8', }} >
                                {this.renderView()}
                            </Card>
                        </Content>
                }
                {this.renderButton()}
            </View>
        )
    }
}

export default TestTransmissionWifi;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonAlign: {
        marginLeft: 15,
        flexDirection: 'row'
    },
    testButton: {
        width: '98%',
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        marginBottom: 15,
        paddingRight: 5
    },
    testButtonText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#FFFFFF"
    }
});
