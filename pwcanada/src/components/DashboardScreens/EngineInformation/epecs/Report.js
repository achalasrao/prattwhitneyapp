import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, Dimensions } from 'react-native';
import {
    Text,
    Content,
    Card
} from 'native-base';
import { UpdateSuccessNotifier } from '../../../common';
import { UpdateFailedNotifier } from '../../../common';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { _, keys, values } from 'underscore';
import { FetchEngineUsageData, FetchLCFData, FetchCreepData } from '../../../../actions';

let { width, height } = Dimensions.get('window');
var reportList = []
var displayDataList = []
var isUpdateFailed = false

class Report extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isVisible: false,
            loading: false
        }
    }

    async UNSAFE_componentWillMount() {
        var displayData = []

        reportList = this.props.navigation.state.params.param;

        //ParameterNameTrim =[];

        displayData = _.groupBy(reportList, reportList => reportList.disp_name);
        var paramNames = keys(displayData)
        var channelValues = values(displayData)

        for (var i = 0; i < paramNames.length; i++) {

            if (channelValues[i].length > 1) {
                if (!channelValues[i][0].status.includes("Success"))
                    isUpdateFailed = true

                let channelA = channelValues[i][0].channel === "A" ? channelValues[i][0].value : channelValues[i][1].value
                let colorA = channelValues[i][0].channel === "A" ? channelValues[i][0].color : channelValues[i][1].color

                let channelB = channelValues[i][0].channel === "B" ? channelValues[i][0].value : channelValues[i][1].value
                let colorB = channelValues[i][0].channel === "A" ? channelValues[i][0].color : channelValues[i][1].color
                displayDataList.push({
                    // Name: channelValues[i][0].Name,
                    ChannelA: channelA,
                    ChannelB: channelB,
                    disp_name: channelValues[i][0].disp_name,
                    unit: channelValues[i][0].unit,
                    colorA: colorA,
                    colorB: colorB
                })
            }
            else {
                if (!channelValues[i][0].status.includes("Success"))
                    isUpdateFailed = true
                let channelA = channelValues[i][0].channel === "A" ? channelValues[i][0].value : "-"
                let colorA = channelValues[i][0].channel === "A" ? channelValues[i][0].color : "#000000"

                let channelB = channelValues[i][0].channel === "B" ? channelValues[i][0].value : "-"
                let colorB = channelValues[i][0].channel === "B" ? channelValues[i][0].color : "#000000"
                displayDataList.push({
                    // Name: channelValues[i][0].Name,
                    ChannelA: channelA,
                    ChannelB: channelB,
                    disp_name: channelValues[i][0].disp_name,
                    unit: channelValues[i][0].unit,
                    colorA: colorA,
                    colorB: colorB
                })
            }
        }
    }

    componentWillUnmount() {
        displayDataList = [];
        isUpdateFailed = false;
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };


    customBackHandler = () => {

        if (this.props.navigation.state.params.reportTilte == "Engine Usage") {
            this.props.navigation.navigate('EngineUsage')
            this.props.FetchEngineUsageData()
        }
        else if (this.props.navigation.state.params.reportTilte == "LCF") {
            this.props.navigation.navigate('LCF')
            this.props.FetchLCFData()
        }
        else if (this.props.navigation.state.params.reportTilte == "CREEP") {
            this.props.navigation.navigate('Creep')
            this.props.FetchCreepData()
        }
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    render() {

        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <Content>

                    <View>
                        {isUpdateFailed ?
                            <View>
                                <UpdateFailedNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text></Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                        <Text></Text>
                                    </View>
                                </View>
                            </View> :
                            <View>
                                <UpdateSuccessNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text></Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                        <Text></Text>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>

                    <Card>
                        <View
                            style={{
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingBottom: 10,
                                paddingTop: 10,
                                backgroundColor: '#FFFFFF'
                            }}>

                            <View style={{ width: '40%' }}>
                                <Text
                                    style={{
                                        paddingLeft: 18,
                                        fontSize: 16,
                                        color: '#3E485E',
                                        fontFamily: 'Arial',
                                        flex: 1
                                    }}></Text>
                            </View>
                            <View style={{ width: '20%' }}>
                                <Text style={
                                    [styles.channel]}>Channel A</Text>
                            </View>
                            <View style={{ width: '20%' }}>
                                <Text style={
                                    [styles.channel]
                                }>Channel B</Text>
                            </View>
                        </View>
                        <View >
                            <View style={{ paddingLeft: 8 }}>

                                {displayDataList.map((item) => {

                                    let unit = item.unit

                                    if (item.unit.match("NA")) {

                                        unit = ""
                                    }
                                    return (
                                        <View style={{
                                            flexDirection: 'row',
                                            backgroundColor: '#FFFFFF',
                                            alignItems: 'center',
                                            width: '100%',
                                            paddingLeft: 5,
                                            paddingBottom: 5,
                                            paddingTop: 5
                                        }}>

                                            {/* <Text style={{ fontSize: 14, color: '#333', fontFamily: 'Arial', flex: 3, justifyContent: 'center' }}>{tabs.Name}</Text> */}
                                            <View style={{ width: '50%' }}>
                                                <Text style={[styles.label, { textAlign: 'left' }]}>{item.disp_name} </Text>
                                            </View>

                                            <View style={{ width: '15%', marginLeft: 5, marginRight: 0 }}>
                                                <Text style={[styles.value, { textAlign: 'left', color: item.colorA }]}>{item.ChannelA}</Text>
                                            </View>

                                            <View style={{ width: '15%', marginLeft: 5, marginRight: 0 }}>
                                                <Text style={[styles.value, { textAlign: 'center', color: item.colorB }]}>{item.ChannelB}</Text>
                                            </View>

                                            <View style={{ width: '20%', }}>
                                                <Text style={[styles.unit, { textAlign: 'left' }]}>{unit}</Text>
                                            </View>
                                        </View>
                                    )
                                })}
                            </View>
                        </View>
                    </Card>

                    <TouchableOpacity
                        style={styles.backToDashboardButton}
                        onPress={() => {
                            this.props.navigation.navigate('Dashboard')
                        }}
                    >
                        <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        )
    }
}

export default connect(null, { FetchEngineUsageData, FetchLCFData, FetchCreepData })(Report)

const styles = StyleSheet.create({
   
    textStyle: {
        height: 40,
        textAlign: 'left'
    },
    channel: {
        width: 54,
        height: 14,
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376"
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0,
        color: "#5B6376",
        flex: 3
    },
    disabled: {
        width: 57,
        height: 40
    },
    unit: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376"
    },
    value: {
        fontFamily: "Arial",
        fontSize:  width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#5B6376"
    },
    lastUpdateTextStyle: {
        fontFamily: "Arial",
        fontSize:  width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#5B6376"
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: width < 450 ? 16 : 22,
        color: "#00A9E0"
    },
    backToDashboard: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    }
})
