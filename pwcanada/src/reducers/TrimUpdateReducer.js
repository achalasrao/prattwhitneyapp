const initialState = {
  fetchTrim: false,
}

export default (state = initialState, action) => {
  switch (action.type) {

    case 'FetchTrim':
      return { ...state, fetchTrim: true };
    case 'DontFetchTrim':
      return { ...state, fetchTrim: false };

    default:
      return state
  }
};
