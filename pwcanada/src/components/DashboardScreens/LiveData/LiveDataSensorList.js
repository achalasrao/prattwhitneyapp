import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, Dimensions } from 'react-native'
import {
    Text,
    Card,
    Content
} from 'native-base';
import axios from 'axios';

let { width, height } = Dimensions.get('window');
var selectedSensors = []//the sensors selected from previous page(LIVE DATA)

export default class LiveDataSensorList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSource: [],//final data that is displayed
            loading: true// state that represents spinner
        }
    }

    //initial life-cycle method
    UNSAFE_componentWillMount() {

        selectedSensors = this.props.navigation.state.params.param;
        this.fetchData()
    }

    //the following function sends livedata_cmd stop request to microFAST
    async  stopCommand() {
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'livedata_cmd',
                        'arg1': 'stop'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            result = response.data[0].value

        } catch (error) {

        }
    }

    //last life-cycle method called before leaving the page
    componentWillUnmount() {
        clearInterval(this._interval);
        this.stopCommand()
    }

    //the following function sends livedata_cmd start request to microFAST
    fetchData = async () => {
        var expectedresponse = "OK\n"
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'livedata_cmd',
                        'arg1': 'start'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            result = response.data[0].value

            //call getValues every 5 seconds if start status is OK else, send the request again
            if (JSON.stringify(expectedresponse) === JSON.stringify(result)) {

                this._interval = setInterval(async () => {
                    await this.getValues()
                }, 1000);
            }
            //if-start command fails, resend the request again
            else {
                this.fetchData()
            }
        } catch (error) {
            //if there is network error, resend the request again
            this.fetchData()
        }
    }

    //the following function sends livedata_cmd get request to microFAST
    getValues = async () => {

        var responseList = []
        var displayList = [];

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'livedata_cmd',
                        'arg1': 'get'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            result = response.data[0].value
            var lines = await result.split("|");

            for (var i = 1; i < lines.length - 1; i++) {
                s = lines[i]
                s = await s.split(":");
                responseList.push(s)
            }

            var tempSen = []

            for (var i = 1; i < selectedSensors.length; i++) {

                var resultList = responseList.filter(d => {
                    return d[0].match(selectedSensors[i].id);
                });

                displayList.push({
                    id: resultList[0][0],
                    value: resultList[0][1],
                    validity: resultList[0][2],
                    name: selectedSensors[i].name,
                    unit: selectedSensors[i].unit
                })
            }

            if (displayList.length > 0) {
                this.setState({ userDataSource: displayList, loading: false });
            }

        } catch (error) {

        }

        //if the list to be displayed is empty, resend the request
        if (displayList.length == 0) {
            this.getValues()
        }
    }

    render() {

        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }

        return (
            <View style={{ flex: 1, }}>
                <Content>
                    <Text></Text>
                    <Card>
                        <View style={styles.tileStyle}>
                            <View style={{ paddingLeft: 8 }}>
                                {this.state.userDataSource.map((item) => {
                                    var color = "#1F2A44"
                                    if (item.validity === "0") {
                                        color = "#EB1616"
                                    }
                                    return (
                                        <View style={{ flexDirection: "row", paddingLeft: 5 }}>
                                            <View style={{ width: '40%' }}>
                                                <Text style={[styles.label2, { textAlign: 'left' }]}>{item.name}</Text>
                                            </View>
                                            <View style={{ width: '30%' }}>
                                                <Text style={[styles.label2, { textAlign: 'center', color: color }]}>{item.value}</Text>
                                            </View>
                                            <View style={{ width: '20%' }}>
                                                <Text style={[styles.unit, { textAlign: 'left', paddingRight: 5 }]}>{item.unit}</Text>
                                            </View>
                                        </View>
                                    )
                                })}
                            </View>
                        </View>
                    </Card>
                </Content>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    listConatiner: {
        backgroundColor: '#000000',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginRight: 2,
        marginLeft: 2
    },
    container: {
        backgroundColor: '#000000',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    unit: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#848A98",
        paddingTop: 13,
        justifyContent: 'flex-end'
    },
    label2: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#1F2A44",
        paddingTop: 13,
        justifyContent: 'flex-end'
    },
});
