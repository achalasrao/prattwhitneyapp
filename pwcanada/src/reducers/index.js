import { combineReducers } from 'redux';
import FooterReducer from './FooterReducer';
import ConnectionReducer from './connectionReducer';
import RetriveEngineDataReducer from './RetriveEngineDataReducer';
import TrimUpdateReducer from './TrimUpdateReducer';

// ======View/EditEngineData======
import LRUreducer from './ViewEditEngineData/LRU/LRUreducer';
import EngineDataReducer from './ViewEditEngineData/EngineData/EngineDataReducer';
import GasGeneratorReducer from './ViewEditEngineData/EngineData/GasGeneratorReducer';
import PerformanceDataReducer from './ViewEditEngineData/EngineData/PerformanceDataReducer';
import PowerSectionReducer from './ViewEditEngineData/EngineData/PowerSectionReducer';
import BuildSpecReducer from './ViewEditEngineData/EngineData/BuildSpecReducer';
import CellularConfig from './CellularConfigureReducer';
import AircraftUpdateReducer from './AircraftUpdateReducer';
import EpecsReducer from './EpecsReducer';

//Combine reducer we can define all the reducers
export default combineReducers({

    footer: FooterReducer,
    //  ==
    connection: ConnectionReducer,
    RetriveEngineData: RetriveEngineDataReducer,
    TrimReducer: TrimUpdateReducer,

    // =======View/EditEngineData======
    LRUreducer: LRUreducer,
    EngineDataReducer: EngineDataReducer,
    GasGeneratorReducer: GasGeneratorReducer,
    PerformanceDataReducer: PerformanceDataReducer,
    PowerSectionReducer: PowerSectionReducer,
    BuildSpecReducer: BuildSpecReducer,
    CellularConfig: CellularConfig,
    AircraftUpdateReducer: AircraftUpdateReducer,
    EpecsReducer: EpecsReducer
});
