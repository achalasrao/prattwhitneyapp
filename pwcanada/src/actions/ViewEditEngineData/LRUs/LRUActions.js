// Capture the selected LRU item details
export const selectedLRU = (item) => {

    return {
        type: 'selectedLRU',
        payload: item
    }
};

//PartNumber of LRU to update
export const PNtoUpdate = (item) => {

    return {
        type: 'PNtoUpdate',
        payload: item
    }
};

// SerialNumber of LRU to update
export const SNtoUpdate = (item) => {

    return {
        type: 'SNtoUpdate',
        payload: item
    }
};

// fetch LRU details
export const ReFetch = () => {

    return {
        type: 'ReFetch',
        payload: true
    }
};

// dont fetch the LRU details
export const DontReFetch = () => {

    return {
        type: 'DontReFetch',
        payload: false
    }
};

// QR code scan was successfull
export const scanSuccess = () => {

    return {
        type: 'scanSuccess',
        payload: true
    }
};

// QR code scan was failed
export const scanFailed = () => {

    return {
        type: 'scanSuccess',
        payload: false
    }
};

export const enableUpdate = () => {

    return {
        type: 'enableUpdate',
        payload: true
    }
};

export const disableUpdate = () => {

    return {
        type: 'disableUpdate',
        payload: false
    }
};
