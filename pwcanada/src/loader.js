import React, { Component } from 'react';
import { View, StyleSheet, Easing, Animated, Image, NetInfo, Text, Alert, Linking, ActivityIndicator, Dimensions } from 'react-native';
import RNExitApp from 'react-native-exit-app';
import Toast, { DURATION } from 'react-native-easy-toast';
import axios from 'axios';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { connectionStatus } from './actions';

let { width, height } = Dimensions.get('window');

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            response: null,
            fadeAnim: new Animated.Value(0),
            connectionState: '',
            connectionType: '',
            loaderVisible: false,
            initial: true,
            printable: ''
        }
    }

    startAnimation = () => {
        Animated.timing(
            this.state.fadeAnim,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear
            }
        ).start()
    }

    pingMicroFAST = async () => {
        this.setState({ loaderVisible: false })
        setTimeout(() => {

            this.props.navigation.navigate('Dashboard')
        }, 4000)
        setTimeout(() => {

            this.setState({ initial: true })
            this.props.navigation.navigate('Dashboard')
        }, 4000)

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'monitor_status',
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            if (response.status == '200') {
                this.props.connectionStatus('MicroFAST')

                this.setState({ printable: 'Connected to MicroFAST-200' })
                this.refs.toast.show('Connected to DCTU', 2000)
            } else {

                this.props.connectionStatus('Not MicroFAST')
                this.setState({ printable: 'Not 200' })
                // this.microFASTIsDown()

            }
        }
        catch (error) {
            this.props.connectionStatus('Network Error')
            this.setState({ printable: 'Network Error' })
            // this.enableWifiPrompt()
        }
    }

    connectionSettler = async () => {
        setTimeout(async () => {
            if (this.state.connectionState && this.state.connectionType === 'wifi') {

                this.props.connectionStatus('External Wifi')
                this.setState({ printable: 'External Wifi' })
                this.setState({ loaderVisible: false })
                // this.connectToMicroFASTPrompt()
            } else if (!this.state.connectionState && this.state.connectionType === 'none') {
                this.pingMicroFAST()
            }
        }, 4000)
    }

    componentDidMount = async () => {
        this.setState({ loaderVisible: true })
        this.startAnimation()
        this.pingMicroFAST()
    }

    componentWillUnmount() {
        this.clearInterval(this.AnimTimer);
    }

    _handleConnectivityChange = (isConnected) => {

        NetInfo.getConnectionInfo().then((connectionInfo) => {
            this.setState({ connectionType: connectionInfo.type, connectionState: isConnected })

        });
    };

    enableWifiPrompt = () => {
        return (
            Alert.alert(
                'Alert',
                'Please enable WiFi.Redirect to settings',
                [
                    {
                        text: 'Redirect', onPress: () => {

                            this._reDirect()
                        }
                    },
                    {
                        text: 'Exit App', onPress: () => {

                            this._exit()
                        }, style: 'destructive'
                    },
                ],
                { cancelable: false }
            )
        )
    }

    connectToMicroFASTPrompt = () => {
        return (
            Alert.alert(
                'Alert',
                'Please connect to DCTU. Redirect to settings',
                [
                    {
                        text: 'Redirect', onPress: () => {

                            this._reDirect()
                        }
                    },
                    {
                        text: 'Exit App', onPress: () => {

                            this._exit()
                        }, style: 'destructive'
                    },
                ],
                { cancelable: false }
            )
        )
    }

    microFASTIsDown = () => {
        return (
            Alert.alert(
                'Alert',
                'DCTU is down.',
                [
                    {
                        text: 'Exit App', onPress: () => {

                            this._exit()
                        }, style: 'destructive'
                    },
                ],
                { cancelable: false }
            )
        )
    }

    _exit() {
        RNExitApp.exitApp();
    }

    _reDirect() {
        Linking.canOpenURL('app-settings:').then(supported => {
            Linking.openURL("App-Prefs:root=WIFI");
        }).catch(error => {

        })
    }

    render() {
        let { fadeAnim } = this.state;
        return (
            <View style={{
                backgroundColor: '#fff', flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <Animated.View
                    style={{
                        opacity: fadeAnim,
                    }}>
                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: 320,
                        height: 90
                    }}>
                        <Image
                            style={
                                {
                                    flex: 1,
                                    height: undefined,
                                    width: undefined,
                                    alignSelf: 'stretch'
                                }
                            }
                            source={require('./assets/icons/PWC/pwc_eb.jpg')}
                            resizeMode="contain"
                        />
                    </View>
                </Animated.View>
                {this.state.loaderVisible ?
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#f4f4f4', paddingLeft: 20, paddingRight: 20, paddingBottom: 10, paddingTop: 10, borderRadius: 10 }}>
                            <Text style={{ fontSize: 18, fontFamily: 'SourceSansPro-SemiBold' }}>Loading... </Text>
                            <ActivityIndicator size="large" color={"#" + ((1 << 24) * Math.random() | 0).toString(16)} animating={this.state.loaderVisible} />
                        </View>
                    </View> : <View />
                }
                <Toast
                    ref="toast"
                    style={{ backgroundColor: '#71FFB1' }}
                    position='bottom'
                    positionValue={200}
                    fadeInDuration={50}
                    fadeOutDuration={500}
                    textStyle={{ color: '#000000', fontSize: width < 450 ? 14 : 18 }}
                    
                />
            </View>
        )
    }
}


export default connect(null, { connectionStatus })(withNavigationFocus(LoginForm));

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#edeff0'
    },
    imageContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        width: 320,
        height: 90
    },
    imageStyle: {
        flex: 1,
        height: undefined,
        width: undefined,
        alignSelf: 'stretch'
    },
    txtContainer: {
        flexDirection: 'row',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 15,
        justifyContent: 'space-around'
    },
    labelstyle: {
        fontSize: 18,
        paddingLeft: 20,
        justifyContent: 'center',
        alignSelf: 'center',
        fontFamily: 'Arial'

    },
    emailLabelstyle: {
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 2,
        justifyContent: 'center',
        alignSelf: 'center',
        fontFamily: 'Arial'
    },
    textArea: {
        color: '#000',
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: 18,
        lineHeight: 23,
        flex: 1
    },
    emailTextArea: {
        color: '#000',
        paddingRight: 10,
        paddingLeft: 40,
        fontSize: 18,
        lineHeight: 23,
        flex: 1,
        alignContent: 'flex-start'
    },
    visibleContainer: {
        justifyContent: 'center',
        alignContent: 'center',
        paddingLeft: 5,
        paddingRight: 15
    },
    buttonstyle: {
        backgroundColor: '#ffcc33',
        alignSelf: 'stretch'
    },
    buttonContainer: {
        alignSelf: 'stretch',
        justifyContent: 'center',
        flexDirection: 'row',
        elevation: 1,
        paddingLeft: 80,
        paddingRight: 80,
        marginTop: 25,
        alignSelf: 'stretch'
    },
    buttonTextStyle: {
        color: '#333333',
        fontSize: 20
    },
});
