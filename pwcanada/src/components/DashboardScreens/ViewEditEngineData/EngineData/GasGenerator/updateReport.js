import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { FetchEngineData } from '../../../../../actions';
import { UpdateFailedNotifier } from '../../../../common';
import { UpdateSuccessNotifier } from '../../../../common';
import IonicIcon from 'react-native-vector-icons/Ionicons';

class LRUupdateReport extends Component {

    constructor(props) {
        super(props)
        this.customBackHandler = this.customBackHandler.bind(this);
        report = this.props.navigation.state.params.param;
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            title: navigation.getParam('headerTilte'),
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    customBackHandler = () => {
        this.props.navigation.navigate('EngineData')
        this.props.FetchEngineData()
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    render() {
        let {
            oldEngineModel,
            oldEngineSerialNumber,
            oldModuleSerialNumber,
            oldTakeOffDry,
            oldDOTTypeCert,
            oldFAATypeCert,
            engineModel,
            engineSerialNumber,
            moduleSerialNumber,
            takeOffDry,
            DOTTypeCert,
            FAATypeCert,
        } = this.props

        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View>
                        {report.includes("Failed") ?
                            <View>
                                <UpdateFailedNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text style={styles.LRUnameTextStyle}>{this.props.selectedEngineModule.ModuleName} Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View> :
                            <View>
                                <UpdateSuccessNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text style={styles.LRUnameTextStyle}>{this.props.selectedEngineModule.ModuleName} Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>

                    <View style={styles.rectangleConatinerStyle

                    }>
                        {
                            oldEngineModel !== engineModel ?
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>Engine Model</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report[1].includes("success") ? "green" : "red" }]}>  {engineModel}</Text>
                                </View> :
                                <View />
                        }
                        {
                            oldEngineSerialNumber !== engineSerialNumber ?
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>Engine Serial Number</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report[0].includes("success") ? "green" : "red" }]}>  PCE-{engineSerialNumber}</Text>
                                </View> : <View />
                        }
                        {
                            oldModuleSerialNumber !== moduleSerialNumber ?
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>Gas Generator Serial Number</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report[2].includes("success") ? "green" : "red" }]}> {moduleSerialNumber}</Text>
                                </View> : <View />
                        }
                        {
                            oldTakeOffDry !== takeOffDry ?
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>Take-off Dry</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report[3].includes("success") ? "green" : "red" }]}>  {takeOffDry} SHP</Text>
                                </View> : <View />
                        }
                        {
                            oldDOTTypeCert !== DOTTypeCert ?
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>TCCA Certificate</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report[4].includes("success") ? "green" : "red" }]}>  {DOTTypeCert}</Text>
                                </View> : <View />
                        }
                        {
                            oldFAATypeCert !== FAATypeCert ?
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>FAA Certificate</Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report[5].includes("success") ? "green" : "red" }]}>  {FAATypeCert}</Text>
                                </View> : <View />
                        }
                    </View>

                    <TouchableOpacity
                        style={styles.backToDashboardButton}
                        onPress={() => {
                            this.props.navigation.navigate('Dashboard')
                            this.props.FetchEngineData()
                        }}
                    >
                        <Text style={styles.backToDashboard}><IonicIcon name='ios-arrow-back' style={styles.leftIcon} /> Back to Dashboard</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = ({ LRUreducer, GasGeneratorReducer, EngineDataReducer }) => {
    const { selectedLRU, SNValue, PNValue } = LRUreducer
    const {
        oldEngineModel,
        oldEngineSerialNumber,
        oldModuleSerialNumber,
        oldTakeOffDry,
        oldDOTTypeCert,
        oldFAATypeCert,
        engineModel,
        engineSerialNumber,
        moduleSerialNumber,
        takeOffDry,
        DOTTypeCert,
        FAATypeCert,
    } = GasGeneratorReducer
    const { selectedEngineModule } = EngineDataReducer
    return {
        selectedLRU, SNValue, PNValue, selectedEngineModule,
        oldEngineModel,
        oldEngineSerialNumber,
        oldModuleSerialNumber,
        oldTakeOffDry,
        oldDOTTypeCert,
        oldFAATypeCert,
        engineModel,
        engineSerialNumber,
        moduleSerialNumber,
        takeOffDry,
        DOTTypeCert,
        FAATypeCert,
    };
};

export default connect(mapStateToProps, { FetchEngineData })(LRUupdateReport);

const styles = StyleSheet.create({

    LRUnameTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#000000",
        paddingTop: 12
    },
    lastUpdateTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#666666"
    },
    rectangleConatinerStyle: {
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e5e5e5",
        marginTop: 20,
        paddingBottom: 20,
        paddingTop: 20
    },
    childRectangleStyle: {
        paddingTop: 10,
        paddingLeft: 16
    },
    rectangleTitleTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333"
    },
    rectangleContentTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#666666",
        paddingTop: 16
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: 16,
        color: "#0033ab"
    },
    backToDashboard: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#0033ab"
    }
})
