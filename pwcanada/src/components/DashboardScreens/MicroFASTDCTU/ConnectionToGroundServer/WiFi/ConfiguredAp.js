import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, Alert, Image, Dimensions } from 'react-native';
import { Footer, FooterTab, Text, Button, Content } from 'native-base';
import { connect } from 'react-redux';
import { confWifiShowList, updateConfigured, AllConfiguredNetworks } from '../../../../../actions';
import axios from 'axios';
import Modal from "react-native-modal";
import { withNavigationFocus } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

var i = 5;
let { width, height } = Dimensions.get('window');

class ConfiguredAp extends Component {
    constructor(props) {
        super(props)

        this.state = {
            ConfiguredDataSource: [],
            loading: false,
            modalVisible: false,
            isHidden: false,
            refreshing: false,
            isEmpty: false,

        }
    }

    UNSAFE_componentWillMount = async () => {
        this.setState({ loading: true })
    }

    componentDidMount = async () => {

        await this.fetchdata()
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    async componentDidUpdate() {
        if (this.props.updateConfiguration == true) {
            await this.fetchdata()
        }
    }

    fetchdata = async () => {
        let Confdata = [];

        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "wifi_read_aps"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            let responseJson = await response;

            var lines = await JSON.stringify(responseJson.data[0].value);
            var notFound = true;

            var splitwise = await lines.split("\\n");


            for (var line = 0; line < splitwise.length; line++) {


                lines = splitwise[line].split(" ");
                if (splitwise[line] == '"') continue;

                var tabs = await splitwise[line].split('\\t');
                var jsondata = [];

                jsondata['Name'] = tabs[0];
                jsondata['SSID'] = tabs[1];
                jsondata['MAC'] = tabs[2];
                jsondata['loc'] = tabs[3];
                jsondata['sec'] = tabs[4];
                jsondata['key'] = tabs[5];

                if (jsondata['SSID'] != "SSID") {

                    if (jsondata['SSID'] != undefined) {
                        Confdata.push(jsondata);
                    }
                }
            }

            this.props.updateConfigured(false);
            this.props.AllConfiguredNetworks(Confdata);
            this.setState({ ConfiguredDataSource: Confdata, loading: false, isEmpty: false });

            if (Confdata.length == 0 && i >= 1) {

                i--;
                this.props.updateConfigured(false);

                await this.fetchdata()
            }
            else if (Confdata.length == 0 && i < 1) {

                Alert.alert(
                    "Configured Networks are empty",
                    "",
                    [
                        {
                            text: "OK",
                            onPress: () => {

                            }
                        }
                    ],
                    { cancelable: false }
                );
                this.setState({ ConfiguredDataSource: Confdata, loading: false, isEmpty: true });
            }

        }

        catch (error) {

            if (Confdata.length == 0 && i >= 1) {
                i--;
                this.props.updateConfigured(false);

                await this.fetchdata()
            }
            else if (Confdata.length == 0 && i < 1) {
                Alert.alert(
                    "Configured Networks are empty",
                    "",
                    [
                        {
                            text: "OK",
                            onPress: () => {

                            }
                        }
                    ],
                    { cancelable: false }
                );
                this.setState({ ConfiguredDataSource: Confdata, loading: false, isEmpty: true });
            }
        }
    }

    renderRow(item) {

        return (

            <View style={{ flex: 1, height: 60, flexDirection: 'row', backgroundColor: '#FFFFFF', marginBottom: 1.5 }}>
                <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 10 }}>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontSize: width < 450 ? 16 : 22, color: '#1F2A44', fontFamily: 'Arial' }}>{item.SSID}</Text>
                        <Text style={{ fontSize: width < 450 ? 14 : 20, color: '#848A98', fontFamily: 'Arial' }}>{item.sec}</Text>
                    </View>
                </View>
                <View style={{ flex: .5, justifyContent: 'center', alignItems: 'center' }}>

                </View>
                <View style={{ flex: .3, justifyContent: 'center', alignItems: 'center', paddingLeft: 5, paddingRight: 1, paddingTop: 8, paddingBottom: 15 }}>
                    <TouchableOpacity style={styles.circularbtn} onPress={() => this.removeButton(item.SSID)}>
                        <Icon name="minus" size={25} style={styles.checkIcon} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: .3, justifyContent: 'center', alignItems: 'center', paddingRight: 1, paddingTop: 8, paddingBottom: 15 }}>
                    <TouchableOpacity onPress={() => this.infoList(true, item)}>
                        <Image style={{
                            flex: 1,
                            alignSelf: 'stretch',
                            width: 34,
                            height: 34
                        }}
                            source={require('../../../../../assets/IconPack/ConfiguredNetworks/btnInfo.png')}
                            resizeMode='contain' />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    async infoList(visible, item) {
        this.setState({ modalVisible: visible, selectedItem: item });
        this.props.confWifiShowList(item);
    }

    removeButton = async (SSID) => {
        if (SSID) {
            try {
                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "wifi_remove_ap",
                            "arg1": JSON.stringify(SSID)
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })

                let responseJSON = await response;

                this.fetchdata()
            } catch (error) {

            }
        } else {

        }
    }

    async onSubmit() {

        Alert.alert(
            'Do you want to delete all the Configured Networks?',
            '',
            [
                // { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                { text: 'Yes', onPress: () => this.removeAll() },

                { text: 'No', onPress: () => { } },
            ],
            { cancelable: true }
        )
    }

    async removeAll() {
        let configuredNetworks = this.props.allconfigured;

        for (var i = 0; i < configuredNetworks.length; i++) {

            try {
                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            'cmd': 'wifi_remove_ap',
                            "arg1": JSON.stringify(configuredNetworks[i].SSID)
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })

                let responseJSON = await response;


            } catch (error) {

            }
        }
        this.fetchdata();
    }

    renderView() {
        return (
            this.state.ConfiguredDataSource.map((tabs) =>
                this.renderRow(tabs)
            )
        )
    }

    render() {
        return (
            <View style={{ flex: 1, marginTop: 10 }}>
                {
                    this.state.loading ?
                        <View style={[styles.container]}>
                            <ActivityIndicator size="large" color="#5B6376" />
                        </View> :
                        <Content>
                            <View style={{ backgroundColor: '#E3E5E8' }} >
                                {this.renderView()}
                            </View>
                        </Content>
                }

                <Modal
                    isVisible={this.state.modalVisible}
                    animationInTiming={1000}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}
                >
                    <View style={styles.modalContent}>

                        <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', paddingBottom: 10, width: '100%' }}>
                            <Text style={styles.infoListSSID}>{this.props.confwifilist.SSID}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>

                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>SSID :</Text>
                            </View>

                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.confwifilist.SSID}</Text>
                            </View>

                        </View>

                        <View style={{ flexDirection: 'row', width: '100%' }}>

                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Name :</Text>
                            </View>

                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.confwifilist.Name}</Text>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', width: '100%' }}>

                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Mac Address :</Text>
                            </View>

                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.confwifilist.MAC}</Text>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', width: '100%' }}>

                            <View style={{ width: '40%' }}>
                                <Text style={styles.inofListLabel}>Security :</Text>
                            </View>

                            <View style={{ width: '60%' }}>
                                <Text style={styles.infoListValue}>{this.props.confwifilist.sec}</Text>
                            </View>

                        </View>

                        <View>

                            <Button style={styles.closebtn} onPress={() => this.infoList(!this.state.modalVisible, this.state.selectedItem)}><Text style={styles.txtbtnclose}>Close</Text></Button>
                        </View>

                    </View>
                </Modal>

                {
                    this.state.isEmpty == true ? <Footer>
                        <FooterTab>
                            {/* <ActionButton buttonColor="rgba(231,76,60,1)" style={{marginTop:-12}}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Forget All" onPress={() => this.onSubmit()} >
                                <Icon name="md-trash" style={styles.actionButtonIcon} />
                            </ActionButton.Item>

                        </ActionButton> */}
                            <View></View>
                        </FooterTab>
                    </Footer> : <Footer>
                            <FooterTab>
                                {/* <ActionButton buttonColor="rgba(231,76,60,1)" style={{marginTop:-12}}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Forget All" onPress={() => this.onSubmit()} >
                                <Icon name="md-trash" style={styles.actionButtonIcon} />
                            </ActionButton.Item>

                        </ActionButton> */}
                                <View style={[styles.checkbox, { width: '100%' }]}>

                                    <Text style={styles.forgetAll} onPress={() => this.onSubmit()} > <Image style={{
                                        flex: 1,
                                        width: 25,
                                        height: 25,
                                        paddingRight: 5
                                    }}
                                        source={require('../../../../../assets/IconPack/ConfiguredNetworks/trash.png')}
                                        resizeMode='contain' />Forget All Configured Networks</Text>
                                </View>
                            </FooterTab>
                        </Footer>
                }
            </View>
        )
    }
}

const mapStateToProp = ({ footer }) => {

    // const { ConfigureApFocussed, AddConfigurationFocussed, AvailableApFocussed, confwifilist } = footer;
    // return { ConfigureApFocussed, AddConfigurationFocussed, AvailableApFocussed, confwifilist };
    const { confwifilist, updateConfiguration, allconfigured } = footer;
    return { confwifilist, updateConfiguration, allconfigured };
};

export default connect(mapStateToProp, { confWifiShowList, updateConfigured, AllConfiguredNetworks })(withNavigationFocus(ConfiguredAp));

const styles = StyleSheet.create({
    
    txtbtnclose: {
        color: "#FFFFFF"
    },
    closebtn: {
        backgroundColor: '#00A9E0',
        marginTop: 20
    },
    modalContent: {
        backgroundColor: "#FFFFFF",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    circularbtn: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 32,
        height: 32,
        backgroundColor: '#00A9E0',
        borderRadius: 100
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E3E5E8'
    },
    footer: {
        backgroundColor: '#1F2A44'
    },
    title: {
        color: '#5B6376',
        fontWeight: 'bold',
        fontSize: width < 450 ? 18 : 24,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        paddingTop: 20
    },
    checkbox: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 10,
        paddingTop: 10
    },
    forgetAll: {
        height: 22,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        color: "#00A9E0",
        justifyContent: 'center',
        paddingBottom: 10
    },
    checkIcon: {
        height: 22,
        textAlign: 'right',
        color: "#FFFFFF"
    },
    infoListSSID: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.25,
        color: "#1F2A44",
        textAlign: 'left'
    },
    inofListLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 14 : 20,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 16 : 22,
        letterSpacing: 0.25,
        color: "#848A98",
        textAlign: 'left'
    },
    infoListValue: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.25,
        color: "#1F2A44",
        textAlign: 'left'
    }
});
