export const FetchTrim = () => {
    return {
        type: 'FetchTrim'
    }
}
export const DontFetchTrim = () => {
    return {
        type: 'DontFetchTrim'
    }
}
