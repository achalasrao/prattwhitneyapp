import React, { Component } from "react";
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator, 
    Dimensions
} from "react-native";
import { connect } from "react-redux";
import {
    SNtoUpdate,
    PNtoUpdate,
    DontReFetch,
    ReFetch,
    scanFailed,
    disableUpdate,
    enableUpdate
} from "../../../../actions";
import axios from "axios";
import Modal from "react-native-modal";

let { width, height } = Dimensions.get('window');

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Manually edit the fields
// Redirect to QR Code scanner upon clicking the Scan button
// Change the textIput styles
//
// Coded on: Oct 08 2018
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class updateLRU extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam("headerTilte")
        };
    };

    state = {
        partNumer: "",
        serialNumber: "",
        updateSuccess: false,
        updateFailed: false,
        isModalVisible: false,
        isLoading: false
    };

    // Update the fileds and wait for results
    updateFields = async () => {
        let EngineID = this.props.selectedLRU.EngineID,
            LRUID = this.props.selectedLRU.LRUID,
            SN = this.props.SNValue,
            PN = this.props.PNValue;
        timeStamp = new Date().toISOString().split('.')[0]

        try {
            let response = await axios({
                method: "post",
                url: "http://microFAST/cgi-bin/data_access.cgi?update",
                    data: [
                        {
                            cmd: "xml_access",
                            arg1: "write",
                            arg2: `Engine[@ID=${EngineID}]/LRU[@ID=${LRUID}]/SN`,
                            arg3: `${SN}`
                        },
                    {
                        cmd: "xml_access",
                        arg1: "write",
                        arg2: `Engine[@ID=${EngineID}]/LRU[@ID=${LRUID}]/PN`,
                        arg3: `${PN}`
                    },
                    {
                        cmd: "xml_access",
                        arg1: "write",
                        arg2: `Engine[@ID=${EngineID}]/LRU[@ID=${LRUID}]/date_update`,
                        arg3: timeStamp
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });

            ///Parser for the response
            let responseJson = await response;

            var lines = JSON.stringify(responseJson.data);
            let splitlines = lines.split(":")

            for (var i = 0; i < splitlines.length; i++) {
                if (splitlines[i].includes("Success")) {
                    result = "success"
                }
                else if (splitlines[i].includes("Err")) {
                    result = "Failed"
                }
            }

            if (String(responseJson.data).includes("Data Error")) {
                this.updateWasFailure();
            } else {
                this.writeToXml();
            }
        } catch (err) {
            this.updateFields();
        }
    };

    // if update was successful commit the XML file
    writeToXml = async () => {
        try {
            let response = await axios({
                method: "post",
                url: "http://microFAST/cgi-bin/data_access.cgi?update",
                data: [
                    {
                        cmd: "xml_access",
                        arg1: "write"
                    }
                ],
                headers: { "Content-Type": "application/json" }
            });
            let responseJson = await response;
            if (responseJson.data[0].value === "Success") {
                this.updateWasSuccess();
            } else {
                this.updateWasFailure();
            }
        } catch (error) {
            this.writeToXml();
        }
    };

    updateWasFailure = () => {
        this.setState({
            updateFailed: true,
            updateSuccess: false,
            isLoading: false
        });
    };

    updateWasSuccess = () => {
        this.props.navigation.navigate("updateLRUReport", { param: result, headerTilte: this.props.selectedLRU.LRUName});
        this.props.ReFetch();
        this.props.scanFailed();
        this.props.disableUpdate();
        this.setState({
            updateSuccess: true,
            updateFailed: false,
            isLoading: false
        });
    };

    componentDidUpdate = () => {
        if (
            this.props.selectedLRU.SN === this.props.SNValue &&
            this.props.selectedLRU.PN === this.props.PNValue
        ) {
            this.props.disableUpdate();
        } else {
            this.props.enableUpdate();
        }
    };

    reverseValue = () => {
        this.setState({ isModalVisible: false });
        this.props.SNtoUpdate(this.props.selectedLRU.SN);
        this.props.PNtoUpdate(this.props.selectedLRU.PN);
    };

    confirmedUpdate = () => {
        this.setState({ isModalVisible: false, isLoading: true });
        this.updateFields();
    };

    renderModal = () => {
        let previousSN = this.props.selectedLRU.SN,
            previousPN = this.props.selectedLRU.PN,
            newSN = this.props.SNValue,
            newPN = this.props.PNValue;

        return (
            <View style={styles.modalStyle}>
                <View style={{ width: "100%", padding: 10 }}>
                    <Text style={styles.modalHeader}>
                        You are about to load this data to the MicroFAST
          </Text>
                </View>

                <View style={{ width: "100%", padding: 10, paddingTop: 10 }}>
                    {previousSN === newSN ? (
                        <View />
                    ) : (
                            <View style={{ flexDirection: "row" }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>
                                    LRU Serial Number is being changed from {previousSN} to {newSN}
                                </Text>
                            </View>
                        )}
                    {previousPN === newPN ? (
                        <View />
                    ) : (
                            <View style={{ flexDirection: "row" }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>
                                    LRU Part Number is being changed from {previousPN} to {newPN}
                                </Text>
                            </View>
                        )}
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>
                        This will erase previous data
          </Text>
                </View>

                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        paddingTop: 13
                    }}
                >
                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this.reverseValue()}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.confirmedUpdate()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.state.isLoading ? (
                    <View
                        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
                    >
                        <ActivityIndicator size="large" color="#5B6376" />
                    </View>
                ) : (
                        <View>
                            <View style={styles.tileStyle}>
                                {this.props.selectedLRU.scannable === "true" && (
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ updateSuccess: false });
                                                this.props.navigation.navigate("scanLRU", {
                                                    headerTilte: this.props.selectedLRU.LRUName
                                                });
                                            }}
                                        >
                                            <View style={{ flexDirection: "row" }}>
                                                <Image
                                                    style={styles.qrCodeScan}
                                                    source={require("../../../../assets/IconPack/ViewEditEngineData/LRUs/qrCodeScan.png")}
                                                    resizeMode="contain"
                                                />
                                                <Text style={styles.scanQrCode}>Scan 2D barcode</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                )}

                                <View style={{ paddingTop: 10 }}>
                                    <Text style={styles.label}>Part Number</Text>
                                    <TextInput
                                        style={[
                                            styles.textInput,
                                            {
                                                borderColor: this.state.pnfieldFocused
                                                    ? "#000000"
                                                    : "#000000",
                                                backgroundColor: this.state.pnfieldFocused
                                                    ? "#FFFFFF"
                                                    : "#F7F7F7",
                                                
                                            }
                                        ]}
                                        onChangeText={text => {
                                            this.props.PNtoUpdate(text);
                                        }}
                                        onFocus={() => this.setState({ pnfieldFocused: true })}
                                        onBlur={() => this.setState({ pnfieldFocused: false })}
                                        value={this.props.PNValue}
                                        textContentType={"telephoneNumber"}
                                        selectionColor={"#00A9E0"} //change the cursor color
                                    />
                                </View>

                                <View style={{ paddingTop: 10 }}>
                                    <Text style={styles.label}>Serial Number</Text>
                                    <TextInput
                                        style={[
                                            styles.textInput,
                                            {
                                                borderColor: this.state.snfieldFocused
                                                    ? "#000000"
                                                    : "#000000",
                                                backgroundColor: this.state.snfieldFocused
                                                    ? "#FFFFFF"
                                                    : "#F7F7F7"
                                            }
                                        ]}
                                        onFocus={() => this.setState({ snfieldFocused: true })}
                                        onBlur={() => this.setState({ snfieldFocused: false })}
                                        onChangeText={text => {
                                            if (this.props.isScanSuccess) {
                                                this.props.scanFailed();
                                            }
                                            this.props.SNtoUpdate(text);
                                        }}
                                        value={this.props.SNValue}
                                        textContentType={"telephoneNumber"}
                                        selectionColor={"#00A9E0"} //change the cursor color
                                    />
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    justifyContent: "space-between",
                                    marginTop: 30,
                                    marginLeft: 15,
                                    marginRight: 15,
                                    marginBottom: 15
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ updateSuccess: false });
                                        this.reverseValue();
                                    }}
                                    style={styles.cancelButton}
                                >
                                    <Text style={styles.cancelLabel}>Cancel</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    disabled={!this.props.isUpdateRequired}
                                    // disabled={(this.props.selectedLRU.SN === this.props.SNValue) && (this.props.selectedLRU.PN === this.props.PNValue)}
                                    onPress={() => this.setState({ isModalVisible: true })}
                                    style={[ styles.updateButton, { backgroundColor: this.props.isUpdateRequired ? "#00A9E0" : "#62DBFF" }] }>
                                    <Text
                                        style={[ styles.updateLabel, { color: this.props.isUpdateRequired ? '#FFFFFF' : '#5B6376' }] }>
                                        Update
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                <Modal
                    style={styles.modalContainer}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={() => this.setState({ isModalVisible: true })}
                >
                    {this.renderModal()}
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = ({ LRUreducer }) => {
    const {
        selectedLRU,
        PNValue,
        SNValue,
        isScanSuccess,
        isUpdateRequired
    } = LRUreducer;
    return { selectedLRU, PNValue, SNValue, isScanSuccess, isUpdateRequired };
};

export default connect(
    mapStateToProps,
    {
        SNtoUpdate,
        PNtoUpdate,
        DontReFetch,
        ReFetch,
        scanFailed,
        disableUpdate,
        enableUpdate
    }
)(updateLRU);

const styles = StyleSheet.create({

    tileStyle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        padding: 18
    },
    qrCodeScan: {
        width: 24,
        height: 24
    },
    scanQrCode: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "#00A9E0",
        textAlignVertical: "center",
        paddingLeft: 8
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#5B6376",
        paddingBottom: 5
    },
    textInput: {
        width: "100%",
        height: 40,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#A5A9B4",
        textAlign: "left"
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#00A9E0",
        justifyContent: "center"
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: "center"
    },
    cancelLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#00A9E0"
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    modalContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    modalStyle: {
        width: "100%",
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        padding: 23
    },
    modalHeader: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.25,
        color: "#000000"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        justifyContent: "center"
    },
    previousLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    confirmButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: "center"
    },
    confirmLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    }
});
