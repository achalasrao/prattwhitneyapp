import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, TouchableOpacity, TextInput, Alert, Keyboard, ScrollView } from 'react-native';
import {
    Label
} from 'native-base';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';


const totalTime = (14 * 24 * 60)
var option = []

export default class ForceStartIncognito extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSource: [],
            status: null,
            loading: false,
            refreshing: false,
            text: '',
            days: 0,
            hours: 0,
            minutes: 0,
            height:0,
            comments: "",
            disableStatus: false,// to diable or enable buttons
            waitStatus: true,//represents upload wait status 
            confirmationPercentage: 0//represents percentage of confirmation report from ground station

        }
         

    }

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Dashboard')}
                style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                </View>
            </TouchableOpacity>
        )
    });
    UNSAFE_componentWillMount = async () => {
        option = this.props.navigation.state.params.param;
        console.log("options", option)
        if(option[0].type.includes("enable")){
            this.setState({days:option[0].days, hours:option[0].hours, minutes:option[0].minutes, comments:option[0].comments })
        }
    }
    componentDidMount = async () => {

        // option = this.props.navigation.state.params.param;
        // console.log("options", option[0].type)
        // this.setState({days:option[0].days, hours:option[0].hours, minutes:option[0].minutes, comments:option[0].comments })
    }

  
    componentWillUnmount() {
        clearInterval(this._interval);
    }

    _onRefresh = async () => {
        await this.fetchData()
    }

    // the following function enable incognito mode by passing start(requestNumber =1) or force_start(requestNumber =2) based on user input
    async onSubmit(event) {
       
        Keyboard.dismiss()
        this.setState({ loading: true })
   
        let timeEntered = 0
        let days = this.state.days
        let minutes = this.state.minutes
        let hours = this.state.minutes

        timeEntered = parseInt((days * 1440)) + parseInt((hours * 60)) + parseInt(minutes)
       


        //check is maximum allowed days is less than 14
        if (timeEntered <= totalTime && days !== undefined) {

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "incognito",
                            "arg1": "force_start",
                            "arg2": "-d " + days,
                            "arg3": "-h " + hours,
                            "arg4": "-m " + minutes,
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })


                let result = response.data;
                console.log("result, ", result)
                if (result.length > 0) {
                    this.setState({ loading: false })
                }

                //force_start logic
           
                    if (result[0].value.includes("Success")) {
                        this.setState({ disableStatus: true })
                    }
                    else {
                        Alert.alert(
                            'Error',
                            result[0].value,
                            [
                                {
                                    text: 'OK', onPress: () => { this.setState({ loading: false }) },
                                }
                            ],
                            { cancelable: false }
                        )
                    }

                

            }
            catch (error) { }

        }//end-if
        //if number of days> 14
        else {
            Alert.alert(
                'Input cannot be processed ',
                'Maximum allowed is 14 days',

                [
                    {
                        text: 'OK', onPress: () => { this.setState({ loading: false }) },
                    }
                ],
                { cancelable: false }
            )

        }

    }
    //following function will check the status of ground confirmation after start incognito mode is executed successfully.
   

    render() {


        return (

            <View style={{ flex: 1 }}>
                {

                    <View >
                        <ScrollView>
                        <View style={{ flexDirection: 'row', paddingLeft: 30, borderBottomColor: '#e5e5e5', borderBottomWidth: 1 }}>
                            <Text style={styles.ESN}>Disable Cellular/Wifi </Text>
                        </View>
                        <View >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: 'white',
                                    alignItems: 'center',
                                    width: '100%',
                                    paddingLeft: 25,
                                    paddingBottom: 5,
                                    paddingTop: 20
                                }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Days</Text>
                                </View>
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={(text) => this.setState({ days: text })}
                                        style={[styles.textInput, { borderColor: "#333333" }]}
                                        value={this.state.days}
                                    ></TextInput>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                width: '100%',
                                paddingLeft: 25,
                                paddingBottom: 5,
                                paddingTop: 5
                            }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Hours</Text>
                                </View>
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={(text) => this.setState({ hours: text })}
                                        style={[styles.textInput, { borderColor: "#333333" }]}
                                        value={this.state.hours}
                                    ></TextInput>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                width: '100%',
                                paddingLeft: 25,
                                paddingBottom: 5,
                                paddingTop: 5
                            }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Minutes</Text>
                                </View >
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={(text) => this.setState({ minutes: text })}
                                        style={[styles.textInput, { borderColor: "#333333" }]}
                                        value={this.state.minutes}
                                    ></TextInput>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                width: '100%',
                                paddingLeft: 25,
                                paddingBottom: 5,
                                paddingTop: 5
                            }} >
                                <View style={{ width: '45%' }}>
                                    <Text style={styles.label}>Comments</Text>
                                </View >
                                <View style={{ width: '35%' }}>
                                    <TextInput
                                    value={this.state.comments}
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        multiline={true}
                                        placeholder={"Upto 128 chars"}
                                        blurOnSubmit={true}
                                        maxLength ={128}
                                        onChangeText={(text) => this.setState({ comments: text })}
                                        onContentSizeChange={(event) => {
                                            this.setState({ height: event.nativeEvent.contentSize.height })
                                        }}
                                        style={[styles.textInput, { borderColor: "#333333" ,height: Math.max(35, this.state.height)}]}
                                        

                                    ></TextInput>
                                </View>
                            </View>
                        </View>
                        <View style={{ paddingTop: 20, paddingBottom: 20, width: '80%' }}>
                            <Text style={styles.thisScreenDisplays}>Disable Wifi and Cellular Network now until the time period specified above.</Text>
                            <Text style={styles.thisScreenDisplays}>(Maximum allowed is 14 days)</Text>
                        </View>
                        <View>
                            {
                            !this.state.disableStatus?
                            <View style={{ paddingTop: 20 }}>
                                <View style={styles.buttonAlign}>
                                    <TouchableOpacity style={styles.testButton} onPress={this.onSubmit.bind(this, 2)} >
                                        <Label style={styles.testButtonText}><Text style={{ color: '#ffffff' }} >Force Disable</Text></Label>
                                    </TouchableOpacity>
                                </View>
                               
                                {
                                    option[0].type.includes("enable")?
                                    <View>
                                    <Text style={[styles.thisScreenDisplays, { color: "blue", paddingTop: 10, paddingRight: 5 }]}>Ground Confirmation not successful.</Text>
                                    <Text style={[styles.thisScreenDisplays, { color: "blue", paddingRight: 5 }]}>Celluar and Wifi can be forced off.</Text>
                                    </View>:
                                    <View></View>
                                }
                                
                                <Text style={[styles.thisScreenDisplays, { color: "blue", fontWeight: "normal", paddingRight: 5, paddingTop: 5 }]}>Note: Forcing Cellular/wifi without ground confirmation can result in Deficient Data Alert.</Text>

                            </View>:
                            <View>
                                <View style={{ paddingTop: 30 }}>
                                    <Text style={[styles.thisScreenDisplays, { color: "blue" }]}>Cellular and Wifi are "forced" disabled.</Text>
                                </View>
                            </View>
                            }
                        </View>  
                       
                        {
                            this.state.loading ?

                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20 }}>
                                    <View></View>
                                    <ActivityIndicator size="large" color="#0033ab" />
                                </View> :
                                <View></View>
                        }
                        </ScrollView>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    renderContainer: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginTop: 15,
        paddingTop: 15
    },


    aboutDctuRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 5
    },
    heading: {
        width: 156,
        height: 22,
        fontFamily: "HelveticaNeue",
        fontSize: 18,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#000000",
        paddingLeft: 45
    },
    label: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        paddingBottom: 5
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    thisScreenDisplays: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0.5,
        color: "#333333",
        paddingLeft: 25
    },
    buttonAlign: {
        marginLeft: 25,
        marginRight: 25,
        flexDirection: 'row'
    },
    testButton: {
        width: '100%',
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        marginBottom: 15,
        paddingRight: 5
    },
    testButtonText: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 10,
        color: "#ffffff"
    },
    ESN: {
        fontFamily: "HelveticaNeue",
        fontSize: 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "rgb(102,102,102)",
        paddingBottom: 20,
        paddingTop: 20
    },

});
