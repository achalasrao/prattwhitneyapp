import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, TouchableOpacity, TextInput, Alert, Keyboard, ScrollView } from 'react-native';
import {
    Label
} from 'native-base';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';



const totalTime = (14 * 24 * 60)
var requestNumber = 0//1->start 2 ->force_start
var info = []
const data = [{ type: "disable" }]
export default class DisableIncognitoMode extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSource: [],
            status: null,
            loading: false,
            refreshing: false,
            text: '',
            days: 0,
            hours: 0,
            minutes: 0,
            disableStatus: false,// to diable or enable buttons
            waitStatus: true,//represents upload wait status 
            confirmationPercentage: 0//represents percentage of confirmation report from ground station

        }
        this.getInfo()
    }

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.navigate('Dashboard')}
                style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                </View>
            </TouchableOpacity>
        )
    });
    UNSAFE_componentWillMount = async () => {
        info = []

    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    _onRefresh = async () => {
        await this.fetchData()
    }

    // the following function enable incognito mode by passing start(requestNumber =1) or force_start(requestNumber =2) based on user input
    async onSubmit(event) {
        Keyboard.dismiss()
        this.setState({ loading: true })
        let timeEntered = 0
        let command = "force_start"
        let days = this.state.days
        let minutes = this.state.minutes
        let hours = this.state.minutes

        timeEntered = parseInt((days * 1440)) + parseInt((hours * 60)) + parseInt(minutes)

        //check is maximum allowed days is less than 14
        if (timeEntered <= totalTime && days !== undefined) {

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "incognito",
                            "arg1": command,
                            "arg2": "-d " + days,
                            "arg3": "-h " + hours,
                            "arg4": "-m " + minutes,
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })


                let result = response.data;
                if (result.length > 0) {
                    this.setState({ loading: false })
                }

                //force_start logic
                if (requestNumber == 2) {
                    if (result[0].value.includes("Success")) {
                        this.setState({ disableStatus: true })
                    }
                    else {
                        Alert.alert(
                            'Error',
                            result[0].value,
                            [
                                {
                                    text: 'OK', onPress: () => { this.setState({ loading: false }) },
                                }
                            ],
                            { cancelable: false }
                        )
                    }

                }

            }
            catch (error) { }

        }//end-if
        //if number of days> 14
        else {
            Alert.alert(
                'Input cannot be processed ',
                'Maximum allowed is 14 days',

                [
                    {
                        text: 'OK', onPress: () => { this.setState({ loading: false }) },
                    }
                ],
                { cancelable: false }
            )

        }

    }
    //following function will check the status of ground confirmation after start incognito mode is executed successfully.
    async getInfo() {
        info = []
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "incognito",
                        "arg1": "info"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })


            let result = response.data;



            console.log("result", result)
            for (var i = 0; i < result.length; i++) {
                info.push({
                    name: result[i].name,
                    value: result[i].value

                })
            }
            console.log("info", info)

            this.setState({ userDataSource: info })
        }
        catch (error) { }
    }

    async onReEnablePress() {
        this.setState({ disableStatus: true })
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "incognito",
                        "arg1": "stop"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })



        } catch (error) {

        }
    }

    render() {
        console.log("render called")
        console.log("get info", info)


        return (

            <View style={{ flex: 1 }}>
                {

                    <View >
                        <ScrollView>
                            <View style={{ flexDirection: 'row', paddingLeft: 30, borderBottomColor: '#e5e5e5', borderBottomWidth: 1 }}>
                                <Text style={styles.ESN}>Disable Cellular/Wifi </Text>
                            </View>
                            {
                                !this.state.disableStatus ?
                                    <View style={{ paddingTop: 10, paddingBottom: 20, width: '80%' }}>
                                        <Text style={[styles.thisScreenDisplays, { color: "blue" }]}>Cellular and Wifi communication already disabled for
                            the period specified below:</Text>
                                    </View> :
                                    <View style={{ paddingTop: 10, paddingBottom: 20, width: '80%' }}>
                                        <Text style={[styles.thisScreenDisplays, { color: "blue" }]}>Cellular and Wifi communication are re-enabled
                        </Text>
                                    </View>
                            }
                            <View >

                                {this.state.userDataSource.map((item) => {

                                    return (

                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.label}>{item.name}</Text>
                                            <Text style={styles.value}>{item.value}</Text>
                                        </View>

                                    )
                                })}
                            </View>
                            {
                                !this.state.disableStatus ?

                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                                        <TouchableOpacity
                                            onPress={this.onReEnablePress.bind(this)}
                                            style={styles.cancelButton}
                                        >
                                            <Text style={styles.cancelLabel}>Re-enable</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity

                                            onPress={() => this.props.navigation.navigate('ForceStartIncognito', { param: data })}
                                            style={[styles.updateButton, {
                                                backgroundColor: '#0033ab'
                                            }]}>
                                            <Text style={[styles.updateLabel, {
                                                color: '#fff'
                                            }]}>Extend Time</Text>
                                        </TouchableOpacity>
                                    </View> :
                                    <View></View>
                            }
                        </ScrollView>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    renderContainer: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginTop: 15,
        paddingTop: 15
    },


    aboutDctuRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 5
    },
    heading: {
        width: 156,
        height: 22,
        fontFamily: "HelveticaNeue",
        fontSize: 18,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#000000",
        paddingLeft: 45
    },
    label: {
        width: '40%',
        fontFamily: "HelveticaNeue",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0,
        color: "#333333",
        paddingLeft: 15
    },
    value: {
        width: '60%',
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#666666"
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#0033ab",
        justifyContent: 'center',
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        justifyContent: 'center',
    },
    cancelLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#0033ab",
    },
    updateLabel: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#ffffff",
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333",
        textAlign: 'left'
    },
    thisScreenDisplays: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0.5,
        color: "#333333",
        paddingLeft: 25
    },
    buttonAlign: {
        marginLeft: 25,
        marginRight: 25,
        flexDirection: 'row'
    },
    testButton: {
        width: '100%',
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#0033ab",
        marginBottom: 15,
        paddingRight: 5
    },
    testButtonText: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#ffffff"
    },
    ESN: {
        fontFamily: "HelveticaNeue",
        fontSize: 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.34,
        color: "rgb(102,102,102)",
        paddingBottom: 20,
        paddingTop: 20
    },

});
