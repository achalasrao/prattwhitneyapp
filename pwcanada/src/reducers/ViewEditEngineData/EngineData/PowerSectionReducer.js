const INITIAL_STATE = {
    oldModuleSerialNumber: '',
    moduleSerialNumber: '',
    powerSectionUpdate: false,
    scannedPowerSection: false,
    updatedPowerSection: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'OldPowerSectionSerialNumber':

            return { ...state, oldModuleSerialNumber: action.payload };

        case 'updatePowerSectionSerialNumber':

            return { ...state, moduleSerialNumber: action.payload };

        case 'UpdatePowerSection':

            return { ...state, powerSectionUpdate: true };

        case 'DontUpdatePowerSection':

            return { ...state, powerSectionUpdate: false };

        case 'PowerSectionUpdateSuccess':

            return { ...state, updatedPowerSection: true };

        case 'PowerSectionUpdateFailed':

            return { ...state, updatedPowerSection: false };

        case 'PowerSectionScanSuccess':

            return { ...state, scannedPowerSection: true };

        case 'PowerSectionScanFailed':

            return { ...state, scannedPowerSection: false };

        default:
            return state;
    }
}
