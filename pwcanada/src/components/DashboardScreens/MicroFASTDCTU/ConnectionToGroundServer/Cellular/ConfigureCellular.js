import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Switch, TextInput, ActivityIndicator, Dimensions } from 'react-native';
import {
    Text,
    Card,
    Content,
    Label,
    CardItem
} from 'native-base';
import axios from 'axios';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import { connect } from "react-redux";
import {
    oldValueCellularActiveSIM,
    newValueCellularActiveSIM,
    oldValueCellularCarrier,
    oldValueCellularAPN,
    oldValueCellularPassword,
    oldValueCellularUser,
    newValueCellularCarrier,
    newValueCellularAPN,
    newValueCellularUser,
    newValueCellularPassword,
    UpdateRequired,
    UpdateNotRequired,
    ShowPassword,
    newValueCellularInternalCarrier,
    oldValueCellularInternalCarrier,
    FetchCellularData,
    DontFetchCellularData
} from '../../../../../actions';

let { width, height } = Dimensions.get('window')
var newData = [];
responseCollect = [];

class ConfigureCellular extends Component {
    constructor(props) {
        super(props)
        this.state = {

            check: true,
            loading: false,
            refershing: false,
            selectedParam: undefined,
            changedValue: undefined,
            oldValue: undefined,
            Active_sim: '',
            fieldChanged: false,
            fieldFocussed: false,
            hidden: true,
            CellularData: 'empty',
            Active_sim: '',
            Carrier: '',
            APN: '',
            User: '',
            Password: '',
            isModalVisible: false,
            isUpdateRequire: false,
            isHidden: false,
            AppcrashState: false,
            writeToXmlCount: 0,
            updateFieldsCount: 0,
            updateFailed: false,
            updateSuccess: false,
            xmlUpdateCount: 0,
            isLoading: false,
            fetchCount: 0,
            carrierfieldFocused: false,
            ApnfieldFocused: false,
            userFieldFocussed: false,
            passwordFieldFocussed: false
        }
    }

    async UNSAFE_componentWillMount() {

        await this.fetchdata()
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('ConnectionDCTUGround')}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <IonicIcon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    async fetchdata() {
        newData = [];
        this.setState({ loading: true });
        let responseJson = [];
        this.props.DontFetchCellularData();
        this.setState({ fetchCount: ++this.state.fetchCount })
        try {

            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "read",
                        "arg2": "Wireless"

                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response

            responseJson = await response.data;

            if (responseJson[0].value == "Err: Too many access request (>4).  Try again later") {
                this.setState({ AppcrashState: true })
            }

            this.setState({ Active_sim: responseJson[0].value });

            if (responseJson.length > 0) {
                var decoded;
                if (responseJson[0].name == "Active_SIM") {
                    decoded = responseJson[0].value.replace(/&amp;/g, '&');
                    this.props.oldValueCellularActiveSIM(decoded)
                    this.props.newValueCellularActiveSIM(decoded)
                }
                if (responseJson[1].name == "Carrier") {
                    decoded = responseJson[1].value.replace(/&amp;/g, '&');
                    this.props.oldValueCellularCarrier(decoded)
                    this.props.newValueCellularCarrier(decoded)
                }
                if (responseJson[2].name == "APN") {
                    decoded = responseJson[2].value.replace(/&amp;/g, '&');

                    this.props.oldValueCellularAPN(decoded)
                    this.props.newValueCellularAPN(decoded)
                }
                if (responseJson[3].name == "User") {
                    decoded = responseJson[3].value.replace(/&amp;/g, '&');
                    this.props.oldValueCellularUser(decoded)
                    this.props.newValueCellularUser(decoded)
                }
                if (responseJson[4].name == "Password") {
                    decoded = responseJson[4].value.replace(/&amp;/g, '&');
                    this.props.oldValueCellularPassword(decoded)
                    this.props.newValueCellularPassword(decoded)
                }
                if (responseJson[5].name == "Internal/Carrier") {
                    decoded = responseJson[5].value.replace(/&amp;/g, '&');
                    this.props.oldValueCellularInternalCarrier(decoded)
                    this.props.newValueCellularInternalCarrier(decoded)
                }
                this.setState({ loading: false });
            } else if (responseJson.length == 0 && this.state.fetchCount < 3) {
                this.fetchdata();
            }
        } catch (error) {
            if (responseJson.length == 0 && this.state.fetchCount < 3) {
                this.fetchdata();
            }
        }
    }

    toggleVisible() {
        this.setState({ hidden: !this.state.hidden })

    }

    async onSubmit(event) {
        event.preventDefault();

        this.setState({ isModalVisible: true })
    }


    componentDidUpdate = () => {

        let {
            Active_SIM, Carrier, APN, User, Password, OldCarrier, OldAPN, OldUser, OldPassword, OldActive_SIM
        } = this.props

        if ((OldActive_SIM !== Active_SIM) ||
            (OldCarrier !== Carrier) ||
            (OldAPN !== APN) ||
            (OldUser !== User) ||
            (OldPassword !== Password)) {
            this.props.UpdateRequired()
        } else {
            this.props.UpdateNotRequired()
        }

        if (this.props.fetchCellularData) {
            this.fetchdata();
        }

    }

    renderModal = () => {
        let {
            Active_SIM, Carrier, APN, User, Password, OldCarrier, OldAPN, OldUser, OldPassword, OldActive_SIM
        } = this.props

        return (

            <View style={styles.modalStyle}>

                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={styles.modalHeader}>You are about to load this data to the MicroFAST</Text>
                </View>

                <View style={{ width: '100%', padding: 10, paddingTop: 10, paddingRight: 10 }}>
                    {
                        (OldActive_SIM === Active_SIM) ? <View /> :
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>Active_SIM is being changed from {OldActive_SIM} to {Active_SIM}</Text>
                            </View>
                    }
                    {
                        (OldCarrier === Carrier) ? <View /> :
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>Carrier is being changed from {OldCarrier} to {Carrier}</Text>
                            </View>
                    }
                    {
                        (OldAPN === APN) ? <View /> :
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>APN is being changed from {OldAPN} to {APN}</Text>
                            </View>
                    }
                    {
                        (OldUser === User) ? <View /> :
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>User is being changed from {OldUser} to {User}</Text>
                            </View>
                    }
                    {
                        (OldPassword === Password) ? <View /> :
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                <Text style={styles.modalTextStyle}>Password is being changed from {OldPassword} to {Password}</Text>
                            </View>
                    }
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>This will erase previous data</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 13 }}>

                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this.previous()}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.confirmedUpdate()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }

    previous = async () => {
        let {
            OldCarrier, OldAPN, OldUser, OldPassword, OldActive_SIM
        } = this.props
        this.props.newValueCellularActiveSIM(OldActive_SIM)
        this.props.newValueCellularCarrier(OldCarrier)
        this.props.newValueCellularAPN(OldAPN)
        this.props.newValueCellularUser(OldUser)
        this.props.newValueCellularPassword(OldPassword)
        this.setState({ isModalVisible: false, hidden: true, loading: true, changedValue: undefined, selectedParam: undefined, fieldChanged: false })

        await this.fetchdata()
    }

    async confirmedUpdate() {
        let newData = [];
        let {
            Carrier, APN, User, Password, Active_SIM
        } = this.props
        this.setState({ loading: true, writeToXmlCount: ++this.state.writeToXmlCount })

        newData.push({
            'Active_SIM': JSON.stringify(Active_SIM),
            'Carrier': JSON.stringify(Carrier),
            'APN': JSON.stringify(APN),
            'User': JSON.stringify(User),
            'Password': JSON.stringify(Password),
        })

        this.setState({ loading: true })

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Wireless/Active_SIM`,
                        "arg3": `${newData[0].Active_SIM}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": `Wireless/Cellular/Carrier`,
                        "arg3": `${newData[0].Carrier}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Wireless/Cellular/APN',
                        "arg3": `${newData[0].APN}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Wireless/Cellular/User',
                        "arg3": `${newData[0].User}`
                    },
                    {
                        "cmd": "xml_access",
                        "arg1": "write",
                        "arg2": 'Wireless/Cellular/Password',
                        "arg3": `${newData[0].Password}`
                    },
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            let responseJson = await response;

            if (String(responseJson.data).includes('Data Error')) {
                this.updateWasFailure()
            } else {
                this.writeToXml();
            }
        }
        catch (err) {
            if (this.state.writeToXmlCount < 2) {
                return this.confirmedUpdate()
            }
        }
    }

    writeToXml = async () => {
        this.setState({ xmlUpdateCount: ++this.state.xmlUpdateCount })

        try {
            let response = await axios({
                method: 'post',
                url: 'http://microFAST/cgi-bin/data_access.cgi?update',
                data: [
                    {
                        "cmd": "xml_access",
                        "arg1": "write"
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })
            let responseJson = await response;

            if (responseJson.data[0].value === "Success") {
                this.updateWasSuccess()
            } else {
                this.updateWasFailure()
            }
        }
        catch (error) {
            if (this.state.xmlUpdateCount < 2) {
                return this.writeToXml()
            }
            this.updateWasFailure()
        }
    }


    updateWasFailure = () => {
        this.setState({ updateFailed: true, updateSuccess: false })
    }

    updateWasSuccess = () => {
        this.setState({ updateFailed: false, updateSuccess: true, isModalVisible: false })
        this.props.navigation.navigate('ConfigureCellularReport')
    }

    onFocusChanged() {

        this.setState({ fieldFocussed: true })
    }

    newCarrier(text) {
        this.setState({ isUpdateRequire: true })
        this.props.newValueCellularActiveSIM('EXTERNAL')
        this.props.newValueCellularCarrier(text)
    }
    newAPN(text) {
        this.setState({ isUpdateRequire: true })
        this.props.newValueCellularActiveSIM('EXTERNAL')
        this.props.newValueCellularAPN(text)
    }
    newUser(text) {
        this.setState({ isUpdateRequire: true })
        this.props.newValueCellularActiveSIM('EXTERNAL')
        this.props.newValueCellularUser(text)
    }
    newPassword(text) {
        this.setState({ isUpdateRequire: true })
        this.props.newValueCellularActiveSIM('EXTERNAL')
        this.props.newValueCellularPassword(text)
    }

    activeSimChange(currentState) {

        newData = [];
        if (currentState == "EXTERNAL") {

            this.setState({ Carrier: '', APN: '', User: '', Password: '', Active_sim: "INTERNAL", isUpdateRequire: true, fieldFocussed: false })
            this.props.newValueCellularCarrier(this.props.OldCarrier)
            this.props.newValueCellularAPN(this.props.OldAPN)
            this.props.newValueCellularUser(this.props.OldUser)
            this.props.newValueCellularPassword(this.props.OldPassword)
            this.props.newValueCellularActiveSIM('INTERNAL')

        } else {
            this.setState({ isUpdateRequire: true, fieldFocussed: true })
            this.props.newValueCellularActiveSIM('EXTERNAL')

        }
    }

    render() {

        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        if (this.state.AppcrashState == true) {
            return (
                <View>
                    <Card>
                        <CardItem>
                            <Text>Err: Too many access request (>4).  Try again later</Text>
                        </CardItem>
                    </Card>
                </View>
            )
        } else {
            let src = this.state.hidden ? require('../../../../../assets/icons/visible.png') : require('../../../../../assets/icons/hidden.png')
            if (this.state.CellularData[0] === undefined) {
                return (
                    <View>

                    </View>
                )
            }
            else {
                return (
                    <View style={{ flex: 1 }}>
                        <Content>
                            <ScrollView>
                                <Card style={styles.rectangle}>

                                    <View style={{ paddingLeft: 8 }}>

                                        {this.props.OldActive_SIM === "INTERNAL" ?
                                            <Switch value={this.props.Active_SIM === "INTERNAL" ? true : false} onValueChange={() => this.activeSimChange(this.props.Active_SIM)}></Switch>
                                            :
                                            <Switch value={this.props.Active_SIM !== "INTERNAL" ? false : true} onValueChange={() => this.activeSimChange(this.props.Active_SIM)}></Switch>

                                        }

                                        <View>
                                            <Text style={styles.head}><Text>Internal SIM card</Text></Text>
                                        </View>


                                        <View style={{ paddingBottom: 3 }}>
                                            {this.props.Active_SIM === "INTERNAL" ? <View>
                                                <View>
                                                    <Text style={styles.connectedTo}>Connected to
                                                    <Text style={{ fontFamily: 'Arial' }}> {this.props.OldInternalCarrier}</Text>
                                                    </Text>
                                                </View>
                                            </View> : <View>
                                                    <View>
                                                        <Text style={styles.connectedTo}>Not Connected</Text>
                                                    </View>
                                                </View>
                                            }
                                        </View>
                                    </View>
                                </Card>
                                <Card style={styles.rectangle}>
                                    <View style={{ paddingLeft: 8 }}>

                                        {this.props.OldActive_SIM === "EXTERNAL" ?
                                            <Switch value={this.props.Active_SIM === "EXTERNAL" ? true : false} onValueChange={() => this.activeSimChange(this.props.Active_SIM)}></Switch>
                                            :
                                            <Switch value={this.props.Active_SIM !== "EXTERNAL" ? false : true} onValueChange={() => this.activeSimChange(this.props.Active_SIM)}></Switch>
                                        }
                                        <View>
                                            <Text style={styles.head}><Text>External SIM card</Text></Text>
                                        </View>

                                        <View style={{ paddingBottom: 3 }}>
                                            {this.props.Active_SIM === "EXTERNAL" ? <View>
                                                <View>
                                                    <Text style={styles.connectedTo}>Connected to
                                                    <Text style={{ fontFamily: 'Arial' }}> {this.props.OldCarrier}</Text>
                                                    </Text>
                                                </View>
                                            </View> : <View>
                                                    <View>
                                                        <Text style={styles.connectedTo}>Not Connected</Text>
                                                    </View>
                                                </View>
                                            }
                                        </View>

                                        <View style={{ paddingTop: 5 }}>
                                            <View style={{ paddingBottom: 10 }}>
                                                <Text style={styles.label}>Carrier</Text>
                                                <TextInput
                                                    onFocus={() => {
                                                        this.onFocusChanged()
                                                        this.setState({ carrierfieldFocused: true })
                                                    }}
                                                    style={[styles.textInput, {
                                                        borderColor: this.state.carrierfieldFocused ? '#000000' : this.state.updateFailed ? '#FF5C42' : '#000000',
                                                        backgroundColor: this.state.fieldFocussed ? '#FFFFFF' : this.state.updateSuccess ? '#71FFB1' : this.props.scannedGasGenerator ? '#71FFB1' : this.state.updateFailed ? '#EB1616' : '#F7F7F7'
                                                    }]}

                                                    onBlur={() => this.setState({ carrierfieldFocused: false })}
                                                    autoCorrect={false}
                                                    autoCapitalize={'none'}
                                                    value={this.props.Carrier}
                                                    onChangeText={(text) => this.newCarrier(text)}
                                                    selectionColor={"#00A9E0"} //change the cursor color
                                                ></TextInput>
                                            </View>

                                            <View style={{ paddingBottom: 10 }}>
                                                <Text style={styles.label}>APN</Text>
                                                <TextInput
                                                    onFocus={() => {
                                                        this.onFocusChanged()
                                                        this.setState({ ApnfieldFocused: true })
                                                    }}
                                                    style={[styles.textInput, {
                                                        borderColor: this.state.ApnfieldFocused ? '#000000' : this.state.updateFailed ? '#FF5C42' : '#000000',
                                                        backgroundColor: this.state.fieldFocussed ? '#FFFFFF' : this.state.updateSuccess ? '#71FFB1' : this.props.scannedGasGenerator ? '#71FFB1' : this.state.updateFailed ? '#EB1616' : '#F7F7F7'
                                                    }]}

                                                    onBlur={() => this.setState({ ApnfieldFocused: false })}
                                                    autoCorrect={false}
                                                    autoCapitalize={'none'}
                                                    placeholderTextColor={(this.state.valueChanged) ? { fontStyle: 'normal', color: '#1F2A44' } : { fontStyle: 'italic', color: '#1F2A44' }}
                                                    onChangeText={(text) => this.newAPN(text)}
                                                    value={this.props.APN}
                                                    selectionColor={"#00A9E0"} //change the cursor color
                                                ></TextInput>
                                            </View>

                                            <View style={{ paddingBottom: 10 }}>
                                                <Text style={styles.label}>User</Text>
                                                <TextInput
                                                    onFocus={() => {
                                                        this.onFocusChanged()
                                                        this.setState({ userFieldFocussed: true })
                                                    }}
                                                    style={[styles.textInput, {
                                                        borderColor: this.state.userFieldFocussed ? '#000000' : this.state.updateFailed ? '#FF5C42' : '#000000',
                                                        backgroundColor: this.state.fieldFocussed ? '#FFFFFF' : this.state.updateSuccess ? '#71FFB1' : this.props.scannedGasGenerator ? '#71FFB1' : this.state.updateFailed ? '#EB1616' : '#F7F7F7'
                                                    }]}

                                                    onBlur={() => this.setState({ userFieldFocussed: false })}
                                                    autoCorrect={false}
                                                    autoCapitalize={'none'}
                                                    onChangeText={(text) => this.newUser(text)}
                                                    value={this.props.User}
                                                    selectionColor={"#00A9E0"} //change the cursor color
                                                ></TextInput>
                                            </View>
                                            <View>
                                                <Text style={styles.label}>Password</Text>
                                                <View style={[styles.textInput, {
                                                    width: '100%',
                                                    flexDirection: 'row',
                                                    justifyContent: 'center',
                                                    paddingBottom: 10,
                                                    borderWidth: 1,
                                                    borderColor: this.state.passwordFieldFocussed ? '#000000' : this.state.updateFailed ? '#FF5C42' : '#000000',
                                                    backgroundColor: this.state.fieldFocussed ? '#FFFFFF' : this.state.updateSuccess ? '#71FFB1' : this.props.scannedGasGenerator ? '#71FFB1' : this.state.updateFailed ? '#EB1616' : '#F7F7F7'
                                                }]}>
                                                    <TextInput
                                                        onFocus={() => {
                                                            this.onFocusChanged()
                                                            this.setState({ passwordFieldFocussed: true })
                                                        }}
                                                        style={[styles.CustomtextInput, { width: '90%' }]}
                                                        onBlur={() => this.setState({ passwordFieldFocussed: false })}
                                                        autoCorrect={false}
                                                        autoCapitalize={'none'}
                                                        secureTextEntry={this.state.hidden}
                                                        onChangeText={(text) => {
                                                            this.newPassword(text)
                                                        }}
                                                        value={this.props.Password}
                                                        selectionColor={"#00A9E0"} //change the cursor color
                                                    ></TextInput>
                                                    <View style={{ width: '10%', paddingLeft: 30 }}>
                                                        <TouchableOpacity onPress={() => this.setState({ hidden: !this.state.hidden })}>
                                                            <View>
                                                                <IonicIcon
                                                                    name={this.state.hidden ? 'ios-eye-off' : 'ios-eye'}
                                                                    style={{ fontSize: 35, color: '#00A9E0' }} />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </Card>

                                <View>
                                    <View style={styles.buttonAlign}>
                                        <TouchableOpacity
                                            disabled={!this.props.isUpdateRequired}
                                            style={[styles.testButton, {
                                                backgroundColor: this.props.isUpdateRequired ? '#00A9E0' : '#62DBFF'
                                            }]}

                                            onPress={this.onSubmit.bind(this)} >
                                            <Label style={styles.testButtonText}> <Text style={[styles.updateLabel, {
                                                color: this.props.isUpdateRequired ? '#FFFFFF' : '#5B6376'
                                            }]}>Update</Text></Label>
                                        </TouchableOpacity>
                                    </View>

                                </View>
                            </ScrollView>
                        </Content>

                        <Modal
                            style={styles.modalContainer}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={() => this.setState({ isModalVisible: false })}
                        >{this.renderModal()}</Modal>
                    </View>
                )
            }
        }
    }
}

const mapStateToProps = ({ CellularConfig }) => {
    const {
        Active_SIM,
        APN,
        Carrier,
        Password,
        User, OldActive_SIM,
        OldCarrier,
        OldAPN,
        OldUser,
        OldPassword,
        isUpdateRequired,
        OldInternalCarrier,
        fetchCellularData,
        InternalCarrier } = CellularConfig

    return {
        Active_SIM,
        APN, Carrier,
        Password,
        User,
        OldCarrier,
        OldAPN,
        OldUser,
        OldPassword,
        OldActive_SIM,
        isUpdateRequired,
        OldInternalCarrier,
        InternalCarrier,
        fetchCellularData
    };
};

export default connect(mapStateToProps, {
    oldValueCellularActiveSIM,
    newValueCellularActiveSIM,
    oldValueCellularCarrier,
    oldValueCellularAPN,
    oldValueCellularPassword,
    oldValueCellularUser,
    newValueCellularCarrier,
    newValueCellularAPN,
    newValueCellularUser,
    newValueCellularPassword,
    UpdateRequired,
    UpdateNotRequired,
    ShowPassword,
    newValueCellularInternalCarrier,
    oldValueCellularInternalCarrier,
    FetchCellularData,
    DontFetchCellularData
})(ConfigureCellular)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center'
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44"
    },
    textInput: {
        width: '100%',
        height: 40,
        borderRadius: 6,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#F7F7F7",
        paddingLeft: 10,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44",
        textAlign: 'left'
    },
    rectangle: {
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#F7F7F7",
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        padding: 10
    },
    head: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.38,
        lineHeight: width < 450 ? 20 : 22,
        color: "#1F2A44"
    },
    connectedTo: {
        height: 30,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.5,
        color: "#1F2A44"
    },
    buttonAlign: {
        marginLeft: 15,
        marginRight: 15,
        flexDirection: 'row'
    },
    testButton: {
        width: '100%',
        marginTop: 15,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        marginBottom: 15,
        paddingRight: 5
    },
    testButtonText: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        textAlign: "center",
        paddingTop: 20,
        color: "#FFFFFF"
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalStyle: {
        width: '100%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#F7F7F7",
        padding: 23
    },
    modalHeader: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.27,
        color: "#1F2A44"
    },
    modalTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0,
        color: "#1F2A44",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 24,
        letterSpacing: 0.25,
        color: "#1F2A44"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        justifyContent: 'center'
    },
    previousLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    confirmButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center'
    },
    confirmLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    CustomtextInput: {
        height: 40,
        borderRadius: 6,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#1F2A44",
        textAlign: 'left'
    },
});
