export const OldPowerSectionSerialNumber = (item) => {

    return {
        type: 'OldPowerSectionSerialNumber',
        payload: item
    }
};

export const updatePowerSectionSerialNumber = (item) => {
    item = item.replace(/^0+/, '')
    return {
        type: 'updatePowerSectionSerialNumber',
        payload: item
    }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Update Actions

export const UpdatePowerSection = () => {

    return {
        type: 'UpdatePowerSection',
    }
};

export const DontUpdatePowerSection = () => {

    return {
        type: 'DontUpdatePowerSection',
    }
};

export const PowerSectionUpdateSuccess = () => {

    return {
        type: 'PowerSectionUpdateSuccess',
    }
};

export const PowerSectionUpdateFailed = () => {

    return {
        type: 'PowerSectionUpdateFailed',
    }
};
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Scan Actions

export const PowerSectionScanSuccess = () => {

    return {
        type: 'PowerSectionScanSuccess',
    }
};

export const PowerSectionScanFailed = () => {

    return {
        type: 'PowerSectionScanFailed',
    }
};
