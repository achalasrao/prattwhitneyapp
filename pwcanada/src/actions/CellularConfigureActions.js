export const oldValueCellularActiveSIM = (value) => {

    return {
        type: 'oldValueCellularActiveSIM',
        payload: value
    }
};
export const oldValueCellularCarrier = (value) => {

    return {
        type: 'oldValueCellularCarrier',
        payload: value
    }
};
export const oldValueCellularAPN = (value) => {

    return {
        type: 'oldValueCellularAPN',
        payload: value
    }
};
export const oldValueCellularUser = (value) => {

    return {
        type: 'oldValueCellularUser',
        payload: value
    }
};
export const oldValueCellularPassword = (value) => {

    return {
        type: 'oldValueCellularPassword',
        payload: value
    }
};

export const oldValueCellularInternalCarrier = (value) => {

    return {
        type: 'oldValueCellularInternalCarrier',
        payload: value
    }
};

export const newValueCellularActiveSIM = (value) => {

    return {
        type: 'newValueCellularActiveSIM',
        payload: value
    }
};

export const newValueCellularCarrier = (value) => {

    return {
        type: 'newValueCellularCarrier',
        payload: value
    }
};


export const newValueCellularAPN = (value) => {

    return {
        type: 'newValueCellularAPN',
        payload: value
    }
};

export const newValueCellularUser = (value) => {

    return {
        type: 'newValueCellularUser',
        payload: value
    }
};

export const newValueCellularPassword = (value) => {

    return {
        type: 'newValueCellularPassword',
        payload: value
    }
};

export const newValueCellularInternalCarrier = (value) => {

    return {
        type: 'newValueCellularInternalCarrier',
        payload: value
    }
};


export const UpdateRequired = () => {

    return {
        type: 'UpdateRequired',
    }
};

export const UpdateNotRequired = () => {

    return {
        type: 'UpdateNotRequired',
    }
};


export const FetchCellularData = () => {
    return {
        type: 'FetchCellularData',
    }
};

export const DontFetchCellularData = () => {

    return {
        type: 'DontFetchCellularData',
    }
};
