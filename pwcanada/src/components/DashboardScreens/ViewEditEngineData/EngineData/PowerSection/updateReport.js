import React, { Component } from "react";
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView
} from "react-native";
import { connect } from "react-redux";
import { FetchEngineData } from "../../../../../actions";
import { UpdateSuccessNotifier } from "../../../../common";
import { UpdateFailedNotifier } from "../../../../common";
import IonicIcon from "react-native-vector-icons/Ionicons";

class PowerSectionupdateReport extends Component {
    constructor(props) {
        super(props);
        this.customBackHandler = this.customBackHandler.bind(this);
        report = this.props.navigation.state.params.param;

    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: navigation.getParam("headerTilte"),
            headerLeft: (
                <TouchableOpacity
                    onPress={() => {
                        params.backHandler();
                    }}
                    style={{
                        width: "100%",
                        flexDirection: "row",
                        justifyContent: "center"
                    }}
                >
                    <IonicIcon
                        name="ios-arrow-back"
                        style={{ fontSize: 35, color: "#000000", paddingLeft: 8 }}
                    />
                    <View style={{ justifyContent: "center" }}>
                        <Text
                            style={{ textAlign: "center", fontSize: 18, paddingBottom: 4 }}
                        >
                            {" "}
                            Back
            </Text>
                    </View>
                </TouchableOpacity>
            )
        };
    };

    customBackHandler = () => {
        this.props.navigation.navigate("EngineData");
        this.props.FetchEngineData();
    };

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    };

    render() {
        let { oldModuleSerialNumber, moduleSerialNumber } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View>
                        {report.includes("Failed") ?
                            <View>
                                <UpdateFailedNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text style={styles.LRUnameTextStyle}>{this.props.selectedEngineModule.ModuleName} Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View> :
                            <View>
                                <UpdateSuccessNotifier />
                                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                                    <View>
                                        <Text style={styles.LRUnameTextStyle}>{this.props.selectedEngineModule.ModuleName} Values</Text>
                                        <Text style={styles.lastUpdateTextStyle}>Last updated {new Date().toUTCString()}</Text>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>
                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                        <View style={styles.rectangleConatinerStyle}>
                            {oldModuleSerialNumber !== moduleSerialNumber ? (
                                <View style={styles.childRectangleStyle}>
                                    <Text style={styles.rectangleTitleTextStyle}>
                                        Power Section Serial Number
                  </Text>
                                    <Text style={[styles.rectangleContentTextStyle, { color: report.includes("success") ? "green" : "red" }]}>
                                        {" "}
                                        {moduleSerialNumber}
                                    </Text>
                                </View>
                            ) : (
                                    <View />
                                )}
                        </View>

                        <TouchableOpacity
                            style={styles.backToDashboardButton}
                            onPress={() => {
                                this.props.navigation.navigate("Dashboard");
                                this.props.FetchEngineData();
                            }}
                        >
                            <Text style={styles.backToDashboard}>
                                <IonicIcon name="ios-arrow-back" style={styles.leftIcon} /> Back
                                to Dashboard
              </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = ({ PowerSectionReducer, EngineDataReducer }) => {
    const { oldModuleSerialNumber, moduleSerialNumber } = PowerSectionReducer;
    const { selectedEngineModule } = EngineDataReducer;
    return { selectedEngineModule, oldModuleSerialNumber, moduleSerialNumber };
};

export default connect(
    mapStateToProps,
    { FetchEngineData }
)(PowerSectionupdateReport);

const styles = StyleSheet.create({
    LRUnameTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 24,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.5,
        color: "#000000",
        paddingTop: 12
    },
    lastUpdateTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.38,
        color: "#666666"
    },
    rectangleConatinerStyle: {
        backgroundColor: "#ffffff",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#e5e5e5",
        marginTop: 20,
        paddingBottom: 20,
        paddingTop: 20
    },
    childRectangleStyle: {
        paddingTop: 10,
        paddingLeft: 16
    },
    rectangleTitleTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#333333"
    },
    rectangleContentTextStyle: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#666666",
        paddingTop: 16
    },
    backToDashboardButton: {
        marginTop: 16,
        padding: 10
    },
    leftIcon: {
        fontSize: 16,
        color: "#0033ab"
    },
    backToDashboard: {
        fontFamily: "HelveticaNeue",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#0033ab"
    }
});
