import React, { Component } from 'react';
import axios from 'axios';
import { View, StyleSheet, TouchableOpacity, Image, ActivityIndicator, ScrollView, Dimensions } from 'react-native';
import { _, keys, values } from 'underscore';
import {
    Text,
    Content,
    Card,
    Input
} from 'native-base';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Ionicons';

let { width, height } = Dimensions.get('window')
var borderList = Array()
var parameterNameLCF = [];
var newData = [];
var LCFValues = [];
var isMaintenanceModalShow = false
var keycheck = 0;

class Epecs_LCF extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userDataSourceKLCF: [],
            loading: false,
            channelInfo: "",
            rowInfo: "",
            isUpdateRequire: false,
            isUnlock: false,
            isModalVisible: false,
            fetchCount: 0
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state

        return {
            headerLeft: (
                <TouchableOpacity
                    onPress={() => { params.backHandler() }}
                    style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <Icon name='ios-arrow-back' style={{ fontSize: 35, color: '#000000', paddingLeft: 8 }} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center', fontSize: 18, paddingBottom: 4 }}> Back</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }

    componentWillUnmount() {
        userDataSourceKLCF = [];
        borderList = Array();
        newData = [];
    }

    async UNSAFE_componentWillMount() {
        //loading start
        newData = []
        LCFValues = []
        parameterNameLCF = []
        this.setState({ loading: true });
        await this.checkMaintenanceMode()
        await this.ParamName();
    }

    _onRefresh = async () => {
        newData = []
        LCFValues = [];
        borderList = Array();
        userDataSourceKLCF = []
        this.setState({
            loading: true, channelInfo: "",
            rowInfo: "",
            isUpdateRequire: false,
            isUnlock: false,
            isModalVisible: false
        });

        await this.getParamValue()
    }

    customBackHandler = () => {

        if (this.state.fieldChanged) {
            Alert.alert(
                'There are unsaved changes',
                'Do you want to continue?',
                [
                    {
                        text: 'Yes', onPress: () => {
                            this.props.navigation.navigate('Dashboard')
                        }
                    },
                    {
                        text: 'No', onPress: () => {

                        }
                    }
                ],
                { cancelable: false }
            )
        } else {
            this.props.navigation.navigate('Dashboard')
        }
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            backHandler: this.customBackHandler
        });
    }

    async checkMaintenanceMode() {
        valueList = []
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        'cmd': 'monitor_status'
                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })


            valueList = values(response.data)

            maintenanceModeList = valueList.filter(d => d.name.includes("EEC Maintenance Mode"))

            if (!maintenanceModeList[0].value.match("yes")) {
                isMaintenanceModalShow = true
                isEditable = false
            }


        } catch (error) {

        }
    }

    async ParamName() {
        this.setState({ fetchCount: ++this.state.fetchCount })
        try {
            let response = await axios({
                method: 'post',
                url: 'http://microfast/cgi-bin/data_access.cgi?read',
                data: [
                    {
                        "cmd": "epecs_trim",
                        "arg1": "--list"

                    }
                ],
                headers: { 'Content-Type': 'application/json' },
            })

            ///Parser for the response
            let responseJson = await response;
            var lines = JSON.stringify(responseJson.data[0].value);


            var splitwise = await lines.split("\\n");

            for (var line = 1; line < splitwise.length - 1; line++) {


                if (splitwise[line].includes("LCF")) {

                    lines = splitwise[line].split(",");

                    parameterNameLCF.push({
                        'Data': lines[0].replace(/\s*$/, ""),
                        'ParamName': lines[1].replace(/\s*$/, ""),
                        'disp_name': lines[2].replace(/\s*$/, ""),
                        'unit': lines[3].replace(/\s*$/, "")
                    })
                }
            }

            if (parameterNameLCF.length != 0) {
                this.getParamValue();
            } else if (parameterNameLCF.length == 0 && this.state.fetchCount < 3) {
                this.ParamName();
            }
        } catch (error) {

        }
        if (parameterNameLCF.length == 0 && this.state.fetchCount < 3) {

            this.paramName()
        }
    }


    getParamValue = async () => {

        var displayData = [];
        var LCFDisplayDataValues = [];
        var reg = "Err!"
        for (var i = 0; i < parameterNameLCF.length; i++) {

            var paramName = "--field=" + parameterNameLCF[i].ParamName
            paramName = JSON.stringify(paramName)

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?read',
                    data: [
                        {
                            "cmd": "epecs_trim",
                            "arg1": "--field=" + parameterNameLCF[i].ParamName
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })


                let result = response.data;

                if (result[0].name == "Data Error" || (reg.match(result[0].value))) {

                }
                else if (result[0].fmt_other.match("rw")) {
                    LCFValues.push({
                        'Name': parameterNameLCF[i].ParamName,

                        'Channel': (result[0].fmt_other.match("A") ? "A" : "B"),
                        'Value': result[0].value,
                        'disp_name': parameterNameLCF[i].disp_name,
                        'unit': parameterNameLCF[i].unit,
                        "isEditable": (result[0].fmt_other.match("rw") ? true : false)
                    })
                }
            }
            catch (error) {

            }
        }

        displayData = _.groupBy(LCFValues, LCFValues => LCFValues.disp_name);
        var paramNames = keys(displayData)
        var channelValues = values(displayData)
        for (var i = 0; i < paramNames.length; i++) {
            LCFDisplayDataValues.push({
                Name: channelValues[i][0].Name,
                ChannelA: channelValues[i][0].Value,
                ChannelB: channelValues[i][1].Value,
                disp_name: channelValues[i][0].disp_name,
                unit: channelValues[i][0].unit,
                isEditable: channelValues[i][0].isEditable,
                rowNum: i
            })
        }

        LCFDisplayDataValues.map((item) => {

            borderList.push({
                state: item.ChannelA === item.ChannelB ? "same" : "different",
                oldchannelA: item.ChannelA,
                oldchannelB: item.ChannelB,
                newChannelA: "",
                newChannelB: "",
                isRowAChanged: false,
                isRowBChanged: false,
                isRowAFocussed: false,
                isRowBFocussed: false,
                unit: item.unit,
                isEditable: item.isEditable,
                rowNum: item.rowNum,
                disp_name: item.disp_name,
                isRowAupdated: "",
                isRowBupdated: "",
                name: item.Name
            })
        })
        this.setState({ userDataSourceKLCF: borderList, loading: false });
    }

    async isFocussed(channelSelected, tabs) {

        if (channelSelected != null && tabs.rowNum != null) {
            var rowNumSelected = tabs.rowNum
            if (tabs.state === "same") {
                borderList[rowNumSelected].isRowAFocussed = true
                borderList[rowNumSelected].isRowBFocussed = true
                this.setState({ rowInfo: rowNumSelected })
            } else {
                if (channelSelected === "A") {
                    borderList[rowNumSelected].isRowAFocussed = true

                    this.setState({ rowInfo: rowNumSelected })
                } else {
                    borderList[rowNumSelected].isRowBFocussed = true

                    this.setState({ rowInfo: rowNumSelected })
                }
            }
        }
    }

    async  unFocussed(channelSelected, tabs) {

        if (channelSelected != null && tabs.rowNum != null) {
            var rowNumSelected = tabs.rowNum

            if (tabs.state === "same") {
                borderList[rowNumSelected].isRowAFocussed = false
                borderList[rowNumSelected].isRowBFocussed = false
                if (borderList[rowNumSelected].isRowAChanged === true && borderList[rowNumSelected].isRowBChanged === true) {
                    this.storeChangeInfo(2, rowNumSelected, channelSelected)
                }
            } else {
                if (channelSelected === "A") {
                    borderList[rowNumSelected].isRowAFocussed = false
                    if (borderList[rowNumSelected].isRowAChanged === true) {
                        this.storeChangeInfo(1, rowNumSelected, channelSelected)
                    }

                } else if (channelSelected === "B") {

                    borderList[rowNumSelected].isRowBFocussed = false
                    if (borderList[rowNumSelected].isRowBChanged === true) {
                        this.storeChangeInfo(1, rowNumSelected, channelSelected)
                    }
                }
            }
        }
    }


    storeChangeInfo(n, rowNumSelected, channelSelected) {

        let res = LCFValues.filter(d => d.disp_name.match(borderList[rowNumSelected].disp_name));
        if (n == 2) {
            newData.push({
                name: res[0].Name,
                disp_name: borderList[rowNumSelected].disp_name,
                value: borderList[rowNumSelected].newChannelA,
                oldValue: borderList[rowNumSelected].oldchannelA,
                channel: "A",
                unit: borderList[rowNumSelected].unit

            })
            newData.push({
                name: res[1].Name,
                disp_name: borderList[rowNumSelected].disp_name,
                value: borderList[rowNumSelected].newChannelB,
                oldValue: borderList[rowNumSelected].oldchannelB,
                channel: "B",
                unit: borderList[rowNumSelected].unit
            })
        }
        else {

            if (borderList[rowNumSelected].isRowAChanged === true && channelSelected === "A") {
                res = res.filter(d => d.Channel.match("A"));
                newData.push({
                    name: res[0].Name,
                    disp_name: borderList[rowNumSelected].disp_name,
                    value: borderList[rowNumSelected].newChannelA,
                    oldValue: borderList[rowNumSelected].oldchannelA,
                    channel: "A",
                    unit: borderList[rowNumSelected].unit

                })
            } else if (borderList[rowNumSelected].isRowBChanged === true && channelSelected === "B") {
                res = res.filter(d => d.Channel.match("B"));
                newData.push({
                    name: res[0].Name,
                    disp_name: borderList[rowNumSelected].disp_name,
                    value: borderList[rowNumSelected].newChannelB,
                    oldValue: borderList[rowNumSelected].oldchannelB,
                    channel: "B",
                    unit: borderList[rowNumSelected].unit
                })
            }
        }
    }

    async  onValueChange(tabs, channel, i, value) {
        if (tabs.state === "different") {
            if (channel === "A") {
                borderList[i].isRowAupdated = value
                borderList[i].isRowBupdated = ""
                borderList[i].isRowAChanged = true
                borderList[i].newChannelA = value
            } else if (channel === "B") {
                borderList[i].isRowAupdated = ""
                borderList[i].isRowBupdated = value
                borderList[i].isRowBChanged = true
                borderList[i].newChannelB = value
            }

        } else {
            borderList[i].isRowAupdated = value
            borderList[i].isRowBupdated = value
            borderList[i].isRowAChanged = true
            borderList[i].isRowBChanged = true
            borderList[i].newChannelA = value
            borderList[i].newChannelB = value
        }

        if (value != undefined) {
            this.setState({ isUpdateRequire: true })
        }
    }


    async onLockPress(tabs) {
        var num = tabs.rowNum
        if (borderList[num].isRowAChanged === false && borderList[num].isRowBChanged === false) {

        } else {

            if (borderList[num].state === "different") {

                if (tabs.isRowAupdated === "") {
                    borderList[num].newChannelA = tabs.isRowBupdated
                    borderList[num].newChannelB = tabs.isRowBupdated
                    borderList[num].isRowAFocussed = true
                    borderList[num].isRowAChanged = true
                    borderList[num].state = "same"
                    this.setState({ isUnlock: true })
                } else {
                    borderList[num].newChannelA = tabs.isRowAupdated
                    borderList[num].newChannelB = tabs.isRowAupdated
                    borderList[num].isRowBFocussed = true
                    borderList[num].isRowBChanged = true
                    borderList[num].state = "same"
                    this.setState({ isUnlock: true })
                }
                this.storeChangeInfo(2, num, "AB")

            } else {

                let res = LCFValues.filter(d => d.disp_name.match(borderList[num].disp_name));

                if (borderList[num].state === "same") {
                    newData.push({
                        name: res[0].Name,
                        disp_name: borderList[num].disp_name,
                        value: borderList[num].newChannelA,
                        oldValue: borderList[num].oldchannelA,
                        channel: "A",
                        unit: borderList[num].unit

                    })
                    newData.push({
                        name: res[1].Name,
                        disp_name: borderList[num].disp_name,
                        value: borderList[num].newChannelB,
                        oldValue: borderList[num].oldchannelB,
                        channel: "B",
                        unit: borderList[num].unit

                    })
                }
                borderList[num].state = "different"
                this.setState({ isUnlock: true })
            }
        }
    }

    renderModal = () => {

        newData = [...new Map(newData.map(o => [o.name, o])).values()]

        return (

            <View style={styles.modalStyle}>

                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={styles.modalHeader}>You are about to load this data to the EEC</Text>
                </View>


                <View style={{ width: '100%', padding: 10, paddingTop: 10, alignItems: 'flex-start', flexWrap: 'wrap' }}>

                    <ScrollView style={{ width: '100%', height: 250, paddingRight: 5 }} >

                        {newData.map((item) => {
                            var color = item.color
                            return (

                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 20 }}>{`\u2022`}</Text>
                                    <Text style={styles.modalTextStyle}>{item.disp_name + " from " + item.oldValue + " to " + item.value + " for Channel " + item.channel}</Text>
                                </View>
                            )
                        })}

                    </ScrollView>
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.eraseDataTextStyle}>This will erase previous data</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 13 }}>

                    <TouchableOpacity
                        style={styles.previousButtonStyle}
                        onPress={() => this._onRefresh()}
                    >
                        <Text style={styles.previousLabelStyle}>Previous</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => this.updateValue()}
                    >
                        <Text style={styles.confirmLabelStyle}>Confirm</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }


    async  updateValue() {
        this.setState({ loading: true, isModalVisible: false })

        var report = []//Final report containing  update status of each parameter change

        for (var i = 0; i < newData.length; i++) {
            var paramName = JSON.stringify("--field=" + newData[i].name)
            var changedValue = JSON.stringify("--value=" + newData[i].value)

            try {

                let response = await axios({
                    method: 'post',
                    url: 'http://microfast/cgi-bin/data_access.cgi?update',
                    data: [
                        {
                            "cmd": "epecs_trim",
                            "arg1": paramName,
                            "arg2": changedValue
                        }
                    ],
                    headers: { 'Content-Type': 'application/json' },
                })

                let result = response.data;

                if (result[0].value == "Success") {

                    this.setState({ fieldChanged: false })
                    report.push({
                        dataSection: "LCF",
                        disp_name: newData[i].disp_name,
                        value: newData[i].value,
                        status: "Success",
                        channel: newData[i].channel,
                        color: "#34CB81",
                        unit: newData[i].unit
                    })
                }
                else {
                    this.setState({ fieldChanged: false })
                    report.push({
                        dataSection: "LCF",
                        disp_name: newData[i].disp_name,
                        value: newData[i].value,
                        status: "Failed",
                        color: "#EB1616",
                        channel: newData[i].channel,
                        unit: newData[i].unit
                    })
                }

            } catch (error) {
                //retry the update command  in case of network error
                i--;
            }
        }
        this.setState({ loading: false })
        //Display the update status report


        //clear the list 
        newData = []
        id = "LCF"
        this.props.navigation.navigate('Report', { param: report, reportTilte: 'LCF' })
        return
    }


    renderView() {

        return (
            <View>
                {this.state.userDataSourceKLCF.map((tabs) => {
                    return (
                        <View key={tabs.disp_name} style={{ flexDirection: 'row', backgroundColor: '#FFFFFF', alignItems: 'center', width: '100%', paddingLeft: 5, paddingBottom: 5, paddingTop: 5 }}>

                            <View style={{ width: '45%' }}>
                                <Text style={styles.label}>{tabs.disp_name}</Text>
                            </View>

                            <View style={{ width: '15%' }}>
                                {
                                    tabs.isEditable ?
                                        <Input
                                            keyboardType="numeric"
                                            returnKeyType='done'
                                            editable={tabs.isEditable}

                                            value={tabs.isRowAFocussed === true ? tabs.newChannelA : tabs.isRowAChanged === true ? tabs.newChannelA : tabs.isRowAChanged == true ? tabs.newChannelA : tabs.oldchannelA}
                                            onFocus={() => this.isFocussed("A", tabs)}
                                            onBlur={() => this.unFocussed("A", tabs)}
                                            style={{
                                                fontSize: 18,
                                                color: "#848A98",
                                                fontFamily: 'Arial', borderRadius: 10, paddingLeft: 5,
                                                borderColor: tabs.state === "same" ? '#000000' : '#EB1616',
                                                borderWidth: 1,
                                                shadowColor: this.state.fieldFocused ? '#00A9E0' : '#FFFFFF',
                                                shadowOffset: { width: 0, height: 2 },
                                                shadowOpacity: 0.8,
                                                shadowRadius: 2,
                                                textAlign: 'left',
                                                backgroundColor: '#FFFFFF'
                                            }}
                                            onChangeText={this.onValueChange.bind(this, tabs, "A", tabs.rowNum)}
                                            autoCorrect={false}
                                            autoCapitalize={'none'}

                                        />
                                        :
                                        <Text style={{ fontSize: 18, color: "#848A98", fontFamily: 'Arial', borderRadius: 10, paddingLeft: 5, textAlign: 'left', backgroundColor: '#FFFFFF' }}>
                                            {tabs.oldchannelA}
                                        </Text>
                                }
                            </View>

                            <View style={{  width: width < 400 ? '10%' : '12.5%' }}>
                                {!tabs.isEditable ?
                                    <View></View> : 
                                    <View style={{
                                        justifyContent: "center",
                                        alignItems: "center",
                                    }}>
                                        {
                                            tabs.state === "same" ?
                                                <TouchableOpacity onPress={() => this.onLockPress(tabs)}>
                                                    <Image
                                                        style={{
                                                            width: 30,
                                                            height: 30
                                                        }}
                                                        source={require('../../../../assets/IconPack/EngineUsage/off.png')}
                                                        resizeMode='contain'
                                                    /></TouchableOpacity> :
                                                <TouchableOpacity onPress={() => this.onLockPress(tabs)}>

                                                    <Image
                                                        style={{
                                                            width: 30,
                                                            height: 30
                                                        }}
                                                        source={require('../../../../assets/IconPack/EngineUsage/on.png')}
                                                        resizeMode='contain'
                                                    />

                                                </TouchableOpacity>
                                        }
                                    </View>
                                }
                            </View>

                            <View style={{ width: '15%' }}>
                                {tabs.isEditable ?

                                    <Input
                                        keyboardType="numeric"
                                        returnKeyType='done'
                                        value={tabs.isRowBFocussed === true ? tabs.newChannelB : tabs.isRowBChanged === true ? tabs.newChannelB : tabs.isRowBChanged == true ? tabs.newChannelB : tabs.oldchannelB}
                                        onFocus={() => this.isFocussed("B", tabs)}
                                        onBlur={() => this.unFocussed("B", tabs)}
                                        style={{
                                            fontSize: 18, color: "#848A98", fontFamily: 'Arial', borderRadius: 10, paddingLeft: 5,
                                            borderColor: tabs.state === "same" ? '#000000' : '#EB1616',
                                            borderWidth: 1,
                                            shadowColor: this.state.fieldFocused ? '#00A9E0' : '#FFFFFF',
                                            shadowOffset: { width: 0, height: 2 },
                                            shadowOpacity: 0.8,
                                            shadowRadius: 2,
                                            textAlign: 'left',
                                            backgroundColor: '#FFFFFF'
                                        }}
                                        autoCorrect={false}
                                        autoCapitalize={'none'}
                                        onChangeText={this.onValueChange.bind(this, tabs, "B", tabs.rowNum)}

                                    /> :
                                    <Text style={{
                                        fontSize: 18, color: "#848A98", fontFamily: 'Arial', borderRadius: 10, paddingLeft: 5,
                                        textAlign: 'left',
                                        backgroundColor: '#FFFFFF'
                                    }}>{tabs.oldchannelB}</Text>}
                            </View>

                            <View style={{ width: '15%', paddingLeft: 5 }}>
                                <Text style={styles.unit}>{tabs.unit}</Text>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }

    renderMaintenanceModal = () => {
        let {
            oldEngineBuildSpec,

            EngineBuildSpec,

        } = this.props

        return (

            <View style={styles.modalStyle}>

                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={styles.modalHeader}>Maintenance Mode is turned OFF. </Text>
                </View>

                <View style={{ paddingTop: 15 }}>
                    <Text style={styles.modalTextStyle}>Please turn ON the Maintenance mode to update the fields.</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 13 }}>
                    <TouchableOpacity
                        style={styles.confirmButtonStyle}
                        onPress={() => {
                            this.setState({
                                loading: this.state.loading
                            }), isMaintenanceModalShow = false, this.props.navigation.navigate('Dashboard')
                        }}
                    >
                        <Text style={styles.confirmLabelStyle}>Okay</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        keycheck++;
        if (this.state.loading) {
            return (
                <View style={[styles.container]}>
                    <ActivityIndicator size="large" color="#5B6376" />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }} key={{ keycheck }}  >
                <View>
                    <Modal

                        style={styles.modalContainer}
                        isVisible={isMaintenanceModalShow}
                        onBackdropPress={() => isMaintenanceModalShow = false}
                    >{this.renderMaintenanceModal()}
                    </Modal>
                </View>
                <Content contentContainerStyle={{ marginTop: 15 }}>
                    <Card style={{ backgroundColor: '#E3E5E8', }} >

                        <View
                            style={{
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                                paddingBottom: 10,
                                paddingTop: 10,
                                backgroundColor: '#FFFFFF',

                            }}>
                            <View style={{ width: '25%' }}>
                                <Text style={
                                    [styles.channel]}>Channel A</Text>
                            </View>
                            <View style={{ width: '28%' }}>
                                <Text style={
                                    [styles.channel]
                                }>Channel B</Text>
                            </View>
                        </View>
                        {this.renderView()}
                    </Card>
                
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30, marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                    <TouchableOpacity
                        onPress={() => { this._onRefresh() }}
                        style={styles.cancelButton}
                    >
                        <Text style={styles.cancelLabel}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        disabled={!this.state.isUpdateRequire}
                        onPress={() => this.setState({ isModalVisible: true })}
                        style={[styles.updateButton, {
                            backgroundColor: this.state.isUpdateRequire ? '#00A9E0' : '#62DBFF'
                        }]}>
                        <Text style={[styles.updateLabel, {
                            color: this.state.isUpdateRequire ? '#FFFFFF' : '#5B6376'
                        }]}>Update</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Modal
                        style={styles.modalContainer}
                        isVisible={this.state.isModalVisible}
                        onBackdropPress={() => this.setState({ isModalVisible: true })}
                    >{this.renderModal()}</Modal>
                </View>
                </Content>
            </View>
        )
    }
}

export default Epecs_LCF;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center'
    },
    label: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 18 : 22,
        letterSpacing: 0,
        color: "#5B6376",
        textAlign: "left"
    },
    channel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376",
    },
    unit: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 10 : 12,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.25,
        color: "#5B6376",
    },
    cancelButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#00A9E0",
        justifyContent: 'center',
    },
    updateButton: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: 'center',
    },
    cancelLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#00A9E0",
    },
    updateLabel: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF",
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalStyle: {
        width: '100%',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#E3E5E8",
        padding: 23
    },
    modalHeader: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 18 : 24,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: width < 450 ? 20 : 22,
        letterSpacing: 0.27,
        color: "#000000"
    },
    modalTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 20 : 22,
        letterSpacing: 0,
        color: "#000000",
        paddingLeft: 10
    },
    eraseDataTextStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: width < 450 ? 20 : 22,
        letterSpacing: 0.25,
        color: "#000000"
    },
    previousButtonStyle: {
        width: 110,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        justifyContent: 'center'
    },
    previousLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.5,
        color: "#00A9E0"
    },
    confirmButtonStyle: {
        width: 120,
        height: 68,
        borderRadius: 6,
        backgroundColor: "#00A9E0",
        justifyContent: "center",
    },
    confirmLabelStyle: {
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: 0.5,
        textAlign: "center",
        color: "#FFFFFF"
    },
    textInput: {
        height: 40,
        borderRadius: 6,
        backgroundColor: "#FFFFFF",
        borderStyle: "solid",
        borderWidth: 1,
        paddingLeft: 10,
        fontFamily: "Arial",
        fontSize: width < 450 ? 16 : 22,
        fontWeight: "300",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#A5A9B4",
        textAlign: 'left'
    }
});
